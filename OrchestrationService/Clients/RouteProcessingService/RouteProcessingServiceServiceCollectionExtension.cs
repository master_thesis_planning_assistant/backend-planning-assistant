﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Clients.RouteProcessingService
{
    public static class RouteProcessingServiceServiceCollectionExtension
    {
        public static IServiceCollection AddRouteProcessingService(this IServiceCollection serviceCollection,
            string connectionString)
        {
            serviceCollection.AddHttpClient(RouteProcessingServiceClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IRouteProcessingServiceClient, RouteProcessingServiceClient>();
            return serviceCollection;
        }
    }
}
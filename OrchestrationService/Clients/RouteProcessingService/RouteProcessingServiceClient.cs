﻿using System.Linq;
using System.Threading.Tasks;
using OrchestrationService.Clients.RouteProcessingService.Model.RouteQuality;
using OrchestrationService.Clients.RouteProcessingService.Model.TimeAndLocationInstances;

namespace OrchestrationService.Clients.RouteProcessingService
{
    public class RouteProcessingServiceClient : IRouteProcessingServiceClient
    {
        public const string HttpClientName = "RouteProcessingService";

        public async Task<TimeAndLocationInstanceResponse> GetTimeAndLocationInstancesAsync(
            TimeAndLocationInstanceRequest request)
        {
            var response = new TimeAndLocationInstanceResponse
            {
                ActivityWithTimeAndLocationInstance = request.Activities.Select(activity =>
                {
                    if (!activity.Fix)
                        return new ActivityWithTimeAndLocationInstance
                        {
                            ActivityGuid = activity.Guid,
                            Start = MockData.GetFlexibleActivityStartAddress(),
                            Destination = activity.Address ??= MockData.GetActivityAddress(),
                            StartTime = MockData.GetActivityTime(activity.StartTime)
                        };
                    return new ActivityWithTimeAndLocationInstance
                    {
                        ActivityGuid = activity.Guid,
                        Start = request.HomeAddress,
                        Destination = activity.Address ??= MockData.GetActivityAddress(),
                        StartTime = activity.StartTime,
                    };
                }).ToList()
            };
            return await Task.Run(() => response);
        }

        public async Task<RouteQualityResponse> GetRouteQualitiesAsync(RouteQualityRequest request)
        {
            var response = new RouteQualityResponse
            {
                RoutesCosts = request.Routes.Select(route => new RouteCosts
                {
                    RouteGuid = route.Guid,
                    Costs = route.Duration == 0 ? 9999999 : route.Duration,
                }).ToList()
            };
            return await Task.Run(() => response);
        }
    }
}
﻿using System.Collections.Generic;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.RouteProcessingService.Model.TimeAndLocationInstances
{
    public class TimeAndLocationInstanceRequest
    {
        public ICollection<Activity> Activities { get; set; }
        public Address HomeAddress { get; set; }
        public ICollection<Preferences> Preferences { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model;

namespace OrchestrationService.Clients.RouteProcessingService.Model.TimeAndLocationInstances
{
    public class TimeAndLocationInstanceResponse
    {
        public ICollection<ActivityWithTimeAndLocationInstance> ActivityWithTimeAndLocationInstance { get; set; }
    }

    public class ActivityWithTimeAndLocationInstance
    {
        public string ActivityGuid { get; set; }
        public DateTime StartTime { get; set; }
        public Address Start { get; set; }
        public Address Destination { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace OrchestrationService.Clients.RouteProcessingService.Model.RouteQuality
{
    public class RouteQualityResponse
    {
        public ICollection<RouteCosts> RoutesCosts { get; set; }
    }

    public class RouteCosts
    {
        public string RouteGuid { get; set; }
        public double Costs { get; set; }
    }
}
﻿using System.Collections.Generic;
using OrchestrationService.Model.Route;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.RouteProcessingService.Model.RouteQuality
{
    public class RouteQualityRequest
    {
        public ICollection<Route> Routes { get; set; }
        public ICollection<Preferences> Preferences { get; set; }
    }
}
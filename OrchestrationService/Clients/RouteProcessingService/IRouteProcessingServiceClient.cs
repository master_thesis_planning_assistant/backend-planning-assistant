﻿using System.Threading.Tasks;
using OrchestrationService.Clients.RouteProcessingService.Model.RouteQuality;
using OrchestrationService.Clients.RouteProcessingService.Model.TimeAndLocationInstances;

namespace OrchestrationService.Clients.RouteProcessingService
{
    public interface IRouteProcessingServiceClient
    {
        public Task<TimeAndLocationInstanceResponse> GetTimeAndLocationInstancesAsync(
            TimeAndLocationInstanceRequest request);

        public Task<RouteQualityResponse> GetRouteQualitiesAsync(RouteQualityRequest request);
    }
}
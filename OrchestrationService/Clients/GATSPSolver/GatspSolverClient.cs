﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrchestrationService.Clients.GATSPSolver.Model;

namespace OrchestrationService.Clients.GATSPSolver
{
    public class GatspSolverClient : IGatspSolverClient
    {
        public const string HttpClientName = "GatspSolver";


        public Task<GatspResponse> CallGatspSolverAsync(GatspRequest request)
        {
            // Annahme: pro original Activity nur 1 Planned Activity.
            const int maxRoutesToConsider = 4;
            var routesToUse = Math.Min(request.PlannedActivities.Select(pa => pa.Routes.Count).Min(),
                maxRoutesToConsider);
            var response = new GatspResponse
            {
                ActivityArrangements = new List<ActivityArrangement>()
            };

            for (var i = 0; i < routesToUse; i++)
            {
                response.ActivityArrangements.Add(new ActivityArrangement
                    {
                        ArrangedActivities = request.PlannedActivities.Select(pa => new ActivityWithRoute
                        {
                            PlannedActivityGuid = pa.PlannedActivityGuid,
                            RouteGuid = pa.Routes.OrderBy(r => r.Costs).ToList()[i].RouteGuid,
                        }).ToList()
                    }
                );
            }

            return Task.Run(() => response);
        }
    }
}
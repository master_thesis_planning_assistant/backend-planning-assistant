﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.GATSPSolver.Model
{
    public class GatspRequest
    {
        public ICollection<GatspPlannedActivity> PlannedActivities { get; set; }
    }

    public class GatspPlannedActivity
    {
        public string OriginalActivityGuid { get; set; }
        public string PlannedActivityGuid { get; set; }
        public DateTime StartTime { get; set; }
        public double Duration { get; set; }
        public ICollection<GatspRoute> Routes { get; set; }
    }

    public class GatspRoute
    {
        public string RouteGuid { get; set; }
        public double Costs { get; set; }
        public ICollection<RoutingMode> Modalities { get; set; }
    }
}
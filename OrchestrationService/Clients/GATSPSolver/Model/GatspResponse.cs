﻿using System.Collections.Generic;

namespace OrchestrationService.Clients.GATSPSolver.Model
{
    public class GatspResponse
    {
        public ICollection<ActivityArrangement> ActivityArrangements { get; set; }
    }

    public class ActivityArrangement
    {
        public ICollection<ActivityWithRoute> ArrangedActivities { get; set; }
    }

    public class ActivityWithRoute
    {
        public string PlannedActivityGuid { get; set; }
        public string RouteGuid { get; set; }
    }
}
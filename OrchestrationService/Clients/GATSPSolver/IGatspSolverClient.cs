﻿using System.Threading.Tasks;
using OrchestrationService.Clients.GATSPSolver.Model;

namespace OrchestrationService.Clients.GATSPSolver
{
    public interface IGatspSolverClient
    {
        public Task<GatspResponse> CallGatspSolverAsync(GatspRequest request);
    }
}
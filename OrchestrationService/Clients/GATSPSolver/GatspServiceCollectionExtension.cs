﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Clients.GATSPSolver
{
    public static class GatspServiceCollectionExtension
    {
        public static IServiceCollection AddGatspSolverClient(this IServiceCollection serviceCollection,
            string connectionString)
        {
            serviceCollection.AddHttpClient(GatspSolverClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IGatspSolverClient, GatspSolverClient>();
            return serviceCollection;
        }
    }
}
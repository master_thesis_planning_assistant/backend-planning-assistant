﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Clients.GeocodingClient
{
    public static class GeoCodingServiceCollectionExtension
    {
        public static IServiceCollection AddGeoCodingClient(this IServiceCollection serviceCollection,
            IConfigurationSection configurationSection)
        {
            serviceCollection.Configure<GeoCodingClientConfig>(configurationSection);
            serviceCollection.AddScoped<IGeoCodingClient, GeoCodingClient>();
            return serviceCollection;
        }
    }
}
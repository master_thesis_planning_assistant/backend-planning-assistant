﻿using System.Threading.Tasks;
using OrchestrationService.Model;

namespace OrchestrationService.Clients.GeocodingClient
{
    public interface IGeoCodingClient
    {
        Task<GeoLocation> GeocodeAddressAsync(Address address);
    }
}
﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using OpenCage.Geocode;
using OrchestrationService.Model;

namespace OrchestrationService.Clients.GeocodingClient
{
    public class GeoCodingClient : IGeoCodingClient
    {
        private readonly IOptions<GeoCodingClientConfig> _config;

        public GeoCodingClient(IOptions<GeoCodingClientConfig> config)
        {
            _config = config;
        }

        public async Task<GeoLocation> GeocodeAddressAsync(Address address)
        {
            var gc = new Geocoder(_config.Value.OpenCageApiKey);
            var requestString = $"{address.StreetAndNumber}, {address.PostalCode}, {address.City}";
            var response = await Task.Run(() => gc.Geocode(requestString));
            var bestResult = response.Results.OrderByDescending(r => r.Confidence).FirstOrDefault();
            if (bestResult == null)
                throw new ArgumentException($"Could not find GeoCoordinates for address {requestString}");
            return new GeoLocation()
            {
                Longitude = bestResult.Geometry.Longitude,
                Latitude = bestResult.Geometry.Latitude,
            };
        }
    }
}
﻿namespace OrchestrationService.Clients.UserPreferenceService
{
    public class UserPreferenceServiceConfig
    {
        public string ClientId { get; set; }
        public string ClientSecret { get; set; }
        public string HomeAddressKey { get; set; }
    }
}
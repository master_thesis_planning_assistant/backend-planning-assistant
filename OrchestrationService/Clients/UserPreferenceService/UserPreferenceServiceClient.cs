﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Model;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.UserPreferenceService
{
    public class UserPreferenceServiceClient : IUserPreferenceServiceClient
    {
        public const string HttpClientName = "UserPreferenceService";
        private readonly IOptions<UserPreferenceServiceConfig> _config;
        private readonly IHttpClientFactory _httpClientFactory;

        public UserPreferenceServiceClient(IHttpClientFactory httpClientFactory,
            IOptions<UserPreferenceServiceConfig> config)
        {
            _httpClientFactory = httpClientFactory;
            _config = config;
        }

        public async Task<UserPreferenceResponse> GetUserPreferencesAsync(UserPreferenceRequest request)
        {
            // TODO: Remove this to use real user preferences from Userpreference-Service
            return MockData.GetUserPreferences();
            var accessToken = await GetAccessTokenAsync(request.UserName, request.Password);
            var getHomeAddressRequest = GetHomeAddressAsync(accessToken);
            var availablePreferenceProfiles = await GetAvailablePreferenceProfilesAsync(accessToken);
            var pendingPreferenceRequests = availablePreferenceProfiles.Names.Select(profileName =>
                GetUserPreferencesByProfileNameAsync(accessToken, profileName));

            var preferences = await Task.WhenAll(pendingPreferenceRequests);
            var homeAddress = await getHomeAddressRequest;
            return new UserPreferenceResponse
            {
                HomeAddress = new Address
                {
                    StreetAndNumber = homeAddress.Name,
                    GeoLocation = new GeoLocation
                    {
                        Longitude = homeAddress.Longitude,
                        Latitude = homeAddress.Latitude
                    }
                },
                Preferences = preferences,
            };
        }

        private async Task<LocationResponse> GetHomeAddressAsync(string accessToken)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Authorization =
                AuthenticationHeaderValue.Parse(
                    $"Bearer {accessToken}");
            var response = await client.GetAsync($"{client.BaseAddress}/user/locations/{_config.Value.HomeAddressKey}");

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request while getting user home address!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<LocationResponse>(responseBody);
        }

        private async Task<Preferences> GetUserPreferencesByProfileNameAsync(string accessToken, string profileName)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Authorization =
                AuthenticationHeaderValue.Parse(
                    $"Bearer {accessToken}");

            var response = await client.GetAsync($"{client.BaseAddress}/user/preferenceProfiles/{profileName}");
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request while getting user preference profile {profileName}!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<Preferences>(responseBody);
        }

        private async Task<PreferenceProfilesResponse> GetAvailablePreferenceProfilesAsync(string accessToken)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Authorization =
                AuthenticationHeaderValue.Parse(
                    $"Bearer {accessToken}");
            var response = await client.GetAsync($"{client.BaseAddress}/user/preferenceProfiles");
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request while getting user preference profiles!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<PreferenceProfilesResponse>(responseBody);
        }


        private async Task<string> GetAccessTokenAsync(string userName, string password)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Authorization =
                AuthenticationHeaderValue.Parse(
                    $"Basic {EncodeBase64($"{_config.Value.ClientId}:{_config.Value.ClientSecret}")}");
            var body = new List<KeyValuePair<string, string>>()
            {
                new("grant_type", "password"),
                new("username", userName),
                new("password", password)
            };
            var httpContent = new FormUrlEncodedContent(body);
            var response = await client.PostAsync($"{client.BaseAddress}/oauth/token", httpContent);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request while getting auth-token!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            var tokenResponse = JsonConvert.DeserializeObject<TokenRequestResponse>(responseBody);
            return tokenResponse.AccessToken;
        }

        private string EncodeBase64(string toEncode)
        {
            var bytes = Encoding.GetEncoding(28591).GetBytes(toEncode);
            var toReturn = System.Convert.ToBase64String(bytes);
            return toReturn;
        }
    }
}
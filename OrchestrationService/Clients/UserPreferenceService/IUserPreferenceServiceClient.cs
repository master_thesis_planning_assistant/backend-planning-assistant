﻿using System.Threading.Tasks;
using OrchestrationService.Clients.UserPreferenceService.Model;

namespace OrchestrationService.Clients.UserPreferenceService
{
    public interface IUserPreferenceServiceClient
    {
        public Task<UserPreferenceResponse> GetUserPreferencesAsync(UserPreferenceRequest request);
    }
}
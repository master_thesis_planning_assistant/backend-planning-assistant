﻿using System.Collections.Generic;

namespace OrchestrationService.Clients.UserPreferenceService.Model
{
    public class PreferenceProfilesResponse
    {
        public ICollection<string> Names { get; set; }
    }
}
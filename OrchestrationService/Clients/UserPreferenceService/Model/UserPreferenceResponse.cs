﻿using System.Collections.Generic;
using OrchestrationService.Model;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.UserPreferenceService.Model
{
    public class UserPreferenceResponse
    {
        public Address HomeAddress { get; set; }
        public ICollection<Preferences> Preferences { get; set; }
    }
}
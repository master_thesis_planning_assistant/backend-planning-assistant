﻿namespace OrchestrationService.Clients.UserPreferenceService.Model
{
    public class UserPreferenceRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
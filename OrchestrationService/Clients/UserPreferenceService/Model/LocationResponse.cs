﻿namespace OrchestrationService.Clients.UserPreferenceService.Model
{
    public class LocationResponse
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Name { get; set; }
    }
}
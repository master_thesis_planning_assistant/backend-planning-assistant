﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Clients.UserPreferenceService
{
    public static class UserPreferenceServiceServiceCollectionExtension
    {
        public static IServiceCollection AddUserPreferenceServiceClient(this IServiceCollection serviceCollection,
            string connectionString, IConfigurationSection configuration)
        {
            serviceCollection.Configure<UserPreferenceServiceConfig>(configuration);
            serviceCollection.AddHttpClient(UserPreferenceServiceClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IUserPreferenceServiceClient, UserPreferenceServiceClient>();
            return serviceCollection;
        }
    }
}
﻿using System.Threading.Tasks;
using OrchestrationService.Clients.RoutingAdapter.Model;

namespace OrchestrationService.Clients.RoutingAdapter
{
    public interface IRoutingAdapterClient
    {
        public Task<RoutingResponse> GetRoutesAsync(RoutingRequest request);
    }
}
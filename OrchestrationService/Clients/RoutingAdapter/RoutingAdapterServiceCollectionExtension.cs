﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Clients.RoutingAdapter
{
    public static class RoutingAdapterServiceCollectionExtension
    {
        public static IServiceCollection AddRoutingAdapterClient(this IServiceCollection serviceCollection,
            string connectionString)
        {
            serviceCollection.AddHttpClient(RoutingAdapterClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IRoutingAdapterClient, RoutingAdapterClient>();
            return serviceCollection;
        }
    }
}
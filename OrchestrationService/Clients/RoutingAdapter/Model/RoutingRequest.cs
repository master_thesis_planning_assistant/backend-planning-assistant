﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model;
using OrchestrationService.Model.User;

namespace OrchestrationService.Clients.RoutingAdapter.Model
{
    public class RoutingRequest
    {
        public ICollection<Preferences> Preferences { get; set; }
        public ICollection<MobilityDemand> MobilityDemands { get; set; }
        public bool FilterRouteDuplicates { get; set; }
    }

    public class MobilityDemand
    {
        public string ActivityGuid { get; set; }
        public Address From { get; set; }
        public Address To { get; set; }
        public DateTime At { get; set; }
    }
}
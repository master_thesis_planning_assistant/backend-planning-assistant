﻿using System.Collections.Generic;
using OrchestrationService.Model.Route;

namespace OrchestrationService.Clients.RoutingAdapter.Model
{
    public class RoutingResponse
    {
        public ICollection<RoutesForActivity> RoutesForActivities { get; set; }
    }

    public class RoutesForActivity
    {
        public string ActivityGuid { get; set; }
        public ICollection<Route> Routes { get; set; }
    }
}
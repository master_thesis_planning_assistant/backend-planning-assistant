﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OrchestrationService.Clients.RoutingAdapter.Model;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace OrchestrationService.Clients.RoutingAdapter
{
    public class RoutingAdapterClient : IRoutingAdapterClient
    {
        public const string HttpClientName = "RoutingAdapter";
        private readonly IHttpClientFactory _httpClientFactory;

        public RoutingAdapterClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<RoutingResponse> GetRoutesAsync(RoutingRequest request)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            var json = JsonSerializer.Serialize(request);
            var httpContent = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response = await client.PostAsync($"{client.BaseAddress}routing", httpContent);
            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Got status code {response.StatusCode} from {HttpClientName}-Request!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<RoutingResponse>(responseBody);
        }
    }
}
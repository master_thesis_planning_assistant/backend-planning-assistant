﻿using System;
using System.Collections.Generic;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Model;
using OrchestrationService.Model.User;

namespace OrchestrationService
{
    // TODO DELETE ME IF NO MOCK DATAS ARE NEEDED ANYMORE!
    public static class MockData
    {
        public static DateTime GetActivityTime(DateTime activityDay)
        {
            return activityDay.AddHours(16.5);
        }

        public static Address GetActivityAddress()
        {
            return new Address
            {
                StreetAndNumber = "Rüppurrer Straße 1",
                PostalCode = "76137",
                City = "Karlsruhe",
                GeoLocation = null,
            };
        }

        public static Address GetFlexibleActivityStartAddress()
        {
            return new Address
            {
                StreetAndNumber = "Kaiserstraße 57",
                PostalCode = "76131",
                City = "Karlsruhe",
                GeoLocation = null,
            };
        }

        public static UserPreferenceResponse GetUserPreferences()
        {
            return new UserPreferenceResponse
            {
                HomeAddress = new Address
                {
                    StreetAndNumber = "home",
                    GeoLocation = new GeoLocation
                    {
                        Longitude = 8.3972817,
                        Latitude = 48.9954968,
                    }
                },
                Preferences = new List<Preferences>
                {
                    new Preferences
                    {
                        ProfileName = "OpenRouteService",
                        ConnectionPreferences = new ConnectionPreferences
                        {
                            MaxConnectingTime = 1,
                            MaxNumOfChanges = 0,
                            MinConnectingTime = 0,
                            MinimizeChanges = true
                        },
                        CyclingPace = 1,
                        DemandedComfortFactors = null,
                        Geofence = null,
                        LevelOfIntermodality = 1,
                        LuggageSize = 1,
                        MaxCyclingDistance = 1,
                        MaxWalkingDistance = 1,
                        ModePreferences = new ModePreferences
                        {
                            ExcludedModes = new List<RoutingMode> {RoutingMode.BikeSharing, RoutingMode.CarSharing,},
                            NeutralModes = new List<RoutingMode> {RoutingMode.Car, RoutingMode.Walk,},
                            PreferredModes = new List<RoutingMode> {RoutingMode.Bike, RoutingMode.PublicTransport},
                        },
                        NoCyclingInBadWeather = false,
                        Timeframe = null,
                        WalkingPace = 1,
                        Weighting = new Weighting
                        {
                            Comfort = 0.25,
                            Duration = 0.25,
                            Environment = 0.25,
                            Price = 0.25
                        }
                    },
                    new Preferences
                    {
                        ProfileName = "OpenTripPlanner",
                        ConnectionPreferences = new ConnectionPreferences
                        {
                            MaxConnectingTime = 1,
                            MaxNumOfChanges = 0,
                            MinConnectingTime = 0,
                            MinimizeChanges = true
                        },
                        CyclingPace = 1,
                        DemandedComfortFactors = null,
                        Geofence = null,
                        LevelOfIntermodality = 1,
                        LuggageSize = 1,
                        MaxCyclingDistance = 1,
                        MaxWalkingDistance = 1,
                        ModePreferences = new ModePreferences
                        {
                            ExcludedModes = new List<RoutingMode> {RoutingMode.BikeSharing, RoutingMode.CarSharing,},
                            NeutralModes = new List<RoutingMode> {RoutingMode.Car, RoutingMode.Walk,},
                            PreferredModes = new List<RoutingMode> {RoutingMode.Bike, RoutingMode.PublicTransport},
                        },
                        NoCyclingInBadWeather = false,
                        Timeframe = null,
                        WalkingPace = 1,
                        Weighting = new Weighting
                        {
                            Comfort = 0.25,
                            Duration = 0.25,
                            Environment = 0.25,
                            Price = 0.25
                        }
                    },
                    new Preferences
                    {
                        ProfileName = "Trias",
                        ConnectionPreferences = new ConnectionPreferences
                        {
                            MaxConnectingTime = 1,
                            MaxNumOfChanges = 0,
                            MinConnectingTime = 0,
                            MinimizeChanges = true
                        },
                        CyclingPace = 1,
                        DemandedComfortFactors = null,
                        Geofence = null,
                        LevelOfIntermodality = 1,
                        LuggageSize = 1,
                        MaxCyclingDistance = 1,
                        MaxWalkingDistance = 1,
                        ModePreferences = new ModePreferences
                        {
                            ExcludedModes = new List<RoutingMode> {RoutingMode.BikeSharing, RoutingMode.CarSharing,},
                            NeutralModes = new List<RoutingMode> {RoutingMode.Car, RoutingMode.Walk,},
                            PreferredModes = new List<RoutingMode> {RoutingMode.Bike, RoutingMode.PublicTransport},
                        },
                        NoCyclingInBadWeather = false,
                        Timeframe = null,
                        WalkingPace = 1,
                        Weighting = new Weighting
                        {
                            Comfort = 0.25,
                            Duration = 0.25,
                            Environment = 0.25,
                            Price = 0.25
                        }
                    },
                    new Preferences
                    {
                        ProfileName = "Valhalla",
                        ConnectionPreferences = new ConnectionPreferences
                        {
                            MaxConnectingTime = 1,
                            MaxNumOfChanges = 0,
                            MinConnectingTime = 0,
                            MinimizeChanges = true
                        },
                        CyclingPace = 1,
                        DemandedComfortFactors = null,
                        Geofence = null,
                        LevelOfIntermodality = 1,
                        LuggageSize = 1,
                        MaxCyclingDistance = 1,
                        MaxWalkingDistance = 1,
                        ModePreferences = new ModePreferences
                        {
                            ExcludedModes = new List<RoutingMode> {RoutingMode.BikeSharing, RoutingMode.CarSharing,},
                            NeutralModes = new List<RoutingMode> {RoutingMode.Car, RoutingMode.Walk,},
                            PreferredModes = new List<RoutingMode> {RoutingMode.Bike, RoutingMode.PublicTransport},
                        },
                        NoCyclingInBadWeather = false,
                        Timeframe = null,
                        WalkingPace = 1,
                        Weighting = new Weighting
                        {
                            Comfort = 0.25,
                            Duration = 0.25,
                            Environment = 0.25,
                            Price = 0.25
                        }
                    }
                }
            };
        }
    }
}
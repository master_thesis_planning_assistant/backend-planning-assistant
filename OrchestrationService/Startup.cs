using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using OrchestrationService.Builder;
using OrchestrationService.Clients.GATSPSolver;
using OrchestrationService.Clients.GeocodingClient;
using OrchestrationService.Clients.RouteProcessingService;
using OrchestrationService.Clients.RoutingAdapter;
using OrchestrationService.Clients.UserPreferenceService;
using OrchestrationService.Infrastructure.Extensions;

namespace OrchestrationService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLoggingWithFormatter();
            services.AddCors();
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Orchestration-Service";
                    document.Info.Description =
                        "This Service is used to coordinate the service calls needed to calculate activity plans.";
                };
            });
            // Clients
            services.AddRoutingAdapterClient(Configuration.GetConnectionString(RoutingAdapterClient.HttpClientName));
            services.AddUserPreferenceServiceClient(
                Configuration.GetConnectionString(UserPreferenceServiceClient.HttpClientName),
                Configuration.GetSection(UserPreferenceServiceClient.HttpClientName));
            services.AddGatspSolverClient(Configuration.GetConnectionString(GatspSolverClient.HttpClientName));
            services.AddRouteProcessingService(
                Configuration.GetConnectionString(RouteProcessingServiceClient.HttpClientName));
            services.AddGeoCodingClient(Configuration.GetSection("GeoCoding"));
            // Builder
            services.AddActivityPlanBuilder();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            var allowedOrigins = Configuration.GetSection("Cors:AllowedOrigins").Get<string[]>();
            if (allowedOrigins != null && allowedOrigins.Any())
            {
                app.UseCors(options =>
                {
                    options
                        .AllowAnyHeader()
                        .WithMethods("GET", "PUT", "POST", "DELETE")
                        .AllowCredentials()
                        .WithOrigins(allowedOrigins)
                        .WithExposedHeaders("Content-Disposition");
                });
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(settings => { settings.DocumentTitle = "Orchestration-Service"; });

            app.UseCustomExceptionHandler();

            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
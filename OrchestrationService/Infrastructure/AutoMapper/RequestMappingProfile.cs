﻿using AutoMapper;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Controllers.Dtos;
using OrchestrationService.Controllers.Dtos.ActivityPlan;
using OrchestrationService.Controllers.Dtos.Request;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;

namespace OrchestrationService.Infrastructure.AutoMapper
{
    public class RequestMappingProfile : Profile
    {
        public RequestMappingProfile()
        {
            CreateMap<ActivityDto, Activity>();
            CreateMap<AddressDto, Address>();
            CreateMap<GeoLocationDto, GeoLocation>();
            CreateMap<UserPreferenceRequestDto, UserPreferenceRequest>();
        }
    }
}
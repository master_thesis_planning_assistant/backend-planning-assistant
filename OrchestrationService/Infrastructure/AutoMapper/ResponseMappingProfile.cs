﻿using AutoMapper;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Controllers.Dtos;
using OrchestrationService.Controllers.Dtos.ActivityPlan;
using OrchestrationService.Controllers.Dtos.ActivityPlan.Route;
using OrchestrationService.Controllers.Dtos.Response;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;
using OrchestrationService.Model.Route;
using OrchestrationService.Model.User;

namespace OrchestrationService.Infrastructure.AutoMapper
{
    public class ResponseMappingProfile : Profile
    {
        public ResponseMappingProfile()
        {
            CreateMap<UserPreferenceResponse, UserPreferenceResponseDto>();
            CreateMap<ActivityPlan, ActivityPlanDto>();
            CreateMap<User, UserDto>();
            CreateMap<ArrangedActivity, ArrangedActivityDto>();
            CreateMap<Route, RouteDto>();
            CreateMap<Leg, LegDto>();
            CreateMap<Step, StepDto>();
            CreateMap<Address, AddressDto>();
            CreateMap<Bounds, BoundsDto>();
            CreateMap<GeoLocation, GeoLocationDto>();
            CreateMap<NamedGeoLocation, NamedGeoLocationDto>();
            CreateMap<Polyline, PolylineDto>();
        }
    }
}
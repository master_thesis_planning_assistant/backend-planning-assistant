﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OrchestrationService.Builder;
using OrchestrationService.Clients.UserPreferenceService;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Controllers.Dtos.ActivityPlan;
using OrchestrationService.Controllers.Dtos.Request;
using OrchestrationService.Controllers.Dtos.Response;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;

namespace OrchestrationService.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("")]
    public class AppController : ControllerBase
    {
        private readonly IActivityPlanBuilder _activityPlanBuilder;
        private readonly ILogger<AppController> _logger;
        private readonly IMapper _mapper;
        private readonly IUserPreferenceServiceClient _userPreferenceServiceClient;

        public AppController(ILogger<AppController> logger, IActivityPlanBuilder activityPlanBuilder,
            IUserPreferenceServiceClient userPreferenceServiceClient, IMapper mapper)
        {
            _logger = logger;
            _activityPlanBuilder = activityPlanBuilder;
            _userPreferenceServiceClient = userPreferenceServiceClient;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the user preferences containing all preference profiles as well as the home address.
        /// </summary>
        /// <param name="requestDto">Data to request the preferences</param>
        /// <returns>Preferences with all preference profiles and the home address</returns>
        [HttpPut("preferences")]
        [ProducesResponseType(typeof(UserPreferenceResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserPreferenceResponseDto>> GetUserPreferencesAsync(
            [FromBody] UserPreferenceRequestDto requestDto)
        {
            if (requestDto.Password == null || requestDto.UserName == null)
            {
                return BadRequest("Password and UserName are required to get preferences");
            }

            var mappedRequest = _mapper.Map<UserPreferenceRequest>(requestDto);

            var preferences = await _userPreferenceServiceClient.GetUserPreferencesAsync(mappedRequest);

            return Ok(_mapper.Map<UserPreferenceResponseDto>(preferences));
        }

        /// <summary>
        /// Creates activity plans for the given activities using the preferences of the given user.
        /// </summary>
        /// <param name="requestDto">Activities and user information to get preferences</param>
        /// <returns>Activity plans for the given activities</returns>
        [HttpPost("activityPlan")]
        [ProducesResponseType(typeof(PlanningResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<PlanningResponseDto>> CreatePlanFromPlanningRequestAsync(
            [FromBody] PlanningRequestDto requestDto)
        {
            var mappedActivities = _mapper.Map<ICollection<ActivityDto>, ICollection<Activity>>(requestDto.Activities);

            _activityPlanBuilder.InitializeActivityPlanFromActivities(requestDto.UserName, requestDto.Password,
                requestDto.Day,
                mappedActivities);
            await _activityPlanBuilder.AddUserPreferencesAsync();
            await _activityPlanBuilder.AddGeoLocationsToActivitiesAsync();
            await _activityPlanBuilder.AddTimeAndLocationInstancesAsync();
            await _activityPlanBuilder.AddGeolocationsToPlannedActivitiesAsync();
            await _activityPlanBuilder.AddRoutesAsync(requestDto.FilterRouteDuplicates);
            await _activityPlanBuilder.AddRouteCostsAsync();
            await _activityPlanBuilder.AddActivityArrangementAsync();

            var activityPlans = _activityPlanBuilder.BuildActivityPlans();

            var response = new PlanningResponseDto
            {
                ActivityPlans = _mapper.Map<ICollection<ActivityPlan>, ICollection<ActivityPlanDto>>(activityPlans)
            };

            return Ok(response);
        }
    }
}
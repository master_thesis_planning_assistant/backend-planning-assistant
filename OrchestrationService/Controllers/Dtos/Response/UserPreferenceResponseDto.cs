﻿using System.Collections.Generic;
using OrchestrationService.Model.User;

namespace OrchestrationService.Controllers.Dtos.Response
{
    public class UserPreferenceResponseDto
    {
        public AddressDto HomeAddress { get; set; }

        // Do not use DTO for Preferences since their model is already determined by another service. Therefore they are practically a dto per se
        public ICollection<Preferences> Preferences { get; set; }
    }
}
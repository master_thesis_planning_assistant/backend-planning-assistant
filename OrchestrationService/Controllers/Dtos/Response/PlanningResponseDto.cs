﻿using System.Collections.Generic;
using OrchestrationService.Controllers.Dtos.ActivityPlan;

namespace OrchestrationService.Controllers.Dtos.Response
{
    public class PlanningResponseDto
    {
        public ICollection<ActivityPlanDto> ActivityPlans { get; set; }
    }
}
﻿namespace OrchestrationService.Controllers.Dtos
{
    public class GeoLocationDto
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
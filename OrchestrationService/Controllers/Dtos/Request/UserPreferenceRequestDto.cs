﻿namespace OrchestrationService.Controllers.Dtos.Request
{
    public class UserPreferenceRequestDto
    {
        public string UserName { set; get; }
        public string Password { get; set; }
    }
}
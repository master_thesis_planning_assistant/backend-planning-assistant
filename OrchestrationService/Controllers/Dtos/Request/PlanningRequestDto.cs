﻿using System;
using System.Collections.Generic;
using OrchestrationService.Controllers.Dtos.ActivityPlan;

namespace OrchestrationService.Controllers.Dtos.Request
{
    public class PlanningRequestDto
    {
        public ICollection<ActivityDto> Activities { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime Day { get; set; }
        public bool FilterRouteDuplicates { get; set; }
    }
}
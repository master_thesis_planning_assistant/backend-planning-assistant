﻿namespace OrchestrationService.Controllers.Dtos
{
    public class AddressDto
    {
        public string StreetAndNumber { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public GeoLocationDto GeoLocation { get; set; }
    }
}
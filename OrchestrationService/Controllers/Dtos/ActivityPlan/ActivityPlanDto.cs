﻿using System;
using System.Collections.Generic;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan
{
    public class ActivityPlanDto
    {
        public string Guid { get; set; }
        public UserDto User { get; set; }
        public DateTime Day { get; set; }
        public ICollection<ArrangedActivityDto> ArrangedActivities { get; set; }
    }
}
﻿using System;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan
{
    public class ActivityDto
    {
        public string Guid { get; set; }

        public string Title { get; set; }

        // Can be null if no concrete Address is given (e.g. Shopping)
        public AddressDto Address { get; set; }

        // Only gives the day if Fix == false
        public DateTime StartTime { get; set; }
        public double Duration { get; set; }
        public bool Fix { get; set; }
    }
}
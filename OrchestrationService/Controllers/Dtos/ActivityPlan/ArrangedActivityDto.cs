﻿using System;
using System.Collections.Generic;
using OrchestrationService.Controllers.Dtos.ActivityPlan.Route;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan
{
    public class ArrangedActivityDto
    {
        public string Guid { get; set; }
        public string Title { get; set; }
        public AddressDto Address { get; set; }
        public DateTime StartTime { get; set; }
        public double Duration { get; set; }
        public string OriginalActivityGuid { get; set; }
        public AddressDto StartAddress { get; set; }
        public RouteDto Route { get; set; }
        public ICollection<RouteDto> AlternativeRoutes { get; set; }
    }
}
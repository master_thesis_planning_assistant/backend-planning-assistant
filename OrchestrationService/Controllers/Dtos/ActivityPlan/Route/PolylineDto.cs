﻿using System.Collections.Generic;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan.Route
{
    public class PolylineDto
    {
        public List<GeoLocationDto> Points { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model.User;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan.Route
{
    public class LegDto
    {
        public RoutingMode Modality { get; set; }
        public string Description { get; set; }
        public NamedGeoLocationDto StartLocation { get; set; }
        public DateTime Departure { get; set; }
        public NamedGeoLocationDto EndLocation { get; set; }
        public DateTime Arrival { get; set; }
        public PolylineDto Polyline { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
        public ICollection<StepDto> Steps { get; set; }
    }
}
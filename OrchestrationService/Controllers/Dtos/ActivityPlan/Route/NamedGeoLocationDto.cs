﻿namespace OrchestrationService.Controllers.Dtos.ActivityPlan.Route
{
    public class NamedGeoLocationDto
    {
        public string Name { get; set; }
        public GeoLocationDto GeoLocation { get; set; }
    }
}
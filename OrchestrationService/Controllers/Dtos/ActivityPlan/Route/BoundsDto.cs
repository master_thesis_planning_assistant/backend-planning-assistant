﻿namespace OrchestrationService.Controllers.Dtos.ActivityPlan.Route
{
    public class BoundsDto
    {
        public GeoLocationDto Southwest { get; set; }
        public GeoLocationDto Northeast { get; set; }
    }
}
﻿using System.Collections.Generic;
using OrchestrationService.Model.User;

namespace OrchestrationService.Controllers.Dtos.ActivityPlan
{
    public class UserDto
    {
        public string Name { get; set; }
        public string Password { get; set; }

        public AddressDto HomeAddress { get; set; }

        // Do not use DTO for Preferences since their model is already determined by another service. Therefore they are practically a dto per se
        public ICollection<Preferences> Preferences { get; set; }
    }
}
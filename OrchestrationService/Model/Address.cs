﻿namespace OrchestrationService.Model
{
    public class Address
    {
        public string StreetAndNumber { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public GeoLocation GeoLocation { get; set; }
    }
}
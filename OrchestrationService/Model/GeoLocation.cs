﻿namespace OrchestrationService.Model
{
    public class GeoLocation
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace OrchestrationService.Model.Activity
{
    public class ActivityArrangement
    {
        public ICollection<ArrangedActivity> Arrangement { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace OrchestrationService.Model.Activity
{
    public class ArrangedActivity : BaseActivity
    {
        public string OriginalActivityGuid { get; set; }
        public string PlannedActivityGuid { get; set; }
        public Address StartAddress { get; set; }
        public Route.Route Route { get; set; }
        public ICollection<Route.Route> AlternativeRoutes { get; set; }
    }
}
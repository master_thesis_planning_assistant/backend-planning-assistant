﻿using System.Collections.Generic;

namespace OrchestrationService.Model.Activity
{
    /// <summary>
    /// Address and StartAddress can not be null
    /// StartTime has to specify a time
    /// </summary>
    public class PlannedActivity : BaseActivity
    {
        public string OriginalActivityGuid { get; set; }
        public Address StartAddress { get; set; }
        public ICollection<Route.Route> Routes { get; set; }
    }
}
﻿using System;

namespace OrchestrationService.Model.Activity
{
    public abstract class BaseActivity
    {
        public string Guid { get; set; }
        public string Title { get; set; }
        public Address Address { get; set; }
        public DateTime StartTime { get; set; }
        public double Duration { get; set; }
    }
}
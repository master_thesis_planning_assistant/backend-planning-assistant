﻿namespace OrchestrationService.Model.Activity
{
    /// <summary>
    /// Address can be null if no concrete Address is given (e.g. for Shopping)
    /// If Fix == false the StartTime only specifies the day
    /// </summary>
    public class Activity : BaseActivity
    {
        public bool Fix { get; set; }
    }
}
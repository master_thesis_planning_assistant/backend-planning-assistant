﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model.Activity;

namespace OrchestrationService.Model
{
    public class ActivityPlan
    {
        public string Guid { get; set; }
        public User.User User { get; set; }
        public DateTime Day { get; set; }
        public ICollection<ArrangedActivity> ArrangedActivities { get; set; }
    }
}
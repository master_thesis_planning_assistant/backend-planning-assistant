﻿using System.Collections.Generic;

namespace OrchestrationService.Model.Route
{
    public class Polyline
    {
        public List<GeoLocation> Points { get; set; }
    }
}
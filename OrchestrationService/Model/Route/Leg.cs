﻿using System;
using System.Collections.Generic;
using OrchestrationService.Model.User;

namespace OrchestrationService.Model.Route
{
    public class Leg
    {
        public RoutingMode Modality { get; set; }
        public string Description { get; set; }
        public NamedGeoLocation StartLocation { get; set; }
        public DateTime Departure { get; set; }
        public NamedGeoLocation EndLocation { get; set; }
        public DateTime Arrival { get; set; }
        public Polyline Polyline { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
        public ICollection<Step> Steps { get; set; }
    }
}
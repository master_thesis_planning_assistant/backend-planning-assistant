﻿namespace OrchestrationService.Model.Route
{
    public class Bounds
    {
        public GeoLocation Southwest { get; set; }
        public GeoLocation Northeast { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace OrchestrationService.Model.User
{
    public class User
    {
        public string Name { get; set; }
        public string Password { get; set; }
        public Address HomeAddress { get; set; }
        public ICollection<Preferences> Preferences { get; set; }
    }
}
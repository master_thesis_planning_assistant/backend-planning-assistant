﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OrchestrationService.Model.User
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoutingMode
    {
        [EnumMember(Value = "walk")] Walk,
        [EnumMember(Value = "Bikesharing")] BikeSharing,
        [EnumMember(Value = "bike")] Bike,

        [EnumMember(Value = "public transport")]
        PublicTransport,
        [EnumMember(Value = "car")] Car,
        [EnumMember(Value = "carsharing")] CarSharing,
    }
}
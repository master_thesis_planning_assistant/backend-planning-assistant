﻿namespace OrchestrationService.Model.User
{
    public class Weighting
    {
        public double Comfort { get; set; }
        public double Duration { get; set; }
        public double Environment { get; set; }
        public double Price { get; set; }
    }
}
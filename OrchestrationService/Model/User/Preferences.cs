﻿using System.Collections.Generic;

namespace OrchestrationService.Model.User
{
    public class Preferences
    {
        public string ProfileName { get; set; }
        public ConnectionPreferences ConnectionPreferences { get; set; }
        public int CyclingPace { get; set; }
        public ICollection<string> DemandedComfortFactors { get; set; }
        public Geofence Geofence { get; set; }
        public int LevelOfIntermodality { get; set; }
        public int LuggageSize { get; set; }
        public double MaxCyclingDistance { get; set; }
        public double MaxWalkingDistance { get; set; }
        public ModePreferences ModePreferences { get; set; }
        public bool NoCyclingInBadWeather { get; set; }
        public ICollection<string> Timeframe { get; set; }
        public int WalkingPace { get; set; }
        public Weighting Weighting { get; set; }
    }
}
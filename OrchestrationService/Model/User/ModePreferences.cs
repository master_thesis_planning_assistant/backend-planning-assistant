﻿using System.Collections.Generic;

namespace OrchestrationService.Model.User
{
    public class ModePreferences
    {
        public List<RoutingMode> ExcludedModes { get; set; }
        public List<RoutingMode> NeutralModes { get; set; }
        public List<RoutingMode> PreferredModes { get; set; }
    }
}
﻿namespace OrchestrationService.Model.User
{
    public class ConnectionPreferences
    {
        public int MaxConnectingTime { get; set; }
        public int MaxNumOfChanges { get; set; }
        public int MinConnectingTime { get; set; }
        public bool MinimizeChanges { get; set; }
    }
}
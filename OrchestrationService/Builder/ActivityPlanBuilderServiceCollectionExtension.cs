﻿using Microsoft.Extensions.DependencyInjection;

namespace OrchestrationService.Builder
{
    public static class ActivityPlanBuilderServiceCollectionExtension
    {
        public static IServiceCollection AddActivityPlanBuilder(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<IActivityPlanBuilder, ActivityPlansBuilder>();
            return serviceCollection;
        }
    }
}
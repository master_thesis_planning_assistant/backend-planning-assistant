﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OrchestrationService.Clients.GATSPSolver;
using OrchestrationService.Clients.GATSPSolver.Model;
using OrchestrationService.Clients.GeocodingClient;
using OrchestrationService.Clients.RouteProcessingService;
using OrchestrationService.Clients.RouteProcessingService.Model.RouteQuality;
using OrchestrationService.Clients.RouteProcessingService.Model.TimeAndLocationInstances;
using OrchestrationService.Clients.RoutingAdapter;
using OrchestrationService.Clients.RoutingAdapter.Model;
using OrchestrationService.Clients.UserPreferenceService;
using OrchestrationService.Clients.UserPreferenceService.Model;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;
using OrchestrationService.Model.User;
using ActivityArrangement = OrchestrationService.Model.Activity.ActivityArrangement;

namespace OrchestrationService.Builder
{
    public class ActivityPlansBuilder : IActivityPlanBuilder
    {
        private readonly IGatspSolverClient _gatspSolverClient;
        private readonly IGeoCodingClient _geoCodingClient;
        private readonly IRouteProcessingServiceClient _routeProcessingServiceClient;
        private readonly IRoutingAdapterClient _routingAdapterClient;
        private readonly IUserPreferenceServiceClient _userPreferenceServiceClient;

        private ICollection<Activity> _activities;
        private ICollection<ActivityArrangement> _activityArrangements;
        private DateTime _day;
        private ICollection<PlannedActivity> _plannedActivities;
        private User _user;


        public ActivityPlansBuilder(IRoutingAdapterClient routingAdapterClient,
            IRouteProcessingServiceClient routeProcessingServiceClient, IGatspSolverClient gatspSolverClient,
            IUserPreferenceServiceClient userPreferenceServiceClient, IGeoCodingClient geoCodingClient)
        {
            _routingAdapterClient = routingAdapterClient;
            _routeProcessingServiceClient = routeProcessingServiceClient;
            _gatspSolverClient = gatspSolverClient;
            _userPreferenceServiceClient = userPreferenceServiceClient;
            _geoCodingClient = geoCodingClient;

            _user = null;
            _activities = null;
            _day = DateTime.Today;
            _plannedActivities = null;
            _activityArrangements = null;
        }

        public void InitializeActivityPlanFromActivities(string userName, string password, DateTime day,
            ICollection<Activity> activities)
        {
            _user = new User
            {
                Name = userName,
                Password = password,
                HomeAddress = null,
                Preferences = null
            };

            _day = day;
            _activities = activities;
        }

        public async Task AddGeoLocationsToActivitiesAsync()
        {
            // find all addresses that are present but have no geo location
            var addressesToGeocode = _activities.Where(activity => activity.Address is {GeoLocation: null})
                .Select(activity => activity.Address).ToList();

            await AddGeoLocationsToAddresses(addressesToGeocode);
        }

        public async Task AddGeolocationsToPlannedActivitiesAsync()
        {
            // find all addresses that are present but have no geo location
            var addressesToGeocode = _plannedActivities.Where(activity => activity.Address is {GeoLocation: null})
                .Select(activity => activity.Address).ToList();

            // find all start addresses that are present but have no geo locations
            addressesToGeocode.AddRange(_plannedActivities
                .Where(activity => activity.StartAddress is {GeoLocation: null})
                .Select(activity => activity.StartAddress));

            await AddGeoLocationsToAddresses(addressesToGeocode);
        }

        public async Task AddUserPreferencesAsync()
        {
            if (_user?.Password == null || _user?.Name == null)
                throw new ApplicationException("To add preferences username and password are required.");
            var request = new UserPreferenceRequest {UserName = _user.Name, Password = _user.Password};
            var response = await _userPreferenceServiceClient.GetUserPreferencesAsync(request);
            _user.Preferences = response.Preferences;
            _user.HomeAddress = response.HomeAddress;
        }

        public async Task AddTimeAndLocationInstancesAsync()
        {
            if (_user.Preferences == null || _user.Preferences.Count == 0)
                throw new ApplicationException("To add time and location instances preferences are required.");
            if (_user.HomeAddress == null)
                throw new ApplicationException("To add time and location instances a home address is required");
            if (_activities == null)
                throw new ApplicationException("To add time and location instances activities are required.");

            var request = new TimeAndLocationInstanceRequest
            {
                Activities = _activities,
                Preferences = _user.Preferences,
                HomeAddress = _user.HomeAddress
            };
            var response = await _routeProcessingServiceClient.GetTimeAndLocationInstancesAsync(request);

            var pairs = from activityWithTimeInstance in response.ActivityWithTimeAndLocationInstance
                join activity in _activities on
                    activityWithTimeInstance.ActivityGuid equals activity.Guid
                select new {activityWithTimeInstance, activity};

            _plannedActivities = pairs.Select(pair => new PlannedActivity
            {
                Guid = Guid.NewGuid().ToString(),
                OriginalActivityGuid = pair.activity.Guid,
                Title = pair.activity.Title,
                Duration = pair.activity.Duration,
                StartAddress = pair.activityWithTimeInstance.Start,
                Address = pair.activity.Address,
                StartTime = pair.activityWithTimeInstance.StartTime
            }).ToList();
        }

        public async Task AddRoutesAsync(bool filterRouteDuplicates)
        {
            if (_plannedActivities == null
                || _plannedActivities.Any(a => a.Address == null || a.StartAddress == null))
                throw new ApplicationException(
                    "To add routes planned activities with start address, destination address and start time are required.");


            var request = new RoutingRequest
            {
                // preferences can be null / not present since routing adapter will simply apply default mapping configuration without them
                Preferences = _user.Preferences,
                FilterRouteDuplicates = filterRouteDuplicates,
                MobilityDemands = _plannedActivities.Select(planedActivity => new MobilityDemand
                {
                    ActivityGuid = planedActivity.Guid,
                    From = planedActivity.StartAddress,
                    To = planedActivity.Address,
                    At = planedActivity.StartTime
                }).ToList()
            };

            var routesForActivities = await _routingAdapterClient.GetRoutesAsync(request);

            var pairs = from routesForActivity in routesForActivities.RoutesForActivities
                join plannedActivity in _plannedActivities on routesForActivity.ActivityGuid equals plannedActivity.Guid
                select new {routesForActivity, plannedActivity};
            foreach (var pair in pairs)
            {
                pair.plannedActivity.Routes = pair.routesForActivity.Routes;
            }
        }

        public async Task AddRouteCostsAsync()
        {
            if (_user.Preferences == null || _user.Preferences.Count == 0)
                throw new ApplicationException("To add route qualities preferences are required.");
            if (_plannedActivities == null || _plannedActivities.Any(pa => pa.Routes == null))
                throw new ApplicationException("To add route qualities routes are required");

            var routes = _plannedActivities.SelectMany(x => x.Routes).ToList();
            var request = new RouteQualityRequest
            {
                Routes = _plannedActivities.SelectMany(x => x.Routes).ToList(),
                Preferences = _user.Preferences,
            };
            var response = await _routeProcessingServiceClient.GetRouteQualitiesAsync(request);

            var pairs = from route in routes
                join routeCosts in response.RoutesCosts on route.Guid equals routeCosts.RouteGuid
                select new {route, routeCosts};
            foreach (var pair in pairs)
            {
                pair.route.Costs = pair.routeCosts.Costs;
            }
        }

        public async Task AddActivityArrangementAsync()
        {
            if (_plannedActivities == null || _plannedActivities.Any(pa => pa.Routes == null) ||
                _plannedActivities.Any(pa => pa.Routes.Any(r => !r.Costs.HasValue)))
                throw new ApplicationException("To add an activity Arrangement routes with cots are required");

            // Build Requests
            var request = new GatspRequest
            {
                PlannedActivities = _plannedActivities.Select(plannedActivity => new GatspPlannedActivity
                {
                    OriginalActivityGuid = plannedActivity.OriginalActivityGuid,
                    PlannedActivityGuid = plannedActivity.Guid,
                    StartTime = plannedActivity.StartTime,
                    Duration = plannedActivity.Duration,
                    Routes = plannedActivity.Routes.Select(route => new GatspRoute
                    {
                        RouteGuid = route.Guid,
                        Costs = route.Costs.Value,
                        Modalities = route.Legs.Select(leg => leg.Modality).ToList(),
                    }).ToList(),
                }).ToList(),
            };

            var response = await _gatspSolverClient.CallGatspSolverAsync(request);

            // Build ArrangedActivities
            _activityArrangements = response.ActivityArrangements.Select(arrangement =>
            {
                var pairs = from arrangedActivity in arrangement.ArrangedActivities
                    join plannedActivity in _plannedActivities on arrangedActivity.PlannedActivityGuid equals
                        plannedActivity.Guid
                    select new {arrangedActivity, plannedActivity};

                return new ActivityArrangement
                {
                    Arrangement = pairs.Select(pair => new ArrangedActivity
                    {
                        Guid = Guid.NewGuid().ToString(),
                        Title = pair.plannedActivity.Title,
                        Address = pair.plannedActivity.Address,
                        StartTime = pair.plannedActivity.StartTime,
                        Duration = pair.plannedActivity.Duration,
                        OriginalActivityGuid = pair.plannedActivity.OriginalActivityGuid,
                        PlannedActivityGuid = pair.plannedActivity.Guid,
                        StartAddress = pair.plannedActivity.StartAddress,
                        Route = pair.plannedActivity.Routes.FirstOrDefault(r =>
                            r.Guid == pair.arrangedActivity.RouteGuid),
                        AlternativeRoutes = pair.plannedActivity.Routes
                            .Where(r => r.Guid != pair.arrangedActivity.RouteGuid).ToList()
                    }).ToList(),
                };
            }).ToList();
        }


        public ICollection<ActivityPlan> BuildActivityPlans()
        {
            if (_activityArrangements == null)
                throw new ApplicationException("To build activity plan activity arrangements are required");

            return _activityArrangements
                // select planned activity as well as their order
                .Select(activityArrangement => new ActivityPlan
                {
                    Guid = Guid.NewGuid().ToString(),
                    User = _user,
                    Day = _day,
                    ArrangedActivities = activityArrangement.Arrangement,
                }).ToList();
        }

        private async Task AddGeoLocationsToAddresses(ICollection<Address> addresses)
        {
            // start all geocoding tasks
            var geocodingTaskPairs = addresses.Select(address =>
                new {address, geocodingTask = _geoCodingClient.GeocodeAddressAsync(address)});

            // update addresses
            foreach (var pair in geocodingTaskPairs)
            {
                pair.address.GeoLocation = await pair.geocodingTask;
            }
        }
    }
}
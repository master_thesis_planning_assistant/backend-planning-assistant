﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OrchestrationService.Model;
using OrchestrationService.Model.Activity;

namespace OrchestrationService.Builder
{
    public interface IActivityPlanBuilder
    {
        /// <summary>
        /// Initializes the activity plan builder
        /// </summary>
        /// <param name="userName">User name to request user preference service</param>
        /// <param name="password">Password to request user preference service</param>
        /// <param name="day">Day of the activity plan</param>
        /// <param name="activities">Activities of the activity plan</param>
        void InitializeActivityPlanFromActivities(string userName, string password, DateTime day,
            ICollection<Activity> activities);

        /// <summary>
        /// Adds User preferences to the already initialized builder.
        /// Requires userName and password to be present
        /// </summary>
        Task AddUserPreferencesAsync();

        /// <summary>
        /// Adds time and location instances to the activities given on initialization to obtain planned activities.
        /// Requires user preferences and activities
        /// </summary>
        Task AddTimeAndLocationInstancesAsync();

        /// <summary>
        /// Adds geolocations to all activities with a determined address without geolocations
        /// </summary>
        Task AddGeoLocationsToActivitiesAsync();

        /// <summary>
        /// Adds routes for the given planned activities.
        /// Requires planned activities and user preferences.
        /// </summary>
        /// <param name="filterRouteDuplicates">Filter to similar routes</param>
        Task AddRoutesAsync(bool filterRouteDuplicates);

        /// <summary>
        /// Adds costs to all routes.
        /// Requires routes and user preferences.
        /// </summary>
        Task AddRouteCostsAsync();

        /// <summary>
        /// Adds one ore more arrangements for the given planned activities.
        /// Requires planned activities and routes with costs.
        /// </summary>
        Task AddActivityArrangementAsync();

        /// <summary>
        /// Builds the resulting activity plans
        /// </summary>
        /// <returns>List of Activity plans</returns>
        ICollection<ActivityPlan> BuildActivityPlans();

        /// <summary>
        /// Adds geolocations to all planned activities without geolocations
        /// </summary>
        Task AddGeolocationsToPlannedActivitiesAsync();
    }
}
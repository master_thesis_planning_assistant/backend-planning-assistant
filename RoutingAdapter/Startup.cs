using System.Linq;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RoutingAdapter.Infrastructure.Extensions;
using RoutingAdapter.RoutingServices.OpenRouteService;
using RoutingAdapter.RoutingServices.OpenRouteService.Client;
using RoutingAdapter.RoutingServices.OpenTripPlanner;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Client;
using RoutingAdapter.RoutingServices.Trias;
using RoutingAdapter.RoutingServices.Trias.Client;
using RoutingAdapter.RoutingServices.Valhalla;
using RoutingAdapter.RoutingServices.Valhalla.Client;
using RoutingAdapter.Services.RouteConsolidation;
using RoutingAdapter.Services.RoutingServiceFiltering;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddLoggingWithFormatter();
            services.AddCors();
            services.AddControllers();
            services.AddAutoMapper(typeof(Startup));
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "Routing-Adapter";
                    document.Info.Description =
                        "This Service is used to request different routing services in order to obtain mobility options in the form of routes.";
                };
            });

            // Register Routing services
            services.AddOpenRoutingService(Configuration.GetConnectionString(OrsClient.HttpClientName));
            services.AddOpenTripPlanner(Configuration.GetConnectionString(OtpClient.HttpClientName));
            services.AddValhalla(Configuration.GetConnectionString(ValhallaClient.HttpClientName));
            services.AddTrias(Configuration.GetConnectionString(TriasClient.HttpClientName),
                Configuration.GetSection(TriasClient.HttpClientName));

            // Add Other services
            services.AddRouteConsolidationService(Configuration.GetSection("RouteConsolidation"));
            services.AddScoped<IXsltTransformerService, XsltTransformerService>();
            services.AddScoped<IRoutingServiceFilteringService, RoutingServiceFilteringService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (!env.IsDevelopment())
            {
                app.UseHsts();
            }

            var allowedOrigins = Configuration.GetSection("Cors:AllowedOrigins").Get<string[]>();
            if (allowedOrigins != null && allowedOrigins.Any())
            {
                app.UseCors(options =>
                {
                    options
                        .AllowAnyHeader()
                        .WithMethods("GET", "PUT", "POST", "DELETE")
                        .AllowCredentials()
                        .WithOrigins(allowedOrigins)
                        .WithExposedHeaders("Content-Disposition");
                });
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(settings => { settings.DocumentTitle = "Routing-Adapter"; });

            app.UseCustomExceptionHandler();

            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
    }
}
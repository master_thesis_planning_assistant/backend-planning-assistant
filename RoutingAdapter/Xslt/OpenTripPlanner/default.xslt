<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequest>
            <Parameters>
                <ArriveBy>
                    true
                </ArriveBy>
                <Date>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                </Date>
                <FromPlace>
                    <Longitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                    </Longitude>
                    <Latitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                    </Latitude>
                </FromPlace>
                <Time>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                </Time>
                <ToPlace>
                    <Longitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                    </Longitude>
                    <Latitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                    </Latitude>
                </ToPlace>
            </Parameters>
        </RoutingRequest>
    </xsl:template>
</xsl:stylesheet>
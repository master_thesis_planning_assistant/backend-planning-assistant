﻿<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequest>
            <Parameters>
                <ArriveBy>
                    true
                </ArriveBy>
                <Date>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                </Date>
                <FromPlace>
                    <Longitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                    </Longitude>
                    <Latitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                    </Latitude>
                </FromPlace>
                <Time>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                </Time>
                <ToPlace>
                    <Longitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                    </Longitude>
                    <Latitude>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                    </Latitude>
                </ToPlace>
                <Mode>
                    <ArrayOfRoutingMode>
                        <xsl:for-each
                                select="SingleRoutingRequest/Modalities/RoutingMode">
                            <xsl:call-template name="RoutingModeEnum"/>
                        </xsl:for-each>
                    </ArrayOfRoutingMode>
                </Mode>
            </Parameters>
        </RoutingRequest>
    </xsl:template>
    <!--RoutingMode Enum-->
    <!--RoutingMode carsharing is not supported by OTP-->
    <xsl:template match="RoutingMode" name="RoutingModeEnum">
        <xsl:variable name="mode"
                      select="."/>
        <xsl:choose>
            <xsl:when test=".='car'">
                <RoutingMode>CAR</RoutingMode>
            </xsl:when>
            <xsl:when test=".='walk'">
                <RoutingMode>WALK</RoutingMode>
            </xsl:when>
            <xsl:when test=".='bike'">
                <RoutingMode>BICYCLE</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Bikesharing'">
                <RoutingMode>BICYCLE_RENT</RoutingMode>
            </xsl:when>
            <xsl:when test=".='public transport'">
                <RoutingMode>TRANSIT</RoutingMode>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequest>
            <JsonParameters>
                <CostingModel>auto</CostingModel>
                <Locations>
                    <Location>
                        <Lat>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                        </Lat>
                        <Lon>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                        </Lon>
                        <Type>break</Type>
                        <Street>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/StreetAndNumber"/>
                        </Street>
                    </Location>
                    <Location>
                        <Lat>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                        </Lat>
                        <Lon>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                        </Lon>
                        <Type>break</Type>
                        <Street>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/StreetAndNumber"/>
                        </Street>
                    </Location>
                </Locations>
                <DateTime>
                    <Type>1</Type>
                    <Value>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                    </Value>
                </DateTime>
            </JsonParameters>
        </RoutingRequest>
    </xsl:template>
</xsl:stylesheet>
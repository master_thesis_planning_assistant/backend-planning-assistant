﻿<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequest>
            <JsonParameters>
                <!--This wil result in only one mode since it is handled in the code-->
                <xsl:for-each
                        select="SingleRoutingRequest/Modalities/RoutingMode">
                    <xsl:call-template name="RoutingModeEnum"/>
                </xsl:for-each>
                <Locations>
                    <Location>
                        <Lat>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                        </Lat>
                        <Lon>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                        </Lon>
                        <Type>break</Type>
                        <Street>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/StreetAndNumber"/>
                        </Street>
                    </Location>
                    <Location>
                        <Lat>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                        </Lat>
                        <Lon>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                        </Lon>
                        <Type>break</Type>
                        <Street>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/StreetAndNumber"/>
                        </Street>
                    </Location>
                </Locations>
                <DateTime>
                    <Type>1</Type>
                    <Value>
                        <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                    </Value>
                </DateTime>
            </JsonParameters>
        </RoutingRequest>
    </xsl:template>
    <!--RoutingMode Enum-->
    <!--RoutingMode carsharing and public transport is not supported by Valhalla-->
    <xsl:template match="RoutingMode" name="RoutingModeEnum">
        <xsl:variable name="mode"
                      select="."/>
        <xsl:choose>
            <xsl:when test=".='car'">
                <CostingModel>auto</CostingModel>
            </xsl:when>
            <xsl:when test=".='walk'">
                <CostingModel>pedestrian</CostingModel>
            </xsl:when>
            <xsl:when test=".='bike'">
                <CostingModel>bicycle</CostingModel>
            </xsl:when>
            <xsl:when test=".='Bikesharing'">
                <CostingModel>bikeshare</CostingModel>
            </xsl:when>
            <xsl:when test=".='public transport'">
                <CostingModel>multimodal</CostingModel>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
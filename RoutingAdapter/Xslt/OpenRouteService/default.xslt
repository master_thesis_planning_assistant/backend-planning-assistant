<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <Profile>driving-car</Profile>
        <RoutingRequest>
            <BodyParameter>
                <InternalCoordinates>
                    <GeoCoordinate>
                        <Longitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                        </Longitude>
                        <Latitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                        </Latitude>
                    </GeoCoordinate>
                    <GeoCoordinate>
                        <Longitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                        </Longitude>
                        <Latitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                        </Latitude>
                    </GeoCoordinate>
                </InternalCoordinates>
                <AlternativeRoutes>
                    <ShareFactor>
                        0.6
                    </ShareFactor>
                    <WeightFactor>
                        1.4
                    </WeightFactor>
                </AlternativeRoutes>
            </BodyParameter>
        </RoutingRequest>
    </xsl:template>
</xsl:stylesheet>
<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequest>
            <!--This wil result in only one mode since it is handled in the code-->
            <xsl:for-each
                    select="SingleRoutingRequest/Modalities/RoutingMode">
                <xsl:call-template name="RoutingModeEnum"/>
            </xsl:for-each>
            <BodyParameter>
                <InternalCoordinates>
                    <GeoCoordinate>
                        <Longitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                        </Longitude>
                        <Latitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                        </Latitude>
                    </GeoCoordinate>
                    <GeoCoordinate>
                        <Longitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                        </Longitude>
                        <Latitude>
                            <xsl:value-of select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                        </Latitude>
                    </GeoCoordinate>
                </InternalCoordinates>
                <AlternativeRoutes>
                    <ShareFactor>
                        0.6
                    </ShareFactor>
                    <WeightFactor>
                        1.4
                    </WeightFactor>
                </AlternativeRoutes>
            </BodyParameter>
        </RoutingRequest>
    </xsl:template>
    <!--RoutingMode Enum-->
    <!--RoutingMode carsharing, Bikesharing, public transport are not supported by ORS-->
    <xsl:template match="RoutingMode" name="RoutingModeEnum">
        <xsl:variable name="mode"
                      select="."/>
        <xsl:choose>
            <xsl:when test=".='car'">
                <Profile>driving-car</Profile>
            </xsl:when>
            <xsl:when test=".='walk'">
                <Profile>foot-walking</Profile>
            </xsl:when>
            <xsl:when test=".='bike'">
                <Profile>cycling-regular</Profile>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:a="http://www.vdv.de/trias" version="1.0">
    <xsl:template match="/">
        <a:Trias>
            <a:ServiceRequest>
                <a:RequestPayload>
                    <a:TripRequest>
                        <a:Origin>
                            <a:LocationRef>
                                <a:GeoPosition>
                                    <a:Longitude>
                                        <xsl:value-of
                                                select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Longitude"/>
                                    </a:Longitude>
                                    <a:Latitude>
                                        <xsl:value-of
                                                select="SingleRoutingRequest/MobilityDemand/From/GeoLocation/Latitude"/>
                                    </a:Latitude>
                                </a:GeoPosition>
                            </a:LocationRef>
                        </a:Origin>
                        <a:Destination>
                            <a:LocationRef>
                                <a:GeoPosition>
                                    <a:Longitude>
                                        <xsl:value-of
                                                select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Longitude"/>
                                    </a:Longitude>
                                    <a:Latitude>
                                        <xsl:value-of
                                                select="SingleRoutingRequest/MobilityDemand/To/GeoLocation/Latitude"/>
                                    </a:Latitude>
                                </a:GeoPosition>
                            </a:LocationRef>
                            <a:DepArrTime>
                                <xsl:value-of select="SingleRoutingRequest/MobilityDemand/At"/>
                            </a:DepArrTime>
                        </a:Destination>
                        <a:Params>
                            <a:NumberOfResults>5</a:NumberOfResults>
                            <a:IncludeTrackSections>true</a:IncludeTrackSections>
                            <a:IncludeLegProjection>true</a:IncludeLegProjection>
                            <a:IncludeTurnDescription>true</a:IncludeTurnDescription>
                            <a:IncludeIntermediateStops>true</a:IncludeIntermediateStops>
                            <a:PtModeFilter>
                                <a:Exclude>false</a:Exclude>
                                <xsl:for-each
                                        select="SingleRoutingRequest/Modalities/RoutingMode">
                                    <xsl:call-template name="RoutingModeEnum"/>
                                </xsl:for-each>
                            </a:PtModeFilter>
                        </a:Params>
                    </a:TripRequest>
                </a:RequestPayload>
            </a:ServiceRequest>
        </a:Trias>
    </xsl:template>
    <!--RoutingMode Enum-->
    <!--Only RoutingMode public transport is supported by trias-->
    <xsl:template match="RoutingMode" name="RoutingModeEnum">
        <xsl:variable name="mode"
                      select="."/>
        <xsl:choose>
            <xsl:when test=".='public transport'">
                <PtMode>all</PtMode>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
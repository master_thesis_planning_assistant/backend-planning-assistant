﻿<?xml version='1.0' ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:template match="/">
        <RoutingRequestParameters>
            <ArriveBy>true</ArriveBy>
            <BikeSpeed>
                <xsl:value-of select="SingleRoutingRequest/Preferences/CyclingPace"/>
            </BikeSpeed>
            <Date>
                <xsl:value-of select="SingleRoutingRequest/MobilityDemands/At"/>
            </Date>
            <FromPlace>
                <Longitude>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemands/From/GeoLocation/Longitude"/>
                </Longitude>
                <Latitude>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemands/From/GeoLocation/Latitude"/>
                </Latitude>
            </FromPlace>
            <!--Null-check-->
            <xsl:if test="SingleRoutingRequest/Preferences/ConnectionPreferences/MaxNumOfChanges">
                <MaxTransfers>
                    <xsl:value-of select="SingleRoutingRequest/Preferences/ConnectionPreferences/MaxNumOfChanges"/>
                </MaxTransfers>
            </xsl:if>
            <MaxWalkDistance>
                <xsl:value-of select="SingleRoutingRequest/Preferences/MaxWalkingDistance"/>
            </MaxWalkDistance>
            <!--Null-check-->
            <xsl:if test="SingleRoutingRequest/Preferences/ConnectionPreferences/MinConnectingTime">
                <MinTransferTime>
                    <xsl:value-of select="SingleRoutingRequest/Preferences/ConnectionPreferences/MinConnectingTime"/>
                </MinTransferTime>
            </xsl:if>
            <Mode>
                <ArrayOfRoutingMode>
                    <xsl:for-each
                            select="SingleRoutingRequest/Preferences/ModePreferences/NeutralModes/RoutingMode">
                        <xsl:call-template name="RoutingModeEnum"/>
                    </xsl:for-each>
                    <xsl:for-each
                            select="SingleRoutingRequest/Preferences/ModePreferences/PreferredModes/RoutingMode">
                        <xsl:call-template name="RoutingModeEnum"/>
                    </xsl:for-each>
                </ArrayOfRoutingMode>
            </Mode>
            <Time>
                <xsl:value-of select="SingleRoutingRequest/MobilityDemands/At"/>
            </Time>
            <ToPlace>
                <Longitude>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemands/To/GeoLocation/Longitude"/>
                </Longitude>
                <Latitude>
                    <xsl:value-of select="SingleRoutingRequest/MobilityDemands/To/GeoLocation/Latitude"/>
                </Latitude>
            </ToPlace>
            <!--Null-check-->
            <xsl:if test="SingleRoutingRequest/Preferences/WalkingPace">
                <WalkSpeed>
                    <xsl:value-of select="SingleRoutingRequest/Preferences/WalkingPace"/>
                </WalkSpeed>
            </xsl:if>
        </RoutingRequestParameters>
    </xsl:template>
    <!--RoutingMode Enum-->
    <xsl:template match="RoutingMode" name="RoutingModeEnum">
        <xsl:variable name="mode"
                      select="."/>
        <xsl:choose>
            <xsl:when test=".='Walk'">
                <RoutingMode>WALK</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Transit'">
                <RoutingMode>TRANSIT</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Bicycle'">
                <RoutingMode>BICYCLE</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Car'">
                <RoutingMode>CAR</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Tram'">
                <RoutingMode>TRAM</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Subway'">
                <RoutingMode>SUBWAY</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Bus'">
                <RoutingMode>BUS</RoutingMode>
            </xsl:when>
            <xsl:when test=".='Airplane'">
                <RoutingMode>AIRPLANE</RoutingMode>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
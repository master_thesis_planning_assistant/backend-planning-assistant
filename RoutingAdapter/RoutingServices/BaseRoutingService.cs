﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter.RoutingServices
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">RoutingRequest</typeparam>
    /// <typeparam name="TK">RoutingResponse</typeparam>
    public abstract class BaseRoutingService<T, TK> : IRoutingService
    {
        private readonly ILogger _logger;
        private readonly IXsltTransformerService _xsltTransformer;

        protected BaseRoutingService(ILogger logger, IXsltTransformerService xsltTransformer)
        {
            _logger = logger;
            _xsltTransformer = xsltTransformer;
        }

        private string DefaultXsltPath => Path.Combine(Directory.GetCurrentDirectory(),
            "Xslt", ServiceName, "default.xslt");

        private string CustomXsltPath => Path.Combine(Directory.GetCurrentDirectory(),
            "Xslt", ServiceName, "mapping.xslt");

        public abstract string ServiceName { get; }

        public abstract ICollection<RoutingMode> SupportedModalities { get; }

        public async Task<ICollection<RoutesForActivity>> MakeRoutingRequestAsync(
            ICollection<MobilityDemand> mobilityDemands, ICollection<Preferences> preferences)
        {
            var watch = Stopwatch.StartNew();
            var mappedRoutingRequestWrappers = MapRequest(mobilityDemands, preferences);
            var mappingTime = watch.Elapsed;
            var pendingRequest = mappedRoutingRequestWrappers.Select(MakeSpecificRoutingRequestAsync).ToList();
            var routingResponseWrappers = await Task.WhenAll(pendingRequest);
            var requestTime = watch.Elapsed;
            var joinedElements =
                from mobilityDemand in mobilityDemands
                join routingResponseWrapper in routingResponseWrappers on mobilityDemand.ActivityGuid equals
                    routingResponseWrapper.ActivityGuid
                join routingRequestWrapper in mappedRoutingRequestWrappers on routingResponseWrapper.ActivityGuid equals
                    routingRequestWrapper.ActivityGuid
                select new {mobilityDemand, routingResponseWrapper, routingRequestWrapper};
            var response = joinedElements
                .Select(elements => BuildResultingRoutes(elements.routingResponseWrapper,
                    elements.routingRequestWrapper, elements.mobilityDemand)).ToList();
            var responseMappingTime = watch.Elapsed;
            _logger.LogTrace($"{ServiceName} - Request:\n" +
                             $"\tRequests: {mappedRoutingRequestWrappers.Count}\n" +
                             $"\tFound Routes: {response.Count}\n" +
                             $"\tTotal Time: {responseMappingTime.TotalSeconds}s\n" +
                             $"\tTime for parameter mapping: {mappingTime.TotalSeconds}s\n" +
                             $"\tTime for routing service request: {(requestTime - mappingTime).TotalSeconds}s\n" +
                             $"\tTime for response construction: {(responseMappingTime - requestTime).TotalSeconds}s");
            return response;
        }

        public async Task UpdateMappingAsync(string mapping)
        {
            await File.WriteAllTextAsync(CustomXsltPath, mapping);
        }

        public string GetMapping()
        {
            try
            {
                return File.ReadAllText(CustomXsltPath);
            }
            catch (IOException)
            {
                _logger.LogWarning($"Could not read mapping file at {CustomXsltPath}");
                return null;
            }
        }

        public string GetDefaultMapping()
        {
            try
            {
                return File.ReadAllText(DefaultXsltPath);
            }
            catch (IOException)
            {
                _logger.LogWarning($"Could not read default mapping file at {DefaultXsltPath}");
                return null;
            }
        }

        protected abstract RoutesForActivity BuildResultingRoutes(
            SpecificRoutingResponseWrapper<TK> specificRoutingResponseWrapper,
            SpecificRoutingRequestWrapper<T> specificRoutingRequestWrapper,
            MobilityDemand originalMobilityDemand);

        private string GetTransformationXslt(bool hasPreferences)
        {
            if (hasPreferences)
            {
                var specificXslt = GetMapping();
                if (!string.IsNullOrWhiteSpace(specificXslt)) return specificXslt;
                _logger.LogInformation(
                    $"No custom mapper configuration for {ServiceName} found, using default configuration");
            }
            else
            {
                _logger.LogInformation(
                    $"Using default mapping configuration for {ServiceName} since no calibrated preferences for this service were found");
            }

            var defaultXslt = GetDefaultMapping();
            if (string.IsNullOrWhiteSpace(defaultXslt))
            {
                throw new ApplicationException($"No default mapping configuration for {ServiceName} found.");
            }

            return defaultXslt;
        }

        private ICollection<SpecificRoutingRequestWrapper<T>> MapRequest(ICollection<MobilityDemand> mobilityDemands,
            ICollection<Preferences> preferences)
        {
            // get transformation xslt by looking for the right preferences-profile
            var calibratedPreferences = preferences.FirstOrDefault(p => p.ProfileName == ServiceName);
            var xslt = GetTransformationXslt(calibratedPreferences != null);

            // build one singleRoutingRequest per mobility demand without modalities
            var singleRoutingRequests = mobilityDemands.Select(mobilityDemand =>
                new SingleRoutingRequest
                {
                    Preferences = calibratedPreferences,
                    MobilityDemand = mobilityDemand,
                }).ToList();

            // find all relevant modalities from preferences that are supported by this services
            // -> Will be empty if no preferences for this routing service are given
            var preferredModalities =
                calibratedPreferences?.ModePreferences?.PreferredModes ?? new List<RoutingMode>();
            var neutralModalities = calibratedPreferences?.ModePreferences?.NeutralModes ?? new List<RoutingMode>();
            var modalities = preferredModalities.Concat(neutralModalities)
                .Where(m => SupportedModalities.Contains(m))
                .ToList();

            // handle modalities
            if (modalities.Count > 0)
            {
                // handle the cardinality of the modalities.
                // some routing request support a list of modalities, other only one modality per request
                // For those with only one modality, multiple requests for the different modalities have to be made!
                singleRoutingRequests = singleRoutingRequests
                    .SelectMany(srr => HandleModalityCardinality(srr, modalities)).ToList();
            }

            // Map single routing request to specific routing request using xslt transformation
            return singleRoutingRequests.Select(srr =>
            {
                var mappedRequest =
                    _xsltTransformer.Transform<SingleRoutingRequest, T>(srr, xslt);
                return new SpecificRoutingRequestWrapper<T>()
                {
                    ActivityGuid = srr.MobilityDemand.ActivityGuid,
                    RoutingRequest = mappedRequest,
                };
            }).ToList();
        }

        protected abstract Task<SpecificRoutingResponseWrapper<TK>> MakeSpecificRoutingRequestAsync(
            SpecificRoutingRequestWrapper<T> requestWrapper);

        protected abstract ICollection<SingleRoutingRequest> HandleModalityCardinality(
            SingleRoutingRequest singleRoutingRequest, ICollection<RoutingMode> modalities);
    }
}
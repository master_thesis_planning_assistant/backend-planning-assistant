﻿using System.Threading.Tasks;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Response;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Client
{
    public interface IOrsClient
    {
        Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper);
    }
}
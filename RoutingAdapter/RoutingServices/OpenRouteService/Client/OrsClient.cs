﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Response;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Client
{
    public class OrsClient : IOrsClient
    {
        public const string HttpClientName = "OpenRoutingService";

        private readonly IHttpClientFactory _httpClientFactory;

        public OrsClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public async Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.TryAddWithoutValidation("accept",
                "application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8");
            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type",
                "application/json; charset=utf-8");

            var request = requestWrapper.RoutingRequest;

            // set empty lists to zero because it will trigger a badRequest otherwise.
            if (request.BodyParameter.Attributes.Count == 0) request.BodyParameter.Attributes = null;
            if (request.BodyParameter.Bearings.Count == 0) request.BodyParameter.Bearings = null;
            if (request.BodyParameter.ExtraInfos.Count == 0) request.BodyParameter.ExtraInfos = null;
            if (request.BodyParameter.Radiuses.Count == 0) request.BodyParameter.Radiuses = null;

            var json = JsonConvert.SerializeObject(request.BodyParameter, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
            });

            var httpContent = new StringContent(json, Encoding.UTF8, MediaTypeNames.Application.Json);

            var response =
                await client.PostAsync($"v2/directions/{request.Profile.GetStringValue()}/json", httpContent);
            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request! Message: {responseBody}");
            }


            var routingResponse = JsonConvert.DeserializeObject<RoutingResponse>(responseBody);
            return new SpecificRoutingResponseWrapper<RoutingResponse>
            {
                ActivityGuid = requestWrapper.ActivityGuid,
                RoutingResponse = routingResponse,
            };
        }
    }
}
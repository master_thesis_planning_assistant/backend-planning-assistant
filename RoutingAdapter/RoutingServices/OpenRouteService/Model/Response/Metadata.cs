﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Metadata
    {
        /// <summary>
        /// Copyright and attribution information
        /// </summary>
        [JsonProperty("attribution")]
        public string Attribution { get; set; }

        /// <summary>
        /// Information about the routing service
        /// </summary>
        [JsonProperty("engine")]
        public Engine Engine { get; set; }

        /// <summary>
        /// ID of the request (as passed in by the query)
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// The MD5 hash of the OSM planet file that was used for generating graphs
        /// </summary>
        [JsonProperty("osm_file_md5_hash")]
        public string OsmFileMd5Hash { get; set; }

        /// <summary>
        /// The information that was used for generating the route
        /// </summary>
        [JsonProperty("query")]
        public object Query { get; set; }

        /// <summary>
        /// The service that was requested
        /// </summary>
        [JsonProperty("service")]
        public string Service { get; set; }

        /// <summary>
        /// System message
        /// </summary>
        [JsonProperty("system_message")]
        public string SystemMessage { get; set; }

        /// <summary>
        /// Time that the request was made (UNIX Epoch time)
        /// </summary>
        [JsonConverter(typeof(MillisecondsEpochConverter))]
        [JsonProperty("timestamp")]
        public DateTime Timestamp { get; set; }
    }
}
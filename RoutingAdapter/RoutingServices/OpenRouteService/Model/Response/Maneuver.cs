﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Maneuver
    {
        /// <summary>
        /// The azimuth angle (in degrees) of the direction right after the maneuver
        /// </summary>
        [JsonProperty("bearing_after")]
        public int BearingAfter { get; set; }

        /// <summary>
        /// The azimuth angle (in degrees) of the direction right before the maneuver.
        /// </summary>
        [JsonProperty("bearing_before")]
        public int BearingBefore { get; set; }

        /// <summary>
        /// The coordinate of the point where a maneuver takes place. 
        /// </summary>
        [JsonProperty("location")]
        public ICollection<double> Location { get; set; }
    }
}
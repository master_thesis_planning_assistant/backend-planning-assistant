﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class RoutingResponse
    {
        /// <summary>
        /// Bounding box that covers all returned routes
        /// </summary>
        [JsonProperty("bbox")]
        public ICollection<double> Bbox { get; set; }

        /// <summary>
        /// Information about the service and request
        /// </summary>
        [JsonProperty("metadata")]
        public Metadata Metadata { get; set; }

        /// <summary>
        /// A list of routes returned from the request
        /// </summary>
        [JsonProperty("routes")]
        public ICollection<Route> Routes { get; set; }
    }
}
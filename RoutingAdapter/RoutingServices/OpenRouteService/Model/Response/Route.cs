﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Route
    {
        /// <summary>
        /// Arrival date and time
        /// </summary>
        [JsonProperty("arrival")]
        public string Arrival { get; set; }

        /// <summary>
        /// A bounding box which contains the entire route
        /// </summary>
        [JsonProperty("bbox")]
        public List<double> Bbox { get; set; }

        /// <summary>
        ///   A list of coordinates encoded as a string.<br/>
        ///   See <a href="http://code.google.com/apis/maps/documentation/polylinealgorithm.html">Encoded<br/>
        ///   polyline algorithm format</a><br/>
        /// </summary>
        [JsonProperty("geometry")]
        public string Geometry { get; set; }

        /// <summary>
        /// Departure date and time
        /// </summary>
        [JsonProperty("departure")]
        public string Departure { get; set; }

        /// <summary>
        /// List of extra info objects representing the extra info items that were requested for the route.
        /// </summary>
        [JsonProperty("extras")]
        public object Extras { get; set; }

        /// <summary>
        /// List containing the segments and its corresponding steps which make up the route.
        /// </summary>
        [JsonProperty("segments")]
        public ICollection<Segment> Segments { get; set; }

        /// <summary>
        /// Summary information about the route
        /// </summary>
        [JsonProperty("summary")]
        public Summary Summary { get; set; }

        /// <summary>
        /// List of warnings that have been generated for the route
        /// </summary>
        [JsonProperty("warnings")]
        public ICollection<Warning> Warnings { get; set; }

        /// <summary>
        /// List containing the indices of way points corresponding to the geometry.
        /// </summary>
        [JsonProperty("way_points")]
        public ICollection<int> WayPoints { get; set; }
    }
}
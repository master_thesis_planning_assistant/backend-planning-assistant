﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Summary
    {
        /// <summary>
        /// Total ascent in meters.
        /// </summary>
        [JsonProperty("ascent")]
        public double Ascent { get; set; }

        /// <summary>
        /// total descent in meters
        /// </summary>
        [JsonProperty("descent")]
        public double Descent { get; set; }

        /// <summary>
        /// Total route distance in specified units.
        /// </summary>
        [JsonProperty("distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Total duration in seconds.
        /// </summary>
        [JsonProperty("duration")]
        public double Duration { get; set; }
    }
}
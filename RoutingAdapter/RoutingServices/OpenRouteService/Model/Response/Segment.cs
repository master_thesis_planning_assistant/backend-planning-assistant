﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Segment
    {
        /// <summary>
        /// Contains ascent of this segment in metres.
        /// </summary>
        [JsonProperty("ascent")]
        public double Ascent { get; set; }

        /// <summary>
        /// Contains the average speed of this segment in km/h
        /// </summary>
        [JsonProperty("avgspeed")]
        public double AvgSpeed { get; set; }

        /// <summary>
        /// Contains descent of this segment in metres.
        /// </summary>
        [JsonProperty("descent")]
        public double Descent { get; set; }

        /// <summary>
        /// Contains the deviation compared to a straight line that would have the factor 1. Double the Distance would be a 2.
        /// </summary>
        [JsonProperty("detourfactor")]
        public double Detourfactor { get; set; }

        /// <summary>
        /// Contains the distance of the segment in specified units.
        /// </summary>
        [JsonProperty("distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Contains the duration of the Segment in seconds.
        /// </summary>
        [JsonProperty("duration")]
        public double Duration { get; set; }

        /// <summary>
        ///  Contains the proportion of the route in percent.
        /// </summary>
        [JsonProperty("percentage")]
        public double Percentage { get; set; }

        /// <summary>
        /// List containing the specific steps the segment consists of.
        /// </summary>
        [JsonProperty("steps")]
        public List<Step> Steps { get; set; }
    }
}
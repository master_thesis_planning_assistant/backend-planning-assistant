﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Warning
    {
        /// <summary>
        /// Identification code for the warning
        /// </summary>
        [JsonProperty("code")]
        public int Code { get; set; }

        /// <summary>
        /// The message associated with the warning
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
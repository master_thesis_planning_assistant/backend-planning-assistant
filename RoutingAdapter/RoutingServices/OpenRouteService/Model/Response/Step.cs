﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Step
    {
        /// <summary>
        /// Contains the distance of the step metres.
        /// </summary>
        [JsonProperty("distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Contains the duration of the step in seconds.
        /// </summary>
        [JsonProperty("duration")]
        public double Duration { get; set; }

        /// <summary>
        /// Contains the bearing of the entrance and all passed exits in a roundabout
        /// </summary>
        [JsonProperty("exit_bearings")]
        public ICollection<int> ExitBearings { get; set; }

        /// <summary>
        /// Only for roundabouts. Contains the number of the exit to take.
        /// </summary>
        [JsonProperty("exit_number")]
        public int ExitNumber { get; set; }

        /// <summary>
        /// The routing instruction text for the step.
        /// </summary>
        [JsonProperty("instruction")]
        public string Instruction { get; set; }

        /// <summary>
        /// THe maneuver to be performed
        /// </summary>
        [JsonProperty("maneuver")]
        public Maneuver Maneuver { get; set; }

        /// <summary>
        ///  The name of the next street.
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// The Instruction action for symbolisation purposes.
        /// see <a href="https://github.com/GIScience/openrouteservice-docs#instruction-types" />
        /// </summary>
        [JsonProperty("type")]
        public int Type { get; set; }

        /// <summary>
        /// List containing the indices of the steps start- and endpoint corresponding to the geometry.
        /// </summary>
        [JsonProperty("way_points")]
        public List<int> WayPoints { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Response
{
    public class Engine
    {
        /// <summary>
        /// The date that the service was last updated
        /// </summary>
        [JsonProperty("build_date")]
        public string BuildDate { get; set; }

        /// <summary>
        /// The date that the graph data was last updated
        /// </summary>
        [JsonProperty("graph_date")]
        public string GraphDate { get; set; }

        /// <summary>
        /// The backend version of the openrouteservice that was queried
        /// </summary>
        [JsonProperty("version")]
        public string Version { get; set; }
    }
}
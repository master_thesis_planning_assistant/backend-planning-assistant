﻿using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class RoutingRequest
    {
        /// <summary>
        /// Specifies the route profile.
        /// </summary>
        public ProfileOption Profile { get; set; }

        public BodyParameter BodyParameter { get; set; }
    }
}
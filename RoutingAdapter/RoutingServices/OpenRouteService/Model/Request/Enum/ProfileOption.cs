﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ProfileOption
    {
        [XmlEnum("driving-car")] [EnumMember(Value = "driving-car")]
        DrivingCar,

        [XmlEnum("driving-hgv")] [EnumMember(Value = "driving-hgv")]
        DrivingHgv,

        [XmlEnum("cycling-regular")] [EnumMember(Value = "cycling-regular")]
        CyclingRegular,

        [XmlEnum("cycling-road")] [EnumMember(Value = "cycling-road")]
        CyclingRoad,

        [XmlEnum("cycling-mountain")] [EnumMember(Value = "cycling-mountain")]
        CyclingMountain,

        [XmlEnum("cycling-electric")] [EnumMember(Value = "cycling-electric")]
        CyclingElectric,

        [XmlEnum("foot-walking")] [EnumMember(Value = "foot-walking")]
        FootWalking,

        [XmlEnum("foot-hiking")] [EnumMember(Value = "foot-hiking")]
        FootHiking,

        [XmlEnum("wheelchair")] [EnumMember(Value = "wheelchair")]
        Wheelchair
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RouteAttribute
    {
        [XmlEnum("avgspeed")] [EnumMember(Value = "avgspeed")]
        AvgSpeed,

        [XmlEnum("detourfactor")] [EnumMember(Value = "detourfactor")]
        Detourfactor,

        [XmlEnum("percentage")] [EnumMember(Value = "percentage")]
        Percentage
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PreferenceOption
    {
        [XmlEnum("fastest")] [EnumMember(Value = "fastest")]
        Fast,

        [XmlEnum("shortest")] [EnumMember(Value = "shortest")]
        Shortest,

        [XmlEnum("recommended")] [EnumMember(Value = "recommended")]
        Recommended
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AvoidBorderOption
    {
        [XmlEnum("all")] [EnumMember(Value = "all")]
        All,

        [XmlEnum("controlled")] [EnumMember(Value = "controlled")]
        Controlled,

        [XmlEnum("none")] [EnumMember(Value = "none")]
        None,
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ExtraInfo
    {
        [XmlEnum("steepness")] [EnumMember(Value = "steepness")]
        Steepness,

        [XmlEnum("suitability")] [EnumMember(Value = "suitability")]
        Suitability,

        [XmlEnum("surface")] [EnumMember(Value = "surface")]
        Surface,

        [XmlEnum("waycategory")] [EnumMember(Value = "waycategory")]
        Waycategory,

        [XmlEnum("waytype")] [EnumMember(Value = "waytype")]
        Waytype,

        [XmlEnum("tollways")] [EnumMember(Value = "tollways")]
        Tollways,

        [XmlEnum("traildifficulty")] [EnumMember(Value = "traildifficulty")]
        Traildifficulty,

        [XmlEnum("osmid")] [EnumMember(Value = "osmid")]
        Osmid,

        [XmlEnum("roadaccessrestrictions")] [EnumMember(Value = "roadaccessrestrictions")]
        Roadaccessrestrictions,

        [XmlEnum("countryinfo")] [EnumMember(Value = "countryinfo")]
        Countryinfo,

        [XmlEnum("green")] [EnumMember(Value = "green")]
        Green,

        [XmlEnum("noise")] [EnumMember(Value = "noise")]
        Noise
    }
}
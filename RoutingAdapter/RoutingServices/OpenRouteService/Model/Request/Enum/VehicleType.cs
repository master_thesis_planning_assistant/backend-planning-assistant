﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum VehicleType
    {
        [XmlEnum("hgv")] [EnumMember(Value = "hgv")]
        Hgv,

        [XmlEnum("bus")] [EnumMember(Value = "bus")]
        Bus,

        [XmlEnum("agricultural")] [EnumMember(Value = "agricultural")]
        Agricultural,

        [XmlEnum("delivery")] [EnumMember(Value = "delivery")]
        Delivery,

        [XmlEnum("forestry")] [EnumMember(Value = "forestry")]
        Forestry,

        [XmlEnum("goods")] [EnumMember(Value = "goods")]
        Goods,

        [XmlEnum("unknown")] [EnumMember(Value = "unknown")]
        Unknown,
    }
}
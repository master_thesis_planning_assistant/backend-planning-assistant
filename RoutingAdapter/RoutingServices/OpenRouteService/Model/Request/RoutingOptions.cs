﻿using System.Collections.Generic;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class RoutingOptions
    {
        /// <summary>
        /// Requires profile driving-*
        /// "all" for no border crossing. "controlled" to cross open borders but avoid controlled ones
        /// </summary>
        [JsonProperty("avoid_borders")]
        public AvoidBorderOption? AvoidBorders { get; set; }

        /// <summary>
        /// Requires profile driving-*
        /// List of countries to exclude from routing with driving-* profiles.
        /// Can be used together with 'avoid_borders': 'controlled'.
        /// [ 11, 193 ] would exclude Austria and Switzerland. List of countries and application examples can be found
        /// here: <a href="https://github.com/GIScience/openrouteservice-docs#country-list"/>.
        /// Also, ISO standard country codes cna be used in place of the numerical ids, for example, DE or DEU for Germany.
        /// </summary>
        [JsonProperty("avoid_countries")]
        public List<object> AvoidCountries { get; set; }

        /// <summary>
        /// List of features to avoid.
        /// </summary>
        [JsonProperty("avoid_features")]
        public List<AvoidFeatureOption> AvoidFeatures { get; set; }

        /// <summary>
        /// Comprises areas to be avoided for the route. Formatted in GeoJSON as either a Polygon or Multipolygon object.
        /// </summary>
        [JsonProperty("avoid_polygons")]
        public object AvoidPolygons { get; set; }

        /// <summary>
        /// Options to be applied on round trip routes.
        /// </summary>
        [JsonProperty("round_trip")]
        public RoundTripOptions RoundTrip { get; set; }

        /// <summary>
        /// Requires profile not to be driving-*
        /// Specifies additional routing parameters.
        /// </summary>
        [JsonProperty("profile_params")]
        public ProfileParams ProfileParams { get; set; }

        /// <summary>
        /// Requires Profile driving-hgv
        ///  hgv,bus,agricultural,delivery,forestry and goods. It is needed for vehicle restrictions to work
        /// </summary>
        [JsonProperty("vehicle_type")]
        public VehicleType? VehicleType { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class AlternativeRoutesOptions
    {
        /// <summary>
        /// Maximum fraction of the route that alternatives may share with the optimal route.
        /// The default value of 0.6 means alternatives can share up to 60% of path segments with the optimal route.
        /// </summary>
        [JsonProperty("share_factor")]
        public double ShareFactor { get; set; } = 0.6;

        /// <summary>
        /// Target number of alternative routes to compute. Service returns up to this number of routes that fulfill
        /// the share-factor and weight-factor constraints.
        /// </summary>
        [JsonProperty("target_count")]
        public int? TargetCount { get; set; }


        /// <summary>
        /// Maximum factor by which route weight may diverge from the optimal route. The default value of 1.4 means
        /// alternatives can be up to 1.4 times longer (costly) than the optimal route.
        /// </summary>
        [JsonProperty("weight_factor")]
        public double WeightFactor { get; set; } = 1.4;
    }
}
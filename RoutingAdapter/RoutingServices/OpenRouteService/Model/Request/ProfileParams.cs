﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class ProfileParams
    {
        /// <summary>
        /// Describe restrictions to be applied to edges on the routing. any edges that do not match these restrictions are not traversed.
        /// </summary>
        [JsonProperty("restrictions")]
        public ProfileParamsRestrictions Restrictions { get; set; }

        /// <summary>
        /// Describe additional weightings to be applied to edges on the routing.
        /// </summary>
        [JsonProperty("weightings")]
        public ProfileParamsWeightings Weightings { get; set; }
    }
}
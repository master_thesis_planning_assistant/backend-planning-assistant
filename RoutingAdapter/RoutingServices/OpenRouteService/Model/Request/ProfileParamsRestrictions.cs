﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class ProfileParamsRestrictions
    {
        /// <summary>
        /// Requires profile driving-hgv
        /// Axleload restriction in tons.
        /// </summary>
        [JsonProperty("axleload")]
        public double? Axleload { get; set; }

        /// <summary>
        /// Requires profile driving-hgv
        /// Specifies whether to use appropriate routing for delivering hazardous goods and avoiding water protected areas.
        /// Default is false.
        /// </summary>
        [JsonProperty("hazmat")]
        public bool Hazmat { get; set; } = false;

        /// <summary>
        /// Requires profile driving-hgv
        /// Height restriction in metres.
        /// </summary>
        [JsonProperty("height")]
        public double? Height { get; set; }

        /// <summary>
        /// Requires profile driving-hgv
        /// Length restriction in metres.
        /// </summary>
        [JsonProperty("length")]
        public double? Length { get; set; }

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the maximum incline as a percentage. 3, 6 (default), 10, `15.
        /// </summary>
        [JsonProperty("maximum_incline")]
        public int MaximumIncline { get; set; } = 6;

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the maximum height of the sloped curb in metres. Values are 0.03, 0.06 (default), 0.1.
        /// </summary>
        [JsonProperty("maximum_sloped_kerb")]
        public double MaximumSlopedKerb { get; set; } = 0.6;

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the minimum width of the footway in metres.
        /// </summary>
        [JsonProperty("minimum_width")]
        public double? MinimumWidth { get; set; }

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the minimum smoothness of the route. Default is good.
        /// </summary>
        [JsonProperty("smoothness_type")]
        public string SmoothnessType { get; set; } = "good";

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the minimum surface type. Default is cobblestone:flattened.
        /// </summary>
        [JsonProperty("surface_type")]
        public string SurfaceType { get; set; } = "cobblestone:flattened";

        /// <summary>
        /// Requires profile wheelchair
        /// Specifies the minimum grade of the route. Default is grade1.
        /// </summary>
        [JsonProperty("track_type")]
        public string TrackType { get; set; } = "grade1";

        /// <summary>
        /// Requires profile driving-hgv
        /// Weight restriction in tons.
        /// </summary>
        [JsonProperty("weight")]
        public double? Weight { get; set; }

        /// <summary>
        /// Requires profile driving-hgv
        /// Width restriction in metres.
        /// </summary>
        [JsonProperty("width")]
        public double? Width { get; set; }
    }
}
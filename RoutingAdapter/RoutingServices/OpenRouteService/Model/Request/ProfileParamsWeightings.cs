﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class ProfileParamsWeightings
    {
        /// <summary>
        /// Requires profile foot-*
        /// Specifies the Green factor for foot-* profiles
        /// factor: Multiplication factor range from 0 to 1. 0 is the green routing base factor without multiplying it
        /// by the manual factor and is already different from normal routing.
        /// 1 will prefer ways through green areas over a shorter route.
        /// </summary>
        [JsonProperty("green")]
        public double? Green { get; set; }

        /// <summary>
        /// Requires profile foot-*
        /// Specifies the Quiet factor for foot-* profiles.
        /// factor: Multiplication factor range from 0 to 1. 0 is the quiet routing base factor without multiplying it
        /// by the manual factor and is already different from normal routing.
        /// 1 will prefer quiet ways over a shorter route.
        /// </summary>
        [JsonProperty("quiet")]
        public double? Quiet { get; set; }

        /// <summary>
        /// Requires profile cycling-*
        /// Specifies the fitness level for cycling-* profile.
        /// level: 0 = Novice, 1 = Moderate, 2 = Amateur, 3 = Pro. The prefered gradient increases with level.
        /// </summary>
        [JsonProperty("steepness_difficulty")]
        public int? SteepnessDifficulty { get; set; }
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class RoundTripOptions
    {
        /// <summary>
        /// The target length of the route in m (note that this is a preferred value, but results may be different).
        /// </summary>
        [JsonProperty("length")]
        public double? Length { get; set; }

        /// <summary>
        /// The number of points to use on the route. Larger values create more circular routes.
        /// </summary>
        [JsonProperty("points")]
        public int? Points { get; set; }

        /// <summary>
        /// A seed to use for adding randomisation to the overall direction of the generated route
        /// </summary>
        [JsonProperty("seed")]
        public int? Seed { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum;

namespace RoutingAdapter.RoutingServices.OpenRouteService.Model.Request
{
    public class BodyParameter
    {
        /// <summary>
        /// The waypoints to use for the route as an array of longitude/latitude pairs.
        /// </summary>
        [JsonIgnore]
        public List<GeoCoordinate> InternalCoordinates { get; set; }

        /// <summary>
        /// The waypoints to use for the route as an array of longitude/latitude pairs.
        /// This is just a computed Property, use <see cref="InternalCoordinates"/> to set the value.
        /// </summary>
        [JsonProperty("coordinates")]
        public double[][] Coordinates =>
            InternalCoordinates.Select(c => new double[] {c.Longitude, c.Latitude}).ToArray();

        /// <summary>
        /// Specifies whether alternative routes are computed, and parameters for the algorithm determining suitable alternatives.
        /// </summary>
        [JsonProperty("alternative_routes")]
        public AlternativeRoutesOptions AlternativeRoutes { get; set; }

        /// <summary>
        /// List of route attributes
        /// </summary>
        [JsonProperty("attributes")]
        public List<RouteAttribute> Attributes { get; set; }

        /// <summary>
        /// Forces the route to keep going straight at waypoints restricting uturns there even if it would be faster.
        /// Default value false
        /// </summary>
        [JsonProperty("continue_straight")]
        public bool ContinueStraight { get; set; } = false;

        /// <summary>
        /// Specifies whether to return elevation values for points. Please note that elevation also gets encoded for
        /// json response encoded polyline. Default value false
        /// </summary>
        [JsonProperty("elevation")]
        public bool Elevation { get; set; } = false;

        /// <summary>
        /// The extra info items to include in the response
        /// </summary>
        [JsonProperty("extra_info")]
        public List<ExtraInfo> ExtraInfos { get; set; }

        /// <summary>
        /// Specifies whether to simplify the geometry. Simplify geometry cannot be applied to routes with
        /// more than one segment and when <see cref="ExtraInfos"/> is required. Default value false
        /// </summary>
        [JsonProperty("geometry_simplify")]
        public bool GeometrySimplify { get; set; } = false;

        /// <summary>
        /// Arbitrary identification string of the request reflected in the meta information.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// Specifies whether to return instructions.
        /// Default Value true
        /// </summary>
        [JsonProperty("instructions")]
        public bool Instructions { get; set; } = true;

        /// <summary>
        /// Select html for more verbose instructions. Default Text
        /// </summary>
        [JsonProperty("instructions_format")]
        public InstructionFormat InstructionFormat { get; set; } = InstructionFormat.Text;

        /// <summary>
        /// Language for the route instructions. (Other options are de, es...)
        /// For more infos see <a href="https://openrouteservice.org/dev/#/api-docs/v2/directions/{profile}/json/post"/>
        /// </summary>
        [JsonProperty("language")]
        public string Language { get; set; } = "en";

        /// <summary>
        /// Specifies whether the maneuver object is included into the step object or not.
        /// default value false
        /// </summary>
        [JsonProperty("maneuvers")]
        public bool Maneuvers { get; set; } = false;

        /// <summary>
        /// For advanced options. 
        /// </summary>
        [JsonProperty("options")]
        public RoutingOptions Options { get; set; }

        /// <summary>
        /// Specifies the route preference.
        /// Default value is "recommended"
        /// </summary>
        [JsonProperty("preference")]
        public PreferenceOption Preferences { get; set; } = PreferenceOption.Recommended;

        /// <summary>
        /// A pipe list of maximum distances (measured in metres) that limit the search of nearby road segments
        /// to every given waypoint. The values must be greater than 0, the value of -1 specifies no limit in the search.
        /// The number of radiuses correspond to the number of waypoints.
        /// </summary>
        [JsonProperty("radiuses")]
        public List<int> Radiuses { get; set; }

        /// <summary>
        /// Provides bearings of the entrance and all passed roundabout exits.
        /// Adds the exit_bearings array to the step object in the response.
        /// default value is false
        /// </summary>
        [JsonProperty("roundabout_exits")]
        public bool RoundaboutExits { get; set; } = false;

        /// <summary>
        /// Specifies the distance unit.
        /// Default value is m
        /// </summary>
        [JsonProperty("units")]
        public Unit Units { get; set; } = Unit.M;

        /// <summary>
        /// Specifies whether to return geometry.
        /// Default value is True
        /// </summary>
        [JsonProperty("geometry")]
        public bool Geometry { get; set; } = true;

        /// <summary>
        /// Only for Profile driving-*
        /// The maximum speed specified by user.
        /// </summary>
        [JsonProperty("maximum_speed")]
        public double? MaximumSpeed { get; set; }

        /// <summary>
        /// Only for Profile cycling-*
        /// Specifies a list of pairs (bearings and deviations) to filter the segments of the road network a waypoint
        /// can snap to. For example bearings=[[45,10],[120,20]].
        /// Each pair is a comma-separated list that can consist of one or two float values, where the first value
        /// is the bearing and the second one is the allowed deviation from the bearing. The bearing can take values
        /// between 0 and 360 clockwise from true north. If the deviation is not set, then the default value of 100 degrees
        /// is used. The number of pairs must correspond to the number of waypoints.
        /// The number of bearings corresponds to the length of waypoints-1 or waypoints. If the bearing information
        /// for the last waypoint is given, then this will control the sector from which the destination waypoint may
        /// be reached. You can skip a bearing for a certain waypoint by passing an empty value for an array,
        /// e.g. [30,20],[],[40,20].
        /// </summary>
        [JsonProperty("bearings")]
        public List<List<double>> Bearings { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;
using RoutingAdapter.RoutingServices.OpenRouteService.Client;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Request.Enum;
using RoutingAdapter.RoutingServices.OpenRouteService.Model.Response;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter.RoutingServices.OpenRouteService
{
    /// <summary>
    /// Adapter for the OpenRouteService (ORS) Routing-Service. For Doku see
    /// ORS: https://github.com/GIScience/openrouteservice
    /// Model: https://openrouteservice.org/dev/#/api-docs/v2/directions/{profile}/json/post
    /// </summary>
    public class OpenRouteServiceRoutingServiceAdapter : BaseRoutingService<RoutingRequest, RoutingResponse>
    {
        private readonly IOrsClient _client;
        private readonly ILogger<OpenRouteServiceRoutingServiceAdapter> _logger;

        public OpenRouteServiceRoutingServiceAdapter(IOrsClient client,
            ILogger<OpenRouteServiceRoutingServiceAdapter> logger, IXsltTransformerService xsltTransformer) : base(
            logger,
            xsltTransformer)
        {
            _client = client;
            _logger = logger;
        }

        public override string ServiceName => "OpenRouteService";


        public override ICollection<RoutingMode> SupportedModalities => new List<RoutingMode>
        {
            // RoutingMode.Bike, TODO: this modality throws error
            RoutingMode.Car,
            // RoutingMode.Walk, TODO: this modality throws error
        };

        protected override Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeSpecificRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            return _client.MakeRoutingRequestAsync(requestWrapper);
        }

        protected override ICollection<SingleRoutingRequest> HandleModalityCardinality(
            SingleRoutingRequest singleRoutingRequest, ICollection<RoutingMode> modalities)
        {
            // ORS can not handle multiple modalities in one request.
            // See: RoutingRequest > Profile
            return modalities.Select(modality => new SingleRoutingRequest
            {
                Preferences = singleRoutingRequest.Preferences,
                MobilityDemand = singleRoutingRequest.MobilityDemand,
                Modalities = new List<RoutingMode> {modality}
            }).ToList();
        }


        protected override RoutesForActivity BuildResultingRoutes(
            SpecificRoutingResponseWrapper<RoutingResponse> specificRoutingResponseWrapper,
            SpecificRoutingRequestWrapper<RoutingRequest> specificRoutingRequestWrapper,
            MobilityDemand originalMobilityDemand)
        {
            var result = new RoutesForActivity
            {
                ActivityGuid = specificRoutingResponseWrapper.ActivityGuid,
                Routes = new List<ActivityRoute>()
            };

            foreach (var route in specificRoutingResponseWrapper.RoutingResponse.Routes)
            {
                var newRoute = new ActivityRoute
                {
                    Guid = Guid.NewGuid().ToString(),
                    ServiceName = ServiceName,
                    Polyline = new Polyline
                    {
                        Points = Polyline.DecodeEncodedPolyline(route.Geometry).ToList(),
                    },
                    Start = originalMobilityDemand.From,
                    Departure = route.Arrival != null
                        ? DateTime.Parse(route.Departure)
                        : originalMobilityDemand.At -
                          TimeSpan.FromSeconds(route.Summary.Duration),
                    Destination = originalMobilityDemand.To,
                    Arrival = route.Arrival != null
                        ? DateTime.Parse(route.Departure)
                        : originalMobilityDemand.At,
                    Bounds = new Bounds
                    {
                        Southwest = new GeoLocation
                        {
                            Longitude = route.Bbox[0],
                            Latitude = route.Bbox[1],
                        },
                        Northeast = new GeoLocation
                        {
                            Longitude = route.Bbox[2],
                            Latitude = route.Bbox[3]
                        }
                    },
                    Duration = route.Summary.Duration,
                    Distance = route.Summary.Distance,
                    Legs = new List<ActivityRouteLeg>(),
                };

                var lastStepArrival = newRoute.Departure;
                foreach (var segment in route.Segments)
                {
                    var firstStep = segment.Steps.First();
                    var firstWaypoint = firstStep.WayPoints[0];
                    var lastStep = segment.Steps.Last();
                    var lastWaypoint = firstStep.WayPoints[1];
                    var newLeg = new ActivityRouteLeg
                    {
                        Modality = GetModeEquivalent(specificRoutingRequestWrapper.RoutingRequest.Profile),
                        StartLocation = new NamedGeoLocation
                        {
                            Name = firstStep.Name,
                            GeoLocation = newRoute.Polyline.GetPointAt(firstWaypoint)
                        },
                        Departure = lastStepArrival,
                        EndLocation = new NamedGeoLocation
                        {
                            Name = lastStep.Name,
                            GeoLocation = newRoute.Polyline.GetPointAt(lastWaypoint)
                        },
                        Arrival = lastStepArrival + TimeSpan.FromSeconds(segment.Duration),
                        Polyline = newRoute.Polyline.GetSegment(firstWaypoint, lastWaypoint),
                        Duration = segment.Duration,
                        Distance = segment.Distance,
                        Steps = new List<ActivityRouteStep>(),
                    };
                    lastStepArrival = newLeg.Arrival;

                    foreach (var segmentStep in segment.Steps)
                    {
                        var firstStepWaypoint = segmentStep.WayPoints[0];
                        var lastStepWaypoint = segmentStep.WayPoints[1];
                        var newStep = new ActivityRouteStep
                        {
                            StartLocation = new NamedGeoLocation
                            {
                                Name = segmentStep.Name,
                                GeoLocation = newRoute.Polyline.GetPointAt(firstStepWaypoint)
                            },
                            EndLocation = new NamedGeoLocation
                            {
                                Name = segmentStep.Name,
                                GeoLocation = newRoute.Polyline.GetPointAt(lastWaypoint)
                            },
                            Polyline = newRoute.Polyline.GetSegment(firstStepWaypoint, lastStepWaypoint),
                            Duration = segmentStep.Duration,
                            Distance = segmentStep.Distance,
                            Action = segmentStep.Instruction
                        };
                        newLeg.Steps.Add(newStep);
                    }

                    newRoute.Legs.Add(newLeg);
                }

                result.Routes.Add(newRoute);
            }

            return result;
        }

        private RoutingMode GetModeEquivalent(ProfileOption specificMode)
        {
            return specificMode switch
            {
                ProfileOption.DrivingCar => RoutingMode.Car,
                ProfileOption.DrivingHgv => RoutingMode.Car,
                ProfileOption.CyclingRegular => RoutingMode.Bike,
                ProfileOption.CyclingRoad => RoutingMode.Bike,
                ProfileOption.CyclingMountain => RoutingMode.Bike,
                ProfileOption.CyclingElectric => RoutingMode.Bike,
                ProfileOption.FootWalking => RoutingMode.Walk,
                ProfileOption.FootHiking => RoutingMode.Walk,
                ProfileOption.Wheelchair => RoutingMode.Walk,
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to ProfileOption {specificMode.GetStringValue()} found")
            };
        }
    }
}
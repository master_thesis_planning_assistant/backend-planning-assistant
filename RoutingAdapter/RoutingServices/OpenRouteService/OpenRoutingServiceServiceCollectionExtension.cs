﻿using System;
using Microsoft.Extensions.DependencyInjection;
using RoutingAdapter.RoutingServices.OpenRouteService.Client;

namespace RoutingAdapter.RoutingServices.OpenRouteService
{
    public static class OpenRoutingServiceServiceCollectionExtension
    {
        public static IServiceCollection AddOpenRoutingService(this IServiceCollection serviceCollection,
            string connectionString)
        {
            // Add ORS client
            serviceCollection.AddHttpClient(OrsClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IOrsClient, OrsClient>();
            // Add ORS adapter to routing services
            serviceCollection.AddScoped<IRoutingService, OpenRouteServiceRoutingServiceAdapter>();
            return serviceCollection;
        }
    }
}
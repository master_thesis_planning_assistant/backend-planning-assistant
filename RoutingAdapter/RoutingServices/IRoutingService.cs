﻿using System.Collections.Generic;
using System.Threading.Tasks;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;

namespace RoutingAdapter.RoutingServices
{
    public interface IRoutingService
    {
        /// <summary>
        /// Name of the routing service
        /// </summary>
        public string ServiceName { get; }

        /// <summary>
        /// Supported modalities of the routing service
        /// </summary>
        public ICollection<RoutingMode> SupportedModalities { get; }

        /// <summary>
        /// Requests routes for the mobilityDemands from the routing service respecting the preferences.
        /// </summary>
        /// <param name="mobilityDemands">Mobility demands to get routes for</param>
        /// <param name="preferences">Preferences to respect building the routes</param>
        /// <returns>Routes for the given mobility demands</returns>
        public Task<ICollection<RoutesForActivity>> MakeRoutingRequestAsync(ICollection<MobilityDemand> mobilityDemands,
            ICollection<Preferences> preferences);

        /// <summary>
        /// Updates the specific xslt mapping of the routing service
        /// </summary>
        /// <param name="mapping">New xslt of the mapping</param>
        public Task UpdateMappingAsync(string mapping);

        /// <summary>
        /// Gets the specific xslt of the routing service
        /// </summary>
        /// <returns>Specific xslt of the routing service</returns>
        public string GetMapping();

        /// <summary>
        /// Gets the default xslt of the routing service
        /// </summary>
        /// <returns>Specific xslt of the routing service</returns>
        public string GetDefaultMapping();
    }
}
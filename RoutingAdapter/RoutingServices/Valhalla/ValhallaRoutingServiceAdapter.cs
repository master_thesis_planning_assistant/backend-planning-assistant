﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;
using RoutingAdapter.RoutingServices.Valhalla.Client;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;
using RoutingAdapter.RoutingServices.Valhalla.Model.Response;
using RoutingAdapter.RoutingServices.Valhalla.Model.Response.Enums;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter.RoutingServices.Valhalla
{
    /// <summary>
    /// Adapter for the Valhalla Routing-Service.
    /// Valhalla: https://github.com/valhalla/valhalla
    /// Model: https://valhalla.readthedocs.io/en/latest/api/turn-by-turn/api-reference/#locations
    /// </summary>
    public class ValhallaRoutingServiceAdapter : BaseRoutingService<RoutingRequest, RoutingResponse>
    {
        private readonly IValhallaClient _client;
        private readonly ILogger<ValhallaRoutingServiceAdapter> _logger;

        public ValhallaRoutingServiceAdapter(IValhallaClient client, ILogger<ValhallaRoutingServiceAdapter> logger,
            IXsltTransformerService xsltTransformer) :
            base(logger, xsltTransformer)
        {
            _client = client;
            _logger = logger;
        }

        public override string ServiceName => "Valhalla";

        public override ICollection<RoutingMode> SupportedModalities => new List<RoutingMode>
        {
            RoutingMode.Bike,
            // RoutingMode.BikeSharing, //BETA -> Not available!
            RoutingMode.Car,
            RoutingMode.Walk,
            // RoutingMode.PublicTransport //BETA -> Not available!
        };

        protected override Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeSpecificRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            return _client.MakeRoutingRequestAsync(requestWrapper);
        }

        protected override ICollection<SingleRoutingRequest> HandleModalityCardinality(
            SingleRoutingRequest singleRoutingRequest, ICollection<RoutingMode> modalities)
        {
            // Valhalla can not handle multiple modalities in one request.
            // See RoutingRequest > JsonParameters > CostingModel
            return modalities.Select(modality => new SingleRoutingRequest
            {
                Preferences = singleRoutingRequest.Preferences,
                MobilityDemand = singleRoutingRequest.MobilityDemand,
                Modalities = new List<RoutingMode> {modality}
            }).ToList();
        }

        protected override RoutesForActivity BuildResultingRoutes(
            SpecificRoutingResponseWrapper<RoutingResponse> specificRoutingResponseWrapper,
            SpecificRoutingRequestWrapper<RoutingRequest> specificRoutingRequestWrapper,
            MobilityDemand originalMobilityDemand)
        {
            var result = new RoutesForActivity
            {
                ActivityGuid = originalMobilityDemand.ActivityGuid,
                Routes = new List<ActivityRoute>(),
            };
            var trip = specificRoutingResponseWrapper.RoutingResponse.Trip;
            // factors for kilometers and miles
            var lengthFactor = trip.Units == Unit.Kilometers ? 1000 : 1609.34;
            var route = new ActivityRoute
            {
                Guid = Guid.NewGuid().ToString(),
                ServiceName = ServiceName,
                Start = originalMobilityDemand.From,
                Departure = originalMobilityDemand.At -
                            TimeSpan.FromSeconds(trip.Summary.Time),
                Destination = originalMobilityDemand.To,
                Arrival = originalMobilityDemand.At,
                Bounds = new Bounds
                {
                    Southwest = new GeoLocation
                    {
                        Longitude = trip.Summary.MinLon,
                        Latitude = trip.Summary.MinLat
                    },
                    Northeast = new GeoLocation
                    {
                        Longitude = trip.Summary.MaxLon,
                        Latitude = trip.Summary.MaxLat,
                    },
                },
                Duration = trip.Summary.Time,
                Distance = trip.Summary.Length * lengthFactor,
                Legs = new List<ActivityRouteLeg>()
            };

            var lastArrival = route.Departure;
            foreach (var leg in trip.Legs)
            {
                var polylinePoints = Polyline.DecodeEncodedPolyline(leg.Shape, 6).ToList();
                var newLeg = new ActivityRouteLeg
                {
                    Modality = GetModeEquivalent(leg.Maneuvers.First().TravelMode),
                    StartLocation = new NamedGeoLocation
                    {
                        GeoLocation = polylinePoints.First()
                    },
                    Departure = lastArrival,
                    EndLocation = new NamedGeoLocation
                    {
                        GeoLocation = polylinePoints.Last()
                    },
                    Arrival = lastArrival + TimeSpan.FromSeconds(leg.Summary.Time),
                    Polyline = new Polyline
                    {
                        Points = polylinePoints
                    },
                    Duration = leg.Summary.Time,
                    Distance = leg.Summary.Length * lengthFactor,
                    Steps = new List<ActivityRouteStep>()
                };
                lastArrival = newLeg.Arrival;

                foreach (var maneuver in leg.Maneuvers)
                {
                    var newStep = new ActivityRouteStep
                    {
                        StartLocation = new NamedGeoLocation
                        {
                            Name = maneuver.StreetNames?.FirstOrDefault(),
                            GeoLocation = newLeg.Polyline.GetPointAt(maneuver.BeginShapeIndex),
                        },
                        EndLocation = new NamedGeoLocation
                        {
                            Name = maneuver.StreetNames?.LastOrDefault(),
                            GeoLocation = newLeg.Polyline.GetPointAt(maneuver.EndShapeIndex),
                        },
                        Polyline = newLeg.Polyline.GetSegment(maneuver.BeginShapeIndex, maneuver.EndShapeIndex),
                        Duration = maneuver.Time,
                        Distance = maneuver.Length * lengthFactor,
                        Action = maneuver.Instruction
                    };
                    newLeg.Steps.Add(newStep);
                }

                route.Legs.Add(newLeg);
            }

            route.Polyline = new Polyline
            {
                Points = route.Legs.Aggregate(new List<GeoLocation>(),
                    (points, leg) => (points.Concat(leg.Polyline.Points)).ToList()).ToList(),
            };
            result.Routes.Add(route);
            return result;
        }

        private RoutingMode GetModeEquivalent(TravelMode specificMode)
        {
            return specificMode switch
            {
                TravelMode.Drive => RoutingMode.Car,
                TravelMode.Pedestrian => RoutingMode.Walk,
                TravelMode.Bicycle => RoutingMode.Bike,
                TravelMode.Transit => RoutingMode.PublicTransport,
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to TravelMode {specificMode.GetStringValue()} found")
            };
        }
    }
}
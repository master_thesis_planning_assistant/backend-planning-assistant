﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request;
using RoutingAdapter.RoutingServices.Valhalla.Model.Response;

namespace RoutingAdapter.RoutingServices.Valhalla.Client
{
    public class ValhallaClient : IValhallaClient
    {
        public const string HttpClientName = "Valhalla";
        private readonly IHttpClientFactory _httpClientFactory;

        public ValhallaClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }


        public async Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);


            var url = $"{client.BaseAddress}route";

            var request = requestWrapper.RoutingRequest;

            var json = JsonConvert.SerializeObject(request.JsonParameters, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                DateFormatString = "yyyy-MM-ddThh:mm"
            });

            var urlParams = new Dictionary<string, string> {{"json", json}};

            var response = await client.GetAsync(new Uri(QueryHelpers.AddQueryString(url, urlParams)));
            var responseBody = await response.Content.ReadAsStringAsync();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception(
                    $"Got status code {response.StatusCode} from {HttpClientName}-Request! Message: {responseBody}");
            }


            var routingResponse = JsonConvert.DeserializeObject<RoutingResponse>(responseBody);
            return new SpecificRoutingResponseWrapper<RoutingResponse>
            {
                ActivityGuid = requestWrapper.ActivityGuid,
                RoutingResponse = routingResponse,
            };
        }
    }
}
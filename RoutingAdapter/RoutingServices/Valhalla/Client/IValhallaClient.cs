﻿using System.Threading.Tasks;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request;
using RoutingAdapter.RoutingServices.Valhalla.Model.Response;

namespace RoutingAdapter.RoutingServices.Valhalla.Client
{
    public interface IValhallaClient
    {
        Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper);
    }
}
﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class TransitInfo
    {
        /// <summary>
        /// Global transit route identifier from Transitland.
        /// </summary>
        [JsonProperty("onestop_id")]
        public string OnestopId { get; set; }

        /// <summary>
        /// Short name describing the transit route. For example "N".
        /// </summary>
        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        /// <summary>
        /// Long name describing the transit route. For example "Broadway Express".
        /// </summary>
        [JsonProperty("long_name")]
        public string LongName { get; set; }


        /// <summary>
        /// The sign on a public transport vehicle that identifies the route destination to passengers.
        /// For example "ASTORIA - DITMARS BLVD".
        /// </summary>
        [JsonProperty("headsign")]
        public string Headsign { get; set; }

        /// <summary>
        /// The numeric color value associated with a transit route. The value for yellow would be "16567306".
        /// </summary>
        [JsonProperty("color")]
        public string Color { get; set; }

        /// <summary>
        /// The numeric text color value associated with a transit route. The value for black would be "0".
        /// </summary>
        [JsonProperty("text_color")]
        public string TextColor { get; set; }

        /// <summary>
        /// The description of the the transit route. For example "Trains operate from Ditmars Boulevard, Queens,
        /// to Stillwell Avenue, Brooklyn, at all times. N trains in Manhattan operate along Broadway and across the
        /// Manhattan Bridge to and from Brooklyn. Trains in Brooklyn operate along 4th Avenue, then through Borough
        /// Park to Gravesend. Trains typically operate local in Queens, and either express or local in Manhattan and
        /// Brooklyn, depending on the time. Late night trains operate via Whitehall Street, Manhattan.
        /// Late night service is local".
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Global operator/agency identifier from Transitland.
        /// </summary>
        [JsonProperty("operator_onestop_id")]
        public string OperatorOnestopId { get; set; }


        /// <summary>
        /// Operator/agency name. For example, "BART", "King County Marine Division", and so on. Short name is used over long name.
        /// </summary>
        [JsonProperty("operator_name")]
        public string OperatorName { get; set; }


        /// <summary>
        /// Operator/agency URL. For example, "http://web.mta.info/".
        /// </summary>
        [JsonProperty("operator_url")]
        public string OperatorUrl { get; set; }

        /// <summary>
        /// A list of the stops/stations associated with a specific transit route. See below for details.
        /// </summary>
        [JsonProperty("transit_stops")]
        public ICollection<TransitStop> TransitStops { get; set; }
    }
}
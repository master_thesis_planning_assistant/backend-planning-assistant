﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class Summary
    {
        /// <summary>
        /// Estimated elapsed time to complete the trip.
        /// </summary>
        [JsonProperty("time")]
        public double Time { get; set; }

        /// <summary>
        /// Distance traveled for the entire trip. Units are either miles or kilometers based on the input units specified.
        /// </summary>
        [JsonProperty("length")]
        public double Length { get; set; }

        /// <summary>
        /// Minimum latitude of a bounding box containing the route.
        /// </summary>
        [JsonProperty("min_lat")]
        public double MinLat { get; set; }

        /// <summary>
        /// Minimum longitude of a bounding box containing the route.
        /// </summary>
        [JsonProperty("min_lon")]
        public double MinLon { get; set; }

        /// <summary>
        /// Maximum latitude of a bounding box containing the route.
        /// </summary>
        [JsonProperty("max_lat")]
        public double MaxLat { get; set; }

        /// <summary>
        /// Maximum longitude of a bounding box containing the route.
        /// </summary>
        [JsonProperty("max_lon")]
        public double MaxLon { get; set; }
    }
}
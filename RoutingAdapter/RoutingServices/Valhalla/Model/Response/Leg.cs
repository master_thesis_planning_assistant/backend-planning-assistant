﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class Leg
    {
        /// <summary>
        /// Encoded polyline of the route path (with 6 digits decimal precision),
        /// </summary>
        [JsonProperty("shape")]
        public string Shape { get; set; }

        /// <summary>
        /// Same information as a trip summary but applied to the single leg of the trip. 
        /// </summary>
        [JsonProperty("summary")]
        public Summary Summary { get; set; }

        /// <summary>
        /// List of maneuvers
        /// </summary>
        [JsonProperty("maneuvers")]
        public ICollection<Maneuver> Maneuvers { get; set; }
    }
}
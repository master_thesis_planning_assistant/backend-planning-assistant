﻿using System.Collections.Generic;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class Trip
    {
        /// <summary>
        /// Status code.
        /// </summary>
        [JsonProperty("status")]
        public int Status { get; set; }

        /// <summary>
        /// Status message.
        /// </summary>
        [JsonProperty("status_message")]
        public string StatusMessage { get; set; }

        /// <summary>
        /// The specified units of length are returned, either kilometers or miles.
        /// </summary>
        [JsonProperty("units")]
        public Unit Units { get; set; }

        /// <summary>
        /// The language of the narration instructions. If the user specified a language in the directions options and
        /// the specified language was supported - this returned value will be equal to the specified value. Otherwise,
        /// this value will be the default (en-US) language.
        /// </summary>
        [JsonProperty("language")]
        public string Language { get; set; }

        /// <summary>
        /// Location information is returned in the same form as it is entered with additional fields to indicate the
        /// side of the street.
        /// </summary>
        [JsonProperty("locations")]
        public ICollection<Location> Locations { get; set; }

        /// <summary>
        /// Summary of the Trip
        /// </summary>
        [JsonProperty("summary")]
        public Summary Summary { get; set; }

        /// <summary>
        /// A trip contains one or more legs. For n number of break locations, there are n-1 legs. Through locations
        /// do not create separate legs.
        /// </summary>
        [JsonProperty("legs")]
        public ICollection<Leg> Legs { get; set; }
    }
}
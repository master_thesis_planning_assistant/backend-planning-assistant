﻿using System;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class TransitStop
    {
        /// <summary>
        /// Type of stop (simple stop=0; station=1).
        /// </summary>
        [JsonProperty("type")]
        public TransitStopType Type { get; set; }

        /// <summary>
        /// Global transit stop identifier from Transitland.
        /// </summary>
        [JsonProperty("onestop_id")]
        public string OnestopId { get; set; }

        /// <summary>
        /// Name of the stop or station. For example "14 St - Union Sq".
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Arrival date and time using the ISO 8601 format (YYYY-MM-DDThh:mm). For example, "2015-12-29T08:06".
        /// </summary>
        [JsonProperty("arrival_date_time")]
        public DateTime ArrivalDateTime { get; set; }

        /// <summary>
        /// Departure date and time using the ISO 8601 format (YYYY-MM-DDThh:mm). For example, "2015-12-29T08:06".
        /// </summary>
        [JsonProperty("departure_date_time")]
        public DateTime DepatureDateTime { get; set; }

        /// <summary>
        /// True if this stop is a marked as a parent stop.
        /// </summary>
        [JsonProperty("is_parent_stop")]
        public bool IsParentStop { get; set; }

        /// <summary>
        /// True if the times are based on an assumed schedule because the actual schedule is not known.
        /// </summary>
        [JsonProperty("assumed_schedule")]
        public bool AssumedSchedule { get; set; }

        /// <summary>
        /// Latitude of the transit stop in degrees.
        /// </summary>
        [JsonProperty("lat")]
        public double Lat { get; set; }

        /// <summary>
        /// Longitude of the transit stop in degrees.
        /// </summary>
        [JsonProperty("lon")]
        public double Lon { get; set; }
    }
}
﻿namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public enum TransitStopType
    {
        SimpleStop = 0,
        Station = 1
    }
}
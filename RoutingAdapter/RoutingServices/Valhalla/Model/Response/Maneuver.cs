﻿using System.Collections.Generic;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class Maneuver
    {
        /// <summary>
        /// Type of maneuver.
        /// </summary>
        [JsonProperty("type")]
        public ManeuverType Type { get; set; }

        /// <summary>
        /// Written maneuver instruction. Describes the maneuver, such as "Turn right onto Main Street".
        /// </summary>
        [JsonProperty("instruction")]
        public string Instruction { get; set; }

        /// <summary>
        /// Text suitable for use as a verbal alert in a navigation application. The transition alert instruction will
        /// prepare the user for the forthcoming transition. For example: "Turn right onto North Prince Street".
        /// </summary>
        [JsonProperty("verbal_transition_alert_instruction")]
        public string VerbalTransitionAlertInstruction { get; set; }

        /// <summary>
        /// Text suitable for use as a verbal message immediately prior to the maneuver transition.
        /// For example "Turn right onto North Prince Street, U.S. 2 22".
        /// </summary>
        [JsonProperty("verbal_pre_transition_instruction")]
        public string VerbalPreTransitionInstruction { get; set; }

        /// <summary>
        /// Text suitable for use as a verbal message immediately after the maneuver transition.
        /// For example "Continue on U.S. 2 22 for 3.9 miles".
        /// </summary>
        [JsonProperty("verbal_post_transition_instruction")]
        public string VerbalPostTransitionInstruction { get; set; }

        /// <summary>
        /// List of street names that are consistent along the entire maneuver.
        /// </summary>
        [JsonProperty("street_names")]
        public ICollection<string> StreetNames { get; set; }

        /// <summary>
        /// When present, these are the street names at the beginning of the maneuver
        /// (if they are different than the names that are consistent along the entire maneuver).
        /// </summary>
        [JsonProperty("begin_street_names")]
        public ICollection<string> BeginStreetNames { get; set; }

        /// <summary>
        /// Estimated time along the maneuver in seconds.
        /// </summary>
        [JsonProperty("time")]
        public int Time { get; set; }

        /// <summary>
        /// Maneuver length in the units specified.
        /// </summary>
        [JsonProperty("length")]
        public double Length { get; set; }

        /// <summary>
        /// Index into the list of shape points for the start of the maneuver.
        /// </summary>
        [JsonProperty("begin_shape_index")]
        public int BeginShapeIndex { get; set; }

        /// <summary>
        /// Index into the list of shape points for the end of the maneuver.
        /// </summary>
        [JsonProperty("end_shape_index")]
        public int EndShapeIndex { get; set; }

        /// <summary>
        /// True if the maneuver has any toll, or portions of the maneuver are subject to a toll.
        /// </summary>
        [JsonProperty("toll")]
        public bool? Toll { get; set; }

        /// <summary>
        /// True if the maneuver is unpaved or rough pavement, or has any portions that have rough pavement.
        /// </summary>
        [JsonProperty("rough")]
        public bool? Rough { get; set; }

        /// <summary>
        /// True if a gate is encountered on this maneuver.
        /// </summary>
        [JsonProperty("gate")]
        public bool? Gate { get; set; }

        /// <summary>
        /// True if a ferry is encountered on this maneuver.
        /// </summary>
        [JsonProperty("ferry")]
        public bool? Ferry { get; set; }

        /// <summary>
        /// Contains the interchange guide information at a road junction associated with this maneuver. See below for details.
        /// </summary>
        [JsonProperty("sign")]
        public Sign Sign { get; set; }

        /// <summary>
        /// The spoke to exit roundabout after entering.
        /// </summary>
        [JsonProperty("roundabout_exit_count")]
        public int? RoundaboutExitCount { get; set; }

        /// <summary>
        /// Written depart time instruction. Typically used with a transit maneuver, such as "Depart: 8:04 AM from 8 St - NYU".
        /// </summary>
        [JsonProperty("depart_instruction")]
        public string DepartInstruction { get; set; }

        /// <summary>
        /// Text suitable for use as a verbal depart time instruction.
        /// Typically used with a transit maneuver, such as "Depart at 8:04 AM from 8 St - NYU".
        /// </summary>
        [JsonProperty("verbal_depart_instruction")]
        public string VerbalDepartInstruction { get; set; }

        /// <summary>
        /// Written arrive time instruction. Typically used with a transit maneuver, such as "Arrive: 8:10 AM at 34 St - Herald Sq".
        /// </summary>
        [JsonProperty("arrive_instruction")]
        public string ArriveInstruction { get; set; }


        /// <summary>
        /// Text suitable for use as a verbal arrive time instruction. T
        /// ypically used with a transit maneuver, such as "Arrive at 8:10 AM at 34 St - Herald Sq".
        /// </summary>
        [JsonProperty("verbal_arrive_instruction")]
        public string VerbalArriveInstruction { get; set; }


        /// <summary>
        /// Contains the attributes that describe a specific transit route. See below for details.
        /// </summary>
        [JsonProperty("transit_info")]
        public TransitInfo TransitInfo { get; set; }

        /// <summary>
        /// True if the verbal_pre_transition_instruction has been appended with the verbal instruction of the next maneuver.
        /// </summary>
        [JsonProperty("verbal_multi_cue")]
        public bool? VerbalMultiCue { get; set; }

        /// <summary>
        /// Travel mode.
        /// </summary>
        [JsonProperty("travel_mode")]
        public TravelMode TravelMode { get; set; }

        /// <summary>
        /// Type of Travel (eg. "car" for TravelMode "drive"
        /// </summary>
        [JsonProperty("travel_type")]
        public TravelType TravelType { get; set; }

        /// <summary>
        /// Used when travel_mode is bikeshare. Describes bike share maneuver. The default value is "NoneAction
        /// </summary>
        [JsonProperty("bss_maneuver_type")]
        public BssManeuverType? BssManeuverType { get; set; }
    }
}
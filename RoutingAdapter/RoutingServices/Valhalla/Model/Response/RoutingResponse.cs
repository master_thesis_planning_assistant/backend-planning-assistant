﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class RoutingResponse
    {
        /// <summary>
        /// ID given in the Request
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// The route results are returned as a trip. This is a JSON object that contains details about the trip, including
        /// locations, a summary with basic information about the entire trip, and a list of legs.
        /// </summary>
        [JsonProperty("trip")]
        public Trip Trip { get; set; }
    }
}
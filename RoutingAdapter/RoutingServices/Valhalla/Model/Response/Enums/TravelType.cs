﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TravelType
    {
        //	Travel type for drive.
        [EnumMember(Value = "car")] Car,

        //Travel type for pedestrian.
        [EnumMember(Value = "foot")] Foot,

        //Travel type for bicycle.
        [EnumMember(Value = "road")] Road,

        //Travel type for transit.
        /// <summary>
        /// Tram or light rail
        /// </summary>
        [EnumMember(Value = "tram")] Tram,

        /// <summary>
        /// Metro or subway
        /// </summary>
        [EnumMember(Value = "metro")] Metro,
        [EnumMember(Value = "rail")] Rail,
        [EnumMember(Value = "bus")] Bus,
        [EnumMember(Value = "ferry")] Ferry,
        [EnumMember(Value = "cable_car")] CableCar,
        [EnumMember(Value = "gondola")] Gondola,
        [EnumMember(Value = "funicular")] Funicular,
        [EnumMember(Value = "hybrid")] Hybrid,
    }
}
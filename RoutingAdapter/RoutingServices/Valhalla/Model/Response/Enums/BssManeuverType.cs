﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BssManeuverType
    {
        [EnumMember(Value = "NoneAction")] NoneAction,

        [EnumMember(Value = "RentBikeAtBikeShare")]
        RentBikeAtBikeShare,

        [EnumMember(Value = "ReturnBikeAtBikeShare")]
        ReturnBikeAtBikeShare,
    }
}
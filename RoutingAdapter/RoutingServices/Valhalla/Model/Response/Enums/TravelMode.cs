﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum TravelMode
    {
        [EnumMember(Value = "drive")] Drive,
        [EnumMember(Value = "pedestrian")] Pedestrian,
        [EnumMember(Value = "bicycle")] Bicycle,
        [EnumMember(Value = "transit")] Transit,
    }
}
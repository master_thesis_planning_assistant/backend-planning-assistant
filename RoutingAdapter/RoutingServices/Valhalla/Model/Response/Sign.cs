﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class Sign
    {
        /// <summary>
        ///  list of exit number elements. If an exit number element exists, it is typically just one value.
        /// </summary>
        [JsonProperty("exit_number_elements")]
        public ICollection<SignElement> ExitNumberElements { get; set; }

        /// <summary>
        /// list of exit branch elements. The exit branch element text is the subsequent road name or route number after the sign.
        /// </summary>
        [JsonProperty("exit_branch_elements")]
        public ICollection<SignElement> ExitBranchElements { get; set; }

        /// <summary>
        /// list of exit toward elements. The exit toward element text is the location where the road ahead goes
        /// - the location is typically a control city, but may also be a future road name or route number.
        /// </summary>
        [JsonProperty("exit_toward_elements")]
        public ICollection<SignElement> ExitTowardElements { get; set; }

        /// <summary>
        /// list of exit name elements. The exit name element is the interchange identifier - typically not used in the US.
        /// </summary>
        [JsonProperty("exit_name_elements")]
        public ICollection<SignElement> ExitNameElements { get; set; }
    }
}
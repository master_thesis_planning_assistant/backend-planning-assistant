﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Response
{
    public class SignElement
    {
        /// <summary>
        /// Interchange sign text.
        ///     exit number example: 91B.
        ///     exit branch example: I 95 North.
        ///     exit toward example: New York.
        ///     exit name example: Gettysburg Pike.
        /// </summary>
        [JsonProperty("text")]
        public string Text { get; set; }

        /// <summary>
        /// The frequency of this sign element within a set a consecutive signs. This item is optional.
        /// </summary>
        [JsonProperty("consecutive_count")]
        public int ConsecutiveCount { get; set; }
    }
}
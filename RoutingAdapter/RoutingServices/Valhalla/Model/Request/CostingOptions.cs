﻿using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class CostingOptions
    {
        [JsonProperty("auto")] public AutomobileAndBusCostingOptions Auto { get; set; }

        [JsonProperty("Truck")] public TruckCostingOptions Truck { get; set; }

        [JsonProperty("motor_scooter")] public MotorScooterCostingOptions MotorScooter { get; set; }

        [JsonProperty("motorcycle")] public MotorCycleCostingOptions Motorcycle { get; set; }

        [JsonProperty("bicycle")] public BicycleCostingOptions Bicycle { get; set; }

        [JsonProperty("pedestrian")] public PedestrianCostingOptions Pedestrian { get; set; }

        [JsonProperty("transit")] public TransitCostingOptions Transit { get; set; }
    }
}
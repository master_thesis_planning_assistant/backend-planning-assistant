﻿using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class Location
    {
        /// <summary>
        /// Latitude of the location in degrees. This is assumed to be both the routing location and the display location
        /// if no display_lat and display_lon are provided.
        /// </summary>
        [JsonProperty("lat")]
        public double Lat { get; set; }

        /// <summary>
        /// Longitude of the location in degrees. This is assumed to be both the routing location and the display location
        /// if no display_lat and display_lon are provided.
        /// </summary>
        [JsonProperty("lon")]
        public double Lon { get; set; }

        /// <summary>
        /// Type of location, either break, through, via or break_through. Each type controls two characteristics:
        /// whether or not to allow a u-turn at the location and whether or not to generate guidance/legs at the location.
        /// </summary>
        [JsonProperty("type")]
        public LocationType Type { get; set; }

        /// <summary>
        /// (optional) Preferred direction of travel for the start from the location. This can be useful for mobile routing where
        /// a vehicle is traveling in a specific direction along a road, and the route should start in that direction.
        /// The heading is indicated in degrees from north in a clockwise direction, where north is 0°, east is 90°,
        /// south is 180°, and west is 270°.
        /// </summary>
        [JsonProperty("heading")]
        public int? Heading { get; set; }

        /// <summary>
        /// (optional) How close in degrees a given street's angle must be in order for it to be considered as in
        /// the same direction of the heading parameter. The default value is 60 degrees.
        /// </summary>
        [JsonProperty("heading_tolerance")]
        public int? HeadingTolerance { get; set; }

        /// <summary>
        /// (optional) Street name. The street name may be used to assist finding the correct routing location at
        /// the specified latitude, longitude. This is not currently implemented.
        /// </summary>
        [JsonProperty("street")]
        public string Street { get; set; }

        /// <summary>
        /// (optional) OpenStreetMap identification number for a polyline way. The way ID may be used to assist finding
        /// the correct routing location at the specified latitude, longitude. This is not currently implemented.
        /// </summary>
        [JsonProperty("way_id")]
        public int? WayId { get; set; }

        /// <summary>
        /// Minimum number of nodes (intersections) reachable for a given edge (road between intersections) to consider
        /// that edge as belonging to a connected region. When correlating this location to the route network, try to
        /// find candidates who are reachable from this many or more nodes (intersections). If a given candidate edge
        /// reaches less than this number of nodes its considered to be a disconnected island and we'll search for more
        /// candidates until we find at least one that isn't considered a disconnected island. If this value is larger
        /// than the configured service limit it will be clamped to that limit. The default is a minimum of 50 reachable nodes.
        /// </summary>
        [JsonProperty("minimum_reachability")]
        public int MinimumReachability { get; set; } = 50;

        /// <summary>
        /// The number of meters about this input location within which edges (roads between intersections) will be
        /// considered as candidates for said location. When correlating this location to the route network, try to
        /// only return results within this distance (meters) from this location. If there are no candidates within this
        /// distance it will return the closest candidate within reason. If this value is larger than the configured
        /// service limit it will be clamped to that limit. The default is 0 meters.
        /// </summary>
        [JsonProperty("radius")]
        public int Radius { get; set; } = 0;

        /// <summary>
        /// Whether or not to rank the edge candidates for this location. The ranking is used as a penalty within the
        /// routing algorithm so that some edges will be penalized more heavily than others. If true candidates will be
        /// ranked according to their distance from the input and various other attributes. If false the candidates will
        /// all be treated as equal which should lead to routes that are just the most optimal path with emphasis about
        /// which edges were selected.
        /// </summary>
        [JsonProperty("rank_candidates")]
        public bool? RankCandidates { get; set; }

        /// <summary>
        /// If the location is not offset from the road centerline or is closest to an intersection this option has
        /// no effect. Otherwise the determined side of street is used to determine whether or not the location should be
        /// visited from the same, opposite or either side of the road with respect to the side of the road the given
        /// locale drives on. In Germany (driving on the right side of the road), passing a value of same will only
        /// allow you to leave from or arrive at a location such that the location will be on your right. In Australia
        /// (driving on the left side of the road), passing a value of same will force the location to be on your left.
        /// A value of opposite will enforce arriving/departing from a location on the opposite side of the road from
        /// that which you would be driving on while a value of either will make no attempt limit the side of street
        /// that is available for the route.
        /// </summary>
        [JsonProperty("preferred_side")]
        public PreferredSide? PreferredSide { get; set; }

        /// <summary>
        /// Latitude of the map location in degrees. If provided the lat and lon parameters will be treated as the
        /// routing location and the display_lat and display_lon will be used to determine the side of street.
        /// Both display_lat and display_lon must be provided and valid to achieve the desired effect.
        /// </summary>
        [JsonProperty("display_lat")]
        public double? DisplayLat { get; set; }

        /// <summary>
        /// Longitude of the map location in degrees. If provided the lat and lon parameters will be treated as the
        /// routing location and the display_lat and display_lon will be used to determine the side of street.
        /// Both display_lat and display_lon must be provided and valid to achieve the desired effect.
        /// </summary>
        [JsonProperty("display_lon")]
        public double? DisplayLon { get; set; }

        /// <summary>
        /// The cutoff at which we will assume the input is too far away from civilisation to be worth correlating
        /// to the nearest graph elements
        /// </summary>
        [JsonProperty("search_cutoff")]
        public int? SearchCutoff { get; set; }

        /// <summary>
        /// During edge correlation this is the tolerance used to determine whether or not to snap to the intersection
        /// rather than along the street, if the snap location is within this distance from the intersection the
        /// intersection is used instead. The default is 5 meters
        /// </summary>
        [JsonProperty("node_snap_tolerance")]
        public int NodeSnapTolerance { get; set; } = 5;

        /// <summary>
        /// If your input coordinate is less than this tolerance away from the edge centerline then we set your side
        /// of street to none otherwise your side of street will be left or right depending on direction of travel
        /// </summary>
        [JsonProperty("street_side_tolerance")]
        public double? StreetSideTolerance { get; set; }

        /// <summary>
        /// The max distance in meters that the input coordinates or display ll can be from the edge centerline for
        /// them to be used for determining the side of street. Beyond this distance the side of street is set to none
        /// </summary>
        [JsonProperty("street_side_max_distance")]
        public double? StreetSideMaxDistance { get; set; }

        /// <summary>
        /// A set of optional filters to exclude candidate edges based on their attribution. 
        /// </summary>
        [JsonProperty("search_filter")]
        public SearchFilter SearchFilter { get; set; }
    }
}
﻿namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class RoutingRequest
    {
        public Parameters JsonParameters { get; set; }
    }
}
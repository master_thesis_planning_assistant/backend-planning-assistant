﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// In addition to the AutomobileAndBusCostingOptions, the following options are available for truck costing.
    /// </summary>
    public class TruckCostingOptions : AutomobileAndBusCostingOptions
    {
        /// <summary>
        /// The height of the truck (in meters).
        /// </summary>
        [JsonProperty("height")]
        public double? Height { get; set; }

        /// <summary>
        /// The width of the truck (in meters).
        /// </summary>
        [JsonProperty("width")]
        public double? Width { get; set; }

        /// <summary>
        /// The length of the truck (in meters).
        /// </summary>
        [JsonProperty("length")]
        public double? Length { get; set; }

        /// <summary>
        /// The weight of the truck (in metric tons).
        /// </summary>
        [JsonProperty("weight")]
        public double? Weight { get; set; }

        /// <summary>
        /// The axle load of the truck (in metric tons).
        /// </summary>
        [JsonProperty("axle_load")]
        public double? AxleLoad { get; set; }

        /// <summary>
        /// A value indicating if the truck is carrying hazardous materials.
        /// </summary>
        [JsonProperty("hazmat")]
        public double? Hazmat { get; set; }

        // /// <summary>
        // /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        // /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        // /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        // /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        // /// avoid living streets entirely.
        // /// </summary>
        // [JsonProperty("use_living_streets")]
        // public new double UseLivingStreets { get; set; } = 0;
        //
        // /// <summary>
        // /// A penalty applied for transition to generic service road.
        // /// The default penalty is 0 trucks and 15 for cars, buses, motor scooters and motorcycles.
        // /// </summary>
        // [JsonProperty("service_penalty")]
        // public new double ServicePenalty { get; set; } = 0;
    }
}
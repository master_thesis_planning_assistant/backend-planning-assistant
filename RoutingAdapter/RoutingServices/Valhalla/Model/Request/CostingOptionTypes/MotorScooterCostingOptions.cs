﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// Standard costing for travel by motor scooter or moped. By default, motor_scooter costing will avoid higher class
    /// roads unless the country overrides allows motor scooters on these roads. Motor scooter routes follow regular
    /// roads when needed, but avoid roads without motor_scooter, moped, or mofa access. The costing model recognizes
    /// factors unique to motor_scooter travel and offers options for tuning motor_scooter routes. Factors unique to
    /// travel by motor_scooter influence the resulting route.
    /// </summary>
    public class MotorScooterCostingOptions : AutomobileAndBusCostingOptions
    {
        // /// <summary>
        // /// This value indicates the willingness to take track roads. This is a range of values between 0 and 1.
        // /// Values near 0 attempt to avoid tracks and values near 1 will favor tracks a little bit.
        // /// The default value is 0 for autos, 0.5 for motor scooters and motorcycles. Note that sometimes tracks are
        // /// required to complete a route so values of 0 are not guaranteed to avoid tracks entirely.
        // /// </summary>
        // [JsonProperty("use_tracks")]
        // public new double UseTracks { get; set; } = 0.5;
        //
        // /// <summary>
        // /// A penalty applied for transition to generic service road.
        // /// The default penalty is 0 trucks and 15 for cars, buses, motor scooters and motorcycles.
        // /// </summary>
        // [JsonProperty("service_penalty")]
        // public new double ServicePenalty { get; set; } = 15;
        //
        // /// <summary>
        // /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        // /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        // /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        // /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        // /// avoid living streets entirely.
        // /// </summary>
        // [JsonProperty("use_living_streets")]
        // public new double UseLivingStreets { get; set; } = 0.1;

        /// <summary>
        /// A cyclist's desire to tackle hills in their routes. This is a range of values from 0 to 1, where 0 attempts
        /// to avoid hills and steep grades even if it means a longer (time and distance) path, while 1 indicates the
        /// rider does not fear hills and steeper grades. Based on the use_hills factor, penalties are applied to roads
        /// based on elevation change and grade. These penalties help the path avoid hilly roads in favor of flatter
        /// roads or less steep grades where available. Note that it is not always possible to find alternate paths to
        /// avoid hills (for example when route locations are in mountainous areas). The default value is 0.5.
        /// </summary>
        [JsonProperty("use_hills")]
        public double UseHills { get; set; } = 0.5;

        /// <summary>
        /// A riders's propensity to use primary roads. This is a range of values from 0 to 1, where 0 attempts to
        /// avoid primary roads, and 1 indicates the rider is more comfortable riding on primary roads. Based on the
        /// use_primary factor, roads with certain classifications and higher speeds are penalized in an attempt to
        /// avoid them when finding the best path. The default value is 0.5.
        /// </summary>
        [JsonProperty("use_primary")]
        public double UsePrimary { get; set; } = 0.5;

        // /// <summary>
        // /// Top speed the motorized scooter can go. Used to avoid roads with higher speeds than this value.
        // /// For motor_scooter this value must be between 20 and 120 KPH. The default value is 45 KPH (~28 MPH)
        // /// </summary>
        // [JsonProperty("top_speed")]
        // public new double TopSpeed { get; set; } = 45;
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// BETA
    /// Standard costing for travel by motorcycle. By default, motorcycle costing will default to higher class roads.
    /// The costing model recognizes factors unique to motorcycle travel and offers options for tuning motorcycle routes.
    /// </summary>
    public class MotorCycleCostingOptions : AutomobileAndBusCostingOptions
    {
        /// <summary>
        /// This value indicates the willingness to take track roads. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid tracks and values near 1 will favor tracks a little bit.
        /// The default value is 0 for autos, 0.5 for motor scooters and motorcycles. Note that sometimes tracks are
        /// required to complete a route so values of 0 are not guaranteed to avoid tracks entirely.
        /// </summary>
        [JsonProperty("use_tracks")]
        public new double UseTracks { get; set; } = 0.5;

        /// <summary>
        /// A penalty applied for transition to generic service road.
        /// The default penalty is 0 trucks and 15 for cars, buses, motor scooters and motorcycles.
        /// </summary>
        [JsonProperty("service_penalty")]
        public new double ServicePenalty { get; set; } = 15;

        // /// <summary>
        // /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        // /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        // /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        // /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        // /// avoid living streets entirely.
        // /// </summary>
        // [JsonProperty("use_living_streets")]
        // public new double UseLivingStreets { get; set; } = 0.1;

        /// <summary>
        /// A riders's desire for adventure in their routes. This is a range of values from 0 to 1, where 0 will
        /// avoid trails, tracks, unclassified or bad surfaces and values towards 1 will tend to avoid major roads and
        /// route on secondary roads. The default value is 0.0.
        /// </summary>
        [JsonProperty("use_trails")]
        public double UseTrails { get; set; } = 0;
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    public abstract class VehicleBaseCostingOptions
    {
        /// <summary>
        /// A penalty applied when transitioning between roads that do not have consistent naming–in other words, no
        /// road names in common. This penalty can be used to create simpler routes that tend to have fewer maneuvers
        /// or narrative guidance instructions. The default maneuver penalty is five seconds.
        /// </summary>
        [JsonProperty("maneuver_penalty")]
        public double ManeuverPenalty { get; set; } = 5;

        /// <summary>
        /// A cost applied when a gate is encountered. This cost is added to the estimated time / elapsed time.
        /// The default gate cost is 30 seconds.
        /// </summary>
        [JsonProperty("gate_cost")]
        public double GateCost { get; set; } = 30;

        /// <summary>
        /// A cost applied when encountering an international border.
        /// This cost is added to the estimated and elapsed times. The default cost is 600 seconds.
        /// </summary>
        [JsonProperty("country_crossing_cost")]
        public double CountryCrossingCost { get; set; } = 600;

        /// <summary>
        /// A penalty applied for a country crossing. This penalty can be used to create paths that avoid spanning
        /// country boundaries. The default penalty is 0.
        /// </summary>
        [JsonProperty("country_crossing_penalty")]
        public double CountryCrossingPenalty { get; set; } = 0;

        /// <summary>
        /// Changes the metric to quasi-shortest, i.e. purely distance-based costing. Note, this will disable all other
        /// costings and penalties. Also note, shortest will not disable hierarchy pruning, leading to potentially
        /// sub-optimal routes for some costing models. The default is false.
        /// </summary>
        [JsonProperty("shortest")]
        public bool Shortest { get; set; } = false;

        /// <summary>
        /// This value indicates the willingness to take ferries. This is a range of values between 0 and 1. Values near
        /// 0 attempt to avoid ferries and values near 1 will favor ferries. The default value is 0.5. Note that sometimes
        /// ferries are required to complete a route so values of 0 are not guaranteed to avoid ferries entirely.
        /// </summary>
        [JsonProperty("use_ferry")]
        public double UserFerry { get; set; } = 0.5;
    }
}
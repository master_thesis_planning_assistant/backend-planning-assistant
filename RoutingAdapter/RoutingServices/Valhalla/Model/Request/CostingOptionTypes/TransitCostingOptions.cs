﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// These options are available for transit costing when the multimodal costing model is used.
    /// </summary>
    public class TransitCostingOptions
    {
        /// <summary>
        /// User's desire to use buses. Range of values from 0 (try to avoid buses) to 1 (strong preference for riding buses).
        /// </summary>
        [JsonProperty("use_bus")]
        public double? UseBus { get; set; }

        /// <summary>
        /// User's desire to use rail/subway/metro. Range of values from 0 (try to avoid rail) to 1 (strong preference for riding rail).
        /// </summary>
        [JsonProperty("use_rail")]
        public double? UseRail { get; set; }

        /// <summary>
        /// User's desire to use rail/subway/metro. Range of values from 0 (try to avoid rail) to 1 (strong preference for riding rail).
        /// </summary>
        [JsonProperty("use_transfers")]
        public double? UseTransfer { get; set; }

        /// <summary>
        /// A pedestrian option that can be added to the request to extend the defaults (2145 meters or approximately 1.5 miles).
        /// This is the maximum walking distance at the beginning or end of a route.
        /// </summary>
        [JsonProperty("transit_start_end_max_distance")]
        public double? TransitStartAndEndMaxDistance { get; set; }

        /// <summary>
        /// A pedestrian option that can be added to the request to extend the defaults (800 meters or 0.5 miles).
        /// This is the maximum walking distance between transfers.
        /// </summary>
        [JsonProperty("transit_transfer_max_distance")]
        public double? TransitTransferMaxDistance { get; set; }

        /// <summary>
        /// A way to filter for one or more stops, routes, or operators. Filters must contain a list of Onestop IDs,
        /// which is a unique identifier for Transitland data, and an action.
        /// </summary>
        [JsonProperty("filters")]
        public List<Filter> Filters { get; set; }
    }

    public class Filter
    {
        /// <summary>
        /// any number of Onestop IDs (such as o-9q9-bart)
        /// </summary>
        [JsonProperty("ids")]
        public string Ids { get; set; }

        /// <summary>
        /// either exclude to exclude all of the ids listed in the filter or include to include only the ids listed in the filter
        /// </summary>
        [JsonProperty("action")]
        public FilterAction Action { get; set; }
    }

    [JsonConverter(typeof(StringEnumConverter))]
    public enum FilterAction
    {
        [EnumMember(Value = "exclude")] Exclude,
        [EnumMember(Value = "include")] Include,
    }
}
﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// These options are available for pedestrian costing methods.
    /// </summary>
    public class PedestrianCostingOptions
    {
        /// <summary>
        /// Walking speed in kilometers per hour. Must be between 0.5 and 25 km/hr. Defaults to 5.1 km/hr (3.1 miles/hour).
        /// </summary>
        [JsonProperty("walking_speed")]
        public double WalkingSpeed { get; set; } = 5.1;

        /// <summary>
        /// A factor that modifies the cost when encountering roads classified as footway (no motorized vehicles allowed),
        /// which may be designated footpaths or designated sidewalks along residential roads. Pedestrian routes generally
        /// attempt to favor using these walkways and sidewalks. The default walkway_factor is 1.0.
        /// </summary>
        [JsonProperty("walkway_factor")]
        public double WalkwayFactor { get; set; } = 1;

        /// <summary>
        /// A factor that modifies (multiplies) the cost when alleys are encountered. Pedestrian routes generally want
        /// to avoid alleys or narrow service roads between buildings. The default alley_factor is 2.0.
        /// </summary>
        [JsonProperty("alley_factor")]
        public double AlleyFactor { get; set; } = 2;

        /// <summary>
        /// A factor that modifies (multiplies) the cost when encountering a driveway, which is often a private, service
        /// road. Pedestrian routes generally want to avoid driveways (private). The default driveway factor is 5.0.
        /// </summary>
        [JsonProperty("driveway_factor")]
        public double DrivewayFactor { get; set; } = 5;

        /// <summary>
        /// A penalty in seconds added to each transition onto a path with steps or stairs. Higher values apply larger
        /// cost penalties to avoid paths that contain flights of steps.
        /// </summary>
        [JsonProperty("step_penalty")]
        public double? StepPenalty { get; set; }

        /// <summary>
        /// This value indicates the willingness to take ferries. This is a range of values between 0 and 1. Values near
        /// 0 attempt to avoid ferries and values near 1 will favor ferries. The default value is 0.5. Note that sometimes
        /// ferries are required to complete a route so values of 0 are not guaranteed to avoid ferries entirely.
        /// </summary>
        [JsonProperty("use_ferry")]
        public double UserFerry { get; set; } = 0.5;

        /// <summary>
        /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        /// avoid living streets entirely.
        /// </summary>
        [JsonProperty("use_living_streets")]
        public double UseLivingStreets { get; set; } = 0;

        /// <summary>
        /// This value indicates the willingness to take track roads. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid tracks and values near 1 will favor tracks a little bit.
        /// The default value is 0 for autos, 0.5 for motor scooters and motorcycles. Note that sometimes tracks are
        /// required to complete a route so values of 0 are not guaranteed to avoid tracks entirely.
        /// </summary>
        [JsonProperty("use_tracks")]
        public double UseTracks { get; set; } = 0.5;

        /// <summary>
        /// A penalty applied for transition to generic service road. The default penalty is 0.
        /// </summary>
        [JsonProperty("service_penalty")]
        public double ServicePenalty { get; set; } = 0;

        /// <summary>
        /// A factor that modifies (multiplies) the cost when generic service roads are encountered.
        /// The default service_factor is 1.
        /// </summary>
        [JsonProperty("service_factor")]
        public double ServiceFactor { get; set; } = 1;

        /// <summary>
        /// This value indicates the maximum difficulty of hiking trails that is allowed. Values between 0 and 6 are
        /// allowed. The values correspond to sac_scale values within OpenStreetMap, see reference here. The default
        /// value is 1 which means that well cleared trails that are mostly flat or slightly sloped are allowed.
        /// Higher difficulty trails can be allowed by specifying a higher value for max_hiking_difficulty.
        /// </summary>
        [JsonProperty("max_hiking_difficulty")]
        public double MaxHikingDifficulty { get; set; } = 1;

        /// <summary>
        /// This value is useful when bikeshare is chosen as travel mode. It is meant to give the time will be used to
        /// return a rental bike. This value will be displayed in the final directions and used to calculate the whole
        /// duation. The default value is 120 seconds.
        /// </summary>
        [JsonProperty("bss_return_cost")]
        public double BssReturnCost { get; set; } = 120;

        /// <summary>
        /// This value is useful when bikeshare is chosen as travel mode. It is meant to describe the potential
        /// effort to return a rental bike. This value won't be displayed and used only inside of the algorithm.
        /// </summary>
        [JsonProperty("bss_return_penalty")]
        public double? BssReturnPenalty { get; set; }

        /// <summary>
        /// Changes the metric to quasi-shortest, i.e. purely distance-based costing. Note, this will disable all other
        /// costings and penalties. Also note, shortest will not disable hierarchy pruning, leading to potentially
        /// sub-optimal routes for some costing models. The default is false.
        /// </summary>
        [JsonProperty("shortest")]
        public bool Shortest { get; set; } = false;
    }
}
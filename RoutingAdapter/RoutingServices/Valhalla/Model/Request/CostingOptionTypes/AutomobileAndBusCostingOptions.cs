﻿using Newtonsoft.Json;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// These options are available for auto, bus, and truck costing methods.
    /// </summary>
    public class AutomobileAndBusCostingOptions : VehicleBaseCostingOptions
    {
        /// <summary>
        /// A cost applied when a toll booth is encountered. This cost is added to the estimated and elapsed times.
        /// The default cost is 15 seconds.
        /// </summary>
        [JsonProperty("toll_booth_cost")]
        public double TollBoothCost { get; set; } = 15;

        /// <summary>
        /// A cost applied when entering a ferry. This cost is added to the estimated and elapsed times.
        /// The default cost is 300 seconds (5 minutes).
        /// </summary>
        [JsonProperty("ferry_cost")]
        public double FerryCost { get; set; } = 300;

        /// <summary>
        /// This value indicates the willingness to take highways. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid highways and values near 1 will favor highways. The default value is 1.0.
        /// Note that sometimes highways are required to complete a route so values of 0 are not guaranteed to avoid highways entirely.
        /// </summary>
        [JsonProperty("use_highways")]
        public double UseHighways { get; set; } = 1.0;

        /// <summary>
        /// This value indicates the willingness to take roads with tolls. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid tolls and values near 1 will not attempt to avoid them.
        /// The default value is 0.5. Note that sometimes roads with tolls are required to complete a route so values
        /// of 0 are not guaranteed to avoid them entirely.
        /// </summary>
        [JsonProperty("use_tolls")]
        public double UseTolls { get; set; } = 0.5;

        /// <summary>
        /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        /// avoid living streets entirely.
        /// </summary>
        [JsonProperty("use_living_streets")]
        public double? UseLivingStreets { get; set; } = 0.1;

        /// <summary>
        /// This value indicates the willingness to take track roads. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid tracks and values near 1 will favor tracks a little bit.
        /// The default value is 0 for autos, 0.5 for motor scooters and motorcycles. Note that sometimes tracks are
        /// required to complete a route so values of 0 are not guaranteed to avoid tracks entirely.
        /// </summary>
        [JsonProperty("use_tracks")]
        public double UseTracks { get; set; } = 0;

        /// <summary>
        /// A factor that modifies (multiplies) the cost when generic service roads are encountered.
        /// The default service_factor is 1.
        /// </summary>
        [JsonProperty("service_factor")]
        public double ServiceFactor { get; set; } = 1;

        /// <summary>
        /// Top speed the vehicle can go. Also used to avoid roads with higher speeds than this value.
        /// top_speed must be between 10 and 252 KPH. The default value is 140 KPH.
        /// </summary>
        [JsonProperty("top_speed")]
        public double TopSpeed { get; set; } = 140;

        /// <summary>
        /// If set to true, ignores all closures, marked due to live traffic closures, during routing.
        /// Note: This option cannot be set if location.search_filter.exclude_closures is also specified in the request
        /// and will return an error if it is
        /// </summary>
        [JsonProperty("ignore_closures")]
        public bool? IgnoreClosures { get; set; }

        /// <summary>
        /// A factor that penalizes the cost when traversing a closed edge (eg: if search_filter.exclude_closures
        /// is false for origin and/or destination location and the route starts/ends on closed edges).
        /// Its value can range from 1.0 - don't penalize closed edges, to 10.0 - apply high cost penalty to closed edges.
        /// Default value is 9.0. Note: This factor is applicable only for motorized modes of transport, i.e auto,
        /// motorcycle, motor_scooter, bus, truck, hov and taxi.
        /// </summary>
        [JsonProperty("closure_factor")]
        public double ClosureFactor { get; set; } = 9.0;

        /// <summary>
        /// A penalty applied for transition to generic service road.
        /// The default penalty is 0 trucks and 15 for cars, buses, motor scooters and motorcycles.
        /// </summary>
        [JsonProperty("service_penalty")]
        public double ServicePenalty { get; set; } = 15;
    }
}
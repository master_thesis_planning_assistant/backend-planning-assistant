﻿using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.CostingOptionTypes
{
    /// <summary>
    /// The default bicycle costing is tuned toward road bicycles with a slight preference for using cycleways or roads
    /// with bicycle lanes. Bicycle routes use regular roads where needed or where no direct bicycle lane options exist,
    /// but avoid roads without bicycle access. The costing model recognizes several factors unique to bicycle travel
    /// and offers several options for tuning bicycle routes. Several factors unique to travel by bicycle influence
    /// the resulting route
    /// </summary>
    public class BicycleCostingOptions : VehicleBaseCostingOptions
    {
        /// <summary>
        /// The type of bicycle. The default type is Hybrid.
        /// </summary>
        [JsonProperty("bicycle_type")]
        public BicycleType BicycleType { get; set; } = BicycleType.Hybrid;

        /// <summary>
        /// Cycling speed is the average travel speed along smooth, flat roads. This is meant to be the speed a
        /// rider can comfortably maintain over the desired distance of the route. It can be modified
        /// (in the costing method) by surface type in conjunction with bicycle type and (coming soon) by hilliness of
        /// the road section. When no speed is specifically provided, the default speed is determined by the bicycle
        /// type and are as follows: Road = 25 KPH (15.5 MPH), Cross = 20 KPH (13 MPH), Hybrid/City = 18 KPH (11.5 MPH),
        /// and Mountain = 16 KPH (10 MPH).
        /// </summary>
        [JsonProperty("cycling_speed")]
        public double? CyclingSpeed { get; set; }

        /// <summary>
        /// A cyclist's propensity to use roads alongside other vehicles. This is a range of values from 0 to 1, where
        /// 0 attempts to avoid roads and stay on cycleways and paths, and 1 indicates the rider is more comfortable
        /// riding on roads. Based on the use_roads factor, roads with certain classifications and higher speeds are
        /// penalized in an attempt to avoid them when finding the best path. The default value is 0.5.
        /// </summary>
        [JsonProperty("use_roads")]
        public double UseRoads { get; set; } = 0.5;

        /// <summary>
        /// A cyclist's desire to tackle hills in their routes. This is a range of values from 0 to 1, where 0 attempts
        /// to avoid hills and steep grades even if it means a longer (time and distance) path, while 1 indicates the
        /// rider does not fear hills and steeper grades. Based on the use_hills factor, penalties are applied to roads
        /// based on elevation change and grade. These penalties help the path avoid hilly roads in favor of flatter
        /// roads or less steep grades where available. Note that it is not always possible to find alternate paths to
        /// avoid hills (for example when route locations are in mountainous areas). The default value is 0.5.
        /// </summary>
        [JsonProperty("use_hills")]
        public double UseHills { get; set; } = 0.5;

        /// <summary>
        /// This value is meant to represent how much a cyclist wants to avoid roads with poor surfaces relative to the
        /// bicycle type being used. This is a range of values between 0 and 1. When the value is 0, there is no
        /// penalization of roads with different surface types; only bicycle speed on each surface is taken into account.
        /// As the value approaches 1, roads with poor surfaces for the bike are penalized heavier so that they are only
        /// taken if they significantly improve travel time. When the value is equal to 1, all bad surfaces are completely
        /// disallowed from routing, including start and end points. The default value is 0.25.
        /// </summary>
        [JsonProperty("avoid_bad_surfaces")]
        public double AvoidBadSurfaces { get; set; } = 0.25;

        /// <summary>
        /// This value is useful when bikeshare is chosen as travel mode. It is meant to give the time will be used to
        /// return a rental bike. This value will be displayed in the final directions and used to calculate the whole
        /// duation. The default value is 120 seconds.
        /// </summary>
        [JsonProperty("bss_return_cost")]
        public double BssReturnCost { get; set; } = 120;

        /// <summary>
        /// This value is useful when bikeshare is chosen as travel mode. It is meant to describe the potential
        /// effort to return a rental bike. This value won't be displayed and used only inside of the algorithm.
        /// </summary>
        [JsonProperty("bss_return_penalty")]
        public double? BssReturnPenalty { get; set; }

        /// <summary>
        /// This value indicates the willingness to take living streets. This is a range of values between 0 and 1.
        /// Values near 0 attempt to avoid living streets and values near 1 will favor living streets.
        /// The default value is 0 for trucks, 0.5 for bicycle, 0.1 for cars, buses, motor scooters and motorcycles.
        /// Note that sometimes living streets are required to complete a route so values of 0 are not guaranteed to
        /// avoid living streets entirely.
        /// </summary>
        [JsonProperty("use_living_streets")]
        public double UseLivingStreets { get; set; } = 0.5;
    }
}
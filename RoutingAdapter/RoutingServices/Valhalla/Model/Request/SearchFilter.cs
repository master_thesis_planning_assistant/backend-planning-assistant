﻿using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class SearchFilter
    {
        /// <summary>
        /// (boolean, defaults to false): whether to exclude roads marked as tunnels
        /// </summary>
        [JsonProperty("exclude_tunnel")]
        public bool ExcludeTunnel { get; set; } = false;

        /// <summary>
        /// (boolean, defaults to false): whether to exclude roads marked as bridges
        /// </summary>
        [JsonProperty("exclude_bridge")]
        public bool ExcludeBridge { get; set; } = false;

        /// <summary>
        /// (boolean, defaults to false): whether to exclude link roads marked as ramps, note that some turn channels
        /// are also marked as ramps
        /// </summary>
        [JsonProperty("exclude_ramp")]
        public bool ExcludeRamp { get; set; } = false;

        /// <summary>
        /// (boolean, defaults to true): whether to exclude roads considered closed due to live traffic closure.
        /// Note: This option cannot be set if <code>costing_options{costing}.ignore_closures</code> is also specified.
        /// An error is returned if both options are specified. Note 2: Ignoring closures at destination and source
        /// locations does NOT work for date_time type 0 or 1 and 2 respectively
        /// </summary>
        [JsonProperty("exclude_closures")]
        public bool ExcludeClosures { get; set; } = false;

        /// <summary>
        /// (string, defaults to "service_other"): lowest road class allowed
        /// </summary>
        [JsonProperty("min_road_class")]
        public RoadClass MinRoadClass { get; set; } = RoadClass.ServiceOther;

        /// <summary>
        /// max_road_class (string, defaults to "motorway"): highest road class allowed
        /// </summary>
        [JsonProperty("max_road_class")]
        public RoadClass MaxRoadClass { get; set; } = RoadClass.Motorway;
    }
}
﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class RoutingDateTime
    {
        /// <summary>
        /// Type of the routing dateTime
        /// </summary>
        [JsonProperty("type")]
        public RoutingDateTimeType Type { get; set; }

        /// <summary>
        /// the date and time is specified in ISO 8601 format (YYYY-MM-DDThh:mm) in the local time zone of
        /// departure or arrival. For example "2016-07-03T08:06"
        /// </summary>
        [JsonProperty("value")]
        public DateTime Value { get; set; }
    }
}
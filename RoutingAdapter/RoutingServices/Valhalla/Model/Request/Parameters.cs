﻿using System.Collections.Generic;
using Newtonsoft.Json;
using RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request
{
    public class Parameters
    {
        /// <summary>
        /// You specify locations as an ordered list of two or more locations within a JSON array.
        /// Locations are visited in the order specified.
        /// To build a route, you need to specify two break locations. In addition, you can include through, via or
        /// break_through locations to influence the route path.
        /// </summary>
        [JsonProperty("locations")]
        public List<Location> Locations { get; set; }

        /// <summary>
        /// Valhalla's routing service uses dynamic, run-time costing to generate the route path.
        /// The route request must include the name of the costing model and can include optional parameters available
        /// for the chosen costing model.
        /// </summary>
        [JsonProperty("costing")]
        public CostingModel CostingModel { get; set; }

        /// <summary>
        /// Costing methods can have several options that can be adjusted to develop the route path, as well as for
        /// estimating time along the path. Specify costing model options in your request using the format of
        /// costing_options.type, such as costing_options.auto.
        /// </summary>
        [JsonProperty("costing_options")]
        public CostingOptions CostingOptions { get; set; }

        /// <summary>
        /// Distance units for output. Allowable unit types are miles (or mi) and kilometers (or km).
        /// If no unit type is specified, the units default to kilometers.
        /// </summary>
        [JsonProperty("units")]
        public Unit Units { get; set; } = Unit.Kilometers;

        /// <summary>
        /// The language of the narration instructions based on the IETF BCP 47 language tag string.
        /// If no language is specified or the specified language is unsupported, United States-based English (en-US) is used.
        /// Currently supported language list
        /// <a href="https://valhalla.readthedocs.io/en/latest/api/turn-by-turn/api-reference/#supported-language-tags" />
        /// </summary>
        [JsonProperty("language")]
        public string Language { get; set; } = "en-US";

        /// <summary>
        /// Modifies if maneuvers and instructions are returned
        /// Default is "instructions"
        /// </summary>
        [JsonProperty("directions_type")]
        public DirectionsType DirectionsType { get; set; } = DirectionsType.Instructions;

        /// <summary>
        /// A set of locations to exclude or avoid within a route can be specified using a JSON array of avoid_locations.
        /// The avoid_locations have the same format as the locations list. At a minimum each avoid location must
        /// include latitude and longitude. The avoid_locations are mapped to the closest road or roads and these roads
        /// are excluded from the route path computation.
        /// </summary>
        [JsonProperty("avoid_locations")]
        private List<Location> AvoidLocations { get; set; }

        /// <summary>
        /// One or multiple exterior rings of polygons in the form of nested JSON arrays,
        /// e.g. [[[lon1, lat1], [lon2,lat2]],[[lon1,lat1],[lon2,lat2]]].
        /// Roads intersecting these rings will be avoided during path finding. If you only need to avoid a few specific
        /// roads, it's much more efficient to use avoid_locations. Valhalla will close open rings
        /// (i.e. copy the first coordingate to the last position).
        /// </summary>
        [JsonProperty("avoid_polygons")]
        public List<List<double>> AvoidPolygons { get; set; }

        /// <summary>
        /// This is the local date and time at the location.
        /// </summary>
        [JsonProperty("date_time")]
        public RoutingDateTime DateTime { get; set; }

        /// <summary>
        /// Output format. If no out_format is specified, JSON is returned.
        /// Future work includes PBF (protocol buffer) support.
        /// </summary>
        [JsonProperty("out_format")]
        public string OutFormat { get; set; } = null;

        /// <summary>
        /// Name your route request. If id is specified, the naming will be sent thru to the response.
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }

        /// <summary>
        /// When present and true, the successful route response will include a key linear_references.
        /// Its value is an array of base64-encoded OpenLR location references, one for each graph edge of the
        /// road network matched by the input trace.
        /// </summary>
        [JsonProperty("linear_references")]
        public bool? LinearReferences { get; set; }
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum LocationType
    {
        /// <summary>
        /// A break is a location at which we allows u-turns and generate legs and arrival/departure maneuvers.
        /// </summary>
        [XmlEnum("break")] [EnumMember(Value = "break")]
        Break,

        /// <summary>
        /// A through location is a location at which we neither allow u-turns nor generate legs or arrival/departure maneuvers.
        /// </summary>
        [XmlEnum("through")] [EnumMember(Value = "through")]
        Trough,

        /// <summary>
        /// A via location is a location at which we allow u-turns but do not generate legs or arrival/departure maneuvers. 
        /// </summary>
        [XmlEnum("via")] [EnumMember(Value = "via")]
        Via,

        /// <summary>
        /// A break_through location is a location at which we do not allow u-turns but do generate legs and
        /// arrival/departure maneuvers. If no type is provided, the type is assumed to be a break. The types of the
        /// first and last locations are ignored and are treated as breaks.
        /// </summary>
        [XmlEnum("break_through")] [EnumMember(Value = "break_through")]
        BreakTrough,
    }
}
﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    /// <summary>
    /// Road Classes from highes to lowes
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoadClass
    {
        [XmlEnum("motorway")] [EnumMember(Value = "motorway")]
        Motorway,

        [XmlEnum("trunk")] [EnumMember(Value = "trunk")]
        Trunk,

        [XmlEnum("primary")] [EnumMember(Value = "primary")]
        Primary,

        [XmlEnum("secondary")] [EnumMember(Value = "secondary")]
        Secondary,

        [XmlEnum("tertiary")] [EnumMember(Value = "tertiary")]
        Tertiary,

        [XmlEnum("unclassified")] [EnumMember(Value = "unclassified")]
        Unclassified,

        [XmlEnum("residential")] [EnumMember(Value = "residential")]
        Residential,

        [XmlEnum("service_other")] [EnumMember(Value = "service_other")]
        ServiceOther,
    }
}
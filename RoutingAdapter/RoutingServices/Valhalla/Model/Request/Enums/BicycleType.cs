﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum BicycleType
    {
        /// <summary>
        ///  a road-style bicycle with narrow tires that is generally lightweight and designed for speed on paved surfaces.
        /// </summary>
        [XmlEnum("Road")] [EnumMember(Value = "Road")]
        Road,

        /// <summary>
        /// a bicycle made mostly for city riding or casual riding on roads and paths with good surfaces.
        /// </summary>
        [XmlEnum("Hybrid")] [EnumMember(Value = "Hybrid")]
        Hybrid,

        /// <summary>
        /// a cyclo-cross bicycle, which is similar to a road bicycle but with wider tires suitable to rougher surfaces.
        /// </summary>
        [XmlEnum("Cross")] [EnumMember(Value = "Cross")]
        Cross,

        /// <summary>
        /// a mountain bicycle suitable for most surfaces but generally heavier and slower on paved surfaces.
        /// </summary>
        [XmlEnum("Mountain")] [EnumMember(Value = "Mountain")]
        Mountain,
    }
}
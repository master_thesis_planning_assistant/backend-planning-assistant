﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CostingModel
    {
        /// <summary>
        /// Standard costing for driving routes by car, motorcycle, truck, and so on that obeys automobile driving rules,
        /// such as access and turn restrictions. Auto provides a short time path (though not guaranteed to be shortest time)
        /// and uses intersection costing to minimize turns and maneuvers or road name changes. Routes also tend to favor
        /// highways and higher classification roads, such as motorways and trunks.
        /// </summary>
        [XmlEnum("auto")] [EnumMember(Value = "auto")]
        Auto,

        /// <summary>
        /// Standard costing for travel by bicycle, with a slight preference for using cycleways or roads with bicycle lanes.
        /// Bicycle routes follow regular roads when needed, but avoid roads without bicycle access.
        /// </summary>
        [XmlEnum("bicycle")] [EnumMember(Value = "bicycle")]
        Bicycle,

        /// <summary>
        /// Standard costing for bus routes. Bus costing inherits the auto costing behaviors,
        /// but checks for bus access on the roads.
        /// </summary>
        [XmlEnum("bus")] [EnumMember(Value = "bus")]
        Bus,

        /// <summary>
        /// BETA
        /// A combination of pedestrian and bicycle. Use bike share station(amenity:bicycle_rental) to change the travel mode
        /// </summary>
        [XmlEnum("bikeshare")] [EnumMember(Value = "bikeshare")]
        Bikeshare,

        /// <summary>
        /// BETA
        /// Standard costing for trucks. Truck costing inherits the auto costing behaviors, but checks for truck access,
        /// width and height restrictions, and weight limits on the roads.
        /// </summary>
        [XmlEnum("Truck")] [EnumMember(Value = "Truck")]
        Truck,

        /// <summary>
        /// BETA
        /// Standard costing for high-occupancy vehicle (HOV) routes. HOV costing inherits the auto costing behaviors,
        /// but checks for HOV lane access on the roads and favors those roads.
        /// </summary>
        [XmlEnum("Hov")] [EnumMember(Value = "Hov")]
        Hov,

        /// <summary>
        /// BETA
        /// Standard costing for taxi routes. Taxi costing inherits the auto costing behaviors,
        /// but checks for taxi lane access on the roads and favors those roads.
        /// </summary>
        [XmlEnum("taxi")] [EnumMember(Value = "taxi")]
        Taxi,

        /// <summary>
        /// BETA
        /// Standard costing for travel by motor scooter or moped. By default, motor_scooter costing will avoid higher
        /// class roads unless the country overrides allows motor scooters on these roads. Motor scooter routes follow
        /// regular roads when needed, but avoid roads without motor_scooter, moped, or mofa access.
        /// </summary>
        [XmlEnum("motor_scooter")] [EnumMember(Value = "motor_scooter")]
        MotorScooter,

        /// <summary>
        /// BETA
        /// Standard costing for travel by motorcycle. This costing model provides options to tune the route to take
        /// roadways (road touring) vs. tracks and trails (adventure motorcycling).
        /// </summary>
        [XmlEnum("motorcycle")] [EnumMember(Value = "motorcycle")]
        MotorCycle,

        /// <summary>
        /// BETA
        /// Currently supports pedestrian and transit. In the future, multimodal will support a combination of all
        /// of the above.
        /// </summary>
        [XmlEnum("multimodal")] [EnumMember(Value = "multimodal")]
        Multimodal,

        /// <summary>
        /// BETA
        /// Standard walking route that excludes roads without pedestrian access. In general, pedestrian routes are
        /// shortest distance with the following exceptions: walkways and footpaths are slightly favored, while steps or stairs and alleys are slightly avoided.
        /// </summary>
        [XmlEnum("pedestrian")] [EnumMember(Value = "pedestrian")]
        Pedestrian,
    }
}
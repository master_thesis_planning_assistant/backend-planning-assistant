﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    [JsonConverter(typeof(StringEnumConverter))]
    public enum PreferredSide
    {
        [XmlEnum("same")] [EnumMember(Value = "same")]
        Same,

        [XmlEnum("opposite")] [EnumMember(Value = "opposite")]
        Opposite,

        [XmlEnum("either")] [EnumMember(Value = "either")]
        Either,
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Valhalla.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper
    public enum RoutingDateTimeType
    {
        /// <summary>
        /// Current departure time.
        /// </summary>
        [XmlEnum("0")] Current = 0,

        /// <summary>
        /// Specified departure time
        /// </summary>
        [XmlEnum("1")] Departure = 1,

        /// <summary>
        /// Specified arrival time. Not yet implemented for multimodal costing method.
        /// </summary>
        [XmlEnum("2")] Arrival = 2,

        /// <summary>
        /// Invariant specified time. Time does not vary over the course of the path. Not implemented for multimodal or bike share routing
        /// </summary>
        [XmlEnum("3")] Invariant = 3,
    }
}
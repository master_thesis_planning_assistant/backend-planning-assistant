﻿using System;
using Microsoft.Extensions.DependencyInjection;
using RoutingAdapter.RoutingServices.Valhalla.Client;

namespace RoutingAdapter.RoutingServices.Valhalla
{
    public static class ValhallaServiceCollectionExtension
    {
        public static IServiceCollection AddValhalla(this IServiceCollection serviceCollection,
            string connectionString)
        {
            // Add Valhalla client
            serviceCollection.AddHttpClient(ValhallaClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IValhallaClient, ValhallaClient>();
            // Add valhalla adapter to routing services
            serviceCollection.AddScoped<IRoutingService, ValhallaRoutingServiceAdapter>();
            return serviceCollection;
        }
    }
}
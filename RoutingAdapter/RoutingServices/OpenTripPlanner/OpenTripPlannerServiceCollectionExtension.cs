﻿using System;
using Microsoft.Extensions.DependencyInjection;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Client;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner
{
    public static class OpenTripPlannerServiceCollectionExtension
    {
        public static IServiceCollection AddOpenTripPlanner(this IServiceCollection serviceCollection,
            string connectionString)
        {
            // Add OTP client
            serviceCollection.AddHttpClient(OtpClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<IOtpClient, OtpClient>();
            // Add OTP adapter to routing services
            serviceCollection.AddScoped<IRoutingService, OpenTripPlannerRoutingServiceAdapter>();
            return serviceCollection;
        }
    }
}
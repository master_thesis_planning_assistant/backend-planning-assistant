﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Client
{
    public class OtpClient : IOtpClient
    {
        public const string HttpClientName = "OpenTripPlanner";

        private readonly IHttpClientFactory _httpClientFactory;

        public OtpClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
        }

        public async Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            var request = requestWrapper.RoutingRequest;
            var url = $"{client.BaseAddress}routers/{request.RouterId}/plan";
            var urlParams = request.Parameters.GetParamsAsDictionary();

            var response = await client.GetAsync(new Uri(QueryHelpers.AddQueryString(url, urlParams)));

            if (response.StatusCode != HttpStatusCode.OK)
            {
                throw new Exception($"Got status code {response.StatusCode} from {HttpClientName}-Request!");
            }

            var responseBody = await response.Content.ReadAsStringAsync();
            var routingResponse = JsonConvert.DeserializeObject<RoutingResponse>(responseBody);
            return new SpecificRoutingResponseWrapper<RoutingResponse>()
            {
                ActivityGuid = requestWrapper.ActivityGuid,
                RoutingResponse = routingResponse,
            };
        }
    }
}
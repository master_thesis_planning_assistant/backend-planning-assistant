﻿using System.Threading.Tasks;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Client
{
    public interface IOtpClient
    {
        public Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper);
    }
}
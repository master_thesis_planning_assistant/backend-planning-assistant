﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Client;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner
{
    /// <summary>
    /// Adapter for the OpenTripPlanner (OTP) Routing-Service. For Doku see
    /// OPT: https://github.com/opentripplanner/OpenTripPlanner
    /// Model: https://dev.opentripplanner.org/apidoc/1.4.0/resource_PlannerResource.html
    /// </summary>
    public class OpenTripPlannerRoutingServiceAdapter : BaseRoutingService<RoutingRequest, RoutingResponse>
    {
        private readonly IOtpClient _client;
        private readonly ILogger<OpenTripPlannerRoutingServiceAdapter> _logger;

        public OpenTripPlannerRoutingServiceAdapter(IOtpClient client,
            ILogger<OpenTripPlannerRoutingServiceAdapter> logger, IXsltTransformerService xsltTransformer) : base(
            logger,
            xsltTransformer)
        {
            _client = client;
            _logger = logger;
        }

        public override string ServiceName => "OpenTripPlanner";

        public override ICollection<RoutingMode> SupportedModalities => new List<RoutingMode>
        {
            RoutingMode.Bike,
            RoutingMode.Car,
            RoutingMode.Walk,
            RoutingMode.PublicTransport,
            RoutingMode.BikeSharing
        };

        protected override async Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeSpecificRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            return await _client.MakeRoutingRequestAsync(requestWrapper);
        }

        protected override ICollection<SingleRoutingRequest> HandleModalityCardinality(
            SingleRoutingRequest singleRoutingRequest, ICollection<RoutingMode> modalities)
        {
            // OTP can handle multiple modalities in one request!
            // See RoutingRequest > RoutingRequestParameters > Mode
            return new List<SingleRoutingRequest>
            {
                new SingleRoutingRequest
                {
                    Preferences = singleRoutingRequest.Preferences,
                    MobilityDemand = singleRoutingRequest.MobilityDemand,
                    Modalities = modalities.ToList()
                }
            };
        }


        protected override RoutesForActivity BuildResultingRoutes(
            SpecificRoutingResponseWrapper<RoutingResponse> specificRoutingResponseWrapper,
            SpecificRoutingRequestWrapper<RoutingRequest> specificRoutingRequestWrapper,
            MobilityDemand originalMobilityDemand)
        {
            if (specificRoutingResponseWrapper.RoutingResponse.Error != null)
            {
                var error = specificRoutingResponseWrapper.RoutingResponse.Error;
                throw new ApplicationException(
                    $"Error calling {ServiceName}: {error.Message.GetStringValue()} - {error.Msg}");
            }

            var response = new RoutesForActivity
            {
                ActivityGuid = specificRoutingResponseWrapper.ActivityGuid,
                Routes = new List<ActivityRoute>(),
            };
            foreach (var itinerary in specificRoutingResponseWrapper.RoutingResponse.Plan.Itineraries)
            {
                var newRoute = new ActivityRoute
                {
                    Guid = Guid.NewGuid().ToString(),
                    ServiceName = ServiceName,
                    Start = originalMobilityDemand.From,
                    Departure = itinerary.StartTime ??
                                originalMobilityDemand.At - TimeSpan.FromSeconds(itinerary.Duration ?? 0),
                    Destination = originalMobilityDemand.To,
                    Arrival = itinerary.EndTime ?? originalMobilityDemand.At,
                    Duration = itinerary.Duration ?? 0,
                    Legs = new List<ActivityRouteLeg>(),
                };
                foreach (var leg in itinerary.Legs)
                {
                    var newLeg = new ActivityRouteLeg
                    {
                        Modality = GetModeEquivalent(leg.Mode),
                        Description = leg.Route,
                        StartLocation = new NamedGeoLocation
                        {
                            Name = leg.From.Name,
                            GeoLocation = leg.From.Lon.HasValue && leg.From.Lat.HasValue
                                ? new GeoLocation
                                {
                                    Longitude = leg.From.Lon.Value,
                                    Latitude = leg.From.Lat.Value,
                                }
                                : null,
                        },
                        Departure = leg.EndTime ?? default, // TODO how to handle
                        EndLocation = new NamedGeoLocation()
                        {
                            Name = leg.To.Name,
                            GeoLocation = leg.To.Lon.HasValue && leg.To.Lat.HasValue
                                ? new GeoLocation
                                {
                                    Longitude = leg.To.Lon.Value,
                                    Latitude = leg.To.Lat.Value,
                                }
                                : null,
                        },
                        Arrival = leg.StartTime ?? default, // TODO how to handle
                        Polyline = new Polyline
                        {
                            Points = Polyline.DecodeEncodedPolyline(leg.LegGeometry.Points).ToList(),
                        },
                        Duration = leg.Distance ?? 0,
                        Distance = leg.Duration,
                        Steps = new List<ActivityRouteStep>(),
                    };
                    var prevStepStart = newLeg.StartLocation;
                    foreach (var step in leg.Steps)
                    {
                        var newStep = new ActivityRouteStep
                        {
                            Action = step.RelativeDirection.ToString(),
                            StartLocation = new NamedGeoLocation()
                            {
                                Name = prevStepStart.Name,
                                GeoLocation = prevStepStart.GeoLocation != null
                                    ? new GeoLocation
                                    {
                                        Latitude = prevStepStart.GeoLocation.Latitude,
                                        Longitude = prevStepStart.GeoLocation.Longitude,
                                    }
                                    : null,
                            },
                            EndLocation = new NamedGeoLocation()
                            {
                                Name = step.StreetName,
                                GeoLocation = new GeoLocation
                                {
                                    Latitude = step.Lat,
                                    Longitude = step.Lon
                                }
                            },
                            Distance = step.Distance
                        };
                        newStep.Polyline = new Polyline
                        {
                            Points = new List<GeoLocation>
                            {
                                newStep.StartLocation.GeoLocation, newStep.EndLocation.GeoLocation
                            },
                        };
                        prevStepStart = newStep.EndLocation;
                        newLeg.Steps.Add(newStep);
                    }

                    newRoute.Legs.Add(newLeg);
                }

                newRoute.Distance = newRoute.Legs.Aggregate(0.0, ((i, leg) => i + leg.Distance));
                newRoute.Polyline = new Polyline
                {
                    Points = newRoute.Legs.Aggregate(new List<GeoLocation>(),
                        (points, leg) => (points.Concat(leg.Polyline.Points)).ToList()).ToList(),
                };
                newRoute.Bounds = newRoute.Polyline.GetBounds();
                response.Routes.Add(newRoute);
            }

            return response;
        }

        private RoutingMode GetModeEquivalent(TraverseMode specificMode)
        {
            return specificMode switch
            {
                TraverseMode.Walk => RoutingMode.Walk,
                TraverseMode.Bicycle => RoutingMode.Bike,
                TraverseMode.Car => RoutingMode.Car,
                TraverseMode.Tram => RoutingMode.PublicTransport,
                TraverseMode.Subway => RoutingMode.PublicTransport,
                TraverseMode.Rail => RoutingMode.PublicTransport,
                TraverseMode.Bus => RoutingMode.PublicTransport,
                TraverseMode.Ferry => RoutingMode.PublicTransport,
                TraverseMode.CableCar => RoutingMode.PublicTransport,
                TraverseMode.Gondola => RoutingMode.PublicTransport,
                TraverseMode.Funicular => RoutingMode.PublicTransport,
                TraverseMode.Transit => RoutingMode.PublicTransport,
                TraverseMode.Trainish => RoutingMode.PublicTransport,
                TraverseMode.Busish => RoutingMode.PublicTransport,
                // TraverseMode.LegSwitch => expr
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to TraverseMode {specificMode.GetStringValue()} found")
            };
        }
    }
}
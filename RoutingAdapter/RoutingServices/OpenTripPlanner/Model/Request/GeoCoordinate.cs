﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request
{
    public class GeoCoordinate
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
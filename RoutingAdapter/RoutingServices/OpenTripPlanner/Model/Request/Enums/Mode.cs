﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    public enum Mode
    {
        /// <summary>
        /// Walking some or all of the route.
        /// </summary>
        [XmlEnum("WALK")] [EnumMember(Value = "WALK")]
        Walk,

        /// <summary>
        /// General catch-all for all public transport modes.
        /// </summary>
        [XmlEnum("TRANSIT")] [EnumMember(Value = "TRANSIT")]
        Transit,

        /// <summary>
        /// Cycling for the entirety of the route or taking a bicycle onto the public transport
        /// and cycling from the arrival station to the destination.
        /// </summary>
        [XmlEnum("BICYCLE")] [EnumMember(Value = "BICYCLE")]
        Bicycle,

        /// <summary>
        /// Taking a rented, shared-mobility bike for part or the entirety of the route.
        /// Prerequisite: Vehicle positions need to be added to OTP either as static stations or dynamic data feeds.
        /// </summary>
        [XmlEnum("BICYCLE_RENT")] [EnumMember(Value = "BICYCLE_RENT")]
        BicycleRent,


        /// <summary>
        /// Leaving the bicycle at the departure station and walking from the arrival station to the destination.
        /// This mode needs to be combined with at least one transit mode (or TRANSIT) otherwise it behaves like an ordinary bicycle journey.
        /// Prerequisite: Bicycle parking stations present in the OSM file and visible to OTP
        /// by enabling the property staticBikeParkAndRide during graph build.
        /// </summary>
        [XmlEnum("BICYCLE_PARK")] [EnumMember(Value = "BICYCLE_PARK")]
        BicyclePark,

        /// <summary>
        ///  Driving your own car the entirety of the route.
        /// If this is combined with TRANSIT it will return routes with a Kiss and Ride component.
        /// This means that the car is not parked in a permanent parking area but rather the passenger is dropped off
        /// (for example, at an airport) and the driver continues driving the car away from the drop off location.
        /// </summary>
        [XmlEnum("CAR")] [EnumMember(Value = "CAR")]
        Car,

        /// <summary>
        /// Driving a car to the park-and-ride facilities near a station and taking public transport.
        /// This mode needs to be combined with at least one transit mode (or TRANSIT) otherwise it behaves like an ordinary car journey.
        /// Prerequisite: Park-and-ride areas near the station need to be present in the OSM input file.
        /// </summary>
        [XmlEnum("CAR_PARK")] [EnumMember(Value = "CAR_PARK")]
        CarPark,

        /// <summary>
        /// Tram, streetcar, or light rail. Used for any light rail or street-level system within a metropolitan area.
        /// </summary>
        [XmlEnum("TRAM")] [EnumMember(Value = "TRAM")]
        Tram,

        /// <summary>
        /// Subway or metro. Used for any underground rail system within a metropolitan area.
        /// </summary>
        [XmlEnum("SUBWAY")] [EnumMember(Value = "SUBWAY")]
        Subway,

        /// <summary>
        /// Used for intercity or long-distance travel.
        /// </summary>
        [XmlEnum("RAIL")] [EnumMember(Value = "RAIL")]
        Rail,

        /// <summary>
        /// Used for short- and long-distance bus routes.
        /// </summary>
        [XmlEnum("BUS")] [EnumMember(Value = "BUS")]
        Bus,

        /// <summary>
        /// Ferry. Used for short- and long-distance boat service.
        /// </summary>
        [XmlEnum("FERRY")] [EnumMember(Value = "FERRY")]
        Ferry,

        /// <summary>
        /// Cable car. Used for street-level cable cars where the cable runs beneath the car.
        /// </summary>
        [XmlEnum("CABLE_CAR")] [EnumMember(Value = "CABLE_CAR")]
        CableCar,

        /// <summary>
        /// Gondola or suspended cable car. Typically used for aerial cable cars where the car is suspended from the cable.
        /// </summary>
        [XmlEnum("GONDOLA")] [EnumMember(Value = "GONDOLA")]
        Gondola,

        /// <summary>
        /// Funicular. Used for any rail system that moves on steep inclines with a cable traction system.
        /// </summary>
        [XmlEnum("FUNICULAR")] [EnumMember(Value = "FUNICULAR")]
        Funicular,

        /// <summary>
        /// Taking an airplane.
        /// </summary>
        [XmlEnum("AIRPLANE")] [EnumMember(Value = "AIRPLANE")]
        Airplane,
    }
}
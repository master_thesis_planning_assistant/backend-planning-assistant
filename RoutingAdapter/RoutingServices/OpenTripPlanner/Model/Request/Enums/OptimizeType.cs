﻿using System.Runtime.Serialization;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request.Enums
{
    // XMLEnum is needed for deserialization from the mapper, EnumMember is needed for request at routing service
    public enum OptimizeType
    {
        [XmlEnum("FLAT")] [EnumMember(Value = "FLAT")]
        Flat,

        [XmlEnum("GREENWAYS")] [EnumMember(Value = "GREENWAYS")]
        Greenways,

        [XmlEnum("QUICK")] [EnumMember(Value = "QUICK")]
        Quick,

        [XmlEnum("SAFE")] [EnumMember(Value = "SAFE")]
        Safe,

        [XmlEnum("TRANSFERS")] [EnumMember(Value = "TRANSFERS")]
        Transfers,

        [XmlEnum("TRIANGLE")] [EnumMember(Value = "TRIANGLE")]
        Triangle,
    }
}
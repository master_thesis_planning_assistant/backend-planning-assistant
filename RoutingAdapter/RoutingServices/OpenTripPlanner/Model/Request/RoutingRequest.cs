﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request
{
    public class RoutingRequest
    {
        /// <summary>
        /// The routerId selects between several graphs on the same server.
        /// The routerId is pulled from the path, not the query parameters.
        /// </summary>
        public string RouterId { get; set; } = "default";

        /// <summary>
        /// Query Parameter of the request
        /// </summary>
        public RoutingRequestParameters Parameters { get; set; }
    }
}
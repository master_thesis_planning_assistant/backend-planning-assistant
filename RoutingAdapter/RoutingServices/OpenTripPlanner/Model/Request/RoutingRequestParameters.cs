﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request.Enums;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Request
{
    public class RoutingRequestParameters
    {
        private string _alightSlack;
        private string _arriveBy;
        private string _bannedAgencies;
        private string _bannedRoutes;
        private string _bannedStopsHard;
        private string _bannedTrips;
        private string _batch;
        private string _bikeBoardCost;
        private string _bikeSpeed;
        private string _bikeSwitchCost;
        private string _bikeSwitchTime;
        private string _boardSlack;
        private string _clampInitialWait;
        private string _date;
        private string _disableAlertFiltering;
        private string _disableRemainingWeightHeuristic;
        private string _flexFlagStopBufferSize;
        private string _flexIgnoreDrtAdvanceBookMin;
        private string _flexUseEligibilityServices;
        private string _flexUseReservationServices;
        private string _fromPlace;
        private string _geoidElevation;
        private string _ignoreRealtimeUpdates;
        private string _intermediatePlaces;
        private string _locale;
        private string _maxHours;
        private string _maxPreTransitTime;
        private string _maxTransfers;
        private string _maxWalkDistance;
        private string _minTransferTime;
        private string _mode;
        private string _nonpreferredTransferPenalty;
        private string _numItineraries;
        private string _optimize;
        private string _otherThanPreferredRoutesPenalty;
        private string _pathComparator;
        private string _preferredAgencies;
        private string _preferredRoutes;
        private string _reverseOptimizeOnTheFly;
        private string _showIntermediateStops;
        private string _startTransitStopId;
        private string _startTransitTripId;
        private string _time;
        private string _toPlace;
        private string _transferPenalty;
        private string _triangleSafetyFactor;
        private string _triangleSlopeFactor;
        private string _triangleTimeFactor;
        private string _unpreferredAgencies;
        private string _unpreferredRoutes;
        private string _useRequestedDateTimeInMaxHours;
        private string _waitAtBeginningFactor;
        private string _waitReluctance;
        private string _walkBoardCost;
        private string _walkReluctance;
        private string _walkSpeed;
        private string _wheelchair;
        private string _whiteListedAgencies;
        private string _whiteListedRoutes;

        public int? AlightSlack
        {
            get => _alightSlack != null ? (int?) int.Parse(_alightSlack) : null;
            set => _alightSlack = value?.ToString();
        }

        /// <summary>
        /// Whether the trip should depart (false) or arrive at (true) the specified date and time.
        /// </summary>
        public bool? ArriveBy
        {
            get => GetAsBool(_arriveBy);
            set => _arriveBy = SetFromBool(value);
        }

        /// <summary>
        /// The comma-separated list of banned agencies.
        /// </summary>
        public List<string> BannedAgencies
        {
            get => GetAsStringCollection(_bannedAgencies);
            set => _bannedAgencies = SetFromStringCollection(value);
        }

        /// <summary>
        /// The comma-separated list of banned routes. The format is agency_[routename][_routeid],
        /// so TriMet_100 (100 is route short name) or Trimet__42 (two underscores, 42 is the route internal ID).
        /// </summary>
        public List<string> BannedRoutes
        {
            get => GetAsStringCollection(_bannedRoutes);
            set => _bannedRoutes = SetFromStringCollection(value);
        }


        /// <summary>
        /// A comma-separated list of banned stops. A stop is banned by ignoring its pre-board and pre-alight edges.
        /// This means the stop will be reachable via the street network.
        /// Also, it is still possible to travel through the stop. Just boarding and alighting is prohibited.
        /// The format is agencyId_stopId, so: TriMet_2107
        /// </summary>
        public List<string> BannedStopsHard
        {
            get => GetAsStringCollection(_bannedStopsHard);
            set => _bannedStopsHard = SetFromStringCollection(value);
        }

        /// <summary>
        /// The comma-separated list of banned trips.
        /// The format is agency_trip[:stop*], so: TriMet_24601 or TriMet_24601:0:1:2:17:18:19
        /// </summary>
        public List<string> BannedTrips
        {
            get => GetAsStringCollection(_bannedTrips);
            set => _bannedTrips = SetFromStringCollection(value);
        }

        /// <summary>
        /// If true, goal direction is turned off and a full path tree is built (specify only once)
        /// </summary>
        public bool? Batch
        {
            get => GetAsBool(_batch);
            set => _batch = SetFromBool(value);
        }

        /// <summary>
        /// Prevents unnecessary transfers by adding a cost for boarding a vehicle.
        /// This is the cost that is used when boarding while cycling.
        /// This is usually higher that walkBoardCost.
        /// </summary>
        public int? BikeBoardCost
        {
            get => GetAsInt(_bikeBoardCost);
            set => _bikeBoardCost = SetFromInt(value);
        }

        /// <summary>
        /// The user's biking speed in meters/second. Defaults to approximately 11 MPH, or 9.5 for bikeshare.
        /// </summary>
        public double? BikeSpeed
        {
            get => GetAsDouble(_bikeSpeed);
            set => _bikeSpeed = SetFromDouble(value);
        }

        /// <summary>
        /// The cost of the user fetching their bike and parking it again. Defaults to 0.
        /// </summary>
        public int? BikeSwitchCost
        {
            get => GetAsInt(_bikeSwitchCost);
            set => _bikeSwitchCost = SetFromInt(value);
        }

        /// <summary>
        /// The time it takes the user to fetch their bike and park it again in seconds. Defaults to 0.
        /// </summary>
        public int? BikeSwitchTime
        {
            get => GetAsInt(_bikeSwitchTime);
            set => _bikeSwitchTime = SetFromInt(value);
        }

        public int? BoardSlack
        {
            get => GetAsInt(_boardSlack);
            set => _boardSlack = SetFromInt(value);
        }

        /// <summary>
        /// When subtracting initial wait time, do not subtract more than this value, to prevent overly optimistic trips.
        /// Reasoning is that it is reasonable to delay a trip start 15 minutes to make a better trip,
        /// but that it is not reasonable to delay a trip start 15 hours; if that is to be done,
        /// the time needs to be included in the trip time. This number depends on the transit system;
        /// for transit systems where trips are planned around the vehicles, this number can be much higher.
        /// For instance, it's perfectly reasonable to delay one's trip 12 hours
        /// if one is taking a cross-country Amtrak train from Emeryville to Chicago.
        /// Has no effect in stock OTP, only in Analyst. A value of 0 means that initial wait time
        /// will not be subtracted out (will be clamped to 0). A value of -1 (the default) means that clamping is disabled,
        /// so any amount of initial wait time will be subtracted out.
        /// </summary>
        public long? ClampInitialWait
        {
            get => GetAsLong(_clampInitialWait);
            set => _clampInitialWait = SetFromLong(value);
        }

        /// <summary>
        /// The date that the trip should depart (or arrive, for requests where arriveBy is true).
        /// Format 2021-05-02
        /// </summary>
        public DateTime? Date
        {
            get => _date != null ? (DateTime?) DateTime.Parse(_date) : null;
            set => _date = value != null ? $"{value:MM-dd-yyyy}" : null;
        }

        public bool? DisableAlertFiltering
        {
            get => GetAsBool(_disableAlertFiltering);
            set => _disableAlertFiltering = SetFromBool(value);
        }

        /// <summary>
        /// If true, the remaining weight heuristic is disabled.
        /// Currently only implemented for the long distance path service.
        /// </summary>
        public bool? DisableRemainingWeightHeuristic
        {
            get => GetAsBool(_disableRemainingWeightHeuristic);
            set => _disableRemainingWeightHeuristic = SetFromBool(value);
        }

        public double? FlexFlagStopBufferSize
        {
            get => _flexFlagStopBufferSize != null ? (double?) double.Parse(_flexFlagStopBufferSize) : null;
            set => _flexFlagStopBufferSize = value?.ToString();
        }

        /// <summary>
        /// Whether to ignore DRT time limits. According to the GTFS-flex spec,
        /// demand-response transit (DRT) service must be reserved at least `drt_advance_book_min` minutes in advance.
        /// OTP not allow DRT service to be used inside that time window, unless this parameter is set to true.
        /// </summary>
        public bool? FlexIgnoreDrtAdvanceBookMin
        {
            get => GetAsBool(_flexIgnoreDrtAdvanceBookMin);
            set => _flexIgnoreDrtAdvanceBookMin = SetFromBool(value);
        }

        /// <summary>
        /// Whether to use eligibility-based services
        /// </summary>
        public bool? FlexUseEligibilityServices
        {
            get => GetAsBool(_flexUseEligibilityServices);
            set => _flexUseEligibilityServices = SetFromBool(value);
        }

        /// <summary>
        /// Whether to use reservation-based services
        /// </summary>
        public bool? FlexUseReservationServices
        {
            get => GetAsBool(_flexUseReservationServices);
            set => _flexUseReservationServices = SetFromBool(value);
        }

        /// <summary>
        /// The start location -- either latitude, longitude pair in degrees or a Vertex label.
        /// For example, 40.714476,-74.005966 or mtanyctsubway_A27_S
        /// </summary>
        public GeoCoordinate FromPlace
        {
            get => GetAsGeoCoordinate(_fromPlace);
            set => _fromPlace = SetFromGeoCoordinate(value);
        }

        /// <summary>
        /// If true, the Graph's ellipsoidToGeoidDifference is applied to all elevations returned by this query.
        /// </summary>
        public bool? GeoidElevation
        {
            get => GetAsBool(_geoidElevation);
            set => _geoidElevation = SetFromBool(value);
        }

        /// <summary>
        /// If true, realtime updates are ignored during this search.
        /// </summary>
        public bool? IgnoreRealtimeUpdates
        {
            get => GetAsBool(_ignoreRealtimeUpdates);
            set => _ignoreRealtimeUpdates = SetFromBool(value);
        }

        /// <summary>
        /// An ordered list of intermediate locations to be visited (see the fromPlace for format).
        /// Parameter can be specified multiple times.
        /// </summary>
        public string IntermediatePlaces
        {
            get => _intermediatePlaces;
            set => _intermediatePlaces = value;
        }

        public string Locale
        {
            get => _locale;
            set => _locale = value;
        }

        public double? MaxHours
        {
            get => GetAsDouble(_maxHours);
            set => _maxHours = SetFromDouble(value);
        }

        /// <summary>
        /// The maximum time (in seconds) of pre-transit travel when using drive-to-transit (park and ride or kiss and ride).
        /// Defaults to unlimited.
        /// </summary>
        public int? MaxPreTransitTime
        {
            get => GetAsInt(_maxPreTransitTime);
            set => _maxPreTransitTime = SetFromInt(value);
        }

        /// <summary>
        /// The maximum number of transfers (that is, one plus the maximum number of boardings) that a trip will be allowed.
        /// Larger values will slow performance, but could give better routes.
        /// This is limited on the server side by the MAX_TRANSFERS value in org.opentripplanner.api.ws.Planner.
        /// </summary>
        public int? MaxTransfers
        {
            get => GetAsInt(_maxTransfers);
            set => _maxTransfers = SetFromInt(value);
        }

        /// <summary>
        /// The maximum distance (in meters) the user is willing to walk. Defaults to unlimited.
        /// </summary>
        public double? MaxWalkDistance
        {
            get => GetAsDouble(_maxWalkDistance);
            set => _maxWalkDistance = SetFromDouble(value);
        }

        /// <summary>
        /// The minimum time, in seconds, between successive trips on different vehicles.
        /// This is designed to allow for imperfect schedule adherence. This is a minimum;
        /// transfers over longer distances might use a longer time.
        /// </summary>
        public int? MinTransferTime
        {
            get => GetAsInt(_minTransferTime);
            set => _minTransferTime = SetFromInt(value);
        }

        /// <summary>
        /// The set of modes that a user is willing to use,
        /// with qualifiers stating whether vehicles should be parked, rented, etc.
        /// Comma separated List: TRANSIT,WALK,...
        /// For options see <see cref="Enums.Mode"/>
        /// </summary>
        public List<Mode> Mode
        {
            get => _mode?.Split(',').Select(str => str.GetEnumValue<Mode>()).ToList();
            set => _mode = value != null && value.Count > 0
                ? string.Join(',', value.Select(x => x.GetStringValue()).ToList())
                : null;
        }

        /// <summary>
        /// An additional penalty added to boardings after the first when the transfer is not preferred.
        /// Preferred transfers also include timed transfers. The value is in OTP's internal weight units,
        /// which are roughly equivalent to seconds. Set this to a high value to discourage transfers
        /// that are not preferred. Of course, transfers that save significant time or walking will still be taken.
        /// When no preferred or timed transfer is defined, this value is ignored.
        /// </summary>
        public int? NonpreferredTransferPenalty
        {
            get => GetAsInt(_nonpreferredTransferPenalty);
            set => _nonpreferredTransferPenalty = SetFromInt(value);
        }

        /// <summary>
        /// The maximum number of possible itineraries to return.
        /// </summary>
        public int? NumItineraries
        {
            get => GetAsInt(_numItineraries);
            set => _numItineraries = SetFromInt(value);
        }

        /// <summary>
        /// The set of characteristics that the user wants to optimize for.
        /// For options see <see cref="OptimizeType"/>
        /// </summary>
        public OptimizeType? Optimize
        {
            get => _optimize?.GetEnumValue<OptimizeType>();
            set => _optimize = value?.GetStringValue();
        }


        /// <summary>
        /// Penalty added for using every route that is not preferred if user set any route as preferred,
        /// i.e. number of seconds that we are willing to wait for preferred route.
        /// </summary>
        public int? OtherThanPreferredRoutesPenalty
        {
            get => GetAsInt(_otherThanPreferredRoutesPenalty);
            set => _otherThanPreferredRoutesPenalty = SetFromInt(value);
        }

        /// <summary>
        /// Set the method of sorting itineraries in the response. Right now, the only supported value is "duration";
        /// otherwise it uses default sorting. More sorting methods may be added in the future.
        /// </summary>
        public string PathComparator
        {
            get => _pathComparator;
            set => _pathComparator = value;
        }

        /// <summary>
        /// The comma-separated list of preferred agencies.
        /// </summary>
        public List<string> PreferredAgencies
        {
            get => _preferredAgencies?.Split(',').ToList();
            set => _preferredAgencies = SetFromStringCollection(value);
        }

        /// <summary>
        /// The list of preferred routes. The format is agency_[routename][_routeid],
        /// so TriMet_100 (100 is route short name) or Trimet__42 (two underscores, 42 is the route internal ID).
        /// </summary>
        public List<string> PreferredRoutes
        {
            get => _preferredRoutes?.Split(',').ToList();
            set => _preferredRoutes = SetFromStringCollection(value);
        }

        /// <summary>
        /// If true, this trip will be reverse-optimized on the fly.
        /// Otherwise, reverse-optimization will occur once a trip has been chosen (in Analyst, it will not be done at all).
        /// </summary>
        public bool? ReverseOptimizeOnTheFly
        {
            get => GetAsBool(_reverseOptimizeOnTheFly);
            set => _reverseOptimizeOnTheFly = SetFromBool(value);
        }

        /// <summary>
        /// Whether intermediate stops -- those that the itinerary passes in a vehicle,
        /// but does not board or alight at -- should be returned in the response.
        /// For example, on a Q train trip from Prospect Park to DeKalb Avenue, whether 7th Avenue and Atlantic Avenue
        /// should be included.
        /// </summary>
        public bool? ShowIntermediateStops
        {
            get => GetAsBool(_showIntermediateStops);
            set => _showIntermediateStops = SetFromBool(value);
        }

        /// <summary>
        /// A transit stop required to be the first stop in the search (AgencyId_StopId)
        /// </summary>
        public string StartTransitStopId
        {
            get => _startTransitStopId;
            set => _startTransitStopId = value;
        }

        /// <summary>
        /// A transit trip acting as a starting "state" for depart-onboard routing (AgencyId_TripId)
        /// </summary>
        public string StartTransitTripId
        {
            get => _startTransitTripId;
            set => _startTransitTripId = value;
        }

        /// <summary>
        /// The time that the trip should depart (or arrive, for requests where arriveBy is true).
        /// Format 3:15pm
        /// </summary>
        public DateTime? Time
        {
            get => _time != null ? (DateTime?) DateTime.Parse(_time) : null;
            set => _time = value != null ? $"{value:HH:mm}" : null;
        }

        /// <summary>
        /// The end location -- either latitude, longitude pair in degrees or a Vertex label.
        /// For example, 40.714476,-74.005966 or mtanyctsubway_A27_S
        /// </summary>
        public GeoCoordinate ToPlace
        {
            get => GetAsGeoCoordinate(_toPlace);
            set => _toPlace = SetFromGeoCoordinate(value);
        }

        /// <summary>
        /// An additional penalty added to boardings after the first. The value is in OTP's internal weight units,
        /// which are roughly equivalent to seconds. Set this to a high value to discourage transfers.
        /// Of course, transfers that save significant time or walking will still be taken.
        /// </summary>
        public int? TransferPenalty
        {
            get => GetAsInt(_transferPenalty);
            set => _transferPenalty = SetFromInt(value);
        }

        /// <summary>
        /// For bike triangle routing, how much safety matters (range 0-1)
        /// </summary>
        public double? TriangleSafetyFactor
        {
            get => GetAsDouble(_triangleSafetyFactor);
            set => _triangleSafetyFactor = SetFromDouble(value);
        }

        /// <summary>
        /// For bike triangle routing, how much slope matters (range 0-1).
        /// </summary>
        public double? TriangleSlopeFactor
        {
            get => GetAsDouble(_triangleSlopeFactor);
            set => _triangleSlopeFactor = SetFromDouble(value);
        }

        /// <summary>
        /// For bike triangle routing, how much time matters (range 0-1).
        /// </summary>
        public double? TriangleTimeFactor
        {
            get => GetAsDouble(_triangleTimeFactor);
            set => _triangleTimeFactor = SetFromDouble(value);
        }

        /// <summary>
        /// The comma-separated list of unpreferred agencies.
        /// </summary>
        public List<string> UnpreferredAgencies
        {
            get => _unpreferredAgencies?.Split(',').ToList();
            set => _unpreferredAgencies = SetFromStringCollection(value);
        }

        /// <summary>
        /// The list of unpreferred routes. The format is agency_[routename][_routeid],
        /// so TriMet_100 (100 is route short name) or Trimet__42 (two underscores, 42 is the route internal ID).
        /// </summary>
        public List<string> UnpreferredRoutes
        {
            get => _unpreferredRoutes?.Split(',').ToList();
            set => _unpreferredRoutes = SetFromStringCollection(value);
        }


        public bool? UseRequestedDateTimeInMaxHours
        {
            get => GetAsBool(_useRequestedDateTimeInMaxHours);
            set => _useRequestedDateTimeInMaxHours = SetFromBool(value);
        }

        /// <summary>
        /// How much less bad is waiting at the beginning of the trip (replaces waitReluctance)
        /// </summary>
        public double? WaitAtBeginningFactor
        {
            get => GetAsDouble(_waitAtBeginningFactor);
            set => _waitAtBeginningFactor = SetFromDouble(value);
        }

        /// <summary>
        /// How much worse is waiting for a transit vehicle than being on a transit vehicle, as a multiplier.
        /// The default value treats wait and on-vehicle time as the same. It may be tempting to set this higher
        /// than walkReluctance (as studies often find this kind of preferences among riders) but the planner will take
        /// this literally and walk down a transit line to avoid waiting at a stop.
        /// This used to be set less than 1 (0.95) which would make waiting offboard preferable to waiting onboard
        /// in an interlined trip. That is also undesirable. If we only tried the shortest possible transfer at each
        /// stop to neighboring stop patterns, this problem could disappear.
        /// </summary>
        public double? WaitReluctance
        {
            get => GetAsDouble(_waitReluctance);
            set => _waitReluctance = SetFromDouble(value);
        }

        /// <summary>
        /// Prevents unnecessary transfers by adding a cost for boarding a vehicle.
        /// This is the cost that is used when boarding while walking.
        /// </summary>
        public int? WalkBoardCost
        {
            get => GetAsInt(_walkBoardCost);
            set => _walkBoardCost = SetFromInt(value);
        }

        /// <summary>
        /// A multiplier for how bad walking is, compared to being in transit for equal lengths of time. Defaults to 2.
        /// Empirically, values between 10 and 20 seem to correspond well to the concept of not wanting to walk too much
        /// without asking for totally ridiculous itineraries, but this observation should in no way be taken
        /// as scientific or definitive. Your mileage may vary.
        /// </summary>
        public double? WalkReluctance
        {
            get => GetAsDouble(_walkReluctance);
            set => _walkReluctance = SetFromDouble(value);
        }

        /// <summary>
        /// The user's walking speed in meters/second. Defaults to approximately 3 MPH.
        /// </summary>
        public double? WalkSpeed
        {
            get => GetAsDouble(_walkSpeed);
            set => _walkSpeed = SetFromDouble(value);
        }

        /// <summary>
        /// Whether the trip must be wheelchair accessible.
        /// </summary>
        public bool? Wheelchair
        {
            get => GetAsBool(_wheelchair);
            set => _wheelchair = SetFromBool(value);
        }

        /// <summary>
        /// Functions the same as banned agencies, except only the listed agencies are allowed.
        /// </summary>
        public List<string> WhiteListedAgencies
        {
            get => _whiteListedAgencies?.Split(',').ToList();
            set => _whiteListedAgencies = SetFromStringCollection(value);
        }

        /// <summary>
        /// Functions the same as bannnedRoutes, except only the listed routes are allowed.
        /// </summary>
        public List<string> WhiteListedRoutes
        {
            get => _whiteListedRoutes?.Split(',').ToList();
            set => _whiteListedRoutes = SetFromStringCollection(value);
        }

        public IDictionary<string, string> GetParamsAsDictionary()
        {
            var paramDict = new Dictionary<string, string>();

            foreach (var propertyInfo in this.GetType().GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                var value = propertyInfo.GetValue(this);
                if (value != null && value is string)
                {
                    paramDict.Add(propertyInfo.Name[1..], value.ToString());
                }
            }

            return paramDict;
        }

        private List<string> GetAsStringCollection(string value) => value?.Split(',').ToList();

        private string SetFromStringCollection(ICollection<string> value) =>
            value != null && value.Count > 0 ? string.Join(',', value) : null;

        private bool? GetAsBool(string value) => value != null ? (bool?) bool.Parse(value) : null;
        private string SetFromBool(bool? value) => value?.ToString().ToLower();

        private double? GetAsDouble(string value) => value != null ? (double?) double.Parse(value) : null;

        private string SetFromDouble(double? value) =>
            value?.ToString(System.Globalization.CultureInfo.InvariantCulture);

        private int? GetAsInt(string value) => value != null ? (int?) int.Parse(value) : null;
        private string SetFromInt(int? value) => value?.ToString(System.Globalization.CultureInfo.InvariantCulture);

        private long? GetAsLong(string value) => value != null ? (long?) long.Parse(value) : null;
        private string SetFromLong(long? value) => value?.ToString(System.Globalization.CultureInfo.InvariantCulture);

        private GeoCoordinate GetAsGeoCoordinate(string value)
        {
            if (value == null) return null;
            var strings = value.Split(',');
            return new GeoCoordinate
            {
                Latitude = double.Parse(strings[0]),
                Longitude = double.Parse(strings[1])
            };
        }

        private string SetFromGeoCoordinate(GeoCoordinate value) => _toPlace = value != null
            ? $"{value.Latitude.ToString(System.Globalization.CultureInfo.InvariantCulture)},{value.Longitude.ToString(System.Globalization.CultureInfo.InvariantCulture)}"
            : null;
    }
}
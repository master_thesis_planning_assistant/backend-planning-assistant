﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    /// <summary>
    /// The purpose of Messages is to read supply Message.properties to underlying calling code...<br/>
    ///   The ENUM's enumerated values should be named to reflect the property names inside of Message.properties<br/>
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum Message
    {
        [EnumMember(Value = "PLAN_OK")] PlanOk,
        [EnumMember(Value = "SYSTEM_ERROR")] SystemError,

        [EnumMember(Value = "GRAPH_UNAVAILABLE")]
        GraphUnavailable,
        [EnumMember(Value = "OUTSIDE_BOUNDS")] OutsideBounds,
        [EnumMember(Value = "PATH_NOT_FOUND")] PathNotFound,

        [EnumMember(Value = "NO_TRANSIT_TIMES")]
        NoTransitTimes,

        [EnumMember(Value = "REQUEST_TIMEOUT")]
        RequestTimeout,

        [EnumMember(Value = "BOGUS_PARAMETER")]
        BogusParameter,

        [EnumMember(Value = "GEOCODE_FROM_NOT_FOUND")]
        GeocodeFromNotFound,

        [EnumMember(Value = "GEOCODE_TO_NOT_FOUND")]
        GeocodeToNotFound,

        [EnumMember(Value = "GEOCODE_FROM_TO_NOT_FOUND")]
        GeocodeFromToNotFound,
        [EnumMember(Value = "TOO_CLOSE")] TooClose,

        [EnumMember(Value = "LOCATION_NOT_ACCESSIBLE")]
        LocationNotAccessible,

        [EnumMember(Value = "GEOCODE_FROM_AMBIGUOUS")]
        GeocodeFromAmbiguous,

        [EnumMember(Value = "GEOCODE_TO_AMBIGUOUS")]
        GeocodeToAmbiguous,

        [EnumMember(Value = "GEOCODE_FROM_TO_AMBIGUOUS")]
        GeocodeFromToAmbiguous,

        [EnumMember(Value = "UNDERSPECIFIED_TRIANGLE")]
        UnderspecifiedTriangle,

        [EnumMember(Value = "TRIANGLE_NOT_AFFINE")]
        TriangleNotAffine,

        [EnumMember(Value = "TRIANGLE_OPTIMIZE_TYPE_NOT_SET")]
        TriangleOptimizeTypeNotSet,

        [EnumMember(Value = "TRIANGLE_VALUES_NOT_SET")]
        TriangleValuesNotSet,
    }
}
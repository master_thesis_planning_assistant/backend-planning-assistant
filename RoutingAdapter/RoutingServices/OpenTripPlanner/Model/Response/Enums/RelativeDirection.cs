﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    /// <summary>
    ///   Represents a turn direction, relative to the current heading.<br/>
    ///   CIRCLE_CLOCKWISE and CIRCLE_CLOCKWISE are used to represent traffic circles.<br/>
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RelativeDirection
    {
        [EnumMember(Value = "DEPART")] Depart,
        [EnumMember(Value = "HARD_LEFT")] HardLeft,
        [EnumMember(Value = "LEFT")] Left,
        [EnumMember(Value = "SLIGHTLY_LEFT")] SlightlyLeft,
        [EnumMember(Value = "CONTINUE")] Continue,
        [EnumMember(Value = "SLIGHTLY_RIGHT")] SlightlyRight,
        [EnumMember(Value = "RIGHT")] Right,
        [EnumMember(Value = "HARD_RIGHT")] HardRight,

        [EnumMember(Value = "CIRCLE_CLOCKWISE")]
        CircleClockwise,

        [EnumMember(Value = "CIRCLE_COUNTERCLOCKWISE")]
        CircleCounterclockwise,
        [EnumMember(Value = "ELEVATOR")] Elevator,
        [EnumMember(Value = "UTURN_LEFT")] UturnLeft,
        [EnumMember(Value = "UTURN_RIGHT")] UturnRight,
    }
}
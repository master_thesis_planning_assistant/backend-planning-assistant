﻿using System.Runtime.Serialization;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    // [JsonConverter(typeof(StringEnumConverter))]
    public enum TraverseMode
    {
        [EnumMember(Value = "WALK")] Walk,
        [EnumMember(Value = "BICYCLE")] Bicycle,
        [EnumMember(Value = "CAR")] Car,
        [EnumMember(Value = "TRAM")] Tram,
        [EnumMember(Value = "SUBWAY")] Subway,
        [EnumMember(Value = "RAIL")] Rail,
        [EnumMember(Value = "BUS")] Bus,
        [EnumMember(Value = "FERRY")] Ferry,
        [EnumMember(Value = "CABLE_CAR")] CableCar,
        [EnumMember(Value = "GONDOLA")] Gondola,
        [EnumMember(Value = "FUNICULAR")] Funicular,
        [EnumMember(Value = "TRANSIT")] Transit,
        [EnumMember(Value = "TRAINISH")] Trainish,
        [EnumMember(Value = "BUSISH")] Busish,
        [EnumMember(Value = "LEG_SWITCH")] LegSwitch,
    }
}
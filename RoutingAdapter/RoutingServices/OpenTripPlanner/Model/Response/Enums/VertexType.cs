﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    /// <summary>
    ///   Represent type of vertex, used in Place aka from, to in API for easier client side localization
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum VertexType
    {
        [EnumMember(Value = "NORMAL")] Normal,
        [EnumMember(Value = "BIKESHARE")] BikeShare,
        [EnumMember(Value = "BIKEPARK")] Bikepark,
        [EnumMember(Value = "TRANSIT")] Transit,
    }
}
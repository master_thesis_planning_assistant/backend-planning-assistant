﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FareType
    {
        [EnumMember(Value = "Regular")] Regular,
        [EnumMember(Value = "Student")] Student,
        [EnumMember(Value = "Senior")] Senior,
        [EnumMember(Value = "Tram")] Tram,
        [EnumMember(Value = "Special")] Special,
        [EnumMember(Value = "Youth")] Youth,
    }
}
﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums
{
    /// <summary>An absolute cardinal or intermediate direction.</summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum AbsoluteDirection
    {
        [EnumMember(Value = "NORTH")] North,
        [EnumMember(Value = "NORTHEAST")] Northeast,
        [EnumMember(Value = "EAST")] East,
        [EnumMember(Value = "SOUTHEAST")] Southeast,
        [EnumMember(Value = "SOUTH")] South,
        [EnumMember(Value = "SOUTHWEST")] Southwest,
        [EnumMember(Value = "WEST")] West,
        [EnumMember(Value = "NORTHWEST")] Northwest,
    }
}
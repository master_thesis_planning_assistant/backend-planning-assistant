﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>An Itinerary is one complete way of getting from the start location to the end location.</summary>
    public class Itinerary
    {
        /// <summary>Duration of the trip on this itinerary, in seconds.</summary>
        public long? Duration { get; set; }

        /// <summary>
        ///   How much elevation is gained, in total, over the course of the trip, in meters. See elevationLost.
        /// </summary>
        public double? ElevationGained { get; set; }

        /// <summary>
        ///   How much elevation is lost, in total, over the course of the trip, in meters. As an example,<br/>
        ///   a trip that went from the top of Mount Everest straight down to sea level, then back up K2,<br/>
        ///   then back down again would have an elevationLost of Everest + K2.<br/>
        /// </summary>
        public double? ElevationLost { get; set; }

        /// <summary>Time that the trip arrives.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? EndTime { get; set; }

        /// <summary>The cost of this trip</summary>
        public Fare Fare { get; set; }

        /// <summary>
        ///   A list of Legs. Each Leg is either a walking (cycling, car) portion of the trip, or a transit<br/>
        ///   trip on a particular vehicle. So a trip where the use walks to the Q train, transfers to the<br/>
        ///   6, then walks to their destination, has four legs.<br/>
        /// </summary>
        public Leg[] Legs { get; set; }

        /// <summary>Time that the trip departs.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? StartTime { get; set; }

        /// <summary>
        ///   This itinerary has a greater slope than the user requested (but there are no possible<br/>
        ///   itineraries with a good slope).<br/>
        /// </summary>
        public bool TooSloped { get; set; }

        /// <summary>The number of transfers this trip has.</summary>
        public int? Transfers { get; set; }

        /// <summary>How much time is spent on transit, in seconds.</summary>
        public long TransitTime { get; set; }

        /// <summary>How much time is spent waiting for transit to arrive, in seconds.</summary>
        public long WaitingTime { get; set; }

        /// <summary>How far the user has to walk, in meters.</summary>
        public double? WalkDistance { get; set; }

        /// <summary>Indicates that the walk limit distance has been exceeded for this itinerary when true.</summary>
        public bool WalkLimitExceeded { get; set; }

        /// <summary>How much time is spent walking, in seconds.</summary>
        public long WalkTime { get; set; }
    }
}
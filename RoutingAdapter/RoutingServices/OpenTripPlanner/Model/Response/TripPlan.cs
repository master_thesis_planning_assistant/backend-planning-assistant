﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>A TripPlan is a set of ways to get from point A to point B at time T.</summary>
    public class TripPlan
    {
        /// <summary>The time and date of travel</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? Date { get; set; }

        /// <summary>The origin</summary>
        public Place From { get; set; }

        /// <summary>A list of possible itineraries</summary>
        public Itinerary[] Itineraries { get; set; }

        /// <summary>The destination</summary>
        public Place To { get; set; }
    }
}
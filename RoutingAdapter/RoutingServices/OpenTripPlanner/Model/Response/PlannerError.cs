﻿using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>This API response element represents an error in trip planning.</summary>
    public class PlannerError
    {
        public int Id { get; set; }
        public Message? Message { get; set; }
        public string[] Missing { get; set; }
        public string Msg { get; set; }
        public bool NoPath { get; set; }
    }
}
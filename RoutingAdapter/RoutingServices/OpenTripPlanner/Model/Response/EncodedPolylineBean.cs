﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>
    ///   A list of coordinates encoded as a string.<br/>
    ///   See <a href="http://code.google.com/apis/maps/documentation/polylinealgorithm.html">Encoded<br/>
    ///   polyline algorithm format</a><br/>
    /// </summary>
    public class EncodedPolylineBean
    {
        /// <summary>The number of points in the string</summary>
        public int Length { get; set; }

        /// <summary>
        ///   Levels describes which points should be shown at various zoom levels. Presently, we show all<br/>
        ///   points at all zoom levels.<br/>
        /// </summary>
        public string Levels { get; set; }

        /// <summary>The encoded points of the polyline.</summary>
        public string Points { get; set; }
    }
}
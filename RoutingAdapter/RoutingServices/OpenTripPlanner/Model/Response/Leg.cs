﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>
    ///   One leg of a trip -- that is, a temporally continuous piece of the journey that takes place on a
    ///   particular vehicle (or on foot).
    /// </summary>
    public class Leg
    {
        /// <summary>
        ///   For transit legs, the ID of the transit agency that operates the service used for this leg.
        ///   For non-transit legs, null.
        /// </summary>
        public string AgencyId { get; set; }

        public string AgencyName { get; set; }
        public int AgencyTimeZoneOffset { get; set; }
        public string AgencyUrl { get; set; }
        public string AgencyBrandingUrl { get; set; }

        public Alert[] Alerts { get; set; }
        public string AlightRule { get; set; }

        /// <summary>
        ///   For transit leg, the offset from the scheduled arrival-time of the alighting stop in this leg.<br/>
        ///   "scheduled time of arrival at alighting stop" = endTime - arrivalDelay
        /// </summary>
        public int ArrivalDelay { get; set; }

        public string BoardRule { get; set; }

        /// <summary>
        ///   For transit leg, the offset from the scheduled departure-time of the boarding stop in this leg.<br/>
        ///   "scheduled time of departure at boarding stop" = startTime - departureDelay   
        /// </summary>
        public int DepartureDelay { get; set; }

        /// <summary>The distance traveled while traversing the leg in meters.</summary>
        public double? Distance { get; set; }

        /// <summary>The leg's duration in seconds</summary>
        public double Duration { get; set; }

        /// <summary>The date and time this leg ends.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? EndTime { get; set; }

        /// <summary>The Place where the leg originates.</summary>
        public Place From { get; set; }

        /// <summary>For transit legs, the headsign of the bus or train being used. For non-transit legs, null.</summary>
        public string Headsign { get; set; }

        /// <summary>
        ///   The best estimate of the time between two arriving vehicles. This is particularly important<br/>
        ///   for non-strict frequency trips, but could become important for real-time trips, strict<br/>
        ///   frequency trips, and scheduled trips with empirical headways.
        /// </summary>
        public int? Headway { get; set; }

        /// <summary>For transit legs, if the rider should stay on the vehicle as it changes route names.</summary>
        public bool? InterlineWithPreviousLeg { get; set; }

        /// <summary>
        ///   For transit legs, intermediate stops between the Place where the leg originates and the Place where the leg ends.<br/>
        ///   For non-transit legs, null.<br/>
        ///   This field is optional i.e. it is always null unless "showIntermediateStops" parameter is set to "true" in the planner request.<br/>
        /// </summary>
        public Place[] IntermediateStops { get; set; }

        /// <summary>Is this a frequency-based trip with non-strict departure times?</summary>
        public bool? IsNonExactFrequency { get; set; }

        /// <summary>The leg's geometry.</summary>
        public EncodedPolylineBean LegGeometry { get; set; }

        /// <summary>The mode (e.g., <tt>Walk</tt>) used when traversing this leg.</summary>
        public TraverseMode Mode { get; set; }

        /// <summary>
        ///  Deprecated field formerly used for notes -- will be removed. See alerts 
        /// </summary>
        public Note[] Notes { get; set; }

        /// <summary>Is this leg a traversing pathways?</summary>
        public bool? Pathway { get; set; }

        /// <summary>Whether there is real-time data about this Leg</summary>
        public bool? RealTime { get; set; }

        public bool? RentedBike { get; set; }

        public int? FlexCallAndRideMaxStartTime { get; set; }
        public int? FlexCallAndRideMinEndTime { get; set; }

        /// <summary>
        /// trip.drt_advance_book_min if this is a demand-response leg
        /// </summary>
        public string FlexDrtAdvanceBookMin { get; set; }

        /// <summary>
        /// Agency message if this is leg has a demand-response pickup and the Trip has `drt_pickup_message` defined.
        /// </summary>
        public string FlexDrtPickupMessage { get; set; }

        /// <summary>
        /// Agency message if this is leg has a demand-response dropoff and the Trip has `drt_drop_off_message` defined.
        /// </summary>
        public string FlexDrtDropOffMessage { get; set; }

        /// <summary>
        /// Agency message if this is leg has a flag stop pickup and the Trip has `continuous_pickup_message` defined.
        /// </summary>
        public string FlexFlagStopPickupMessage { get; set; }

        /// <summary>
        /// Agency message if this is leg has a flag stop dropoff and the Trip has `continuous_drop_off_message` defined.
        /// </summary>
        public string FlexFlagStopDropOffMessage { get; set; }

        /// <summary>
        ///   For transit legs, the route of the bus or train being used. For non-transit legs, the name of 
        ///   the street being traversed.
        /// </summary>
        public string Route { get; set; }

        /// <summary>
        /// For transit leg, the route's branding URL (if one exists). For non-transit legs, null.
        /// </summary>
        public string RouteBrandingUrl { get; set; }

        /// <summary>For transit leg, the route's (background) color (if one exists). For non-transit legs, null.</summary>
        public string RouteColor { get; set; }

        /// <summary>
        ///   For transit legs, the ID of the route. For non-transit legs, null.
        /// </summary>
        public string RouteId { get; set; }

        public string RouteLongName { get; set; }

        public string RouteShortName { get; set; }

        /// <summary>For transit leg, the route's text color (if one exists). For non-transit legs, null.</summary>
        public string RouteTextColor { get; set; }

        /// <summary>
        ///   For transit legs, the type of the route. Non transit -1<br/>
        ///   When 0-7: 0 Tram, 1 Subway, 2 Train, 3 Bus, 4 Ferry, 5 Cable Car, 6 Gondola, 7 Funicular<br/>
        ///   When equal or highter than 100, it is coded using the Hierarchical Vehicle Type (HVT) codes from the European TPEG standard<br/>
        ///   Also see http://groups.google.com/group/gtfs-changes/msg/ed917a69cf8c5bef<br/>
        /// </summary>
        public int? RouteType { get; set; }

        /// <summary>
        ///   For transit legs, the service date of the trip. For non-transit legs, null.
        /// </summary>
        public string ServiceDate { get; set; }

        /// <summary>The date and time this leg begins.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? StartTime { get; set; }

        /// <summary>A series of turn by turn instructions used for walking, biking and driving.</summary>
        public WalkStep[] Steps { get; set; }

        /// <summary>The Place where the leg begins.</summary>
        public Place To { get; set; }

        /// <summary>For transit leg, the trip's block ID (if one exists). For non-transit legs, null.</summary>
        public string TripBlockId { get; set; }

        /// <summary>
        ///   For transit legs, the ID of the trip. For non-transit legs, null.
        /// </summary>
        public string TripId { get; set; }

        /// <summary>For transit leg, the trip's short name (if one exists). For non-transit legs, null.</summary>
        public string TripShortName { get; set; }

        /// <summary>
        /// Whether this leg is a transit leg or not.
        /// </summary>
        public bool? TransitLeg { get; set; }
    }
}
﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    public class ElevationMetadata
    {
        public double EllipsoidToGeoidDifference { get; set; }
        public bool GeoidElevation { get; set; }
    }
}
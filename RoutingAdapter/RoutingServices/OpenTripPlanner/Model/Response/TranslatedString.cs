﻿using System.Collections.Generic;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    public class TranslatedString
    {
        public IDictionary<string, string> Translations { get; set; }
    }
}
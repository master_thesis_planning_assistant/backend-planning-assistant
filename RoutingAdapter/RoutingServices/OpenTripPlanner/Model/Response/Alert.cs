﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    public class Alert
    {
        public TranslatedString AlertDescriptionText { get; set; }
        public TranslatedString AlertHeaderText { get; set; }
        public TranslatedString AlertUrl { get; set; }

        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? EffectiveStartDate { get; set; }
    }
}
﻿using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>A mapping from FareType to Money.</summary>
    public class FareEntry
    {
        public FareType Key { get; set; }
        public Money Value { get; set; }
    }
}
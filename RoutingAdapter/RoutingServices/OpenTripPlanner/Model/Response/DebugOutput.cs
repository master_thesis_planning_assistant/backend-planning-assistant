﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>
    ///   Holds information to be included in the REST Response for debugging and profiling purposes.<br/>
    ///   startedCalculating is called in the routingContext constructor.<br/>
    ///   finishedCalculating and finishedRendering are all called in PlanGenerator.generate().<br/>
    ///   finishedPrecalculating and foundPaths are called in the SPTService implementations.<br/>
    /// </summary>
    public class DebugOutput
    {
        public long PathCalculationTime { get; set; }
        public long[] PathTimes { get; set; }
        public long PrecalculationTime { get; set; }
        public long RenderingTime { get; set; }
        public bool TimedOut { get; set; }
        public long TotalTime { get; set; }
    }
}
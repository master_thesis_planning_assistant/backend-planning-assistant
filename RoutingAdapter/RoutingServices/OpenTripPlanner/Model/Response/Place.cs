﻿using System;
using Newtonsoft.Json;
using RoutingAdapter.Infrastructure.JsonConverter;
using RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>A Place is where a journey starts or ends, or a transit stop along the way.</summary>
    public class Place
    {
        /// <summary>The time the rider will arrive at the place.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? Arrival { get; set; }

        /// <summary>The time the rider will depart the place.</summary>
        [JsonConverter(typeof(JavaTimeStampToNullableDateTimeJsonConverter))]
        public DateTime? Departure { get; set; }

        /// <summary>The latitude of the place.</summary>
        public double? Lat { get; set; }

        /// <summary>The longitude of the place.</summary>
        public double? Lon { get; set; }

        /// <summary>For transit stops, the name of the stop. For points of interest, the name of the POI.</summary>
        public string Name { get; set; }

        public string Orig { get; set; }

        /// <summary>The code or name identifying the quay/platform the vehicle will arrive at or depart from</summary>
        public string PlatformCode { get; set; }

        /// <summary>
        ///   The "code" of the stop. Depending on the transit agency, this is often<br/>
        ///   something that users care about.             
        /// </summary>
        public string StopCode { get; set; }

        /// <summary>The ID of the stop. This is often something that users don't care about.</summary>
        public string StopId { get; set; }

        /// <summary>For transit trips, the stop index (numbered from zero from the start of the trip</summary>
        public int? StopIndex { get; set; }

        /// <summary>For transit trips, the sequence number of the stop. Per GTFS, these numbers are increasing.</summary>
        public int? StopSequence { get; set; }

        public string ZoneId { get; set; }

        /// <summary>
        /// Type of vertex. (Normal, Bike sharing station, Bike P+R, Transit stop) Mostly used for better localization
        /// of bike sharing and P+R station names
        /// </summary>
        public VertexType VertexType { get; set; }

        /// <summary>
        /// In case the vertex is of type Bike sharing station.
        /// </summary>
        public string BikeShareId { get; set; }

        /// <summary>
        /// This is an optional field which can be used to distinguish among ways a passenger's boarding or alighting at
        /// a stop can differ among services operated by a transit agency. This will be "default" in most cases.
        /// Currently the only non-default values are for GTFS-Flex board or alight types.
        /// </summary>
        public string BoardAlightType { get; set; }

        /// <summary>
        /// Board or alight area for flag stops
        /// A list of coordinates encoded as a string. See Encoded polyline algorithm format
        /// </summary>
        public string FlagStopArea { get; set; }
    }
}
﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary><strong>Fare support is very, very preliminary.</strong></summary>
    public class Money
    {
        public int Cents { get; set; }
        public WrappedCurrency Currency { get; set; }
    }
}
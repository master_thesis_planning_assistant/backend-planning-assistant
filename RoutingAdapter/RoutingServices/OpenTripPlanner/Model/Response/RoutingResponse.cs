﻿using System.Collections.Generic;

namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>Represents a trip planner response, will be serialized into XML or JSON by Jersey</summary>
    public class RoutingResponse
    {
        /// <summary>A dictionary of the parameters provided in the request that triggered this response.</summary>
        public IDictionary<string, string> RequestParameters { get; set; }

        /// <summary>Debugging and profiling information</summary>
        public DebugOutput DebugOutput { get; set; }

        public ElevationMetadata ElevationMetadata { get; set; }

        /// <summary>The actual trip plan.</summary>
        public TripPlan Plan { get; set; }

        /// <summary>The error (if any) that this response raised.</summary>
        public PlannerError Error { get; set; }
    }
}
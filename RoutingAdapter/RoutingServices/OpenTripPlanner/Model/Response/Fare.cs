﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    /// <summary>
    ///   Fare is a set of fares for different classes of users.<br/>
    /// </summary>
    public class Fare
    {
        /// <summary>A mapping from FareType to Money.</summary>
        public FareEntry[] Fares { get; set; }
    }
}
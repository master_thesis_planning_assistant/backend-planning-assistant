﻿namespace RoutingAdapter.RoutingServices.OpenTripPlanner.Model.Response
{
    public class Note
    {
        public string Text { get; set; }
    }
}
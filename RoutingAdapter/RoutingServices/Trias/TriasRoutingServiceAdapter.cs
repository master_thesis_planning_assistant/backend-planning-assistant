﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;
using RoutingAdapter.RoutingServices.Trias.Client;
using RoutingAdapter.RoutingServices.Trias.Model.Request;
using RoutingAdapter.RoutingServices.Trias.Model.Response;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;
using RoutingAdapter.Services.XsltTransformer;

namespace RoutingAdapter.RoutingServices.Trias
{
    /// <summary>
    /// Adapter for the Trias Routing-Service of the Karlsruher Verkehrs Verbund (KVV).
    /// Trias: https://www.vdv.de/ip-kom-oev.aspx
    /// Model: https://github.com/VDVde/TRIAS
    /// </summary>
    public class TriasRoutingServiceAdapter : BaseRoutingService<RoutingRequest, RoutingResponse>
    {
        private readonly ITriasClient _client;
        private readonly ILogger<TriasRoutingServiceAdapter> _logger;

        public TriasRoutingServiceAdapter(ITriasClient client, ILogger<TriasRoutingServiceAdapter> logger,
            IXsltTransformerService xsltTransformer) :
            base(logger, xsltTransformer)
        {
            _client = client;
            _logger = logger;
        }

        public override string ServiceName => "Trias";


        public override ICollection<RoutingMode> SupportedModalities =>
            new List<RoutingMode> {RoutingMode.PublicTransport};

        protected override Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeSpecificRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            return _client.MakeRoutingRequestAsync(requestWrapper);
        }

        protected override ICollection<SingleRoutingRequest> HandleModalityCardinality(
            SingleRoutingRequest singleRoutingRequest, ICollection<RoutingMode> modalities)
        {
            // Trias can handle multiple modalities in one request!
            // see RoutingRequest > ServiceRequest > RequestPayload > TripParams > PtModeFilter > PtModes
            return new List<SingleRoutingRequest>
            {
                new SingleRoutingRequest
                {
                    Preferences = singleRoutingRequest.Preferences,
                    MobilityDemand = singleRoutingRequest.MobilityDemand,
                    Modalities = modalities.ToList()
                }
            };
        }

        protected override RoutesForActivity BuildResultingRoutes(
            SpecificRoutingResponseWrapper<RoutingResponse> specificRoutingResponseWrapper,
            SpecificRoutingRequestWrapper<RoutingRequest> specificRoutingRequestWrapper,
            MobilityDemand originalMobilityDemand)
        {
            var errors = specificRoutingResponseWrapper.RoutingResponse.ServiceDelivery?.DeliveryPayload?.TripResponse
                ?.ErrorMessage;
            if (errors != null && errors.Any())
            {
                var errorText = string.Join(" | ", errors.Select(e => e.GetErrorMessageString()));
                throw new ApplicationException($"Trias responded with error. {errorText}");
            }

            var result = new RoutesForActivity
            {
                ActivityGuid = originalMobilityDemand.ActivityGuid,
                Routes = new List<ActivityRoute>()
            };

            var trips = specificRoutingResponseWrapper.RoutingResponse?.ServiceDelivery?.DeliveryPayload?.TripResponse
                ?.TripResult;
            if (trips == null || !trips.Any())
            {
                _logger.LogWarning($"No trips found by {ServiceName}");
                return result;
            }

            foreach (var trip in trips)
            {
                var newRoute = new ActivityRoute
                {
                    Guid = Guid.NewGuid().ToString(),
                    ServiceName = ServiceName,
                    Polyline = null,
                    Start = originalMobilityDemand.From,
                    Departure = trip.Trip.StartTime,
                    Destination = originalMobilityDemand.To,
                    Arrival = trip.Trip.EndTime,
                    Bounds = null,
                    Duration = trip.Trip.DurationInternal ?? 0,
                    Distance = trip.Trip.Distance,
                    Legs = new List<ActivityRouteLeg>(),
                };

                var lastArrival = newRoute.Arrival;
                foreach (var tripLeg in trip.Trip.TripLeg)
                {
                    if (tripLeg.ContinuousLeg != null)
                    {
                        var projection = tripLeg.ContinuousLeg.LegTrack.TrackSection[0].Projection.Position.Select(
                            position => new GeoLocation
                            {
                                Longitude = position.Longitude,
                                Latitude = position.Latitude
                            }).ToList();

                        var newLeg = new ActivityRouteLeg
                        {
                            Modality = GetModeEquivalent(tripLeg.ContinuousLeg.Service.IndividualMode),
                            StartLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.ContinuousLeg.LegStart.LocationName.Text,
                                GeoLocation = projection.First(),
                            },
                            Departure = tripLeg.ContinuousLeg.TimeWindowStart ?? lastArrival,
                            EndLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.ContinuousLeg.LegEnd.LocationName.Text,
                                GeoLocation = projection.Last(),
                            },
                            Arrival = tripLeg.ContinuousLeg.TimeWindowStart ?? lastArrival +
                                TimeSpan.FromSeconds(tripLeg.ContinuousLeg.DurationInternal ?? 0),
                            Polyline = new Polyline
                            {
                                Points = projection,
                            },
                            Duration = tripLeg.ContinuousLeg.DurationInternal ?? 0,
                            Distance = tripLeg.ContinuousLeg.Length ?? 0,
                            Steps = new List<ActivityRouteStep>(),
                        };
                        lastArrival = newLeg.Arrival;

                        foreach (var navigationSection in tripLeg.ContinuousLeg.NavigationPath.NavigationSection)
                        {
                            var stepProjection = navigationSection.TrackSection?.Projection?.Position.Select(position =>
                                new GeoLocation
                                {
                                    Latitude = position.Latitude,
                                    Longitude = position.Longitude
                                }).ToList() ?? new List<GeoLocation>();

                            var newStep = new ActivityRouteStep
                            {
                                StartLocation = new NamedGeoLocation
                                {
                                    Name = navigationSection.TrackSection.RoadName,
                                    GeoLocation = stepProjection.FirstOrDefault()
                                },
                                EndLocation = new NamedGeoLocation
                                {
                                    Name = navigationSection.TrackSection.RoadName,
                                    GeoLocation = stepProjection.LastOrDefault()
                                },
                                Polyline = new Polyline
                                {
                                    Points = stepProjection,
                                },
                                Duration = navigationSection.TrackSection.DurationInternal ?? 0,
                                Distance = navigationSection.TrackSection.Length,
                                Action =
                                    $"{navigationSection.Manoeuvre.ToString()} - {navigationSection.TurnAction.ToString()}"
                            };
                            newLeg.Steps.Add(newStep);
                        }

                        newRoute.Legs.Add(newLeg);
                    }
                    else if (tripLeg.InterchangeLeg != null)
                    {
                        var newLeg = new ActivityRouteLeg
                        {
                            Modality = tripLeg.InterchangeLeg.InterchangeMode.HasValue
                                ? GetModeEquivalent(tripLeg.InterchangeLeg.InterchangeMode.Value)
                                : RoutingMode.PublicTransport,
                            Description = "Umstieg: " + string.Join(" | ",
                                tripLeg.InterchangeLeg.LegDescription.Select(d => d.Text)),
                            StartLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.InterchangeLeg.LegStart.LocationName.Text,
                                GeoLocation = tripLeg.InterchangeLeg.LegStart.GeoPosition != null
                                    ? new GeoLocation
                                    {
                                        Longitude = tripLeg.InterchangeLeg.LegStart.GeoPosition.Longitude,
                                        Latitude = tripLeg.InterchangeLeg.LegStart.GeoPosition.Latitude
                                    }
                                    : null
                            },
                            Departure = tripLeg.InterchangeLeg.TimeWindowEnd ?? default, // todo how to handle
                            EndLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.InterchangeLeg.LegEnd.LocationName.Text,
                                GeoLocation = tripLeg.InterchangeLeg.LegEnd.GeoPosition != null
                                    ? new GeoLocation
                                    {
                                        Longitude = tripLeg.InterchangeLeg.LegEnd.GeoPosition.Longitude,
                                        Latitude = tripLeg.InterchangeLeg.LegEnd.GeoPosition.Latitude
                                    }
                                    : null
                            },
                            Arrival = tripLeg.InterchangeLeg.TimeWindowStart ?? default, // todo how to handle,
                            Polyline = null,
                            Duration = tripLeg.InterchangeLeg.DurationInternal ?? 0,
                            Distance = 0,
                            Steps = new List<ActivityRouteStep>()
                        };

                        foreach (var navigationSection in tripLeg.InterchangeLeg.NavigationPath.NavigationSection)
                        {
                            var stepProjection = navigationSection.TrackSection.Projection.Position.Select(position =>
                                new GeoLocation
                                {
                                    Latitude = position.Latitude,
                                    Longitude = position.Longitude
                                }).ToList();

                            var newStep = new ActivityRouteStep
                            {
                                StartLocation = new NamedGeoLocation
                                {
                                    Name = navigationSection.TrackSection.RoadName,
                                    GeoLocation = stepProjection.First(),
                                },
                                EndLocation = new NamedGeoLocation
                                {
                                    Name = navigationSection.TrackSection.RoadName,
                                    GeoLocation = stepProjection.Last(),
                                },
                                Polyline = new Polyline
                                {
                                    Points = stepProjection,
                                },
                                Duration = navigationSection.TrackSection.DurationInternal ?? 0,
                                Distance = navigationSection.TrackSection.Length,
                                Action = $"{navigationSection.Manoeuvre} {navigationSection.TurnAction}"
                            };
                            newLeg.Steps.Add(newStep);
                        }

                        newLeg.Distance = newLeg.Steps.Select(s => s.Distance).Sum();
                        newLeg.StartLocation.GeoLocation = newLeg.Steps.First().StartLocation.GeoLocation;
                        newLeg.EndLocation.GeoLocation = newLeg.Steps.Last().EndLocation.GeoLocation;
                        newLeg.Polyline = new Polyline
                        {
                            Points = newLeg.Steps.Aggregate(new List<GeoLocation>(),
                                (points, step) => (points.Concat(step.Polyline.Points)).ToList()).ToList(),
                        };
                        newRoute.Legs.Add(newLeg);
                    }
                    else //timed Leg
                    {
                        var projection = tripLeg.TimedLeg.LegTrack.TrackSection.First().Projection.Position.Select(
                            position => new GeoLocation
                            {
                                Latitude = position.Latitude,
                                Longitude = position.Longitude,
                            }).ToList();
                        var board = tripLeg.TimedLeg.LegBoard;
                        var intermediates = tripLeg.TimedLeg.LegIntermediates;
                        var alight = tripLeg.TimedLeg.LegAlight;
                        var newLeg = new ActivityRouteLeg
                        {
                            Modality = GetModeEquivalent(tripLeg.TimedLeg.Service.Mode.PtMode),
                            Description = tripLeg.TimedLeg.Service.PublishedLineName.Text,
                            StartLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.TimedLeg.LegTrack.TrackSection.First().TrackStart.LocationName.Text,
                                GeoLocation = projection.FirstOrDefault(),
                            },
                            Departure = board.ServiceDeparture.TimetabledTime,
                            EndLocation = new NamedGeoLocation
                            {
                                Name = tripLeg.TimedLeg.LegTrack.TrackSection.First().TrackEnd.LocationName.Text,
                                GeoLocation = projection.LastOrDefault(),
                            },
                            Arrival = alight.ServiceArrival.TimetabledTime,
                            Polyline = new Polyline
                            {
                                Points = projection,
                            },
                            Duration = tripLeg.TimedLeg.LegTrack.TrackSection.First().DurationInternal ?? 0,
                            Distance = tripLeg.TimedLeg.LegTrack.TrackSection.First().Length,
                            Steps = new List<ActivityRouteStep>(),
                        };

                        var firstStep = new ActivityRouteStep
                        {
                            StartLocation = new NamedGeoLocation
                            {
                                Name = board.StopPointName.First().Text,
                            },
                            EndLocation = null,
                            Polyline = null,
                            Duration = 0,
                            Distance = 0,
                            Action = "Einsteigen"
                        };
                        newLeg.Steps.Add(firstStep);
                        var prefStep = firstStep;
                        var prefDepature = board.ServiceDeparture.TimetabledTime;

                        foreach (var intermediate in intermediates)
                        {
                            var intermediateStep = new ActivityRouteStep
                            {
                                StartLocation = new NamedGeoLocation
                                {
                                    Name = intermediate.StopPointName.First().Text,
                                },
                                EndLocation = null,
                                Polyline = null,
                                Duration = 0,
                                Distance = 0,
                                Action = "Zwischenhalt"
                            };
                            newLeg.Steps.Add(intermediateStep);
                            prefStep.EndLocation = intermediateStep.StartLocation;
                            prefStep.Duration =
                                (intermediate.ServiceArrival.TimetabledTime - prefDepature).TotalSeconds;
                            prefStep = intermediateStep;
                            prefDepature = intermediate.ServiceDeparture.TimetabledTime;
                        }

                        var lastStep = new ActivityRouteStep
                        {
                            StartLocation = new NamedGeoLocation
                            {
                                Name = alight.StopPointName.First().Text,
                            },
                            EndLocation = new NamedGeoLocation
                            {
                                Name = alight.StopPointName.First().Text,
                            },
                            Polyline = null,
                            Duration = 0,
                            Distance = 0,
                            Action = "Aussteigen"
                        };
                        prefStep.EndLocation = lastStep.EndLocation;
                        prefStep.Duration = (alight.ServiceArrival.TimetabledTime - prefDepature).TotalSeconds;
                        newLeg.Steps.Add(lastStep);
                        newRoute.Legs.Add(newLeg);
                    }
                }

                newRoute.Polyline = new Polyline
                {
                    Points = newRoute.Legs.Aggregate(new List<GeoLocation>(),
                        (points, leg) => (points.Concat(leg.Polyline.Points)).ToList()).ToList(),
                };
                newRoute.Bounds = newRoute.Polyline.GetBounds();
                result.Routes.Add(newRoute);
            }

            return result;
        }

        private RoutingMode GetModeEquivalent(InterchangeMode specificMode)
        {
            return specificMode switch
            {
                InterchangeMode.Walk => RoutingMode.Walk,
                InterchangeMode.ParkAndRide => RoutingMode.Car,
                InterchangeMode.BikeAndRide => RoutingMode.Bike,
                InterchangeMode.CarHire => RoutingMode.CarSharing,
                InterchangeMode.BikeHire => RoutingMode.BikeSharing,
                InterchangeMode.ProtectedConnection => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                InterchangeMode.GuaranteedConnection => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                InterchangeMode.RemainInVehicle => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                InterchangeMode.ChangeWithinVehicle => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                InterchangeMode.CheckIn => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                InterchangeMode.CheckOut => RoutingMode
                    .PublicTransport, // handle with PublicTransport since passenger does not have to move
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to InterchangeMode {specificMode.GetStringValue()} found")
            };
        }

        private RoutingMode GetModeEquivalent(IndividualModes specificMode)
        {
            return specificMode switch
            {
                IndividualModes.Walk => RoutingMode.Walk,
                IndividualModes.Cycle => RoutingMode.Bike,
                IndividualModes.Taxi => RoutingMode.Car,
                IndividualModes.SelfDriveCar => RoutingMode.Car,
                IndividualModes.OthersDriveCar => RoutingMode.Car,
                IndividualModes.Motorcycle => RoutingMode.Car,
                IndividualModes.Truck => RoutingMode.Car,
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to IndividualModes {specificMode.GetStringValue()} found")
            };
        }

        private RoutingMode GetModeEquivalent(PtMode specificMode)
        {
            return specificMode switch
            {
                // PtMode.All => ,
                // PtMode.Unknown => ,
                PtMode.Air => RoutingMode.PublicTransport,
                PtMode.Bus => RoutingMode.PublicTransport,
                PtMode.TrolleyBus => RoutingMode.PublicTransport,
                PtMode.Tram => RoutingMode.PublicTransport,
                PtMode.Coach => RoutingMode.PublicTransport,
                PtMode.Rail => RoutingMode.PublicTransport,
                PtMode.IntercityRail => RoutingMode.PublicTransport,
                PtMode.UrbanRail => RoutingMode.PublicTransport,
                PtMode.Metro => RoutingMode.PublicTransport,
                PtMode.Water => RoutingMode.PublicTransport,
                PtMode.CableWay => RoutingMode.PublicTransport,
                PtMode.Funicular => RoutingMode.PublicTransport,
                PtMode.Taxi => RoutingMode.Car,
                _ => throw new ArgumentOutOfRangeException(nameof(specificMode), specificMode,
                    $"No RoutingMode equivalent to PtMode {specificMode.GetStringValue()} found")
            };
        }
    }
}
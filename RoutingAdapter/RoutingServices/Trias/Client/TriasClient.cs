﻿using System;
using System.Globalization;
using System.IO;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Microsoft.Extensions.Options;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.Trias.Model.Request;
using RoutingAdapter.RoutingServices.Trias.Model.Response;

namespace RoutingAdapter.RoutingServices.Trias.Client
{
    public class TriasClient : ITriasClient
    {
        public const string HttpClientName = "Trias";


        private readonly IOptions<TriasClientConfig> _config;
        private readonly IHttpClientFactory _httpClientFactory;

        public TriasClient(IHttpClientFactory httpClientFactory, IOptions<TriasClientConfig> config)
        {
            _httpClientFactory = httpClientFactory;
            _config = config;
        }

        public async Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper)
        {
            using var client = _httpClientFactory.CreateClient(HttpClientName);

            var request = requestWrapper.RoutingRequest;

            // set requestor ref so request can actually be made
            request.ServiceRequest.RequestorRef = _config.Value.TriasRequestorRef;
            request.ServiceRequest.RequestTimeStamp = DateTime.Now.ToString(CultureInfo.InvariantCulture);

            var serializer = new XmlSerializer(typeof(RoutingRequest));
            await using var stringWriter = new StringWriter();
            serializer.Serialize(stringWriter, request);
            var xml = stringWriter.ToString();

            var httpContent = new StringContent(xml, Encoding.UTF8, MediaTypeNames.Application.Xml);

            var response = await client.PostAsync("", httpContent);

            var responseBody = await response.Content.ReadAsStringAsync();

            var deserializer = new XmlSerializer(typeof(RoutingResponse));
            using var reader = new StringReader(responseBody);
            var routingResponse = (RoutingResponse) deserializer.Deserialize(reader);

            return new SpecificRoutingResponseWrapper<RoutingResponse>
            {
                ActivityGuid = requestWrapper.ActivityGuid,
                RoutingResponse = routingResponse,
            };
        }
    }
}
﻿namespace RoutingAdapter.RoutingServices.Trias.Client
{
    public class TriasClientConfig
    {
        public string TriasRequestorRef { get; set; }
    }
}
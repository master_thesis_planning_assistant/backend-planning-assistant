﻿using System.Threading.Tasks;
using RoutingAdapter.Model;
using RoutingAdapter.RoutingServices.Trias.Model.Request;
using RoutingAdapter.RoutingServices.Trias.Model.Response;

namespace RoutingAdapter.RoutingServices.Trias.Client
{
    public interface ITriasClient
    {
        Task<SpecificRoutingResponseWrapper<RoutingResponse>> MakeRoutingRequestAsync(
            SpecificRoutingRequestWrapper<RoutingRequest> requestWrapper);
    }
}
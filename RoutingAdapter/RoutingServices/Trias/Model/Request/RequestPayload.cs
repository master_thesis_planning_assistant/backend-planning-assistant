﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    public class RequestPayload
    {
        [XmlElement(ElementName = "TripRequest")]
        public TripRequest TripRequest { get; set; }
    }
}
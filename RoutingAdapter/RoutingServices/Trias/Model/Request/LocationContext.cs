﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    /// <summary>
    /// Angabe eines Orts und der Möglichkeiten, wie ein Benutzer ihn per IV erreichen kann.
    /// </summary>
    public class LocationContext
    {
        /// <summary>
        /// Required
        /// Angabe eines räumlichen Orts
        /// </summary>
        [XmlElement(ElementName = "LocationRef")]
        public LocationRef LocationRef { get; set; }

        // TripLocation from doku ignored

        /// <summary>
        /// Beabsichtigte Abfahrts- oder Ankunftszeit an dem in Location oder TripLocation bezeichneten Ort.
        /// </summary>
        [XmlElement(ElementName = "DepArrTime", DataType = "dateTime")]
        public DateTime? DepArrTime { get; set; }

        /// <summary>
        /// Angaben des Benutzers, wie er/sie den Ort mittels IV erreichen/verlassen könnte
        /// </summary>
        [XmlElement(ElementName = "IndividualTransportOptions")]
        public List<IndividualTransportationOption> IndividualTransportOptions { get; set; }

        public bool ShouldSerializeIndividualTransportOptions => IndividualTransportOptions != null;

        public bool ShouldSerializeDepArrTime() => DepArrTime.HasValue;
    }
}
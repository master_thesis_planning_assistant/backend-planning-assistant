﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    /// <summary>
    /// Fasst die Anfragedaten für eine Verbindungsauskunft zusammen.
    /// </summary>
    [XmlRoot(ElementName = "TripRequest", Namespace = "http://www.vdv.de/trias")]
    public class TripRequest
    {
        /// <summary>
        /// Required
        /// Ortsdaten für den Abfahrtsort.
        /// </summary>
        [XmlElement(ElementName = "Origin", Namespace = "http://www.vdv.de/trias")]
        public LocationContext Origin { get; set; }

        /// <summary>
        /// Required
        /// Ortsdaten für den Zielort
        /// </summary>
        [XmlElement(ElementName = "Destination", Namespace = "http://www.vdv.de/trias")]
        public LocationContext Destination { get; set; }

        /// <summary>
        /// Haltestellen oder Haltepunkte, an denen die Verbindung keinen Umstieg vorsehen darf.
        /// </summary>
        [XmlElement(ElementName = "NoChangeAt")]
        public NoChangeAt NoChangeAt { get; set; }

        public bool ShouldSerializeNoChangeAt => NoChangeAt != null;


        /// <summary>
        /// Parameter, die die Suche und Rückgabewerte be-einflussen können.
        /// </summary>
        [XmlElement(ElementName = "Params")]
        public TripParams Params { get; set; }

        public bool ShouldSerializeParams => Params != null;
    }
}
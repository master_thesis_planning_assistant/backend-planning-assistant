﻿using System;
using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    /// <summary>
    /// Angaben zu einer Via-Bedingung.
    /// </summary>
    public class Via
    {
        /// <summary>
        /// Referenz auf den Via-Punkt.
        /// </summary>
        [XmlElement(ElementName = "ViaPoint")]
        public LocationRef ViaPoint { get; set; }

        /// <summary>
        /// Vom Benutzer vorgeschriebene Mindestaufenthalts-zeit am Via-Punkt.
        /// </summary>
        [XmlIgnore]
        public TimeSpan? DwellTime { get; set; }

        /// <summary>
        /// XML-Attribut for <see cref="DwellTime"/>
        /// </summary>
        [XmlElement(ElementName = "DwellTime", DataType = "duration")]
        public string XmlDwellTime
        {
            get => DwellTime.HasValue ? XmlConvert.ToString(DwellTime.Value) : null;
            set => DwellTime = value == null ? (TimeSpan?) null : XmlConvert.ToTimeSpan(value);
        }

        public bool ShouldSerializeXmlDwellTime => XmlDwellTime != null;
    }
}
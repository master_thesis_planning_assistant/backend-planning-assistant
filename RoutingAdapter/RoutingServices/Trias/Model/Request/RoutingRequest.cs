﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    [XmlRoot(ElementName = "Trias", Namespace = "http://www.vdv.de/trias")]
    public class RoutingRequest
    {
        [XmlElement(ElementName = "ServiceRequest", Namespace = "http://www.vdv.de/trias")]
        public ServiceRequest ServiceRequest { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; } = "1.1";

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }

        [XmlAttribute(AttributeName = "siri", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Siri { get; set; }

        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }

        [XmlAttribute(AttributeName = "schemaLocation", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
        public string SchemaLocation { get; set; }
    }
}
﻿using System;
using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    /// <summary>
    /// Arten von IV und deren Nutzungsgrenzen, wie sie der Benutzer vorgegeben hat.
    /// </summary>
    public class IndividualTransportationOption
    {
        /// <summary>
        /// Required
        /// Angabe des IV-Typs. Zugelassen sind hier Werte für Fußweg, Fahrrad, Taxi, selbst gefahrenes Auto, durch
        /// andere gefahrenes Auto, Motorrad und LKW. Der Modus „selbst gefahrenes Auto“ benötigt beim Umstieg in ein
        /// anderes Verkehrsmittel einen länger-fristigen Parkplatz und ist daher ein verallgemeiner-tes Synonym für
        /// ParkAndRide. Der Modus „durch andere gefahrenes Auto“ benötigt dagegen nur einen Platz zum Aussteigen lassen.
        /// </summary>
        [XmlElement(ElementName = "Mode")]
        public IndividualModes Mode { get; set; }

        /// <summary>
        /// Maximale Distanz, bis zu der die Nutzung dieses IV-Typs zugelassen ist.
        /// </summary>
        [XmlElement(ElementName = "MaxDistance")]
        public double? MaxDistance { get; set; }

        /// <summary>
        /// Maximale Zeitdauer, bis zu der die Nutzung dieses IV-Typs zugelassen ist.
        /// </summary>
        [XmlIgnore]
        public TimeSpan? MaxDuration { get; set; }

        /// <summary>
        /// XML-Attribut for <see cref="MaxDuration"/>
        /// </summary>
        [XmlElement(ElementName = "MaxDuration", DataType = "duration")]
        public string XmlMaxDuration
        {
            get => MaxDuration.HasValue ? XmlConvert.ToString(MaxDuration.Value) : null;
            set => MaxDuration = value == null ? (TimeSpan?) null : XmlConvert.ToTimeSpan(value);
        }

        /// <summary>
        /// Minimale Distanz, ab der die Nutzung dieses IV-Typs zugelassen ist.
        /// </summary>
        [XmlElement(ElementName = "MinDistance")]
        public double? MinDistance { get; set; }

        /// <summary>
        /// Minimale Zeitdauer, ab der die Nutzung dieses IV-Typs zugelassen ist.
        /// </summary>
        [XmlIgnore]
        public TimeSpan? MinDuration { get; set; }

        /// <summary>
        /// XML-Attribut for <see cref="MinDuration"/>
        /// </summary>
        [XmlElement(ElementName = "MinDuration", DataType = "duration")]
        public string XmlMinDuration
        {
            get => MinDuration.HasValue ? XmlConvert.ToString(MinDuration.Value) : null;
            set => MinDuration = value == null ? (TimeSpan?) null : XmlConvert.ToTimeSpan(value);
        }

        /// <summary>
        /// Relative Geschwindigkeit in Prozent. Wert 100 stellt Standardgeschwindigkeit dar. Werte kleiner 100
        /// verringern die Geschwindigkeit, Werte größer 100 vergrößern die Geschwindigkeit anteilig.
        /// </summary>
        [XmlElement(ElementName = "Speed")]
        public double? Speed { get; set; }

        public bool ShouldSerializeMaxDistance() => MaxDistance.HasValue;

        public bool ShouldSerializeXmlMaxDuration() => XmlMaxDuration != null;

        public bool ShouldSerializeMinDistance() => MinDistance.HasValue;

        public bool ShouldSerializeXmlMinDuration() => XmlMinDuration != null;

        public bool ShouldSerializeXmlSpeed() => Speed.HasValue;
    }
}
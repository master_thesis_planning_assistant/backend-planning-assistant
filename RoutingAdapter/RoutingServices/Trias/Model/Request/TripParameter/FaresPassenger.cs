﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Profil eines Reisenden für die Tarifermittlung.
    /// </summary>
    public class FaresPassenger
    {
        /// <summary>
        /// Alter des Reisenden.
        /// </summary>
        [XmlElement(ElementName = "Age")]
        public int? Age { get; set; }

        public bool ShouldSerializeAge => Age.HasValue;


        /// <summary>
        /// Personengruppe, die der Reisende zugerechnet werden kann
        /// </summary>
        [XmlElement(ElementName = "PassengerCategory")]
        public PassengerCategory? PassengerCategory { get; set; }

        public bool ShouldSerializePassengerCategory => PassengerCategory.HasValue;


        /// <summary>
        /// Eine oder mehrere Vielfahrerkarten, die der Reisende erworben hat und verwenden kann
        /// z. B. „Bahn-Card50“ oder „BahnCard25First
        /// </summary>
        [XmlElement(ElementName = "TravellerCard")]
        public List<string> TravellerCard { get; set; }

        public bool ShouldSerializeTravellerCard => TravellerCard != null;


        /// <summary>
        /// Liste von Tarifzonen, für die der Fahrgast bereits ein gültiges Ticket hat
        /// </summary>
        [XmlElement(ElementName = "ZonesAlreadyPaid")]
        public FareZoneListInArea ZonesAlreadyPaid { get; set; }

        public bool ShouldSerializeZonesAlreadyPaid => ZonesAlreadyPaid != null;

        /// <summary>
        /// Eine oder mehrere Ids von Tickets, die der Fahrgast schon erworben hat und die der Fahrgast für diese Reise
        /// (oder zumindest für Teile davon) benutzen kann.
        /// </summary>
        [XmlElement(ElementName = "OwnedTicket")]
        public List<string> OwnedTicket { get; set; }

        public bool ShouldSerializeOwnedTicket => OwnedTicket != null;
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Struktur zum Filtern nach Verkehrsunternehmen.
    /// </summary>
    public class OperatorFilter
    {
        /// <summary>
        /// Indikator, ob die in der Liste angegebenen Ver-kehrsunternehmen ausgeschlossen (Wert true) oder als
        /// einzige verwendet werden sollen (Wert false). Voreinstellung ist true.
        /// </summary>
        [XmlElement(ElementName = "Exclude", DataType = "boolean")]
        public bool? Exclude { get; set; }

        public bool ShouldSerializeExclude => Exclude.HasValue;

        /// <summary>
        /// Referenz auf Verkehrsunternehmen.
        /// </summary>
        [XmlElement(ElementName = "OperatorRef", DataType = "NMTOKEN")]
        public string OperatorRef { get; set; }

        public bool ShouldSerializeOperatorRef => OperatorRef != null;
    }
}
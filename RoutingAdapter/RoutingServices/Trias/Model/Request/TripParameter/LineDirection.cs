﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Linien-ID, evtl. verfeinert auf eine Richtung
    /// </summary>
    public class LineDirection
    {
        /// <summary>
        /// Referenz auf die Linie.
        /// </summary>
        [XmlElement(ElementName = "LineRef", DataType = "NMTOKEN")]
        public string LineRef { get; set; }

        /// <summary>
        /// Referenz auf die Linienrichtung.
        /// </summary>
        [XmlElement(ElementName = "DirectionRef", DataType = "NMTOKEN")]
        public string DirectionRef { get; set; }

        public bool ShouldSerializeDirectionRef => DirectionRef != null;
    }
}
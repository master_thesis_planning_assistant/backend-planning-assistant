﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Struktur zum Filtern nach Verkehrsmitteltypen
    /// </summary>
    public class PtModeFilter
    {
        /// <summary>
        /// Indikator, ob die in der Liste angegebenen Ver-kehrsmittel ausgeschlossen (Wert true) oder als einzige
        /// verwendet werden sollen (Wert false). Vor-einstellung ist true.
        /// </summary>
        [XmlElement(ElementName = "Exclude", DataType = "boolean")]
        public bool? Exclude { get; set; }

        public bool ShouldSerializeExclude => Exclude.HasValue;

        /// <summary>
        /// ÖV-Verkehrsmitteltypen.
        /// </summary>
        [XmlElement(ElementName = "PtMode")]
        public List<PtMode> PtMode { get; set; }

        public bool ShouldSerializePtMode => PtMode != null;

        // ignored "ptSubModeChoice" ::: from Doku
    }
}
﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.RoutingServices.Trias.Model.Request.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Fasst die Anfragedaten für eine Verbindungs-auskunft zusammen.
    /// </summary>
    public class TripParams
    {
        /// <summary>
        /// Legt fest, ob im Resultat Tarifinformationen mitaus-gegeben werden sollen. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeFares", DataType = "boolean")]
        public bool? IncludeFares { get; set; }

        public bool ShouldSerializeIncludeFares => IncludeFares.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat Informationen zu den Ver-kehrstagen mitausgegeben werden sollen. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeOperatingDays", DataType = "boolean")]
        public bool? IncludeOperatingDays { get; set; }

        public bool ShouldSerializeIncludeOperatingDays => IncludeOperatingDays.HasValue;

        /// <summary>
        /// Parameter für die Tarifermittlung
        /// </summary>
        [XmlElement(ElementName = "FaresParam")]
        public FaresParam FaresParam { get; set; }

        public bool ShouldSerializeFaresParam => FaresParam != null;

        #region TripDataFilter

        /// <summary>
        /// Filter nach Verkehrsmitteltypen.
        /// </summary>
        [XmlElement(ElementName = "PtModeFilter")]
        public PtModeFilter PtModeFilter { get; set; }

        public bool ShouldSerializePtModeFilter => PtModeFilter != null;

        /// <summary>
        /// Erlaubte Linien (ggf. verfeinert auf Richtungen).
        /// </summary>
        [XmlElement(ElementName = "LineFilter")]
        public LineFilter LineFilter { get; set; }

        public bool ShouldSerializeLineFilter => LineFilter != null;

        /// <summary>
        /// Filter nach Verkehrsunternehmen.
        /// </summary>
        [XmlElement(ElementName = "OperatorFilter")]
        public OperatorFilter OperatorFilter { get; set; }

        public bool ShouldSerializeOperatorFilter => OperatorFilter != null;

        #endregion

        #region BaseTripMobilityFilter

        /// <summary>
        /// Legt fest, ob der Benutzer keine Stufe bewältigen kann. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoSingleStep", DataType = "boolean")]
        public bool? NoSingleStep { get; set; }

        public bool ShouldSerializeNoSingleStep => NoSingleStep.HasValue;

        /// <summary>
        /// Legt fest, ob der Benutzer keine Treppe bewältigen kann. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoStairs", DataType = "boolean")]
        public bool? NoStairs { get; set; }

        public bool ShouldSerializeNoStairs => NoStairs.HasValue;

        /// <summary>
        /// Legt fest, ob der Benutzer keine Rolltreppe benut-zen kann. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoEscalator", DataType = "boolean")]
        public bool? NoEscalator { get; set; }

        public bool ShouldSerializeNoEscalator => NoEscalator.HasValue;

        /// <summary>
        /// Legt fest, ob der Benutzer keinen Aufzug benutzen kann. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoElevator", DataType = "boolean")]
        public bool? NoElevator { get; set; }

        public bool ShouldSerializeNoElevator => NoElevator.HasValue;

        /// <summary>
        /// Legt fest, ob der Benutzer keine Rampe bewältigen kann. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoRamp", DataType = "boolean")]
        public bool? NoRamp { get; set; }

        public bool ShouldSerializeNoRamp => NoRamp.HasValue;

        #endregion

        #region TripMobilityFilter

        /// <summary>
        /// Legt fest, ob der Benutzer beim Ein- und Aussteigen in und aus Fahrzeugen einen ebenen Zugang benö-tigt.
        /// Dazu reicht u.U. auch ein Hublift am Fahrzeug oder am Bahnsteig. Falls der ebene Zugang not-wendig ist,
        /// wird dieser Parameter auf true gesetzt. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "LevelEntrance", DataType = "boolean")]
        public bool? LevelEntrance { get; set; }

        public bool ShouldSerializeLevelEntrance => LevelEntrance.HasValue;

        /// <summary>
        /// Legt fest, ob der Benutzer ein Fahrrad an Bord der Verkehrsmittel mitnehmen will. Falls ja, wird dieser
        /// Parameter auf true gesetzt. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "BikeTransport", DataType = "boolean")]
        public bool? BikeTransport { get; set; }

        public bool ShouldSerializeBikeTransport => BikeTransport.HasValue;


        /// <summary>
        /// Veränderung der Standardgehgeschwindigkeit in Prozent. Der Wert 100 stellt den Standard dar. Wer-te
        /// kleiner 100 stellen eine langsamere Geschwindig-keit dar, Werte größer 100 eine schnellere.
        /// </summary>
        [XmlElement(ElementName = "WalkSpeed")]
        public int? WalkSpeed { get; set; }

        public bool ShouldSerializeWalkSpeed => WalkSpeed.HasValue;

        #endregion

        #region BaseTripPolicy

        /// <summary>
        /// Anzahl der Verbindungsauskünfte, die der Benutzer mindestens erwartet.
        /// </summary>
        [XmlElement(ElementName = "NumberOfResults")]
        public int? NumberOfResults { get; set; }

        public bool ShouldSerializeNumberOfResults => NumberOfResults.HasValue;

        /// <summary>
        /// Wenn dieser Parameter gesetzt ist, sollen in der Verbindungssuche keine Echtzeitdaten oder
        /// Störungsinformationen sondern nur Sollfahrplandaten berücksichtigt werden. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IgnoreRealtimeData", DataType = "boolean")]
        public bool? IgnoreRealtimeData { get; set; }

        public bool ShouldSerializeIgnoreRealtimeData => IgnoreRealtimeData.HasValue;

        /// <summary>
        /// Wenn dieser Parameter gesetzt ist, soll die zu su-chende Verbindung unmittelbar an der angegebenen
        /// Startsituation beginnen. Eine Optimierung der Abfahrtszeit am Start nach der Regel „Starte so spät wie
        /// möglich, solange nur die gleiche Ankunftszeit am Ziel gewährleistet ist“ ist dann nicht notwendig.
        /// Default ist false.
        /// </summary>
        [XmlElement(ElementName = "ImmediateTripStart", DataType = "boolean")]
        public bool? ImmediateTripStart { get; set; }

        public bool ShouldSerializeImmediateTripStart => ImmediateTripStart.HasValue;

        #endregion

        #region TripPolicy

        /// <summary>
        /// Anzahl der maximal zugelassenen Umsteigevorgänge.
        /// </summary>
        [XmlElement(ElementName = "InterchangeLimit")]
        public int? InterchangeLimit { get; set; }

        public bool ShouldSerializeInterchangeLimit => InterchangeLimit.HasValue;

        /// <summary>
        /// Art der Zielfunktion, nach der der Algorithmus die Verbindung optimieren soll.
        /// </summary>
        [XmlIgnore]
        public AlgorithmType? AlgorithmType { get; set; }

        public bool ShouldSerializeAlgorithmType => AlgorithmType.HasValue;

        /// <summary>
        /// XML-Attribut for <see cref="AlgorithmType"/>
        /// </summary>
        [XmlElement(ElementName = "AlgorithmType")]
        public string AlgorithmTypeXml
        {
            get => AlgorithmType?.GetStringValue();
            set => AlgorithmType = value?.GetEnumValue<AlgorithmType>();
        }

        /// <summary>
        /// Für jeden IV-Typ (vgl. 0) in dieser Liste soll eine eigene monomodale Verbindung gefunden werden –
        /// zusätzlich zu den intermodalen Verbindungen.
        /// </summary>
        [XmlElement(ElementName = "ItModesToCover")]
        public List<IndividualModes> ItModesToCover { get; set; }

        public bool ShouldSerializeItModesToCover => ItModesToCover != null;

        #endregion

        #region BaseTripContentFillter

        /// <summary>
        /// Legt fest, ob im Resultat TrackSection-Elemente (vgl. 7.6.15) für die detaillierte geografische
        /// Beschreibung des Wegs mitausgegeben werden sol-len. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeTrackSections", DataType = "boolean")]
        public bool? IncludeTrackSections { get; set; }

        public bool ShouldSerializeIncludeTrackSections => IncludeTrackSections.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat der detaillierte geografische Verlauf des Wegs als Koordinatenfolge mitausgegeben
        /// werden soll. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeLegProjection", DataType = "boolean")]
        public bool? IncludeLegProjection { get; set; }

        public bool ShouldSerializeIncludeLegProjection => IncludeLegProjection.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat Routenhinweise mit Abbie-geempfehlungen mitausgegeben werden sollen.
        /// Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeTurnDescription", DataType = "boolean")]
        public bool? IncludeTurnDescription { get; set; }

        public bool ShouldSerializeIncludeTurnDescription => IncludeTurnDescription.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat Informationen zur Barriere-freiheit mitausgegeben werden sollen.
        /// Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeAccessibility", DataType = "boolean")]
        public bool? IncludeAccessibility { get; set; }

        public bool ShouldSerializeIncludeAccessibility => IncludeAccessibility.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat Informationen zur Echtzeit-situation mitausgegeben werden sollen.
        /// Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeEstimatedTimes", DataType = "boolean")]
        public bool? IncludeEstimatedTimes { get; set; }

        public bool ShouldSerializeIncludeEstimatedTimes => IncludeEstimatedTimes.HasValue;

        /// <summary>
        /// Legt fest, ob im Resultat textuelle Echtzeitmeldun-gen mitausgegeben werden sollen.
        /// Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeSituationInfo", DataType = "boolean")]
        public bool? IncludeSituationInfo { get; set; }

        public bool ShouldSerializeIncludeSituationInfo => IncludeSituationInfo.HasValue;

        #endregion

        #region TripContentFilter

        /// <summary>
        /// Legt fest, ob im Resultat die Zwischenhalte mitaus-gegeben werden sollen. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "IncludeIntermediateStops", DataType = "boolean")]
        public bool? IncludeIntermediateStops { get; set; }

        public bool ShouldSerializeIncludeIntermediateStops => IncludeIntermediateStops.HasValue;

        #endregion

        // ignored "Extension" from docu
    }
}
﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Filterstruktur zum Ein/Ausschließen von Li-nien(richtungen)
    /// </summary>
    public class LineFilter
    {
        /// <summary>
        /// Required
        /// Referenz auf die Linie
        /// </summary>
        [XmlElement(ElementName = "Line")]
        public List<LineDirection> Line { get; set; }

        /// <summary>
        /// Indikator, ob die Linien(richtungen) dieser Liste in die Suche aufgenommen oder von ihr ausgeschlossen
        /// werden sollen. Default ist Ausschluss (Exclude).
        /// </summary>
        [XmlElement(ElementName = "Exclude", DataType = "boolean")]
        public bool? Exclude { get; set; }

        public bool ShouldSerializeExclude => Exclude.HasValue;
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Modell einer Tarifzone mit öffentlich bekannter Bezeichnung.
    /// </summary>
    public class FareZone
    {
        /// <summary>
        /// Requried
        /// Code für eine Tarifzone
        /// </summary>
        [XmlElement(ElementName = "FareZoneRef")]
        public string FareZoneRef { get; set; }

        /// <summary>
        /// Requried
        /// Bezeichnung der Tarifzone für die Fahrgäste.
        /// </summary>
        [XmlElement(ElementName = "FareZoneText")]
        public string FareZoneText { get; set; }
    }
}
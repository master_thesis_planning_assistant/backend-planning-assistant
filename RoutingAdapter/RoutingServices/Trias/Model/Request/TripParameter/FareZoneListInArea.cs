﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    /// <summary>
    /// Liste von Tarifzonen bezogen auf ein Tarifgebiet.
    /// </summary>
    public class FareZoneListInArea
    {
        /// <summary>
        /// Requried
        /// Code für ein Tarifgebiet oder einen Unternehmenstarif z. B. „VVS“ oder „DBAG“.
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityRef")]
        public string FaresAuthorityRef { get; set; }

        /// <summary>
        /// Requried
        /// Beschreibung oder Name des Tarifgebiets.
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityText")]
        public string FaresAuthorityText { get; set; }

        /// <summary>
        /// Requried
        /// Eine oder mehrere Tarifzonen
        /// </summary>
        [XmlElement(ElementName = "OwnedTicket")]
        public List<FareZone> OwnedTicket { get; set; }
    }
}
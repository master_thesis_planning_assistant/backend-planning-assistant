﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Request.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.TripParameter
{
    public class FaresParam
    {
        /// <summary>
        /// Codes für Tarifgebiete oder Unternehmenstarife, die berücksichtigt werden sollen
        /// </summary>
        [XmlElement(ElementName = "FareAuthorityFilter", DataType = "NMTOKEN")]
        public List<string> FareAuthorityFilter { get; set; }

        public bool ShouldSerializeFaresParam => FareAuthorityFilter != null;


        /// <summary>
        /// Personengruppen, die berücksichtigt werden sollen
        /// </summary>
        [XmlElement(ElementName = "PassengerCategory")]
        public List<PassengerCategory> PassengerCategory { get; set; }

        public bool ShouldSerializePassengerCategory => PassengerCategory != null;


        /// <summary>
        /// Reiseklasse, die berücksichtigt werden soll
        /// </summary>
        [XmlElement(ElementName = "TravelClass")]
        public TravelClass? TravelClass { get; set; }

        public bool ShouldSerializeTravelClass => TravelClass.HasValue;

        /// <summary>
        /// Anzahl Reisende, für die die Tarifermittlung ausge-führt werden soll
        /// </summary>
        [XmlElement(ElementName = "Traveller")]
        public List<FaresPassenger> Traveller { get; set; }

        public bool ShouldSerializeTraveller => Traveller != null;
    }
}
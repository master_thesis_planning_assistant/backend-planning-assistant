﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    /// <summary>
    /// Nur eine der Optionen a - b Auswählbar!
    /// Angaben zu einer Nicht-Via-Bedingung. Diese Art Bedingung verhindert, dass eine Verbin-dungsauskunft über die
    /// angegebene Haltestelle oder Haltepunkt führt.
    /// </summary>
    public class NotVia
    {
        /// <summary>
        /// Required a
        /// Referenz auf einen Code für einen Haltepunkt.
        /// </summary>
        [XmlElement(ElementName = "StopPointRef", DataType = "normalizedString")]
        public string StopPointRef { get; set; }

        public bool ShouldSerializeStopPointRef => StopPointRef != null;

        /// <summary>
        /// Required b
        /// Referenz auf einen Code für eine Haltestelle.
        /// </summary>
        [XmlElement(ElementName = "StopPlaceRef", DataType = "normalizedString")]
        public string StopPlaceRef { get; set; }

        public bool ShouldSerializeStopPlaceRef => StopPlaceRef != null;
    }
}
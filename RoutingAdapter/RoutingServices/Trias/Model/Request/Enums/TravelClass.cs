﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.Enums
{
    /// <summary>
    /// Reiseklasse
    /// </summary>
    public enum TravelClass
    {
        [XmlEnum("all")] All,
        [XmlEnum("first")] First,
        [XmlEnum("second")] Second,
        [XmlEnum("third")] Third,
        [XmlEnum("business")] Business,
        [XmlEnum("economy")] Economy,
    }
}
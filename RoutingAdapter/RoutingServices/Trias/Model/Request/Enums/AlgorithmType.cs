﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request.Enums
{
    public enum AlgorithmType
    {
        [XmlEnum("fastest")] Fastest,
        [XmlEnum("minChanges")] MinChanges,
        [XmlEnum("leastWalking")] LeastWalking
    }
}
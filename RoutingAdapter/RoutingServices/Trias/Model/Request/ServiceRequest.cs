﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Request
{
    public class ServiceRequest
    {
        [XmlElement(ElementName = "RequestTimeStamp", Namespace = "http://www.siri.org.uk/siri")]
        public string RequestTimeStamp { get; set; }

        [XmlElement(ElementName = "RequestorRef", Namespace = "http://www.siri.org.uk/siri")]
        public string RequestorRef { get; set; }

        [XmlElement(ElementName = "RequestPayload")]
        public RequestPayload RequestPayload { get; set; }
    }
}
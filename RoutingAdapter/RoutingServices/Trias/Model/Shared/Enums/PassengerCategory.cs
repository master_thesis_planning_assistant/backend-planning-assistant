﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums
{
    /// <summary>
    /// Kategorisierung von Fahrgästen in tariflicher Hin-sicht.
    /// </summary>
    public enum PassengerCategory
    {
        [XmlEnum("Adult")] Adult,
        [XmlEnum("Child")] Child,
        [XmlEnum("Senior")] Senior,
        [XmlEnum("Youth")] Youth,
        [XmlEnum("Disabled")] Disabled,
    }
}
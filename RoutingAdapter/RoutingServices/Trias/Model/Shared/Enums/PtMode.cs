﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums
{
    /// <summary>
    /// Klassifizierung der ÖV-Verkehrsmittel (nach TPEG pti_table 01).
    /// </summary>
    public enum PtMode
    {
        [XmlEnum("all")] All,
        [XmlEnum("unknown")] Unknown,
        [XmlEnum("air")] Air,
        [XmlEnum("bus")] Bus,
        [XmlEnum("trolleyBus")] TrolleyBus,
        [XmlEnum("tram")] Tram,
        [XmlEnum("coach")] Coach,
        [XmlEnum("rail")] Rail,
        [XmlEnum("intercityRail")] IntercityRail,
        [XmlEnum("urbanRail")] UrbanRail,
        [XmlEnum("metro")] Metro,
        [XmlEnum("water")] Water,
        [XmlEnum("cableWay")] CableWay,
        [XmlEnum("funicular")] Funicular,
        [XmlEnum("taxi")] Taxi
    }
}
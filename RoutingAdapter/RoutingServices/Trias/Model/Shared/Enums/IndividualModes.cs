﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums
{
    /// <summary>
    /// Klassifizierung der Individualverkehrsarten.
    /// </summary>
    public enum IndividualModes
    {
        [XmlEnum("walk")] Walk,
        [XmlEnum("cycle")] Cycle,
        [XmlEnum("taxi")] Taxi,
        [XmlEnum("self-drive-car")] SelfDriveCar,
        [XmlEnum("others-drive-car")] OthersDriveCar,
        [XmlEnum("motorcycle")] Motorcycle,
        [XmlEnum("truck")] Truck
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Shared
{
    public class GeoPosition
    {
        /// <summary>
        /// Geografische Länge bzgl. des Greenwich-Meridians. Wertebereich von -180 Grad (West) bis +180 Grad (Ost).
        /// </summary>
        [XmlElement(ElementName = "Longitude")]
        public double Longitude { get; set; }

        /// <summary>
        /// Geografische Breite bzgl. des Äquators. Wertebe-reich von -90 Grad (Süden) bis +90 Grad (Norden).
        /// </summary>
        [XmlElement(ElementName = "Latitude")]
        public double Latitude { get; set; }

        /// <summary>
        /// Höhe über dem Meeresspiegel in Meter.
        /// </summary>
        [XmlElement(ElementName = "Altitude")]
        public double? Altitude { get; set; }

        public bool ShouldSerializeAltitude => Altitude.HasValue;
    }
}
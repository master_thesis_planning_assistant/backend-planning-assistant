﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Shared
{
    /// <summary>
    /// Nur eine der Optionen a - f ausfwählbar!
    /// Referenz auf einen allgemeinen Ortspunkt (Hal-tepunkt, Haltestelle, Koordinatenposition, Ort-schaft oder POI).
    /// </summary>
    public class LocationRef
    {
        /// <summary>
        /// Required a
        /// Referenz auf einen Code für einen Haltepunkt.
        /// </summary>
        [XmlElement(ElementName = "StopPointRef", DataType = "normalizedString")]
        public string StopPointRef { get; set; }

        public bool ShouldSerializeStopPointRef => StopPointRef != null;

        /// <summary>
        /// Required b
        /// Referenz auf einen Code für eine Haltestelle.
        /// </summary>
        [XmlElement(ElementName = "StopPlaceRef", DataType = "normalizedString")]
        public string StopPlaceRef { get; set; }

        public bool ShouldSerializeStopPlaceRef => StopPlaceRef != null;

        /// <summary>
        /// Required c
        /// Koordinatenposition.
        /// </summary>
        [XmlElement(ElementName = "GeoPosition")]
        public GeoPosition GeoPosition { get; set; }

        public bool ShouldSerializeGeoPosition => GeoPosition != null;

        /// <summary>
        /// Required d
        /// Referenz auf einen Code für eine Ortschaft
        /// </summary>
        [XmlElement(ElementName = "LocalityRef", DataType = "normalizedString")]
        public string LocalityRef { get; set; }

        public bool ShouldSerializeLocalityRef => LocalityRef != null;

        /// <summary>
        /// Required e
        /// Referenz auf einen Code für einen POI.
        /// </summary>
        [XmlElement(ElementName = "PointOfInterestRef", DataType = "normalizedString")]
        public string PointOfInterestRef { get; set; }

        public bool ShouldSerializePointOfInterestRef => PointOfInterestRef != null;

        /// <summary>
        /// Required f
        /// Referenz auf eine Adresse.
        /// </summary>
        [XmlElement(ElementName = "AddressRef", DataType = "normalizedString")]
        public string AddressRef { get; set; }

        public bool ShouldSerializeAddressRef => AddressRef != null;

        /// <summary>
        /// Name oder Bezeichnung des Ortspunkts.
        /// Referenz auf eine Adresse.
        /// </summary>
        [XmlElement(ElementName = "LocationName", DataType = "normalizedString")]
        public string LocationName { get; set; }

        public bool ShouldSerializeLocationName => LocationName != null;
    }
}
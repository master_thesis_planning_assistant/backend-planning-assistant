﻿using System;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext
{
    [XmlRoot(ElementName = "ValidityPeriod", Namespace = "http://www.siri.org.uk/siri")]
    public class ValidityPeriod
    {
        [XmlElement(ElementName = "StartTime", Namespace = "http://www.siri.org.uk/siri")]
        public DateTime StartTime { get; set; }

        [XmlElement(ElementName = "EndTime", Namespace = "http://www.siri.org.uk/siri")]
        public DateTime EndTime { get; set; }
    }
}
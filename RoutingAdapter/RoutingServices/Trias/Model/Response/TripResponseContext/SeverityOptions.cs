﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext
{
    public enum SeverityOptions
    {
        [XmlEnum("normal")] Normal,
        [XmlEnum("noImpact")] NoImpact,
        [XmlEnum("verySlight")] VerySlight,
        [XmlEnum("slight")] Slight,
        [XmlEnum("severe")] Severe,
        [XmlEnum("verySevere")] VerySevere,
    }
}
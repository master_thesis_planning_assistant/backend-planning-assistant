﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext
{
    /// <summary>
    /// Container für die strukturierte Beschreibung einer Situation im ÖV oder im Straßenverkehr, wie z. B. einer
    /// Störung im ÖV oder auf der Straße oder eines Ereignisses mit Auswirkungen auf das Verkehrsgeschehen.
    /// For documentation see <a href="https://enturas.atlassian.net/wiki/spaces/PUBLIC/pages/637370605/SIRI-SX">here</a>
    /// or <a href="https://laidig.github.io/siri-20-java/doc/schemas/siri_situation-v2_0_xsd/schema-overview.html">here</a>
    /// </summary>
    public class PtSituation
    {
        /// <summary>
        /// Required
        /// Timestamp for when the situation was created.
        /// </summary>
        [XmlElement(ElementName = "CreationTime", Namespace = "http://www.siri.org.uk/siri")]
        public DateTime CreationTime { get; set; }

        /// <summary>
        /// Required
        /// Codespace of the data source
        /// </summary>
        [XmlElement(ElementName = "ParticipantRef", Namespace = "http://www.siri.org.uk/siri")]
        public string ParticipantRef { get; set; }

        /// <summary>
        /// Required
        /// Unique situation-ID for PtSituationElement.
        /// Format: CODESPACE:SituationNumber:ID
        /// e.g.: ABC:SituationNumber:123
        /// </summary>
        [XmlElement(ElementName = "SituationNumber", Namespace = "http://www.siri.org.uk/siri")]
        public string SituationNumber { get; set; }

        // ignored version from doku

        /// <summary>
        /// Required
        /// Information on the source of the message.
        /// </summary>
        [XmlElement(ElementName = "Source", Namespace = "http://www.siri.org.uk/siri")]
        public Source Source { get; set; }

        /// <summary>
        /// Required
        /// Status of a situation message.
        /// Please note that when Progress is set to 'closed' the message is considered expired and should not be presented to the public.
        /// </summary>
        [XmlElement(ElementName = "Progress", Namespace = "http://www.siri.org.uk/siri")]
        public ProgressOption Progress { get; set; }

        /// <summary>
        /// Timestamp when the situation element was updated.
        /// </summary>
        [XmlElement("VersionedAtTime", Namespace = "http://www.siri.org.uk/siri")]
        public DateTime? VersionedAtTime { get; set; }

        /// <summary>
        /// Validity period(s) set with a start time and optionally with an end time.
        /// When the end time of the situation is undefined the expiration of the situation is considered unknown until
        /// cancellation status for the situation is sent. If the situation has several periods,
        /// all but the last period must have an end date.
        /// 
        /// Note that for closed (Progress=closed) messages, the ValidityPeriod must have an EndTime with a minimum of
        /// five hours into the future to ensure the message is properly delivered and received by all systems.
        ///
        /// Once EndTime has expired, the message will no longer be re-distributed in real-time data streams or services.
        /// </summary>
        [XmlElement(ElementName = "ValidityPeriod", Namespace = "http://www.siri.org.uk/siri")]
        public List<ValidityPeriod> ValidityPeriod { get; set; }

        // ignored UnknownReason from Doku

        /// <summary>
        /// How severely the situation affects public transport services.
        /// </summary>
        [XmlElement(ElementName = "Severity", Namespace = "http://www.siri.org.uk/siri")]
        public SeverityOptions? Severity { get; set; }

        /// <summary>
        /// Number value from 1 to 10 indicating the priority (urgency) of the situation message.
        /// 
        /// 1 - First (i.e. highest) message priority. Equivalent to DATEX2 urgency level "extremelyUrgent"
        /// 2 - 10 - Urgent, of various priority. Equivalent to DATEX2 urgency level "urgent" with added priority order.
        /// 
        /// Left blank (default) is equivalent to DATEX2 urgency level "normal urgency".
        /// </summary>
        [XmlElement(ElementName = "Priority", Namespace = "http://www.siri.org.uk/siri")]
        public int? Priority { get; set; }

        // ignored ReportType from Documentation

        /// <summary>
        /// Intended audience of SITUATION.
        /// </summary>
        [XmlElement(ElementName = "Audience", Namespace = "http://www.siri.org.uk/siri")]
        public AudienceOptions? Audience { get; set; }

        // ignored scopeType from docu

        /// <summary>
        /// Indicates if the Situation was planned or happened unplanned
        /// </summary>
        [XmlElement(ElementName = "Planned", Namespace = "http://www.siri.org.uk/siri")]
        public bool? Planned { get; set; }

        [XmlElement(ElementName = "Language", Namespace = "http://www.siri.org.uk/siri")]
        public string Language { get; set; }

        // ignored Advice from docu

        // ignored InfoLink from docu

        // ignored Affects from docu

        /// <summary>
        /// The textual summary of the situation (which is not already described by structured data).
        /// One summary per language (if more than one, the xml:lang attribute must be set).
        /// </summary>
        [XmlElement(ElementName = "Summary", Namespace = "http://www.siri.org.uk/siri")]
        public string Summary { get; set; }

        /// <summary>
        /// Expanded textual description (if more than one, the xml:lang attribute must be set) of the situation
        /// (do not repeat information from Summary, or structured data).
        /// </summary>
        [XmlElement(ElementName = "Description", Namespace = "http://www.siri.org.uk/siri")]
        public string Description { get; set; }

        /// <summary>
        /// Details for the Situation
        /// </summary>
        [XmlElement(ElementName = "Detail", Namespace = "http://www.siri.org.uk/siri")]
        public string Detail { get; set; }
    }
}
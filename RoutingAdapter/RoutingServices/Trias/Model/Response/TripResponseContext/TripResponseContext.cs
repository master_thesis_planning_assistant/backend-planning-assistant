﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext
{
    /// <summary>
    /// Container für Daten, die in der Antwort mehrfach auftreten und referenziert werden.
    /// </summary>
    public class TripResponseContext
    {
        /// <summary>
        /// SIRI-Modellierung eines Ereignisses oder einer Störung
        /// </summary>
        [XmlElement(ElementName = "Situations")]
        public List<Situation> Situations { get; set; }
    }

    public class Situation
    {
        /// <summary>
        /// SIRI-Modellierung eines Ereignisses oder einer Störung
        /// </summary>
        [XmlElement(ElementName = "PtSituation")]
        public PtSituation PtSituation { get; set; }
    }
}
﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext
{
    [XmlRoot(ElementName = "Source", Namespace = "http://www.siri.org.uk/siri")]
    public class Source
    {
        [XmlElement(ElementName = "SourceType", Namespace = "http://www.siri.org.uk/siri")]
        public SourceType SourceType { get; set; }
    }
}
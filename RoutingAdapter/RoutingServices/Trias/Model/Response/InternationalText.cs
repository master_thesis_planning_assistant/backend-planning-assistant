﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    /// <summary>
    /// Ein Text mit einer Text-ID und Angabe der Spra-che, in der er verfasst ist.
    /// </summary>
    public class InternationalText
    {
        /// <summary>
        /// Text
        /// </summary>
        [XmlElement(ElementName = "Text")]
        public string Text { get; set; }

        /// <summary>
        /// ID des Texts.
        /// </summary>
        [XmlElement(ElementName = "TextId")]
        public string TextId { get; set; }

        /// <summary>
        /// Sprache, in der der Text verfasst ist.
        /// </summary>
        [XmlElement(ElementName = "Language", DataType = "language")]
        public string Language { get; set; }
    }
}
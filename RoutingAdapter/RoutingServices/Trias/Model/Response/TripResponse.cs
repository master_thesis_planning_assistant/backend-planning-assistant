﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    /// <summary>
    /// Fasst die Ergebnisdaten für eine intermodale Verbindungsauskunft zusammen.
    /// </summary>
    [XmlRoot(ElementName = "TripResponse")]
    public class TripResponse
    {
        /// <summary>
        /// Fehlermeldungen bezogen auf die Gesamtbeant-wortung der Anfrage.
        /// </summary>
        [XmlElement(ElementName = "ErrorMessage")]
        public List<ErrorMessage> ErrorMessage { get; set; }

        /// <summary>
        /// Container für Daten, die in der Antwort mehrfach auftreten und referenziert werden.
        /// </summary>
        [XmlElement(ElementName = "TripResponseContext")]
        public TripResponseContext.TripResponseContext TripResponseContext { get; set; }

        /// <summary>
        /// Container für eine Verbindungsauskunft.
        /// </summary>
        [XmlElement(ElementName = "TripResult")]
        public List<TripResult.TripResult> TripResult { get; set; }
    }
}
﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    [XmlRoot(ElementName = "Situations", Namespace = "http://www.vdv.de/trias")]
    public class Situations
    {
        /// <summary>
        /// SIRI-Modellierung eines Ereignisses oder einer Stö-rung
        /// </summary>
        [XmlElement(ElementName = "PtSituation", Namespace = "http://www.vdv.de/trias")]
        public List<PtSituation> PtSituation { get; set; }
    }
}
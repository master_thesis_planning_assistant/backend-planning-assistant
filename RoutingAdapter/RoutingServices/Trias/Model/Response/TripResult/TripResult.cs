﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult
{
    /// <summary>
    /// Fasst die Ergebnisdaten für eine einzelne in-termodale Verbindungsauskunft zusammen.
    /// </summary>
    public class TripResult
    {
        /// <summary>
        /// Required
        /// ID des Resultats für spätere Referenzierung bzw. für Debug-Zwecke.
        /// </summary>
        [XmlElement(ElementName = "ResultId")]
        public string ResultId { get; set; }

        /// <summary>
        /// Fehlermeldungen bezogen auf dieses Verbin-dungsresultat. Siehe die nachstehende Tabelle für mögliche Werte.
        /// </summary>
        [XmlElement(ElementName = "ErrorMessage")]
        public List<ErrorMessage> ErrorMessage { get; set; }

        /// <summary>
        /// Required
        /// Daten zu einer intermodalen Verbindung.
        /// </summary>
        [XmlElement(ElementName = "Trip")]
        public Trip.Trip Trip { get; set; }

        /// <summary>
        /// Ticket- und Fahrpreisinformationen zur Verbindung als Ganzes oder zu Teilen der Verbindung
        /// </summary>
        [XmlElement(ElementName = "TripFares")]
        public List<TripFares.TripFares> TripFares { get; set; }
    }
}
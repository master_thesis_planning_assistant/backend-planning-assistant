﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg
{
    /// <summary>
    /// Beinhaltet einen Verbindungsanteil (Teilweg), der einen Umsteigevorgang zwischen zwei Ver-kehrsmitteln darstellt.
    /// </summary>
    public class InterchangeLeg
    {
        /// <summary>
        /// Required a
        /// Klassifizierung von Umsteigevorgängen
        /// </summary>
        [XmlElement(ElementName = "InterchangeMode")]
        public InterchangeMode? InterchangeMode { get; set; }

        /// <summary>
        /// Required b
        /// Modalität für konitnuierliche Verkehre
        /// </summary>
        [XmlElement(ElementName = "ContinuousMode")]
        public ContinuousMode? ContinuousMode { get; set; }

        /// <summary>
        /// Required
        /// Beginn (Ort) dieses Teilwegs
        /// </summary>
        [XmlElement(ElementName = "LegStart")]
        public LocationRefInternational LegStart { get; set; }

        /// <summary>
        /// Required
        /// Ende (Ort) dieses Teilwegs
        /// </summary>
        [XmlElement(ElementName = "LegEnd")]
        public LocationRefInternational LegEnd { get; set; }

        /// <summary>
        /// Frühester Zeitpunkt für den Start dieses Teilwegs
        /// </summary>
        [XmlElement(ElementName = "TimeWindowStart")]
        public DateTime? TimeWindowStart { get; set; }

        /// <summary>
        /// Spätester Zeitpunkt für das Ende dieses Teilwegs
        /// </summary>
        [XmlElement(ElementName = "TimeWindowEnd")]
        public DateTime? TimeWindowEnd { get; set; }

        /// <summary>
        /// Required
        /// Notwendige Gesamtumsteigezeit
        /// </summary>
        [XmlElement(ElementName = "Duration", DataType = "duration")]
        public string Duration { get; set; }

        [XmlIgnore]
        public double? DurationInternal => Duration != null ? XmlConvert.ToTimeSpan(Duration).TotalSeconds : null;

        /// <summary>
        /// Fußweganteil der Gesamtumsteigezeit
        /// </summary>
        [XmlElement(ElementName = "WalkDuration", DataType = "duration")]
        public string WalkDuration { get; set; }

        [XmlIgnore]
        public double? WalkDurationInternal =>
            WalkDuration != null ? XmlConvert.ToTimeSpan(WalkDuration).TotalSeconds : null;

        /// <summary>
        /// Pufferzeitanteil der Gesamtumsteigezeit. Pufferzei-ten („Check-In-Zeiten“) sind bei manchen Verkehrs-mitteln
        /// vorgeschrieben, z. B. im Flugverkehr, bei Fähren oder auch Hochgeschwindigkeitszügen
        /// </summary>
        [XmlElement(ElementName = "BufferTime", DataType = "duration")]
        public string BufferTime { get; set; }

        [XmlIgnore]
        public double? BufferTimeInternal => BufferTime != null ? XmlConvert.ToTimeSpan(BufferTime).TotalSeconds : null;

        /// <summary>
        /// Beschreibung des Umsteigevorgangs. Length 0:
        /// </summary>
        [XmlElement(ElementName = "LegDescription")]
        public List<InternationalText> LegDescription { get; set; }

        /// <summary>
        /// Länge des Umsteigewegs.
        /// </summary>
        [XmlElement(ElementName = "Length")]
        public double? Length { get; set; }

        /// <summary>
        /// Hinweise und Attribute (mit Klassifikationen) zum Umsteigevorgang
        /// </summary>
        [XmlElement(ElementName = "Attribute")]
        public List<GeneralAttribute> Attribute { get; set; }

        /// <summary>
        /// Detaillierte Informationen zum geometrischen Ver-lauf, der Wegfolge und Zugänglichkeit.
        /// </summary>
        [XmlElement(ElementName = "NavigationPath")]
        public NavigationPath NavigationPath { get; set; }

        /// <summary>
        /// Verweis auf eine Störungsnachricht. Diese Nachricht kann im TripResponseContext (vgl. 9.3.2) zu finden sein
        /// oder auf anderem Wege bekannt gemacht wer-den.
        /// </summary>
        [XmlElement(ElementName = "SituationFullRef")]
        public List<SituationFullRef> SituationFullRef { get; set; }

        // irgnored Extension from Documentation
    }
}
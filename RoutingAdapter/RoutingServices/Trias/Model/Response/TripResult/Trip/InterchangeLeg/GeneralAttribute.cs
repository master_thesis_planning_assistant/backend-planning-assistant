﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg
{
    /// <summary>
    /// Struktur für die Definition von Attribu-ten/Hinweisen
    /// </summary>
    public class GeneralAttribute
    {
        /// <summary>
        /// Attributtext für die Fahrgastinformation.
        /// </summary>
        [XmlElement(ElementName = "Text")]
        public List<InternationalText> Text { get; set; }

        /// <summary>
        /// Interner Attribute-Code. Kann verwendet werden, um mehrfaches Auftreten desselben Attributs zu erkennen.
        /// </summary>
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }

        // Ignroed AllFacilitiesGroup from Documentation

        /// <summary>
        /// Legt fest, ob das Attribut in jedem Fall angezeigt werden muss. Voreinstellung ist false.
        /// </summary>
        [XmlElement(ElementName = "Mandatory")]
        public bool? Mandatory { get; set; }

        /// <summary>
        /// Wichtigkeit für die Priorisierung von Attributen ge-geneinander.
        /// </summary>
        [XmlElement(ElementName = "Importance")]
        public int? Importance { get; set; }

        /// <summary>
        /// URL zu weiteren Informationen über dieses Attribut. Falls vorhanden, soll der gesamte Text als Link zu dieser
        /// URL gekennzeichnet werden
        /// </summary>
        [XmlElement(ElementName = "InfoURL")]
        public string InfoUrl { get; set; }

        /// <summary>
        /// Gibt den Status eines Attributs an, z.B. in einer An-reicherungsanfrage.
        /// </summary>
        [XmlElement(ElementName = "Status")]
        public AttributeStatus? Status { get; set; }
    }
}
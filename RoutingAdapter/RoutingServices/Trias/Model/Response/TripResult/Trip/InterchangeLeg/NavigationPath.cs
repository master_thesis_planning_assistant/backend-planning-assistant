﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg
{
    /// <summary>
    /// Container für Wegbeschreibungen.
    /// </summary>
    public class NavigationPath
    {
        /// <summary>
        /// Ein oder mehrere Streckenabschnitte.
        /// </summary>
        [XmlElement(ElementName = "NavigationSection")]
        public List<NavigationSection> NavigationSection { get; set; }
    }
}
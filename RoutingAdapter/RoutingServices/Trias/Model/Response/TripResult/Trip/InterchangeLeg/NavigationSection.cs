﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg
{
    /// <summary>
    /// Beschreibung eines Wegstücks, evtl. mit Angabe der geografischen Einbettung, der Abbiegeanwei-sungen und der
    /// Wegbeschaffenheit (Zugänglichkeit für mobilitätseigeschränkte Personen).
    /// </summary>
    public class NavigationSection
    {
        /// <summary>
        /// Geografische Beschreibung des Streckenabschnitts. Vgl.
        /// </summary>
        [XmlElement(ElementName = "TrackSection")]
        public TrackSection TrackSection { get; set; }

        /// <summary>
        /// Beschreibung des durchzuführenden Manövers. Es sollte in textueller Form die Inhalte von Manoeuvre,
        /// TurnAction und TrackSection.RoadName beschrei-ben.
        /// </summary>
        [XmlElement(ElementName = "TurnDescription")]
        public List<InternationalText> TurnDescription { get; set; }

        /// <summary>
        /// Codierung des durchzuführenden Manövers.
        /// </summary>
        [XmlElement(ElementName = "Manoeuvre")]
        public Manoeuvre? Manoeuvre { get; set; }

        /// <summary>
        /// Codierung der Abbiegevorgänge
        /// </summary>
        [XmlElement(ElementName = "TurnAction")]
        public TurnAction? TurnAction { get; set; }

        /// <summary>
        /// Textueller Richtungshinweis zum besseren Ver-ständnis des nachfolgenden Streckenabschnittes,
        /// z. B. „Folgen Sie der Beschilderung nach Hamburg“.
        /// </summary>
        [XmlElement(ElementName = "DirectionHint")]
        public List<InternationalText> DirectionHint { get; set; }

        /// <summary>
        /// Himmelsrichtung, die nach dem Manöver eingeschlagen ist. Sie bezieht sich nicht auf das gesamte Wegstück.
        /// Kompassrichtung in Grad. Nord = 0 Grad, im Uhrzeigersinn aufsteigende Werte.
        /// </summary>
        [XmlElement(ElementName = "Bearing")]
        public int? Bearing { get; set; }

        /// <summary>
        /// Verweise auf Störungsnachrichten. Diese Nachrichten können im TripResponseContext
        /// zu finden sein oder auf anderem Wege bekannt ge-macht werden
        /// </summary>
        [XmlElement(ElementName = "SituationFullRef")]
        public List<SituationFullRef> SituationFullRef { get; set; }

        /// <summary>
        /// Beschreibung der Zugänglichkeit des Wegstücks
        /// </summary>
        [XmlElement(ElementName = "AccessPath")]
        public AccessPath AccessPath { get; set; }
    }
}
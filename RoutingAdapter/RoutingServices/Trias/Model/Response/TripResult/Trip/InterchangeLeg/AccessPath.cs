﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg
{
    /// <summary>
    /// Beschreibung der Zugänglichkeit eines Wegstücks
    /// </summary>
    public class AccessPath
    {
        /// <summary>
        /// Angabe, ob Weg eben geht oder aufwärts/abwärts führt.
        /// </summary>
        [XmlElement(ElementName = "Transition")]
        public Transition Transition { get; set; }

        /// <summary>
        /// Wegtyp.
        /// </summary>
        [XmlElement(ElementName = "AccessFeatureType")]
        public AccessFeatureType AccessFeatureType { get; set; }

        /// <summary>
        /// Anzahl, wie oft der Wegtyp vorkommt.
        /// </summary>
        [XmlElement(ElementName = "Count")]
        public int? Count { get; set; }
    }
}
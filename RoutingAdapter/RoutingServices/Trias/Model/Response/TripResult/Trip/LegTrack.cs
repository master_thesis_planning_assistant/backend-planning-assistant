﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Container für die Streckenabschnitte entlang eines Verbindungsteilwegs.
    /// </summary>
    public class LegTrack
    {
        /// <summary>
        /// Required
        /// Ein oder mehrere Streckenabschnitte.
        /// </summary>
        [XmlElement(ElementName = "TrackSection")]
        public List<TrackSection> TrackSection { get; set; }
    }
}
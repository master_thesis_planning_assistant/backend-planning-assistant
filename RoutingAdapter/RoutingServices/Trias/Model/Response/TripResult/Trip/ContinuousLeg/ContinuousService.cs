﻿using System.Collections.Generic;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.ContinuousLeg
{
    /// <summary>
    /// Eine Fahrgastbewegung mit Hilfe eines kontinu-ierlichen, nicht fahrplangebundenen Verkehrs-mittels.
    /// </summary>
    public class ContinuousService
    {
        /// <summary>
        /// Required a
        /// Modalität für kontinuierliche Verkehre.
        /// </summary>
        [XmlElement(ElementName = "ContinuousMode")]
        public ContinuousMode ContinuousMode { get; set; }

        /// <summary>
        /// Required b
        /// Verkehrsmittelmodalität für Individualverkehr.
        /// </summary>
        [XmlElement(ElementName = "IndividualMode")]
        public IndividualModes IndividualMode { get; set; }

        /// <summary>
        /// Required for Optional a
        /// Fahrt-ID
        /// </summary>
        [XmlElement("JourneyRef")]
        public string JourneyRef { get; set; }

        /// <summary>
        /// Required for Optional a
        /// Fahrtabschnitte mit Eigenschaften
        /// </summary>
        [XmlElement("ServiceSection")]
        public List<ServiceSection>
            ServiceSection { get; set; }

        /// <summary>
        /// Optional b
        /// Beschreibung eines Mobilitätsangebots mit Ausleih-fahrzeugen
        /// </summary>
        [XmlElement(ElementName = "SharingService")]
        public SharingService SharingService { get; set; }

        /// <summary>
        /// ID des ersten Haltepunkts der Fahrt; Starthaltestelle.
        /// </summary>
        [XmlElement(ElementName = "OriginStopPointRef")]
        public string OriginStopPointRef { get; set; }

        /// <summary>
        /// Name des ersten Haltepunkts der Fahrt, der Start-haltestelle.
        /// </summary>
        [XmlElement(ElementName = "OriginText")]
        public List<InternationalText> OriginText { get; set; }

        /// <summary>
        /// ID des letzten Haltepunkts der Fahrt; Endhaltestelle.
        /// </summary>
        [XmlElement(ElementName = "DestinationStopPointRef")]
        public string DestinationStopPointRef { get; set; }

        /// <summary>
        /// Name des letzten Haltepunkts der Fahrt, der End-haltestelle oder Fahrtziel.
        /// </summary>
        [XmlElement(ElementName = "DestinationText")]
        public List<InternationalText> DestinationText { get; set; }

        /// <summary>
        /// Verweis auf eine Störungsnachricht. Diese Nachricht kann im Kontext der Meldung (ResponseContext) zu
        /// finden sein oder auf anderem Wege bekannt ge-macht werden.
        /// </summary>
        [XmlElement(ElementName = "SituationFullRef")]
        public List<SituationFullRef> SituationFullRef { get; set; }


        #region Optional a

        /// <summary>
        /// Required for Optional a
        /// Betriebstag der Fahrt
        /// </summary>
        [XmlElement("Service")]
        public string OperatingDayRef { get; set; }

        /// <summary>
        /// Fahrzeug-Id
        /// </summary>
        [XmlElement("VehicleRef")]
        public string VehicleRef { get; set; }


        // ignored Attribute from Documentation

        #endregion
    }
}
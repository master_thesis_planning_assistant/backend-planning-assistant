﻿using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.ContinuousLeg
{
    public class SharingService
    {
        /// <summary>
        /// Required
        /// Operator-ID.
        /// </summary>
        [XmlElement(ElementName = "OperatorRef")]
        public string OperatorRef { get; set; }

        /// <summary>
        /// Name des Mobilitätsdienstes.
        /// </summary>
        [XmlElement(ElementName = "Name")]
        public string Name { get; set; }

        /// <summary>
        /// Typ des Ausleih- und Rückgabeverfahrens.
        /// </summary>
        [XmlElement(ElementName = "SharingModel")]
        public SharingModel? SharingModel { get; set; }

        /// <summary>
        /// Typische Zeit, die ein Benutzer einplanen muss,
        /// um sich am System anzumelden und das Fahrzeug in Fahrbereitschaft zu versetzen.
        /// </summary>
        [XmlElement(ElementName = "TimeBufferBefore", DataType = "duration")]
        public string TimeBufferBefore { get; set; }

        [XmlIgnore]
        public double? TimeBufferBeforeInternal =>
            TimeBufferBefore != null ? XmlConvert.ToTimeSpan(TimeBufferBefore).TotalSeconds : null;

        /// <summary>
        /// Typische Zeit, die ein Benutzer einplanen muss, um das Fahrzeug ordnungsgemäß abzustellen,
        /// zu verschließen und sich am System abzumelden.
        /// </summary>
        [XmlElement(ElementName = "TimeBufferAfter", DataType = "duration")]
        public string TimeBufferAfter { get; set; }

        [XmlIgnore]
        public double? TimeBufferAfterInternal =>
            TimeBufferAfter != null ? XmlConvert.ToTimeSpan(TimeBufferAfter).TotalSeconds : null;

        /// <summary>
        /// Link zu Web-Seite mit weiterführenden Informatio-nen
        /// </summary>
        [XmlElement(ElementName = "InfoURL")]
        public WebLink InfoUrl { get; set; }
    }
}
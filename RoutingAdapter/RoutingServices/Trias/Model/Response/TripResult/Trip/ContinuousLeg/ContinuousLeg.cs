﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.InterchangeLeg;
using RoutingAdapter.RoutingServices.Trias.Model.Shared;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.ContinuousLeg
{
    /// <summary>
    /// Beinhaltet einen Verbindungsanteil (Teilweg), der nicht fahrplangebunden ist (z. B. Fußweg).
    /// </summary>
    public class ContinuousLeg
    {
        /// <summary>
        /// Required
        /// Beginn (Ort) des Teilwegs dieser Verbindung.
        /// </summary>
        [XmlElement(ElementName = "LegStart")]
        public LocationRefInternational LegStart { get; set; }

        /// <summary>
        /// Required
        /// Ende (Ort) des Teilwegs dieser Verbindung.
        /// </summary>
        [XmlElement(ElementName = "LegEnd")]
        public LocationRefInternational LegEnd { get; set; }

        /// <summary>
        /// Required
        /// Angaben zum „Verkehrsmittel“ (z. B. Fußweg).
        /// </summary>
        [XmlElement(ElementName = "Service")]
        public ContinuousService Service { get; set; }

        /// <summary>
        /// Frühester Zeitpunkt für den Start dieses Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "TimeWindowStart")]
        public DateTime? TimeWindowStart { get; set; }

        /// <summary>
        /// Spätester Zeitpunkt für das Ende dieses Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "TimeWindowEnd")]
        public DateTime? TimeWindowEnd { get; set; }

        /// <summary>
        /// Dauer dieses Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "Duration", DataType = "duration")]
        public string Duration { get; set; }

        [XmlIgnore]
        public double? DurationInternal => Duration != null ? XmlConvert.ToTimeSpan(Duration).TotalSeconds : null;

        /// <summary>
        /// Beschreibung dieses Verbindungsanteils
        /// </summary>
        [XmlElement(ElementName = "LegDescription")]
        public List<InternationalText> LegDescription { get; set; }

        /// <summary>
        /// Länge dieses Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "Length")]
        public double? Length { get; set; }

        /// <summary>
        /// Detaillierter (geometrischer) Verlauf
        /// </summary>
        [XmlElement(ElementName = "LegTrack")]
        public LegTrack LegTrack { get; set; }

        /// <summary>
        /// Detaillierte Informationen zum geometrischen Ver-lauf, der Wegfolge und Zugänglichkeit.
        /// </summary>
        [XmlElement(ElementName = "NavigationPath")]
        public NavigationPath NavigationPath { get; set; }

        /// <summary>
        /// Verweis auf eine Störungsnachricht. Diese Nachricht kann im TripResponseContext zu finden sein
        /// oder auf anderem Wege bekannt gemacht wer-den.
        /// </summary>
        [XmlElement(ElementName = "SituationFullRef")]
        public List<SituationFullRef> SituationFullRef { get; set; }

        // ignored Extension form documentation
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Teilweg zu einer Verbindung.
    /// One of a, b or c is required
    /// </summary>
    public class TripLeg
    {
        /// <summary>
        /// ID des Teilwegs dieser Verbindung für spätere Refe-renzierung. Eindeutig innerhalb TripResult.
        /// </summary>
        [XmlElement(ElementName = "LegId")]
        public string LegId { get; set; }

        /// <summary>
        /// Required a
        /// Ausprägung des Teilwegs als fahrplanbehafteter
        /// </summary>
        [XmlElement(ElementName = "TimedLeg")]
        public TimedLeg.TimedLeg TimedLeg { get; set; }

        /// <summary>
        /// Required b
        /// Ausprägung des Teilwegs als Umstieg zwischen Verkehrsmitteln
        /// </summary>
        [XmlElement(ElementName = "InterchangeLeg")]
        public InterchangeLeg.InterchangeLeg InterchangeLeg { get; set; }

        /// <summary>
        /// Required c
        /// Ausprägung des Teilwegs als Fortbewegung mit einem kontinuierlich verfügbaren Verkehrsmittel
        /// </summary>
        [XmlElement(ElementName = "ContinuousLeg")]
        public ContinuousLeg.ContinuousLeg ContinuousLeg { get; set; }
    }
}
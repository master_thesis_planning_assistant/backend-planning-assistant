﻿using System;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Struktur für die Definition von Verkehrstagen mittels Bit-Kette.
    /// </summary>
    public class OperatingDays
    {
        /// <summary>
        /// Required
        /// Startdatum des Zeitraums
        /// </summary>
        [XmlElement(ElementName = "From")]
        public DateTime From { get; set; }

        /// <summary>
        /// Required
        /// Endatum des Zeitraums
        /// </summary>
        [XmlElement(ElementName = "To")]
        public DateTime To { get; set; }

        /// <summary>
        /// Bitmuster für die Verkehrstage im Zeitraum von Startdatum (From) bis Enddatum (To). Die Länge des Bitmusters
        /// in Pattern entspricht der Anzahl der Tage von From bis To. Eine „1“ bedeutet, dass das in Frage kommende
        /// Ereignis an dem Tag stattfindet, der der Position in der Bitkette entspricht
        /// </summary>
        [XmlElement(ElementName = "Pattern")]
        public string Pattern { get; set; }
    }
}
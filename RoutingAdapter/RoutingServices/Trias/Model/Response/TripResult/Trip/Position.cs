﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    public class Position
    {
        [XmlElement(ElementName = "Longitude")]
        public double Longitude { get; set; }

        [XmlElement(ElementName = "Latitude")] public double Latitude { get; set; }

        [XmlElement(ElementName = "Altitude")] public double Altitude { get; set; }
    }
}
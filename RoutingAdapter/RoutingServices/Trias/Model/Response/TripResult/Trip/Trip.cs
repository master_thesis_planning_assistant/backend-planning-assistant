﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Daten zu einer einzelnen intermodalen Verbin-dung.
    /// </summary>
    public class Trip
    {
        /// <summary>
        /// Required
        /// ID der Verbindung für spätere Referenzierung bzw. für Debug-Zwecke
        /// </summary>
        [XmlElement(ElementName = "TripId")]
        public string TripId { get; set; }

        /// <summary>
        /// Required
        /// Gesamtdauer der Verbindung.
        /// </summary>
        [XmlElement(ElementName = "Duration", DataType = "duration")]
        public string Duration { get; set; }

        [XmlIgnore]
        public double? DurationInternal => Duration != null ? XmlConvert.ToTimeSpan(Duration).TotalSeconds : null;

        /// <summary>
        /// Required
        /// Startzeitpunkt der Verbindung
        /// </summary>
        [XmlElement(ElementName = "StartTime")]
        public DateTime StartTime { get; set; }

        /// <summary>
        /// Required
        /// Endzeitpunt der Verbindung
        /// </summary>
        [XmlElement(ElementName = "EndTime")]
        public DateTime EndTime { get; set; }

        /// <summary>
        /// Rquired
        /// Anzahl der notwendigen Umsteigevorgänge
        /// </summary>
        [XmlElement(ElementName = "Interchanges")]
        public int Interchanges { get; set; }

        /// <summary>
        /// Gesamtdistanz der Verbindung als Länge des zu-rückzulegenden Weges.
        /// </summary>
        [XmlElement(ElementName = "Distance")]
        public double Distance { get; set; }

        /// <summary>
        /// Teilweg/e dieser Verbindung.
        /// </summary>
        [XmlElement(ElementName = "TripLeg")]
        public List<TripLeg> TripLeg { get; set; }

        /// <summary>
        /// Verkehrstage für diese Verbindung
        /// </summary>
        [XmlElement(ElementName = "OperatingDays")]
        public OperatingDays OperatingDays { get; set; }
    }
}
﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Eigenschaften einer Fahrt zusammen mit dem Fahrtabschnitt, auf dem diese Eigenschaften gelten.
    /// </summary>
    public class ServiceSection
    {
        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, ab dem die Eigenschaften gültig sind.
        /// Falls leer, dann gültig ab Beginn des Fahrwegs.
        /// </summary>
        [XmlElement("FromStopSeqNumber")]
        public int? FromStopSeqNumber { get; set; }

        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, bis zu dem die Eigenschaften gültig sind.
        /// Falls leer, dann gültig bis zum Ende des Fahrwegs.
        /// </summary>
        [XmlElement("ToStopSeqNumber")]
        public int? ToStopSeqNumber { get; set; }

        /// <summary>
        /// Required
        /// Line-Id
        /// </summary>
        [XmlElement("LineRef")]
        public string LineRef { get; set; }

        /// <summary>
        /// Required
        /// Richtungs-ID
        /// </summary>
        [XmlElement("DirectionRef")]
        public string DirectionRef { get; set; }

        /// <summary>
        /// Required
        /// Verkehrsmitteltyp.
        /// </summary>
        [XmlElement("Mode")]
        public Mode Mode { get; set; }

        /// <summary>
        /// Required
        /// Liniennummer oder -name, wie in der Öffentlichkeit bekannt.
        /// </summary>
        [XmlElement("PublishedLineName")]
        public InternationalText PublishedLineName { get; set; }

        /// <summary>
        /// Operator-ID
        /// </summary>
        [XmlElement("OperatorRef")]
        public string OperatorRef { get; set; }

        /// <summary>
        /// Beschreibung des Fahrwegs, z.B. „rechte Rheinstrecke“
        /// </summary>
        [XmlElement("RouteDescription")]
        public InternationalText RouteDescription { get; set; }
        // ignored Via from documentation
    }
}
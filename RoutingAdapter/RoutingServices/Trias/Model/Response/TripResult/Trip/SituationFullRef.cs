﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.TripResponseContext;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    [XmlRoot(ElementName = "SituationFullRef", Namespace = "http://www.vdv.de/trias")]
    public class SituationFullRef
    {
        /// <summary>
        /// Required
        /// Codespace of the data source.
        /// Reference to data in <see cref="TripResponseContext"/>
        /// </summary>
        [XmlElement(ElementName = "ParticipantRef", Namespace = "http://www.siri.org.uk/siri")]
        public string ParticipantRef { get; set; }

        /// <summary>
        /// Required
        /// Unique situation-ID for PtSituationElement.
        /// Format: CODESPACE:SituationNumber:ID
        /// e.g.: ABC:SituationNumber:123
        /// Reference to data in <see cref="TripResponseContext"/>
        /// </summary>
        [XmlElement(ElementName = "SituationNumber", Namespace = "http://www.siri.org.uk/siri")]
        public string SituationNumber { get; set; }
    }
}
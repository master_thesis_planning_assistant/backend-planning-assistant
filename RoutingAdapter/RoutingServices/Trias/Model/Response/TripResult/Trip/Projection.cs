﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip
{
    /// <summary>
    /// Geografische Projektion des Streckenabschnitts als Polygonzug.
    /// </summary>
    public class Projection
    {
        /// <summary>
        /// Required (at least 2)
        /// </summary>
        [XmlElement(ElementName = "Position")]
        public List<Position> Position { get; set; }
    }
}
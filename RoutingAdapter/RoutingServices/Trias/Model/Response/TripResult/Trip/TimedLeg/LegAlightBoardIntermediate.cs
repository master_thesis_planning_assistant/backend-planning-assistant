﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Beschreibt die Einstiegs- mAusstiegssituation oder die Zwischenhalte in ein Verkehrsmittel.
    /// </summary>
    public class LegAlightBoardIntermediate
    {
        /// <summary>
        /// Referenz auf einen Code für einen Haltepunkt
        /// </summary>
        [XmlElement(ElementName = "StopPointRef")]
        public string StopPointRef { get; set; }

        /// <summary>
        /// Required
        /// Name des Haltepunkts für Fahrgastinformation.
        /// </summary>
        [XmlElement("StopPointName")]
        public List<InternationalText> StopPointName { get; set; }

        /// <summary>
        /// Namenszusatz, der bei Platzmangel evtl. auch weg-gelassen werden kann, z. B.: „gegenüber vom Haupteingang“.
        /// </summary>
        [XmlElement("NameSuffix")]
        public List<InternationalText> NameSuffix { get; set; }

        /// <summary>
        /// Name des Steigs/Haltepunkts, wo in das Fahrzeug ein- oder ausgestiegen werden muss (bei Verwen-dung in
        /// Zusammenhang mit einer konkreten Verbin-dungsauskunft, wenn in StopPointName ein allge-meiner Name angegeben
        /// ist, ähnlich Haltestellen-name). Nach Planungsstand.
        /// </summary>
        [XmlElement("PlannedBay")]
        public List<InternationalText> PlannedBay { get; set; }

        /// <summary>
        /// Name des Steigs/Haltepunkts, wo in das Fahrzeug ein- oder ausgestiegen werden muss (bei Verwen-dung in
        /// Zusammenhang mit einer konkreten Verbin-dungsauskunft, wenn in StopPointName ein allge-meiner Name angegeben
        /// ist, ähnlich Haltestellen-name). Nach letztem Prognosestand.
        /// </summary>
        [XmlElement("EstimatedBay")]
        public List<InternationalText> EstimatedBay { get; set; }

        /// <summary>
        /// Informationen zur Ankunft
        /// </summary>
        [XmlElement(ElementName = "ServiceArrival")]
        public ServiceCall ServiceArrival { get; set; }

        /// <summary>
        /// Informationen zur Abfahrt
        /// </summary>
        [XmlElement(ElementName = "ServiceDeparture")]
        public ServiceCall ServiceDeparture { get; set; }

        /// <summary>
        /// Dieser Halt erfüllt eine der in der Anfrage vorgege-benen Via-Bedingungen. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "MeetsViaRequest")]
        public bool? MeetsViaRequest { get; set; }

        /// <summary>
        /// Laufende Nummer des Halts im Fahrweg der Fahrt. Gezählt ab der Starthaltestelle der Fahrt (als Num-mer 1).
        /// </summary>
        [XmlElement(ElementName = "StopSeqNumber")]
        public int? StopSeqNumber { get; set; }

        /// <summary>
        /// Bedarfshalt. Fahrzeug bedient diesen Halt nur nach Voranmeldung. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "DemandStop")]
        public bool? DemandStop { get; set; }

        /// <summary>
        /// Halt, der laut Planung nicht vorgesehen war. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "UnplannedStop")]
        public bool? UnplannedStop { get; set; }

        /// <summary>
        /// Entgegen der Planung findet kein Halt statt. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NotServicedStop")]
        public bool? NotServicedStop { get; set; }

        /// <summary>
        /// An diesem Halt der Fahrt darf nicht eingestiegen werden. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoBoardingAtStop")]
        public bool? NoBoardingAtStop { get; set; }

        /// <summary>
        /// An diesem Halt der Fahrt darf nicht ausgestiegen werden. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "NoAlightingAtStop")]
        public bool? NoAlightingAtStop { get; set; }
    }
}
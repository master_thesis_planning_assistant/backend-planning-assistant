﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Fahrplanfahrt an bestimmtem Tag.
    /// </summary>
    public class DatedJourney
    {
        /// <summary>
        /// Required
        /// Betriebstag der Fahrt
        /// </summary>
        [XmlElement("Service")]
        public string OperatingDayRef { get; set; }

        /// <summary>
        /// Fahrzeug-Id
        /// </summary>
        [XmlElement("VehicleRef")]
        public string VehicleRef { get; set; }

        /// <summary>
        /// Required
        /// Fahrt-ID
        /// </summary>
        [XmlElement("JourneyRef")]
        public string JourneyRef { get; set; }


        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, ab dem die Eigenschaften gültig sind.
        /// Falls leer, dann gültig ab Beginn des Fahrwegs.
        /// </summary>
        [XmlElement("FromStopSeqNumber")]
        public int? FromStopSeqNumber { get; set; }

        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, bis zu dem die Eigenschaften gültig sind.
        /// Falls leer, dann gültig bis zum Ende des Fahrwegs.
        /// </summary>
        [XmlElement("ToStopSeqNumber")]
        public int? ToStopSeqNumber { get; set; }

        /// <summary>
        /// Required
        /// Line-Id
        /// </summary>
        [XmlElement("LineRef")]
        public string LineRef { get; set; }

        /// <summary>
        /// Required
        /// Richtungs-ID
        /// </summary>
        [XmlElement("DirectionRef")]
        public string DirectionRef { get; set; }

        /// <summary>
        /// Required
        /// Verkehrsmitteltyp.
        /// </summary>
        [XmlElement("Mode")]
        public Mode Mode { get; set; }

        /// <summary>
        /// Required
        /// Liniennummer oder -name, wie in der Öffentlichkeit bekannt.
        /// </summary>
        [XmlElement("PublishedLineName")]
        public InternationalText PublishedLineName { get; set; }

        /// <summary>
        /// Operator-ID
        /// </summary>
        [XmlElement("OperatorRef")]
        public string OperatorRef { get; set; }

        /// <summary>
        /// Beschreibung des Fahrwegs, z.B. „rechte Rheinstrecke“
        /// </summary>
        [XmlElement("RouteDescription")]
        public InternationalText RouteDescription { get; set; }
        // ignored Via from documentation
        // ignored Attribute from Documentation

        /// <summary>
        /// ID des ersten Haltepunkts der Fahrt; Starthaltestelle. Vgl
        /// </summary>
        [XmlElement("OriginStopPointRef")]
        public string OriginStopPointRef { get; set; }

        /// <summary>
        /// Name des ersten Haltepunkts der Fahrt, der Start-haltestelle.
        /// </summary>
        [XmlElement("OriginText")]
        public List<InternationalText> OriginText { get; set; }

        /// <summary>
        /// ID des letzten Haltepunkts der Fahrt; Endhaltestelle. Vgl
        /// </summary>
        [XmlElement("DestinationStopPointRef")]
        public string DestinationStopPointRef { get; set; }

        /// <summary>
        /// Required
        /// Name des letzten Haltepunkts der Fahrt, der End-haltestelle oder Fahrtziel.
        /// </summary>
        [XmlElement("DestinationText")]
        public List<InternationalText> DestinationText { get; set; }

        /// <summary>
        /// Gibt an, ob es sich um eine zusätzliche, ungeplante Fahrt handelt. Voreinstellung ist false.
        /// </summary>
        [XmlElement("Unplanned")]
        public bool? Unplanned { get; set; }

        /// <summary>
        /// Gibt an, ob diese Fahrt zur Gänze entfällt. Vorein-stellung ist false.
        /// </summary>
        [XmlElement("Cancelled")]
        public bool? Cancelled { get; set; }

        /// <summary>
        /// Gibt an, ob diese Fahrt einen anderen Weg nimmt. Voreinstellung ist false.
        /// </summary>
        [XmlElement("Deviation")]
        public bool? Deviation { get; set; }

        // ignored Occupancy from docu

        /// <summary>
        /// Verweis auf eine Störungsnachricht. Diese Nachricht kann im Kontext der Antwort (Response context)) zu
        /// finden sein oder auf anderem Wege bekannt ge-macht werden.
        /// </summary>
        [XmlElement("SituationFullRef")]
        public List<SituationFullRef> SituationFullRef { get; set; }
    }
}
﻿using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Ein Streckenabschnitt in einem Verbindungs-teilweg.
    /// </summary>
    public class TrackSection
    {
        /// <summary>
        /// Beginn (Ort) des Streckenabschnitts
        /// </summary>
        [XmlElement(ElementName = "TrackStart")]
        public LocationRefInternational TrackStart { get; set; }

        /// <summary>
        /// Ende (Ort) des Streckenabschnitts
        /// </summary>
        [XmlElement(ElementName = "TrackEnd")]
        public LocationRefInternational TrackEnd { get; set; }

        // ignored Position from docu (replaced with Projection)

        /// <summary>
        /// Geografische Projektion des Streckenabschnitts als Polygonzug.
        /// </summary>
        [XmlElement(ElementName = "Projection")]
        public Projection Projection { get; set; }

        /// <summary>
        /// Name der Straße, auf der dieser Streckenabschnitt liegt.
        /// </summary>
        [XmlElement(ElementName = "RoadName")]
        public string RoadName { get; set; }

        /// <summary>
        /// Zeitdauer, die der Fahrgast braucht, um diesen Streckenabschnitt zu bewältigen.
        /// </summary>
        [XmlElement(ElementName = "Duration", DataType = "duration")]
        public string Duration { get; set; }

        [XmlIgnore]
        public double? DurationInternal => Duration != null ? XmlConvert.ToTimeSpan(Duration).TotalSeconds : null;

        /// <summary>
        /// Länge des Streckenabschnitts
        /// </summary>
        [XmlElement(ElementName = "Length")]
        public double Length { get; set; }

        // ignored Extension from documentation
    }
}
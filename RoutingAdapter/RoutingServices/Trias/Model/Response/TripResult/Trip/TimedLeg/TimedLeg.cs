﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Beinhaltet einen fahrplangebundenen Verbin-dungsanteil (Teilweg).
    /// </summary>
    public class TimedLeg
    {
        /// <summary>
        /// Required
        /// Beginn (Haltepunkt) des Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "LegBoard")]
        public LegAlightBoardIntermediate LegBoard { get; set; }

        /// <summary>
        /// Dazwischenliegende, durchfahrene Haltepunkte auf dem Teilweg zwischen LegBoard und LegAlight
        /// </summary>
        [XmlElement(ElementName = "LegIntermediates")]
        public List<LegAlightBoardIntermediate> LegIntermediates { get; set; }

        /// <summary>
        /// Required
        /// Ende (Haltepunkt) des Teilwegs.
        /// </summary>
        [XmlElement(ElementName = "LegAlight")]
        public LegAlightBoardIntermediate LegAlight { get; set; }

        /// <summary>
        /// Required
        /// Angaben zum Verkehrsmittel, wie Linie, Verkehrs-mitteltyp etc.
        /// </summary>
        [XmlElement(ElementName = "Service")]
        public DatedJourney Service { get; set; }

        /// <summary>
        /// Verkehrstage für diese Verbindung
        /// </summary>
        [XmlElement(ElementName = "OperatingDays")]
        public OperatingDays OperatingDays { get; set; }

        /// <summary>
        /// Menschenlesbare Beschreibung der Verkehrstage, z. B. „Montag bis Freitag“ oder „Sonn- und Feiertag“.
        /// </summary>
        [XmlElement(ElementName = "OperatingDaysDescription")]
        public List<InternationalText> OperatingDaysDescription { get; set; }

        /// <summary>
        /// Detaillierter geometrischer Verlauf.
        /// </summary>
        [XmlElement(ElementName = "LegTrack")]
        public LegTrack LegTrack { get; set; }

        /// <summary>
        /// Parallelfahrten (z.B. bei Flügelungen).
        /// </summary>
        [XmlElement(ElementName = "ParallelService")]
        public List<ParallelService> ParallelService { get; set; }
        // ignored Extension from documentation
    }
}
﻿using System;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Enthält Informationen zu Ankunft oder Abfahrt einer Fahrt an einem Punkt (z.B. Zeiten).
    /// </summary>
    public class ServiceCall
    {
        /// <summary>
        /// Required
        /// Zeit nach Fahrplan.
        /// </summary>
        [XmlElement(ElementName = "TimetabledTime")]
        public DateTime TimetabledTime { get; set; }

        /// <summary>
        /// Tatsächliche Zeit.
        /// </summary>
        [XmlElement(ElementName = "RecordedAtTime")]
        public DateTime RecordedAtTime { get; set; }

        /// <summary>
        /// Erwartete Zeit.
        /// </summary>
        [XmlElement(ElementName = "EstimatedTime")]
        public DateTime EstimatedTime { get; set; }

        /// <summary>
        /// Untere Schranke für erwartete Zeit.
        /// </summary>
        [XmlElement(ElementName = "EstimatedTimeLow")]
        public DateTime EstimatedTimeLow { get; set; }

        /// <summary>
        /// Obere Schranke für erwartete Zeit.
        /// </summary>
        [XmlElement(ElementName = "EstimatedTimeHigh")]
        public DateTime EstimatedTimeHigh { get; set; }
    }
}
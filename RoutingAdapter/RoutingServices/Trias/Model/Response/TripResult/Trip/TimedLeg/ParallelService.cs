﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    /// <summary>
    /// Enthält einen Abschnitt, auf dem eine weitere Fahrt gemeinsam fährt (z.B. bei Flügelungen) und die
    /// entsprechende Parallelfahrt.
    /// </summary>
    public class ParallelService
    {
        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, ab dem die Parallelfahrt beginnt. Falls leer, dann gültig ab
        /// Beginn des Fahrwegs.
        /// </summary>
        [XmlElement(ElementName = "FromStopSeqNumber")]
        public int? FromStopSeqNumber { get; set; }

        /// <summary>
        /// Fahrwegpositionsnummer des Haltepunkts, an dem Parallelfahrt endet. Falls leer, dann gültig bis zum
        /// Ende des Fahrwegs.
        /// </summary>
        [XmlElement(ElementName = "ToStopSeqNumber")]
        public int? ToStopSeqNumber { get; set; }

        /// <summary>
        /// Parallelfahrt.
        /// </summary>
        [XmlElement("Service")]
        public DatedJourney Service { get; set; }
    }
}
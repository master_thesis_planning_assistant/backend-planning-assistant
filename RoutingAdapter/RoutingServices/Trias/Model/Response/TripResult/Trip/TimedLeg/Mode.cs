﻿using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.Trip.TimedLeg
{
    public class Mode
    {
        /// <summary>
        /// Angabe der ÖV-Verkehrsmittelart
        /// </summary>
        [XmlElement("PtMode")]
        public PtMode PtMode { get; set; }

        // ignroed all PtSubmodeChoise Groups from Docu

        /// <summary>
        /// Verkehrsmittelname.
        /// </summary>
        [XmlElement(ElementName = "Name")]
        public InternationalText Name { get; set; }

        /// <summary>
        /// Kurzname oder Abkürzung
        /// </summary>
        [XmlElement(ElementName = "ShortName")]
        public InternationalText ShortName { get; set; }

        /// <summary>
        /// Beschreibender Text.
        /// </summary>
        [XmlElement(ElementName = "Description")]
        public InternationalText Description { get; set; }
    }
}
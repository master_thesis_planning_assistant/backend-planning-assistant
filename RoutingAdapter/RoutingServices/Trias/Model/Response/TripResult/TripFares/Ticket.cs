﻿using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using RoutingAdapter.RoutingServices.Trias.Model.Response.Enums;
using RoutingAdapter.RoutingServices.Trias.Model.Shared.Enums;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.TripFares
{
    public class Ticket
    {
        /// <summary>
        /// Required
        /// Eindeutige Ticket-ID
        /// </summary>
        [XmlElement(ElementName = "TicketId")]
        public string TicketId { get; set; }

        /// <summary>
        /// Required
        /// Name/Bezeichnung des Tickets.
        /// </summary>
        [XmlElement(ElementName = "TicketName")]
        public string TicketName { get; set; }

        /// <summary>
        /// Required
        /// Code für ein Tarifgebiet oder einen Unternehmens-tarif
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityRef")]
        public string FaresAuthorityRef { get; set; }

        /// <summary>
        /// Required
        /// Beschreibung oder Name des Tarifgebiets.
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityText")]
        public string FaresAuthorityText { get; set; }

        /// <summary>
        /// Ticketpreis als Dezimalzahl.
        /// </summary>
        [XmlElement(ElementName = "Price")]
        public double? Price { get; set; }

        /// <summary>
        /// Netto-Ticketpreis als Dezimalzahl für Abrechnungs-zwecke.
        /// </summary>
        [XmlElement(ElementName = "NetPrice")]
        public double? NetPrice { get; set; }

        /// <summary>
        /// Währungscode nach ISO 4217, z. B. „EUR“ oder „GBP“.
        /// </summary>
        [XmlElement(ElementName = "Currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Mehrwertsteuersatz. Voreinstellung ist unknown.
        /// </summary>
        [XmlElement(ElementName = "VatRate")]
        public VatRateOptions? VatRate { get; set; }

        /// <summary>
        /// Tarifstufe (Beispiel aus Nürnberg „10“ oder „10+T“)
        /// </summary>
        [XmlElement(ElementName = "TariffLevel")]
        public string TariffLevel { get; set; }

        /// <summary>
        /// Bezeichnung für Tarifstufen in diesem Zusammenhang (Beispiel aus Nürnberg „Preisstufe“, „Tarifstufe“)
        /// </summary>
        [XmlElement(ElementName = "TariffLevelLabel")]
        public List<InternationalText> TariffLevelLabel { get; set; }

        /// <summary>
        /// Reiseklasse, für die das Ticket gültig ist
        /// </summary>
        [XmlElement(ElementName = "TravelClass")]
        public TravelClass? TravelClass { get; set; }

        /// <summary>
        /// Eine oder mehrere Vielfahrerkarten, die nötig sind, um dieses Ticket erwerben oder nutzen zu dürfen
        /// </summary>
        [XmlElement(ElementName = "RequiredCard")]
        public List<string> RequiredCard { get; set; }

        /// <summary>
        /// Personengruppen, die dieses Ticket nutzen dürfen
        /// </summary>
        [XmlElement(ElementName = "ValidFor")]
        public List<PassengerCategory> ValidFor { get; set; }

        /// <summary>
        /// Maximale zeitliche Gültigkeit des Tickets ab Kauf oder Entwertung.
        /// </summary>
        [XmlElement(ElementName = "ValidityDuration", DataType = "duration")]
        public string ValidityDuration { get; set; }

        public double? ValidityDurationInternal =>
            ValidityDuration != null ? XmlConvert.ToTimeSpan(ValidityDuration).TotalSeconds : null;

        /// <summary>
        /// Beschreibung der zeitlichen Gültigkeit.
        /// </summary>
        [XmlElement(ElementName = "ValidityDurationText")]
        public List<InternationalText> ValidityDurationText { get; set; }

        /// <summary>
        /// Räumliche Gültigkeit des Tickets ausgedrückt durch eine Liste der Tarifzonen, für die das Ticket gilt.
        /// </summary>
        [XmlElement(ElementName = "ValidityFareZones")]
        public FareZoneListInArea ValidityFareZones { get; set; }

        /// <summary>
        /// Beschreibung der räumlichen Gültigkeit.
        /// </summary>
        [XmlElement(ElementName = "ValidityAreaText")]
        public InternationalText ValidityAreaText { get; set; }

        /// <summary>
        /// URL zu Informationsseiten für dieses Ticket
        /// </summary>
        [XmlElement(ElementName = "InfoUrl")]
        public List<WebLink> InfoUrl { get; set; }

        /// <summary>
        /// URL zu Informationsseiten für dieses Ticket
        /// </summary>
        [XmlElement(ElementName = "SaleUrl")]
        public List<WebLink> SaleUrl { get; set; }

        // ignored BookingInfo from documentation

        // ignored Extension from documentation
    }
}
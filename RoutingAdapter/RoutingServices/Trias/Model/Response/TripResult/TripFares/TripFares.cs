﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.TripFares
{
    /// <summary>
    /// Fasst die Ergebnisdaten für die Tarifauskunft zu einer Verbindung (oder zu Teilen einer Verbin-dung) zusammen.
    /// </summary>
    public class TripFares
    {
        /// <summary>
        /// Fehlermeldungen bezogen auf diese Tarifauskunft. Siehe die nachstehende Tabelle für mögliche Werte.
        /// </summary>
        [XmlElement(ElementName = "ErrorMessage")]
        public List<ErrorMessage> ErrorMessage { get; set; }

        /// <summary>
        /// Referenz auf einen Teilweg der Verbindung als Be-ginn der Gültigkeit dieser Tarifauskunft.
        /// </summary>
        [XmlElement(ElementName = "FromTripLegIdRef")]
        public string FromTripLegIdRef { get; set; }

        /// <summary>
        /// Referenz auf einen Teilweg der Verbindung als En-de der Gültigkeit dieser Tarifauskunft.
        /// </summary>
        [XmlElement(ElementName = "ToTripLegIdRef")]
        public string ToTripLegIdRef { get; set; }

        /// <summary>
        /// Die durchfahrenen Tarifzonen auf diesem Abschnitt der Verbindung
        /// </summary>
        [XmlElement(ElementName = "PassedZones")]
        public FareZoneListInArea PassedZones { get; set; }

        /// <summary>
        /// Fahrscheine, die auf diesem Abschnitt der Verbin-dung gültig sind
        /// </summary>
        [XmlElement(ElementName = "Ticket")]
        public List<Ticket> Ticket { get; set; }

        /// <summary>
        /// URL zu Informationsseiten
        /// </summary>
        [XmlElement(ElementName = "StaticInfoURL")]
        public List<WebLink> StaticInfoUrl { get; set; }
    }
}
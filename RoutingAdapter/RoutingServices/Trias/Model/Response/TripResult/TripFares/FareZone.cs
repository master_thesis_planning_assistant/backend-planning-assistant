﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.TripFares
{
    public class FareZone
    {
        /// <summary>
        /// Code für eine Tarifzone
        /// </summary>
        [XmlElement(ElementName = "FareZoneRef")]
        public string FareZoneRef { get; set; }

        /// <summary>
        /// Bezeichnung der Tarifzone für die Fahrgäste.
        /// </summary>
        [XmlElement(ElementName = "FareZoneText")]
        public string FareZoneText { get; set; }
    }
}
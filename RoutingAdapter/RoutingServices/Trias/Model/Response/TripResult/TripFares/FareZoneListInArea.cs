﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.TripResult.TripFares
{
    public class FareZoneListInArea
    {
        /// <summary>
        /// Required
        /// Code für ein Tarifgebiet oder einen Unternehmens-tarif
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityRef")]
        public string FaresAuthorityRef { get; set; }

        /// <summary>
        /// Required
        /// Beschreibung oder Name des Tarifgebiets.
        /// </summary>
        [XmlElement(ElementName = "FaresAuthorityText")]
        public string FaresAuthorityText { get; set; }

        /// <summary>
        /// Required
        /// Eine oder mehrere Tarifzonen
        /// </summary>
        [XmlElement(ElementName = "FareZone")]
        public List<FareZone> FareZone { get; set; }
    }
}
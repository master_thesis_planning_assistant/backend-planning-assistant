﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    /// <summary>
    /// Element zur Auswahl der passenden TRIAS-Antwort.
    /// </summary>
    [XmlRoot(ElementName = "DeliveryPayload", Namespace = "http://www.vdv.de/trias")]
    public class DeliveryPayload
    {
        /// <summary>
        /// Antwort zur intermodalen Verbindungsberechnung
        /// </summary>
        [XmlElement(ElementName = "TripResponse", Namespace = "http://www.vdv.de/trias")]
        public TripResponse TripResponse { get; set; }
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum TurnAction
    {
        [XmlEnum("sharp left")] SharpLeft,
        [XmlEnum("left")] Left,
        [XmlEnum("half left")] HalfLeft,
        [XmlEnum("straight on")] StraightOn,
        [XmlEnum("half right")] HalfRight,
        [XmlEnum("right")] Right,
        [XmlEnum("sharp right")] SharpRight,
        [XmlEnum("uturn")] Uturn,
    }
}
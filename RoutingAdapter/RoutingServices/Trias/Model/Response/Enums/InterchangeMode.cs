﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum InterchangeMode
    {
        [XmlEnum("walk")] Walk,
        [XmlEnum("parkAndRide")] ParkAndRide,
        [XmlEnum("bikeAndRide")] BikeAndRide,
        [XmlEnum("carHire")] CarHire,
        [XmlEnum("bikeHire")] BikeHire,
        [XmlEnum("protectedConnection")] ProtectedConnection,
        [XmlEnum("guaranteedConnection")] GuaranteedConnection,
        [XmlEnum("remainInVehicle")] RemainInVehicle,
        [XmlEnum("changeWithinVehicle")] ChangeWithinVehicle,
        [XmlEnum("checkIn")] CheckIn,
        [XmlEnum("checkOut")] CheckOut,
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum ContinuousMode
    {
        [XmlEnum("walk")] Walk,
        [XmlEnum("demandResponsive")] DemandResponsive,
        [XmlEnum("replacementService")] ReplacementService,
    }
}
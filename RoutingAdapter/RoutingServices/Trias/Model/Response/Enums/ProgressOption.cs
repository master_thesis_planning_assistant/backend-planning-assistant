﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum ProgressOption
    {
        [XmlEnum("open")] Open,

        /// <summary>
        /// the situation is over and traffic has returned to normal
        /// </summary>
        [XmlEnum("closed ")] Closed,
    }
}
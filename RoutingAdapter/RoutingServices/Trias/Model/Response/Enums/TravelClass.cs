﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum TravelClass
    {
        [XmlEnum("all")] All,
        [XmlEnum("first")] First,
        [XmlEnum("second")] Second,
        [XmlEnum("third")] Third,
        [XmlEnum("business")] Business,
        [XmlEnum("economy")] Economy
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum AudienceOptions
    {
        [XmlEnum("public")] Public,
        [XmlEnum("emergencyServices")] EmergencyServices,
        [XmlEnum("staff")] Staff,
        [XmlEnum("stationStaff")] StationStaff,
        [XmlEnum("management")] Management,
        [XmlEnum("infoServices")] InfoServices,
    }
}
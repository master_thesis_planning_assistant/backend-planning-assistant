﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum Manoeuvre
    {
        [XmlEnum("origin")] Origin,
        [XmlEnum("destination")] Destination,
        [XmlEnum("continue")] Continue,
        [XmlEnum("keep")] Keep,
        [XmlEnum("turn")] Turn,
        [XmlEnum("leave")] Leave,
        [XmlEnum("enter")] Enter,
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum SourceType
    {
        [XmlEnum("email")] Email,
        [XmlEnum("phone")] Phone,
        [XmlEnum("fax")] Fax,
        [XmlEnum("post")] Post,
        [XmlEnum("feed")] Feed,
        [XmlEnum("radio")] Radio,
        [XmlEnum("tvweb")] TvWeb,
        [XmlEnum("pager")] Pager,
        [XmlEnum("text")] Text,
        [XmlEnum("other")] Other
    }
}
﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum AccessFeatureType
    {
        [XmlEnum("lift")] Lift,
        [XmlEnum("stairs")] Stairs,
        [XmlEnum("seriesOfStairs")] SeriesOfStairs,
        [XmlEnum("escalator")] Escalator,
        [XmlEnum("ramp")] Ramp,
        [XmlEnum("footpath")] FootPath,
    }
}
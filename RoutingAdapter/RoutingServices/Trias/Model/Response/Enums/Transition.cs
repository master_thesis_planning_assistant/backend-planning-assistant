﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum Transition
    {
        [XmlEnum("up")] Up,
        [XmlEnum("down")] Down,
        [XmlEnum("level")] Level,
        [XmlEnum("upAndDown")] UpAndDown,
        [XmlEnum("downAndUp")] DownAndUp,
    }
}
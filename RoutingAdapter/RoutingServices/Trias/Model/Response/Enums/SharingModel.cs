﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum SharingModel
    {
        [XmlEnum("singleStationBased")] SingleStationBased,
        [XmlEnum("multipleStationBased")] MultipleStationBased,
        [XmlEnum("nonStationBased")] NonStationBased,
    }
}
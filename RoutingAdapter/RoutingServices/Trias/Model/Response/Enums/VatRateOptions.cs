﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum VatRateOptions
    {
        [XmlEnum("no")] No,
        [XmlEnum("full")] Full,
        [XmlEnum("half")] Half,
        [XmlEnum("mixed")] Mixed,
        [XmlEnum("unknown")] Unknown,
    }
}
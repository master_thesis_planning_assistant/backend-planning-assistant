﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response.Enums
{
    public enum AttributeStatus
    {
        [XmlEnum("Unknown")] Unknown,

        /// <summary>
        /// Planned bedeutet, dass die Angabe des Attri-buts auf den Planungsangaben beruht (z.B. ein Zug soll planmäßig
        /// einen Restaurantwagen mit sich führen).
        /// </summary>
        [XmlEnum("Planned")] Planned,

        /// <summary>
        /// AsPlanned bedeutet, dass bereits bekannt ist, dass das Attribut wie geplant vorhanden ist/sein wird
        /// (z.B. ein Zug führt wie geplant ei-nen Restaurantwagen mit sich).
        /// </summary>
        [XmlEnum("AsPlanned")] AsPlanned,

        /// <summary>
        /// NotAsPlanned bedeutet, dass bereits bekannt ist, dass ein Attribut nicht wie geplant vorhan-den ist
        /// (z.B. ein Zug führt im Gegensatz zur Planung doch keinen Restaurantwagen mit sich).
        /// </summary>
        [XmlEnum("NotAsPlanned")] NotAsPlanned,

        /// <summary>
        /// RealtimeUpdate wird verwendet, um ein Attri-but mitzuteilen, das erst nach der Planungszeit bekannt
        /// wurde/entstand (z.B. Hinweis auf die Fahrzeugausstattung).
        /// </summary>
        [XmlEnum("RealtimeUpdate")] RealtimeUpdate,
    }
}
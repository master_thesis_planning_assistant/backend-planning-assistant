﻿using System;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    [XmlRoot(ElementName = "ServiceDelivery", Namespace = "http://www.vdv.de/trias")]
    public class ServiceDelivery
    {
        /// <summary>
        /// Required
        /// Zeitstempel der Antwort.
        /// </summary>
        [XmlElement(ElementName = "ResponseTimestamp", DataType = "dateTime",
            Namespace = "http://www.siri.org.uk/siri")]
        public DateTime ResponseTimestamp { get; set; }

        /// <summary>
        /// ID des antwortenden Teilnehmers.
        /// </summary>
        [XmlElement(ElementName = "ProducerRef", Namespace = "http://www.siri.org.uk/siri")]
        public string ProducerRef { get; set; }

        /// <summary>
        /// Indikator, ob die gesamte Anfrage komplett erfolg-reich bearbeitet werden konnte. Default ist true.
        /// </summary>
        [XmlElement(ElementName = "Status", Namespace = "http://www.siri.org.uk/siri")]
        public bool? Status { get; set; }

        /// <summary>
        /// Indikator, ob noch weitere Aktualisierungen vorlie-gen, die abgerufen werden könnten. Default ist false.
        /// </summary>
        [XmlElement(ElementName = "MoreData", Namespace = "http://www.vdv.de/trias")]
        public bool? MoreData { get; set; }

        /// <summary>
        /// required
        /// Standardsprache, in der Text in der Antwort zurück-gegeben wird, falls nicht pro Element anders ange-geben
        /// </summary>
        [XmlElement(ElementName = "Language", DataType = "language", Namespace = "http://www.vdv.de/trias")]
        public string Language { get; set; }

        [XmlElement(ElementName = "DeliveryPayload", Namespace = "http://www.vdv.de/trias")]
        public DeliveryPayload DeliveryPayload { get; set; }

        // ignored Address from Doku
        // ignored ResponseMessageIdentifier from Doku
        // ignored RequestMessageRef from Doku
        // ignored RequestMessageRef from Doku
        // ignored DataVersion from Doku
        // ignored CalcTime from Doku
        // ignored Signature from Doku
        // ignored CertificateId from Doku
        // ignored Extension from Doku
    }
}
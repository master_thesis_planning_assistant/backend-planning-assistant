﻿using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    [XmlRoot(ElementName = "Trias", Namespace = "http://www.vdv.de/trias")]
    public class RoutingResponse
    {
        /// <summary>
        /// Required
        /// Dienstspezifischer Antwortinhalt
        /// </summary>
        [XmlElement(ElementName = "ServiceDelivery", Namespace = "http://www.vdv.de/trias")]
        public ServiceDelivery ServiceDelivery { get; set; }

        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }

        [XmlAttribute(AttributeName = "version")]
        public string Version { get; set; }
    }
}
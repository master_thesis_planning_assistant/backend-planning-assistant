﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    public class WebLink
    {
        /// <summary>
        /// Required
        /// Beschriftungstext des Links
        /// </summary>
        [XmlElement(ElementName = "ValidityDurationText")]
        public List<InternationalText> Label { get; set; }

        /// <summary>
        /// Required
        /// URL zur Web-Ressource.
        /// </summary>
        [XmlElement(ElementName = "Url")]
        public string Url { get; set; }
    }
}
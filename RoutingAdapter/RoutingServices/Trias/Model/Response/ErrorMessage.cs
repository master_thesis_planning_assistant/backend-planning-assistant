﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace RoutingAdapter.RoutingServices.Trias.Model.Response
{
    /// <summary>
    /// Struktur zur Meldung von Fehlerzuständen.
    /// </summary>
    public class ErrorMessage
    {
        /// <summary>
        /// Code des Fehlerzustands.
        /// </summary>
        [XmlElement(ElementName = "Code")]
        public string Code { get; set; }

        /// <summary>
        /// Beschreibung des Fehlerzustands.
        /// </summary>
        [XmlElement(ElementName = "Text")]
        public List<InternationalText> Text { get; set; }

        public string GetErrorMessageString()
        {
            if (Text == null || !Text.Any()) return Code;

            return $"{Code} - {string.Join(", ", Text.Select(t => t.Text))}";
        }
    }
}
﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using RoutingAdapter.RoutingServices.Trias.Client;

namespace RoutingAdapter.RoutingServices.Trias
{
    public static class TriasServiceCollectionExtension
    {
        public static IServiceCollection AddTrias(this IServiceCollection serviceCollection,
            string connectionString, IConfigurationSection configurationSection)
        {
            // Add Trias client
            serviceCollection.Configure<TriasClientConfig>(configurationSection);
            serviceCollection.AddHttpClient(TriasClient.HttpClientName,
                client => { client.BaseAddress = new Uri(connectionString); });
            serviceCollection.AddScoped<ITriasClient, TriasClient>();
            // Add Trias adapter to routing services
            serviceCollection.AddScoped<IRoutingService, TriasRoutingServiceAdapter>();
            return serviceCollection;
        }
    }
}
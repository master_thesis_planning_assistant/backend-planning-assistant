﻿namespace RoutingAdapter.Model
{
    public class SpecificRoutingResponseWrapper<T>
    {
        public string ActivityGuid { get; set; }
        public T RoutingResponse { get; set; }
    }
}
﻿namespace RoutingAdapter.Model
{
    public class SpecificRoutingRequestWrapper<T>
    {
        public string ActivityGuid { get; set; }
        public T RoutingRequest { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace RoutingAdapter.Model
{
    public class RoutesForActivity
    {
        public string ActivityGuid { get; set; }
        public List<Route.ActivityRoute> Routes { get; set; }
    }
}
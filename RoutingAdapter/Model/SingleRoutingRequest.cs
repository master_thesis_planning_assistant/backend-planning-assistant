﻿using System.Collections.Generic;
using RoutingAdapter.Model.Preferences;

namespace RoutingAdapter.Model
{
    public class SingleRoutingRequest
    {
        public Preferences.Preferences Preferences { get; set; }
        public MobilityDemand MobilityDemand { get; set; }

        /// <summary>
        /// This Modalities get calculated from the preferences.
        /// For routing-Services that only support one modality per request the cardinality will be 1 (handled by the routing service adapter)!
        /// </summary>
        public List<RoutingMode> Modalities { get; set; }
    }
}
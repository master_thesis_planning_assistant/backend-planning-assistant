﻿namespace RoutingAdapter.Model.Route
{
    public class NamedGeoLocation
    {
        public string Name { get; set; }
        public GeoLocation GeoLocation { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace RoutingAdapter.Model.Route
{
    public class ActivityRoute
    {
        public string Guid { get; set; }
        public string ServiceName { get; set; }
        public ICollection<ActivityRouteLeg> Legs { get; set; }
        public Polyline Polyline { get; set; }
        public Address Start { get; set; }
        public DateTime Departure { get; set; }
        public Address Destination { get; set; }
        public DateTime Arrival { get; set; }
        public Bounds Bounds { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
    }
}
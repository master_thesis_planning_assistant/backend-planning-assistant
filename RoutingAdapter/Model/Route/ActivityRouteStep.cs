﻿namespace RoutingAdapter.Model.Route
{
    public class ActivityRouteStep
    {
        public NamedGeoLocation StartLocation { get; set; }
        public NamedGeoLocation EndLocation { get; set; }
        public Polyline Polyline { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
        public string Action { get; set; }
    }
}
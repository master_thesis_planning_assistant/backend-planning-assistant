﻿namespace RoutingAdapter.Model.Route
{
    public class Bounds
    {
        public GeoLocation Southwest { get; set; }
        public GeoLocation Northeast { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RoutingAdapter.Model.Route
{
    public class Polyline
    {
        public List<GeoLocation> Points { get; set; }

        /// <summary>
        /// Decode google style polyline coordinates.
        /// Code from: https://gist.github.com/shinyzhu/4617989
        /// See https://developers.google.com/maps/documentation/utilities/polylinealgorithm for Algorithm references
        /// </summary>
        /// <param name="encodedPoints"></param>
        /// <param name="precision">Determines digits of decimal precision. Default is 5 however e.g. valhalla uses 6</param>
        /// <returns></returns>
        public static IEnumerable<GeoLocation> DecodeEncodedPolyline(string encodedPoints, int precision = 5)
        {
            if (string.IsNullOrEmpty(encodedPoints))
                throw new ArgumentNullException(nameof(encodedPoints));

            char[] polylineChars = encodedPoints.ToCharArray();
            int index = 0;

            int currentLat = 0;
            int currentLng = 0;
            int next5bits;
            int sum;
            int shifter;
            var factor = Math.Pow(10, precision);

            while (index < polylineChars.Length)
            {
                // calculate next latitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int) polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length)
                    break;

                currentLat += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                //calculate next longitude
                sum = 0;
                shifter = 0;
                do
                {
                    next5bits = (int) polylineChars[index++] - 63;
                    sum |= (next5bits & 31) << shifter;
                    shifter += 5;
                } while (next5bits >= 32 && index < polylineChars.Length);

                if (index >= polylineChars.Length && next5bits >= 32)
                    break;

                currentLng += (sum & 1) == 1 ? ~(sum >> 1) : (sum >> 1);

                yield return new GeoLocation
                {
                    Latitude = Convert.ToDouble(currentLat) / factor,
                    Longitude = Convert.ToDouble(currentLng) / factor
                };
            }
        }

        public Bounds GetBounds()
        {
            if (Points == null || Points.Count == 0) return null;
            return new Bounds
            {
                Southwest = new GeoLocation
                {
                    Longitude = Points.Min(p => p.Longitude),
                    Latitude = Points.Min(p => p.Latitude)
                },
                Northeast = new GeoLocation
                {
                    Longitude = Points.Max(p => p.Longitude),
                    Latitude = Points.Max(p => p.Latitude)
                },
            };
        }

        public Polyline GetSegment(int startIndex, int endIndex)
        {
            return new Polyline
            {
                Points = Points.GetRange(startIndex, endIndex - startIndex),
            };
        }

        public GeoLocation GetPointAt(int index)
        {
            return Points[index];
        }

        public List<double[]> ToPointList()
        {
            return Points.Select(p => new[] {p.Latitude, p.Longitude}).ToList();
        }
    }
}
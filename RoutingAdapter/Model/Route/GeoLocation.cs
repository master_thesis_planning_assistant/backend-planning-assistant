﻿namespace RoutingAdapter.Model.Route
{
    public class GeoLocation
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
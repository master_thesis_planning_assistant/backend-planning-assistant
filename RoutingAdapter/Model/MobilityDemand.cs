﻿using System;
using RoutingAdapter.Model.Route;

namespace RoutingAdapter.Model
{
    public class MobilityDemand
    {
        public string ActivityGuid { get; set; }
        public Address From { get; set; }
        public Address To { get; set; }
        public DateTime At { get; set; }
    }
}
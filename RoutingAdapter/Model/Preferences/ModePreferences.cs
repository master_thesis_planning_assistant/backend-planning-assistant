﻿using System.Collections.Generic;

namespace RoutingAdapter.Model.Preferences
{
    public class ModePreferences
    {
        public List<RoutingMode> ExcludedModes { get; set; }
        public List<RoutingMode> NeutralModes { get; set; }
        public List<RoutingMode> PreferredModes { get; set; }
    }
}
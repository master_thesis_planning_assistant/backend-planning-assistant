﻿namespace RoutingAdapter.Model.Preferences
{
    public class Geofence
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public double Radius { get; set; }
    }
}
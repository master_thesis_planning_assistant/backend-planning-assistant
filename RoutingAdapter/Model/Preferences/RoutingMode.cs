﻿using System.Runtime.Serialization;
using System.Xml.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.Model.Preferences
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoutingMode
    {
        [XmlEnum("walk")] [EnumMember(Value = "walk")]
        Walk,

        [XmlEnum("Bikesharing")] [EnumMember(Value = "Bikesharing")]
        BikeSharing,

        [XmlEnum("bike")] [EnumMember(Value = "bike")]
        Bike,

        [XmlEnum("public transport")] [EnumMember(Value = "public transport")]
        PublicTransport,

        [XmlEnum("car")] [EnumMember(Value = "car")]
        Car,

        [XmlEnum("carsharing")] [EnumMember(Value = "carsharing")]
        CarSharing,
    }
}
﻿using System;
using System.Collections.Generic;

namespace RoutingAdapter.Services.RouteConsolidation.RouteDistanceCalculations
{
    /// <summary>
    /// Calculates the Fechet Distance.
    /// Code taken from https://github.com/Thought-Weaver/Frechet-Distance
    /// </summary>
    public static class FrechetDistance
    {
        private static double EuclideanDistance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        }

        private static double ComputeDistance(double[,] distances, int i, int j, List<double[]> P, List<double[]> Q)
        {
            if (distances[i, j] > -1)
                return distances[i, j];

            if (i == 0 && j == 0)
                distances[i, j] = EuclideanDistance(P[0][0], P[0][1], Q[0][0], Q[0][1]);
            else if (i > 0 && j == 0)
                distances[i, j] = Math.Max(ComputeDistance(distances, i - 1, 0, P, Q),
                    EuclideanDistance(P[i][0], P[i][1], Q[0][0], Q[0][1]));
            else if (i == 0 && j > 0)
                distances[i, j] = Math.Max(ComputeDistance(distances, 0, j - 1, P, Q),
                    EuclideanDistance(P[0][0], P[0][1], Q[j][0], Q[j][1]));
            else if (i > 0 && j > 0)
                distances[i, j] = Math.Max(Math.Min(ComputeDistance(distances, i - 1, j, P, Q),
                        Math.Min(ComputeDistance(distances, i - 1, j - 1, P, Q),
                            ComputeDistance(distances, i, j - 1, P, Q))),
                    EuclideanDistance(P[i][0], P[i][1], Q[j][0], Q[j][1]));
            else
                distances[i, j] = Double.PositiveInfinity;

            return distances[i, j];
        }

        public static double Calculate(List<double[]> P, List<double[]> Q)
        {
            double[,] distances = new double[P.Count, Q.Count];
            for (int y = 0; y < P.Count; y++)
            for (int x = 0; x < Q.Count; x++)
                distances[y, x] = -1;

            return ComputeDistance(distances, P.Count - 1, Q.Count - 1, P, Q);
        }
    }
}
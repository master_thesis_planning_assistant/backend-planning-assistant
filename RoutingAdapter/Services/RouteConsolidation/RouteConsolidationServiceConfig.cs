﻿namespace RoutingAdapter.Services.RouteConsolidation
{
    public class RouteConsolidationServiceConfig
    {
        public double FrechetDistanceThreshold { get; set; }
    }
}
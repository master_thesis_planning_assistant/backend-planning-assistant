﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;
using RoutingAdapter.Services.RouteConsolidation.RouteDistanceCalculations;

namespace RoutingAdapter.Services.RouteConsolidation
{
    public class RouteConsolidationService : IRouteConsolidationService
    {
        private readonly IOptions<RouteConsolidationServiceConfig> _config;
        private readonly ILogger<RouteConsolidationService> _logger;

        public RouteConsolidationService(IOptions<RouteConsolidationServiceConfig> config,
            ILogger<RouteConsolidationService> logger)
        {
            _config = config;
            _logger = logger;
        }

        public ICollection<RoutesForActivity> ConsolidateRoutes(ICollection<RoutesForActivity> routesForActivities,
            bool filterRouteDuplicates)
        {
            var watch = Stopwatch.StartNew();
            // group routes by activity
            var activityGroups = routesForActivities.GroupBy(rfa => rfa.ActivityGuid).ToList();
            _logger.LogTrace(
                $"Consolidating {routesForActivities.SelectMany(r => r.Routes).Count()} routes of {activityGroups.Count} different activities");
            var result = new List<RoutesForActivity>();
            foreach (var activityGroup in activityGroups)
            {
                // Group all routes of this activity under one RoutesForActivity object
                var routesForActivityResult = new RoutesForActivity
                {
                    ActivityGuid = activityGroup.Key,
                    Routes = new List<ActivityRoute>(),
                };

                var routesOfActivity = activityGroup.SelectMany(groupElement => groupElement.Routes).ToList();

                // skip filtering of duplicate routes if not wished
                if (!filterRouteDuplicates)
                {
                    routesForActivityResult.Routes = routesOfActivity;
                    _logger.LogTrace(
                        $"Skipping filtering of route duplicates of activity {activityGroup.Key} with {routesOfActivity.Count} routes");
                }
                else
                {
                    // group routes of activity by modality to filter duplicates for each modality
                    var modalityGroups =
                        routesOfActivity.GroupBy(GetModalityKeyOfActivityRoute).ToList();
                    foreach (var modalityGroup in modalityGroups)
                    {
                        // Do not filter duplicates for public transport, since filtering on spatial information does
                        // not work for always same routes (e.g. rail tracks) with different times
                        if (modalityGroup.Key.Contains(RoutingMode.PublicTransport.ToString()))
                        {
                            _logger.LogTrace(
                                $"Skipped filtering of Modality group {modalityGroup.Key} of activity {activityGroup.Key} with {modalityGroup.Count()} routes since it contained modality {RoutingMode.PublicTransport.ToString()}");
                            routesForActivityResult.Routes.AddRange(modalityGroup.ToList());
                        }
                        else
                        {
                            // filter duplicates for this modality group
                            var routesWithoutDuplicates = FilterDuplicates(modalityGroup.ToList());
                            _logger.LogTrace(
                                $"Modality group {modalityGroup.Key} of activity {activityGroup.Key} contained {modalityGroup.Count()} routes with {modalityGroup.Count() - routesWithoutDuplicates.Count} duplicates");
                            routesForActivityResult.Routes.AddRange(routesWithoutDuplicates);
                        }
                    }
                }

                result.Add(routesForActivityResult);
            }

            _logger.LogTrace($"Route consolidation took {(watch.Elapsed).TotalSeconds} seconds");
            return result;
        }

        private string GetModalityKeyOfActivityRoute(ActivityRoute route)
        {
            var distinctModalities = route.Legs
                .Select(l => l.Modality.ToString())
                .Distinct()
                .OrderBy(x => x);
            return string.Join("-", distinctModalities);
        }

        private ICollection<ActivityRoute> FilterDuplicates(ICollection<ActivityRoute> routes)
        {
            if (routes.Count < 2) return routes;

            // create pairs of routes and route trajectories
            var routeTrajectoryPair =
                routes.Select(route => new {route = route, trajectory = route.Polyline.ToPointList()}).ToList();

            // compare each route to all other routes using the Frenchet-Distance.
            // if a route is similar to another route it is detected as duplicate and therefore removed from the comparison.
            var i = 0;
            while (i < routeTrajectoryPair.Count)
            {
                for (var j = 1; j < routeTrajectoryPair.Count; j++)
                {
                    var distance =
                        FrechetDistance.Calculate(routeTrajectoryPair[i].trajectory, routeTrajectoryPair[j].trajectory);
                    if (distance < _config.Value.FrechetDistanceThreshold) routeTrajectoryPair.RemoveAt(j);
                }

                i++;
            }

            return routeTrajectoryPair.Select(dr => dr.route).ToList();
        }
    }
}
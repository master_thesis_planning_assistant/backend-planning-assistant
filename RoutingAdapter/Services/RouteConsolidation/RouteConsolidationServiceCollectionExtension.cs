﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace RoutingAdapter.Services.RouteConsolidation
{
    public static class RouteConsolidationServiceCollectionExtension
    {
        public static IServiceCollection AddRouteConsolidationService(this IServiceCollection serviceCollection,
            IConfigurationSection configuration)
        {
            serviceCollection.Configure<RouteConsolidationServiceConfig>(configuration);
            serviceCollection.AddScoped<IRouteConsolidationService, RouteConsolidationService>();
            return serviceCollection;
        }
    }
}
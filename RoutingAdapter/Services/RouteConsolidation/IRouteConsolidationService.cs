﻿using System.Collections.Generic;
using RoutingAdapter.Model;

namespace RoutingAdapter.Services.RouteConsolidation
{
    public interface IRouteConsolidationService
    {
        /// <summary>
        /// Consolidates routes and filters duplicates, by comparing routes with same mobility and same activity and removing routes that are to similar.
        /// Similarity is calculated using the frechet distance. The threshold for similarity can be set in the appsettings.json. 
        /// </summary>
        /// <param name="routesForActivities">Routes to consolidate</param>
        /// <param name="filterRouteDuplicates">Filter to similar routes or keep them in the consolidated routes</param>
        /// <returns>Routes without to similar routes</returns>
        ICollection<RoutesForActivity> ConsolidateRoutes(ICollection<RoutesForActivity> routesForActivities,
            bool filterRouteDuplicates);
    }
}
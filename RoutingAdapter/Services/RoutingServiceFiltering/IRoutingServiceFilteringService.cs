﻿using System.Collections.Generic;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.RoutingServices;

namespace RoutingAdapter.Services.RoutingServiceFiltering
{
    public interface IRoutingServiceFilteringService
    {
        /// <summary>
        /// Filters the available routing services.
        /// Services that can not provide routes for the modalities requested in the preferences are filtered.
        /// </summary>
        /// <param name="routingServices">Available routing services</param>
        /// <param name="preferences">Preferences containing the preferred modalities of the user</param>
        /// <returns>Routing services suitable for the requested modalities</returns>
        ICollection<IRoutingService> FilterRoutingServicesForRelevance(ICollection<IRoutingService> routingServices,
            ICollection<Preferences> preferences);
    }
}
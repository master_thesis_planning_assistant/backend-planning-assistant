﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using RoutingAdapter.Infrastructure.EnumHelper;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.RoutingServices;

namespace RoutingAdapter.Services.RoutingServiceFiltering
{
    public class RoutingServiceFilteringService : IRoutingServiceFilteringService
    {
        private readonly ILogger<RoutingServiceFilteringService> _logger;

        public RoutingServiceFilteringService(ILogger<RoutingServiceFilteringService> logger)
        {
            _logger = logger;
        }

        public ICollection<IRoutingService> FilterRoutingServicesForRelevance(
            ICollection<IRoutingService> routingServices, ICollection<Preferences> preferences)
        {
            if (preferences == null || preferences.Count == 0)
            {
                _logger.LogTrace("Calling all RoutingServices since no preferences are given");
                return routingServices;
            }

            var relevantModalities = preferences
                .SelectMany(pref => pref.ModePreferences.PreferredModes.Concat(pref.ModePreferences.NeutralModes))
                .Distinct()
                .ToList();

            _logger.LogTrace("Calling only services that support one of the following modalities: " +
                             string.Join(", ", relevantModalities.Select(x => x.GetStringValue())));

            return routingServices.Where(routingService =>
                    routingService.SupportedModalities.Any(mod => relevantModalities.Contains(mod)))
                .ToList();
        }
    }
}
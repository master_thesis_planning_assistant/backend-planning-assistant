﻿using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.Xsl;
using Microsoft.Extensions.Logging;

namespace RoutingAdapter.Services.XsltTransformer
{
    public class XsltTransformerService : IXsltTransformerService
    {
        private readonly ILogger<XsltTransformerService> _logger;

        public XsltTransformerService(ILogger<XsltTransformerService> logger)
        {
            _logger = logger;
        }

        public TK Transform<T, TK>(T origin, string xslt)
        {
            // 1 Serialize origin object
            var originXmlSerializer = new XmlSerializer(typeof(T));
            var originWriter = new StringWriter();
            originXmlSerializer.Serialize(originWriter, origin);
            var originXml = originWriter.ToString();

            var originReader = new StringReader(originXml);
            var originDocument = XDocument.Load(originReader);

            _logger.LogTrace("Transformation Original-Document: \n" + originDocument);

            // Transform origin object (as xml) using XSLT
            var destinationDocument = new XDocument();

            using (var xsltStringReader = new StringReader(xslt))
            {
                using (var xsltReader = XmlReader.Create(xsltStringReader))
                {
                    var transformer = new XslCompiledTransform();
                    transformer.Load(xsltReader);
                    using (var originDocumentReader = originDocument.CreateReader())
                    {
                        using (var destinationDocumentWriter = destinationDocument.CreateWriter())
                        {
                            transformer.Transform(originDocumentReader, destinationDocumentWriter);
                        }
                    }
                }
            }

            // 3 Deserialize destination object.
            _logger.LogTrace("Transformation Destination-Document: \n" + destinationDocument);
            var destinationXml = destinationDocument.ToString();
            var xmlDeserializer = new XmlSerializer(typeof(TK));

            var destinationReader = new StringReader(destinationXml);
            var destination = (TK) xmlDeserializer.Deserialize(destinationReader);
            return destination;
        }
    }
}
﻿namespace RoutingAdapter.Services.XsltTransformer
{
    public interface IXsltTransformerService
    {
        /// <summary>
        /// Transforms an object into another object using xslt.
        /// </summary>
        /// <param name="origin">Object to be transformed</param>
        /// <param name="xslt">Xslt defining the transformation</param>
        /// <typeparam name="T">Type of the origin object</typeparam>
        /// <typeparam name="TK">Type of the destination object</typeparam>
        /// <returns>The transformed object</returns>
        public TK Transform<T, TK>(T origin, string xslt);
    }
}
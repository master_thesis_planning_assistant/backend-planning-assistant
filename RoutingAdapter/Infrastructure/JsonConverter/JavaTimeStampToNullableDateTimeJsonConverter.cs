﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace RoutingAdapter.Infrastructure.JsonConverter
{
    /// <summary>
    /// Custom TimeStamp converter needed for the date format used by the OpenTripPlanner
    /// </summary>
    public class JavaTimeStampToNullableDateTimeJsonConverter : DateTimeConverterBase
    {
        private static readonly DateTime Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
            }
            else
            {
                writer.WriteValue($"{((DateTime?) value).Value:O}");
            }
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                return null;
            }

            return Epoch.AddMilliseconds((long) reader.Value);
        }
    }
}
﻿using System;
using System.Runtime.Serialization;

namespace RoutingAdapter.Infrastructure.EnumHelper
{
    public static class GetStringValueByEnumValueExtension
    {
        /// <summary>
        /// Gets string value for a given enums value, this will
        /// only work if you assign the EnumMember attribute to
        /// the items in your enum.
        /// </summary>
        /// <param name="value">EnumMember to get the string value from</param>
        /// <returns>String value of this EnumMember</returns>
        public static string GetStringValue(this Enum value)
        {
            var fieldInfo = value.GetType().GetField(value.ToString());

            if (fieldInfo == null) return null;

            return fieldInfo.GetCustomAttributes(
                typeof(EnumMemberAttribute), false) is EnumMemberAttribute[] attributes && attributes.Length > 0
                ? attributes[0].Value
                : null;
        }
    }
}
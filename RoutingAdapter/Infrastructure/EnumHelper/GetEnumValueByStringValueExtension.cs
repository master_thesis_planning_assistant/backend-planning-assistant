﻿using System;

namespace RoutingAdapter.Infrastructure.EnumHelper
{
    public static class GetEnumValueByStringValueExtension
    {
        /// <summary>
        /// Gets the EnumMember matching to this String.
        /// Note that this will only work if the used Enum, provided by the Typeparam T uses the EnumMember Attribute!
        /// </summary>
        /// <param name="value">String to get the EnumMember for</param>
        /// <typeparam name="T">Enum to get EnumMember from</typeparam>
        /// <returns>The EnumMMember matching to this String</returns>
        /// <exception cref="ArgumentException">Thrown if no EnumMember for the provided string is found</exception>
        public static T GetEnumValue<T>(this string value) where T : Enum
        {
            foreach (var enumValue in (T[]) Enum.GetValues(typeof(T)))
            {
                if (enumValue.GetStringValue() == value) return enumValue;
            }

            throw new ArgumentException($"No EnumMember for type {nameof(T)} and name {value} found.");
        }
    }
}
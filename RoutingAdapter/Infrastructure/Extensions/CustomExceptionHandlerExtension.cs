﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Reflection;
using System.Text.Json;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;

namespace RoutingAdapter.Infrastructure.Extensions
{
    public static class CustomExceptionHandlerExtension
    {
        public static void UseCustomExceptionHandler(this IApplicationBuilder app)
        {
            app.UseExceptionHandler(errorApp =>
            {
                errorApp.Run(async context =>
                {
                    context.Response.ContentType = MediaTypeNames.Application.Json;
                    var exceptionHandlerPathFeature = context.Features.Get<IExceptionHandlerPathFeature>();
                    var exception = exceptionHandlerPathFeature?.Error;

                    while (exception is TargetInvocationException targetInvocationException &&
                           targetInvocationException.InnerException != null)
                    {
                        exception = targetInvocationException.InnerException;
                    }

                    switch (exception)
                    {
                        case ArgumentException e:
                            context.Response.StatusCode = StatusCodes.Status400BadRequest;

                            await context.Response.WriteAsync(JsonSerializer.Serialize(e.Message));
                            break;

                        case KeyNotFoundException e:
                            context.Response.StatusCode = StatusCodes.Status404NotFound;
                            await context.Response.WriteAsync(JsonSerializer.Serialize(e.Message));
                            break;

                        case { } e:
                            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                            await context.Response.WriteAsync(JsonSerializer.Serialize(e.Message));
                            break;

                        default:
                            context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                            await context.Response.WriteAsync("\"Something went wrong\"");
                            break;
                    }
                });
            });
        }
    }
}
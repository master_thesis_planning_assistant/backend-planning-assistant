﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;

namespace RoutingAdapter.Infrastructure.Extensions
{
    public static class LoggingExtension
    {
        public static void AddLoggingWithFormatter(this IServiceCollection serviceCollection,
            string formatterName = ConsoleFormatterNames.Simple)
        {
            serviceCollection.AddLogging(x => x.AddConsole(c => c.FormatterName = formatterName));
        }
    }
}
﻿using AutoMapper;
using RoutingAdapter.Controllers.Dtos.Response;
using RoutingAdapter.Controllers.Dtos.Response.Route;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Route;

namespace RoutingAdapter.Infrastructure.AutoMapper
{
    public class ResponseMappingProfile : Profile
    {
        public ResponseMappingProfile()
        {
            CreateMap<RoutesForActivity, RoutesForActivityDto>();
            CreateMap<ActivityRoute, RouteDto>();
            CreateMap<ActivityRouteLeg, LegDto>();
            CreateMap<ActivityRouteStep, StepDto>();
            CreateMap<Address, AddressDto>();
            CreateMap<Bounds, BoundsDto>();
            CreateMap<GeoLocation, GeoLocationDto>();
            CreateMap<NamedGeoLocation, NamedGeoLocationDto>();
            CreateMap<Polyline, PolylineDto>();
        }
    }
}
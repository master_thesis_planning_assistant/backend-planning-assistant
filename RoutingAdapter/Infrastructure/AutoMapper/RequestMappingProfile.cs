﻿using AutoMapper;
using RoutingAdapter.Controllers.Dtos.Request;
using RoutingAdapter.Controllers.Dtos.Request.Preferences;
using RoutingAdapter.Controllers.Dtos.Response.Route;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.Model.Route;

namespace RoutingAdapter.Infrastructure.AutoMapper
{
    public class RequestMappingProfile : Profile
    {
        public RequestMappingProfile()
        {
            CreateMap<MobilityDemandDto, MobilityDemand>();
            CreateMap<PreferencesDto, Preferences>();
            CreateMap<ConnectionPreferencesDto, ConnectionPreferences>();
            CreateMap<GeofenceDto, Geofence>();
            CreateMap<ModePreferencesDto, ModePreferences>();
            CreateMap<WeightingDto, Weighting>();
            CreateMap<AddressDto, Address>();
            CreateMap<GeoLocationDto, GeoLocation>();
        }
    }
}
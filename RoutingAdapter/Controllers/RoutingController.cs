﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoutingAdapter.Controllers.Dtos;
using RoutingAdapter.Controllers.Dtos.Request;
using RoutingAdapter.Controllers.Dtos.Request.Preferences;
using RoutingAdapter.Controllers.Dtos.Response;
using RoutingAdapter.Model;
using RoutingAdapter.Model.Preferences;
using RoutingAdapter.RoutingServices;
using RoutingAdapter.Services.RouteConsolidation;
using RoutingAdapter.Services.RoutingServiceFiltering;

namespace RoutingAdapter.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class RoutingController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRouteConsolidationService _routeConsolidationService;
        private readonly IRoutingServiceFilteringService _routingServiceFilteringService;
        private readonly ICollection<IRoutingService> _routingServices;

        public RoutingController(IEnumerable<IRoutingService> routingServices,
            IMapper mapper, IRoutingServiceFilteringService routingServiceFilteringService,
            IRouteConsolidationService routeConsolidationService)
        {
            _mapper = mapper;
            _routingServiceFilteringService = routingServiceFilteringService;
            _routeConsolidationService = routeConsolidationService;
            _routingServices = routingServices.ToList();
        }

        /// <summary>
        /// Get all available routing services of the routing-adapter and their supported modalities
        /// </summary>
        /// <returns>List of available routing services and their supported modalities</returns>
        [HttpGet("services")]
        [ProducesResponseType(typeof(ICollection<AvailableServiceDto>), StatusCodes.Status200OK)]
        public ActionResult<ICollection<AvailableServiceDto>> GetAvailableServices()
        {
            var availableServices = _routingServices.Select(rs => new AvailableServiceDto
            {
                ServiceName = rs.ServiceName,
                SupportedModalities = rs.SupportedModalities,
            });

            return Ok(availableServices);
        }

        /// <summary>
        ///  Given a set of mobility demands and preferences, calculate possible routes for each mobility demand by requesting different routing services.
        ///  The preferences are used to personalize the resulting routes by setting adjusting the parameters of the requested routing services. 
        /// </summary>
        /// <param name="request">Mobility demands to calculate routes for and preferences for those routes</param>
        /// <returns>Routes for the given mobility demands based on the given preferences</returns>
        [HttpPost()]
        [ProducesResponseType(typeof(RoutingResponseDto), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<RoutingResponseDto>> GetRoutesForActivitiesAsync(
            [FromBody] RoutingRequestDto request)
        {
            if (request.MobilityDemands == null || request.MobilityDemands.Count == 0)
            {
                return BadRequest("MobilityDemands have to be present");
            }

            var preferences = _mapper.Map<ICollection<PreferencesDto>, ICollection<Preferences>>(request.Preferences);
            var mobilityDemands =
                _mapper.Map<ICollection<MobilityDemandDto>, ICollection<MobilityDemand>>(request.MobilityDemands);


            // 1. Filter RoutingServices For relevance
            var routingServicesToRequest =
                _routingServiceFilteringService.FilterRoutingServicesForRelevance(_routingServices, preferences);

            // 2. Make Requests on RoutingServices
            var pendingRequests = routingServicesToRequest
                .Select(routingService => routingService.MakeRoutingRequestAsync(mobilityDemands, preferences));
            var routingServiceResponses = await Task.WhenAll(pendingRequests);

            // 3. Consolidate resulting Routes
            var routesForActivities = routingServiceResponses.SelectMany(x => x).ToList();
            routesForActivities = _routeConsolidationService
                .ConsolidateRoutes(routesForActivities, request.FilterRouteDuplicates).ToList();


            var response = new RoutingResponseDto
            {
                RoutesForActivities =
                    _mapper.Map<ICollection<RoutesForActivity>, ICollection<RoutesForActivityDto>>(routesForActivities),
            };
            return response;
        }
    }
}
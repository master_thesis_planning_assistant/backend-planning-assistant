﻿using System.Collections.Generic;
using RoutingAdapter.Model.Preferences;

namespace RoutingAdapter.Controllers.Dtos.Request.Preferences
{
    public class ModePreferencesDto
    {
        public List<RoutingMode> ExcludedModes { get; set; }
        public List<RoutingMode> NeutralModes { get; set; }
        public List<RoutingMode> PreferredModes { get; set; }
    }
}
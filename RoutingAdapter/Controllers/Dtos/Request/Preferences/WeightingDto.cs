﻿namespace RoutingAdapter.Controllers.Dtos.Request.Preferences
{
    public class WeightingDto
    {
        public double Comfort { get; set; }
        public double Duration { get; set; }
        public double Environment { get; set; }
        public double Price { get; set; }
    }
}
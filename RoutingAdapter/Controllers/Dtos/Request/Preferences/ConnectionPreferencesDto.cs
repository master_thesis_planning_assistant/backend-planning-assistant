﻿namespace RoutingAdapter.Controllers.Dtos.Request.Preferences
{
    public class ConnectionPreferencesDto
    {
        public int MaxConnectingTime { get; set; }
        public int MaxNumOfChanges { get; set; }
        public int MinConnectingTime { get; set; }
        public bool MinimizeChanges { get; set; }
    }
}
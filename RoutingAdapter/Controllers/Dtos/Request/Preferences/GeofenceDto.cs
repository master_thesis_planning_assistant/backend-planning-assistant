﻿namespace RoutingAdapter.Controllers.Dtos.Request.Preferences
{
    public class GeofenceDto
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public double Radius { get; set; }
    }
}
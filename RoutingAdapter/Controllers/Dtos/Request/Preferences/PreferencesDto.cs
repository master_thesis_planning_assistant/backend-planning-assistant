﻿using System.Collections.Generic;
using RoutingAdapter.Model.Preferences;

namespace RoutingAdapter.Controllers.Dtos.Request.Preferences
{
    public class PreferencesDto
    {
        public string ProfileName { get; set; }
        public ConnectionPreferencesDto ConnectionPreferences { get; set; }
        public int CyclingPace { get; set; }
        public ICollection<string> DemandedComfortFactors { get; set; }
        public GeofenceDto Geofence { get; set; }
        public int LevelOfIntermodality { get; set; }
        public int LuggageSize { get; set; }
        public double MaxCyclingDistance { get; set; }
        public double MaxWalkingDistance { get; set; }
        public ModePreferences ModePreferences { get; set; }
        public bool NoCyclingInBadWeather { get; set; }
        public ICollection<string> Timeframe { get; set; }
        public int WalkingPace { get; set; }
        public WeightingDto Weighting { get; set; }
    }
}
﻿using System;
using RoutingAdapter.Controllers.Dtos.Response.Route;

namespace RoutingAdapter.Controllers.Dtos.Request
{
    public class MobilityDemandDto
    {
        public string ActivityGuid { get; set; }
        public AddressDto From { get; set; }
        public AddressDto To { get; set; }
        public DateTime At { get; set; }
    }
}
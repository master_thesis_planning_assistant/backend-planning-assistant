﻿using System.Collections.Generic;
using RoutingAdapter.Controllers.Dtos.Request.Preferences;

namespace RoutingAdapter.Controllers.Dtos.Request
{
    public class RoutingRequestDto
    {
        public bool FilterRouteDuplicates { get; set; } = true;
        public ICollection<PreferencesDto> Preferences { get; set; }
        public List<MobilityDemandDto> MobilityDemands { get; set; }
    }
}
﻿using System.Collections.Generic;
using RoutingAdapter.Model.Preferences;

namespace RoutingAdapter.Controllers.Dtos
{
    public class AvailableServiceDto
    {
        public string ServiceName { get; set; }
        public ICollection<RoutingMode> SupportedModalities { get; set; }
    }
}
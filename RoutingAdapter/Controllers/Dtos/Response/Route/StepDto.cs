﻿namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class StepDto
    {
        public NamedGeoLocationDto StartLocation { get; set; }
        public NamedGeoLocationDto EndLocation { get; set; }
        public PolylineDto Polyline { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
        public string Action { get; set; }
    }
}
﻿namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class AddressDto
    {
        public string StreetAndNumber { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public GeoLocationDto GeoLocation { get; set; }
    }
}
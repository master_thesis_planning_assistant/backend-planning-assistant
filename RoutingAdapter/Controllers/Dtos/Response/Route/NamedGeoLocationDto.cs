﻿namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class NamedGeoLocationDto
    {
        public string Name { get; set; }
        public GeoLocationDto GeoLocation { get; set; }
    }
}
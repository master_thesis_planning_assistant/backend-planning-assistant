﻿using System.Collections.Generic;

namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class PolylineDto
    {
        public ICollection<GeoLocationDto> Points { get; set; }
    }
}
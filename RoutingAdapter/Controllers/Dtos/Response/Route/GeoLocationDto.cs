﻿namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class GeoLocationDto
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}
﻿namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class BoundsDto
    {
        public GeoLocationDto Southwest { get; set; }
        public GeoLocationDto Northeast { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;

namespace RoutingAdapter.Controllers.Dtos.Response.Route
{
    public class RouteDto
    {
        public string Guid { get; set; }
        public string ServiceName { get; set; }
        public ICollection<LegDto> Legs { get; set; }
        public PolylineDto Polyline { get; set; }
        public AddressDto Start { get; set; }
        public DateTime Departure { get; set; }
        public AddressDto Destination { get; set; }
        public DateTime Arrival { get; set; }
        public BoundsDto Bounds { get; set; }
        public double Duration { get; set; }
        public double Distance { get; set; }
    }
}
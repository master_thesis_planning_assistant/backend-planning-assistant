﻿using System.Collections.Generic;

namespace RoutingAdapter.Controllers.Dtos.Response
{
    public class RoutingResponseDto
    {
        public ICollection<RoutesForActivityDto> RoutesForActivities { get; set; }
    }
}
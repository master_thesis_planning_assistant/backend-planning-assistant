﻿using System.Collections.Generic;
using RoutingAdapter.Controllers.Dtos.Response.Route;

namespace RoutingAdapter.Controllers.Dtos.Response
{
    public class RoutesForActivityDto
    {
        public string ActivityGuid { get; set; }
        public ICollection<RouteDto> Routes { get; set; }
    }
}
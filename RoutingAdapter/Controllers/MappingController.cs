﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RoutingAdapter.RoutingServices;

namespace RoutingAdapter.Controllers
{
    [Produces("application/json")]
    [ApiController]
    [Route("[controller]")]
    public class MappingController : ControllerBase
    {
        private readonly ICollection<IRoutingService> _routingServices;

        public MappingController(IEnumerable<IRoutingService> routingServices)
        {
            _routingServices = routingServices.ToList();
        }

        /// <summary>
        /// Get the xslt config of a routing service used for mapping preferences and mobility demands to routing parameters.
        /// </summary>
        /// <param name="serviceName">Name of the routing service to get the config of</param>
        /// <param name="defaultMapping">Get default or specific config. The default config does only map the mobility demands and does not respect preferences</param>
        /// <returns>The content of the requested xslt config</returns>
        [HttpGet("{serviceName}/{defaultMapping:bool}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<string> GetMappingByServiceName(string serviceName, bool defaultMapping = false)
        {
            var service = _routingServices.FirstOrDefault(rs =>
                string.Equals(rs.ServiceName, serviceName, StringComparison.InvariantCultureIgnoreCase));
            if (service == null)
            {
                return BadRequest($"No service with name {serviceName} found");
            }

            var mapping = defaultMapping ? service.GetDefaultMapping() : service.GetMapping();

            return Ok(mapping);
        }

        /// <summary>
        /// Update the content of the specific xslt config of a routing service.
        /// </summary>
        /// <param name="serviceName">Name of the routing service to update the config of</param>
        /// <param name="mappingXslt">New content of the xslt config</param>
        [HttpPut("{serviceName}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult UpdateMappingByServiceName(string serviceName, [FromBody] string mappingXslt)
        {
            var service = _routingServices.FirstOrDefault(rs =>
                string.Equals(rs.ServiceName, serviceName, StringComparison.InvariantCultureIgnoreCase));
            if (service == null)
            {
                return BadRequest($"No service with name {serviceName} found");
            }

            service.UpdateMappingAsync(mappingXslt);

            return NoContent();
        }
    }
}
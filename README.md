# Backend Planning Assistant

## Required Configurations!
In order to run this planning assistant some furhter configurations are needed.

Please note, that the Routing-Services OpenTripPlanner, OpenRouteService and Valhalla for this project where hostet by the FZI (Forschungszentrum Informatik).
The services behind the provided connection strings are only reachable from inside the FZIs VPN.

- ````RoutingAdapter/appsettings.json````
    - Fill in ```TriasRequestorRef``` in order to use the TRIAS API of the KVV
- ```OrchestrationService/appsettings.json```
    - Fill in ```OpenCageApiKey``` in order to use the OpenCage-Geocoding service. (You can get a free API-Key [here](https://opencagedata.com/users/sign_up)).
- ```OrchestrationService/appsettings.Development.json``` and ```OrchestrationService/appsettings.Production.json```
    - Fill in ```ClientId``` and  ```ClientSecret``` in order to use the RegioMove Preference-Service

## Deployment
There are dockerfiles for bot Microservices.
For the RoutingAdapter it is especially important that all .xslt-Files are stored in the correct folder so they get copied into the image.

Using the environment configurations (```appsettings.Development.json``` and ```appsettings.Production.json```) the connection strings for the different services can be configured.
The selection of the environment configuration is done by setting the ```ASPNETCORE_ENVIRONMENT``` for the Service as Environment variable (also works in docker)
- ```ASPNETCORE_ENVIRONMENT=Development```
- ```ASPNETCORE_ENVIRONMENT=Production```